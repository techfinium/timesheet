
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import haj.com.entity.Blank;
 
public class App 
{
    public static void main( String[] args )
    {
        SessionFactory sessionFactory;
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
 
        Session session = sessionFactory.openSession();
 
        Transaction tx = session.beginTransaction();
		Blank blank = new Blank();
		Date now = new Date();
		//blank.setNote("Hello: " + now.getTime());
		
        session.save(blank);
        tx.commit();
        session.close();
        System.exit(0);
    }
    
}