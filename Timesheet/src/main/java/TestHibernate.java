import java.util.Calendar;
import java.util.Date;

import haj.com.entity.Timesheet;
import haj.com.entity.TimesheetDetails;
import haj.com.service.TimesheetDetailsService;
import haj.com.utils.GenericUtility;

public class TestHibernate {

	public static Date endOfDay(Date date) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.set(Calendar.HOUR_OF_DAY, 23);
	    cal.set(Calendar.MINUTE, 59);
	    cal.set(Calendar.SECOND, 59);
	    cal.set(Calendar.MILLISECOND, 999);

	    return cal.getTime();
	}
	
	public static void main(String[] args) {
		try {
			TimesheetDetailsService service = new TimesheetDetailsService();
			TimesheetDetails ts = new TimesheetDetails();
			Timesheet timesheet = new Timesheet();
			timesheet.setId(167l);
			
			
			
			ts.setTimesheet(timesheet);
			
			Date today = new Date();
			Date fromDateTime = GenericUtility.getStartOfDay(today);
			//Date toDateTime = GenericUtility.getEndOfDay(today);
			Date toDateTime = endOfDay(today);
			
			
			ts.setCreateDate(today);
			ts.setFromDateTime(fromDateTime);
			ts.setToDateTime(toDateTime);
			
			System.out.println("BEFORE CREATE " +ts.getId() + " " + ts.getFromDateTime() + " " + ts.getToDateTime());
			service.create(ts);
			System.out.println("AFTER CREATE " +ts.getId() + " " + ts.getFromDateTime() + " " + ts.getToDateTime());
			
			//TimesheetDetails ts2 = service.findByKey(ts.getId());
			
			//System.out.println("AFTER FIND " +ts2.getId() + " " + ts2.getFromDateTime() + " " + ts2.getToDateTime());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
       System.exit(0);
	}
}
