
package haj.com.json;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import haj.com.framework.IDataEntity;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "AUD",
    "BGN",
    "BRL",
    "CAD",
    "CHF",
    "CNY",
    "CZK",
    "DKK",
    "HKD",
    "HRK",
    "HUF",
    "IDR",
    "ILS",
    "INR",
    "JPY",
    "KRW",
    "MXN",
    "MYR",
    "NOK",
    "NZD",
    "PHP",
    "PLN",
    "RON",
    "RUB",
    "SEK",
    "SGD",
    "THB",
    "TRY",
    "USD",
    "ZAR",
    "EUR",
    "GBP"
})
@Entity
@Table(name = "currenry_rates")
public class Rates implements IDataEntity {

	@JsonIgnore
	@GenericGenerator(name = "generator", strategy = "foreign",
	parameters = @Parameter(name = "property", value = "currencyBean"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private CurrencyBean currencyBean;
	
    @JsonProperty("AUD")
    private Double AUD;
    @JsonProperty("BGN")
    private Double BGN;
    @JsonProperty("BRL")
    private Double BRL;
    @JsonProperty("CAD")
    private Double CAD;
    @JsonProperty("CHF")
    private Double CHF;
    @JsonProperty("CNY")
    private Double CNY;
    @JsonProperty("CZK")
    private Double CZK;
    @JsonProperty("DKK")
    private Double DKK;
    @JsonProperty("HKD")
    private Double HKD;
    @JsonProperty("HRK")
    private Double HRK;
    @JsonProperty("HUF")
    private Double HUF;
    @JsonProperty("IDR")
    private Double IDR;
    @JsonProperty("ILS")
    private Double ILS;
    @JsonProperty("INR")
    private Double INR;
    @JsonProperty("JPY")
    private Double JPY;
    @JsonProperty("KRW")
    private Double KRW;
    @JsonProperty("MXN")
    private Double MXN;
    @JsonProperty("MYR")
    private Double MYR;
    @JsonProperty("NOK")
    private Double NOK;
    @JsonProperty("NZD")
    private Double NZD;
    @JsonProperty("PHP")
    private Double PHP;
    @JsonProperty("PLN")
    private Double PLN;
    @JsonProperty("RON")
    private Double RON;
    @JsonProperty("RUB")
    private Double RUB;
    @JsonProperty("SEK")
    private Double SEK;
    @JsonProperty("SGD")
    private Double SGD;
    @JsonProperty("THB")
    private Double THB;
    @JsonProperty("TRY")
    private Double TRY;
    @JsonProperty("USD")
    private Double USD;
    @JsonProperty("ZAR")
    private Double ZAR;
    @JsonProperty("EUR")
    private Double EUR;
    @JsonProperty("GBP")
    private Double GBP;   
    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("AUD")
    public Double getAUD() {
        return AUD;
    }

    @JsonProperty("AUD")
    public void setAUD(Double aUD) {
        this.AUD = aUD;
    }

    @JsonProperty("BGN")
    public Double getBGN() {
        return BGN;
    }

    @JsonProperty("BGN")
    public void setBGN(Double bGN) {
        this.BGN = bGN;
    }

    @JsonProperty("BRL")
    public Double getBRL() {
        return BRL;
    }

    @JsonProperty("BRL")
    public void setBRL(Double bRL) {
        this.BRL = bRL;
    }

    @JsonProperty("CAD")
    public Double getCAD() {
        return CAD;
    }

    @JsonProperty("CAD")
    public void setCAD(Double cAD) {
        this.CAD = cAD;
    }

    @JsonProperty("CHF")
    public Double getCHF() {
        return CHF;
    }

    @JsonProperty("CHF")
    public void setCHF(Double cHF) {
        this.CHF = cHF;
    }

    @JsonProperty("CNY")
    public Double getCNY() {
        return CNY;
    }

    @JsonProperty("CNY")
    public void setCNY(Double cNY) {
        this.CNY = cNY;
    }

    @JsonProperty("CZK")
    public Double getCZK() {
        return CZK;
    }

    @JsonProperty("CZK")
    public void setCZK(Double cZK) {
        this.CZK = cZK;
    }

    @JsonProperty("DKK")
    public Double getDKK() {
        return DKK;
    }

    @JsonProperty("DKK")
    public void setDKK(Double dKK) {
        this.DKK = dKK;
    }

    @JsonProperty("HKD")
    public Double getHKD() {
        return HKD;
    }

    @JsonProperty("HKD")
    public void setHKD(Double hKD) {
        this.HKD = hKD;
    }

    @JsonProperty("HRK")
    public Double getHRK() {
        return HRK;
    }

    @JsonProperty("HRK")
    public void setHRK(Double hRK) {
        this.HRK = hRK;
    }

    @JsonProperty("HUF")
    public Double getHUF() {
        return HUF;
    }

    @JsonProperty("HUF")
    public void setHUF(Double hUF) {
        this.HUF = hUF;
    }

    @JsonProperty("IDR")
    public Double getIDR() {
        return IDR;
    }

    @JsonProperty("IDR")
    public void setIDR(Double iDR) {
        this.IDR = iDR;
    }

    @JsonProperty("ILS")
    public Double getILS() {
        return ILS;
    }

    @JsonProperty("ILS")
    public void setILS(Double iLS) {
        this.ILS = iLS;
    }

    @JsonProperty("INR")
    public Double getINR() {
        return INR;
    }

    @JsonProperty("INR")
    public void setINR(Double iNR) {
        this.INR = iNR;
    }

    @JsonProperty("JPY")
    public Double getJPY() {
        return JPY;
    }

    @JsonProperty("JPY")
    public void setJPY(Double jPY) {
        this.JPY = jPY;
    }

    @JsonProperty("KRW")
    public Double getKRW() {
        return KRW;
    }

    @JsonProperty("KRW")
    public void setKRW(Double kRW) {
        this.KRW = kRW;
    }

    @JsonProperty("MXN")
    public Double getMXN() {
        return MXN;
    }

    @JsonProperty("MXN")
    public void setMXN(Double mXN) {
        this.MXN = mXN;
    }

    @JsonProperty("MYR")
    public Double getMYR() {
        return MYR;
    }

    @JsonProperty("MYR")
    public void setMYR(Double mYR) {
        this.MYR = mYR;
    }

    @JsonProperty("NOK")
    public Double getNOK() {
        return NOK;
    }

    @JsonProperty("NOK")
    public void setNOK(Double nOK) {
        this.NOK = nOK;
    }

    @JsonProperty("NZD")
    public Double getNZD() {
        return NZD;
    }

    @JsonProperty("NZD")
    public void setNZD(Double nZD) {
        this.NZD = nZD;
    }

    @JsonProperty("PHP")
    public Double getPHP() {
        return PHP;
    }

    @JsonProperty("PHP")
    public void setPHP(Double pHP) {
        this.PHP = pHP;
    }

    @JsonProperty("PLN")
    public Double getPLN() {
        return PLN;
    }

    @JsonProperty("PLN")
    public void setPLN(Double pLN) {
        this.PLN = pLN;
    }

    @JsonProperty("RON")
    public Double getRON() {
        return RON;
    }

    @JsonProperty("RON")
    public void setRON(Double rON) {
        this.RON = rON;
    }

    @JsonProperty("RUB")
    public Double getRUB() {
        return RUB;
    }

    @JsonProperty("RUB")
    public void setRUB(Double rUB) {
        this.RUB = rUB;
    }

    @JsonProperty("SEK")
    public Double getSEK() {
        return SEK;
    }

    @JsonProperty("SEK")
    public void setSEK(Double sEK) {
        this.SEK = sEK;
    }

    @JsonProperty("SGD")
    public Double getSGD() {
        return SGD;
    }

    @JsonProperty("SGD")
    public void setSGD(Double sGD) {
        this.SGD = sGD;
    }

    @JsonProperty("THB")
    public Double getTHB() {
        return THB;
    }

    @JsonProperty("THB")
    public void setTHB(Double tHB) {
        this.THB = tHB;
    }

    @JsonProperty("TRY")
    public Double getTRY() {
        return TRY;
    }

    @JsonProperty("TRY")
    public void setTRY(Double tRY) {
        this.TRY = tRY;
    }

    @JsonProperty("USD")
    public Double getUSD() {
        return USD;
    }

    @JsonProperty("USD")
    public void setUSD(Double uSD) {
        this.USD = uSD;
    }

    @JsonProperty("ZAR")
    public Double getZAR() {
        return ZAR;
    }

    @JsonProperty("ZAR")
    public void setZAR(Double zAR) {
        this.ZAR = zAR;
    }

    @JsonProperty("EUR")
    public Double getEUR() {
        return EUR;
    }

    @JsonProperty("EUR")
    public void setEUR(Double eUR) {
        this.EUR = eUR;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CurrencyBean getCurrencyBean() {
		return currencyBean;
	}

	public void setCurrencyBean(CurrencyBean currencyBean) {
		this.currencyBean = currencyBean;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

    @JsonProperty("GBP")
	public Double getGBP() {
		return GBP;
	}

    @JsonProperty("GBP")
	public void setGBP(Double gBP) {
		GBP = gBP;
	}

}
