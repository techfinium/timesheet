package haj.com.framework;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.el.ELContext;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.PrimeFaces;

import haj.com.ui.SessionUI;



public abstract class AbstractUI implements Serializable
{
  private static final long serialVersionUID = 1L;

  @ManagedProperty(value = "#{sessionUI}")
  private SessionUI sessionUI;
  
  protected static final String LANDING_PAGE = "/pages/selectPortal.jsf";

  protected final Log logger = LogFactory.getLog(this.getClass());

  /**
   * Return the FacexContext.
   * 
   * @return
   */
  protected FacesContext getContext()
  {
    FacesContext context = FacesContext.getCurrentInstance();
    if (context == null)
      context = OfflineContext.getCurrentInstance();
    return context;
  }
  
  /**
   * Validate that a password is long enough and contains both letters and digits.
   * 
   * @param pwd
   * @param displayErrors If true, FacesMessage objects will be set with any validation failures.
   * @return
   */
  protected boolean validatePassword(String pwd, boolean displayErrors)
  {
    if (pwd==null || pwd.length()<6)
    {
      if (displayErrors)
        addErrorMessage("Your new password must be at least 6 characters long.");
      return false;
    }
    
    if (!pwd.matches(".*[0-9].*") || !pwd.matches(".*[a-zA-Z].*"))
    {
      if (displayErrors)
        addErrorMessage("Your new password must contain both letters and numbers.");
      return false;
    }
    
    return true;
  }

  /**
   * Set an error FacesMessage for specified message.
   * 
   * @param message
   */
  protected void addErrorMessage(String message)
  {
    addFacesMessage(null, FacesMessage.SEVERITY_ERROR, message);
  }

  /**
   * Set an error Faces message for specified message as well as logging to error log.
   * 
   * @param message
   * @param thrown
   */
  protected void addErrorMessage(String message, Throwable thrown)
  {
    addFacesMessage(null, FacesMessage.SEVERITY_ERROR, message);
    logger.error(message, thrown);
  }

  /**
   * Set a warning FacesMessage for specified message.
   * 
   * @param message
   */
  protected void addWarningMessage(String message)
  {
    addFacesMessage(null, FacesMessage.SEVERITY_WARN, message);
  }

  /**
   * Set an info FacesMessage for specified message.
   * 
   * @param message
   */
  protected void addInfoMessage(String message)
  {
    addFacesMessage(null, FacesMessage.SEVERITY_INFO, message);
  }

  /**
   * Set specified key/value.
   * 
   * @param key Key of parameter to set.
   * @param value Value of parameter to set.
   * @param inSession Set parameter on HttpSession if true, else on ServletRequest
   */
  protected void setParameter(String key, Object value, boolean inSession)
  {
    FacesContext facesContext = getContext();
    if (facesContext != null && facesContext.getExternalContext() != null)
    {
      HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
      if (inSession)
        request.getSession().setAttribute(key, value);
      else
        request.setAttribute(key, value);
    }
  }

  /**
   * Retrieve the current value of parameter.
   * 
   * @param key Key of parameter to retrieve.
   * @param inSession Return parameter from HttpSession if true, else from ServletRequest
   * @return
   */
  protected Object getParameter(String key, boolean inSession)
  {
    FacesContext facesContext = getContext();
    if (facesContext != null && facesContext.getExternalContext() != null)
    {
      HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
      if (inSession)
        return request.getSession().getAttribute(key);
      else
        return request.getParameter(key);
    }
    return null;
  }

  private void addFacesMessage(String clientId, FacesMessage.Severity severity, String message)
  {
    getContext().addMessage(clientId, new FacesMessage(severity, message, null));
  }

  private HttpServletRequest getServletRequest()
  {
    return (HttpServletRequest) getContext().getExternalContext().getRequest();
  }

  /**
   * Redirect to specified page.
   * 
   * @param outcome
   */
  protected void ajaxRedirect(String outcome)
  {
    FacesContext ctx = getContext();
    ExternalContext extContext = ctx.getExternalContext();
    String url = extContext.encodeActionURL(ctx.getApplication().getViewHandler().getActionURL(ctx, outcome));
    try
    {
      extContext.redirect(url);
    }
    catch (IOException ioe)
    {
      throw new FacesException(ioe);
    }
  }

  /**
   * Return the originating IP address of specified request.
   * 
   * @param request
   * @return
   */
  protected String getRequestingIP(HttpServletRequest request)
  {
    return request.getRemoteAddr();
  }

  /**
   * Write current event to the event log.
   */
  protected void log()
  {
    log((Object)null);
  }
  
  /**
   * Write the current event to the event log.
   * @param additionalInfo String,Object pairs of any additional/override data to store on the event.
   */
  protected void log(Object... additionalInfo)
  {
    Map<String, Object> map = new HashMap<String, Object>();
    try
    {
      map.put("ip", getRequestingIP(getServletRequest()));

      if (getSessionUI()!=null)
        map.putAll(getSessionUI().getMap());
      
      StackTraceElement[] elements = new Throwable().getStackTrace();
      int counter = 0;
      String callerMethodName = null;
      String callerClassName = null;
      do
      {
        callerMethodName = elements[counter].getMethodName();
        callerClassName = elements[counter].getClassName();
        counter++;
      } while (callerClassName.equalsIgnoreCase(AbstractUI.class.getName()));
      
      map.put("callingClass", callerClassName);
      map.put("callingMethod", callerMethodName);
      
      if (additionalInfo!=null && additionalInfo.length>0)
        for (int i = 1 ; i < additionalInfo.length ; i += 2)
          if (additionalInfo[i-1]!=null)
            map.put(additionalInfo[i-1].toString(), additionalInfo[i]);
      
    //  AuditService auditService = new AuditService();
    //  auditService.auditEvent(map);
    }
    catch (Exception e)
    {
      logger.error("Failed to log event", e);
    }
  }

  /**
   * Return the SessionUI for this Session.
   * 
   * @return
   */
  public SessionUI getSessionUI()
  {
    return sessionUI;
  }

  /**
   * For JSF impl to use to set the Managed Property.
   * DO NOT set this manually.
   * @param sessionUI
   */
  public void setSessionUI(SessionUI sessionUI)
  {
    this.sessionUI = sessionUI;
  }

  /**
   * Add a callback parameter to current Request.
   * 
   * @param parm
   * @param value
   */
  public static void addCallBackParm(String parm, Object value)
  {
 
    PrimeFaces.current().ajax().addCallbackParam(parm, value);
  }

  public static String removeChar(String s, char c)
  {
    StringBuffer r = new StringBuffer(s.length());
    r.setLength(s.length());
    int current = 0;
    for (int i = 0; i < s.length(); i++)
    {
      char cur = s.charAt(i);
      if (cur != c)
        r.setCharAt(current++, cur);
    }
    return r.toString();
  }

  public String getTimeStamp()
  {
    Date today = new Date();
    System.out.println("time stamp is " + new Timestamp(today.getTime()));
    String timeStamp = "" + new Timestamp(today.getTime());

    timeStamp = AbstractUI.removeChar(timeStamp, ':');
    timeStamp = AbstractUI.removeChar(timeStamp, '-');
    timeStamp = AbstractUI.removeChar(timeStamp, '.');
    timeStamp = AbstractUI.removeChar(timeStamp, ' ');

    String last3 = "" + (double) Math.random();

    return timeStamp.substring(0, 17) + last3.substring(3, 6);
  }
  
  /*
   * returns the time zone of the server
   */
  public String getTimeZoneString() {

    return java.util.TimeZone.getDefault().getID();   
  }

  public void runClientSideExecute(String widgetVar) {
	//  RequestContext requestContext = RequestContext.getCurrentInstance();
	 // requestContext.execute(widgetVar);
		PrimeFaces.current().executeScript(widgetVar);
  }

  public void runClientSideUpdate(String id) {
	  PrimeFaces.current().ajax().update(id);
  }
  public void removeParm(String key,boolean session) {
	    FacesContext facesContext = getAppContext();
	  HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
	  if (session)
	   request.getSession().removeAttribute(key);
	  else
	   request.removeAttribute(key);
}

public static FacesContext getAppContext()
{
  FacesContext context = FacesContext.getCurrentInstance();
  if(context == null)
    context = OfflineContext.getCurrentInstance();
  
  return context;
}

public void redirect(String outcome) {
    FacesContext facesContext = getAppContext();
    HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
    HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
    try {
    	response.sendRedirect(request.getContextPath()+outcome);
    } catch (Exception ex) {
    	logger.fatal(ex);
    }
}



public static Object getBean(String beanName){
    Object bean = null;
    FacesContext fc = FacesContext.getCurrentInstance();
    if(fc!=null){
         ELContext elContext = fc.getELContext();
         bean = elContext.getELResolver().getValue(elContext, null, beanName);
    }

    return bean;
}

/*
public boolean isBilling() {
	return BillingConstants.BILLING_ENABLED;
}
*/

public static String getEntryLanguage(String key) {
    String value = "";
    ResourceBundle bundle = ResourceBundle.getBundle("techfinium.lang.language", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    try {
      value = bundle.getString(key);
    } catch (MissingResourceException missingException) {
     // logger.fatal("The Key Message doesn't exist in any resource file key=" + key);
      missingException.printStackTrace();
    }
    return value;
  }

}
