package haj.com.framework;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class AbstractService implements Serializable
{
  private static final long serialVersionUID = 1L;
  
  protected final Log logger = LogFactory.getLog(this.getClass());
 
  public Map<String, Object> auditlog;
  
  /** The  resourceBundle */
  private ResourceBundle resourceBundle;
  
  public AbstractService() {
	super();
	// TODO Auto-generated constructor stub
   }


public AbstractService(Map<String, Object> auditlog) {
	super();
	this.auditlog = auditlog;
}

	public AbstractService(ResourceBundle resourceBundle) {
		super();
		this.resourceBundle = resourceBundle;
	}

	public AbstractService(Map<String, Object> auditlog, ResourceBundle resourceBundle) {
		super();
		this.auditlog = auditlog;
		this.resourceBundle = resourceBundle;
	}


/**
   * Get an instance of current date, no idea why this is synchronized
   * but has been left so from original e-port code.
   * 
   * @return
   */
  protected synchronized Date getSynchronizedDate()
  {
    return new Date();
  }
}
