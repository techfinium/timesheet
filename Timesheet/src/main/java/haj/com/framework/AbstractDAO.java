package haj.com.framework;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.UnsupportedDataTypeException;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.primefaces.model.SortOrder;

public abstract class AbstractDAO implements Serializable {
	private static final long serialVersionUID = 1L;

	protected final Log logger = LogFactory.getLog(this.getClass());

	/**
	 * Return implementation of AbstractDataProvider which will link the DAO
	 * implementations to a data source.
	 * 
	 * @return
	 */
	public abstract AbstractDataProvider getDataProvider();

	/**
	 * Return a Session for data access.
	 * 
	 * @return
	 */
	public Session getSession() {
		return getDataProvider().getSession();
	}

	/**
	 * Insert new db object.
	 * 
	 * @param entity
	 * @throws Exception
	 */
	public void create(IDataEntity entity) throws Exception {
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			create(entity, session);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}
	
	public int countWhere(Map<String, Object> filters, String hql) {
		if (filters != null) {
			Map<String, Object> parms = new HashMap<String, Object>();
			String hvar = null;
			for (Entry<String, Object> entry : filters.entrySet()) {
				hvar = entry.getKey();
				if (hvar.contains(".")) {
					hvar = hvar.replaceAll("\\.", "");
					parms.put(hvar, entry.getValue());
				} else {
					parms.put(entry.getKey(), entry.getValue());
				}
				if (!hql.contains(entry.getKey())) {
					hql += " and o." + entry.getKey() + " like " + " :" + hvar;
				}
			}
			filters = parms;
		}
		return ((Long) getUniqueResult(hql, filters)).intValue();
	}


	/**
	 * Insert a batch of new db objects within same transaction.
	 * 
	 * @param entityList
	 * @throws Exception
	 */
	public void createBatch(List<IDataEntity> entityList) throws Exception {
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			for (IDataEntity entity : entityList)
				create(entity, session);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public void updateBatch(List<IDataEntity> entityList) throws Exception {
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			for (IDataEntity entity : entityList)
				update(entity, session);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	/**
	 * Update existing db object.
	 * 
	 * @param entity
	 * @throws Exception
	 */
	public void update(IDataEntity entity) throws Exception {
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			update(entity, session);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	/**
	 * Delete an existing db object.
	 * 
	 * @param entity
	 * @throws Exception
	 */
	public void delete(IDataEntity entity) throws Exception {
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			delete(entity, session);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public void deleteBatch(List<IDataEntity> entityList) throws Exception {
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			for (IDataEntity entity : entityList)
				delete(entity, session);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public void create(IDataEntity entity, Session session) throws Exception {
		session.save(entity);
	}

	public void update(IDataEntity entity, Session session) throws Exception {
		session.update(entity);
	}

	public void delete(IDataEntity entity, Session session) throws Exception {
		session.delete(entity);
	}

	private void addParameter(Query query, String key, Object value) {
		query.setParameter(key, value);
		/*
		 * if (value == null) query.setParameter(key, value); else if (value
		 * instanceof Timestamp) query.setTimestamp(key, (Timestamp) value);
		 * else if (value instanceof BigDecimal) query.setBigDecimal(key,
		 * (BigDecimal) value); else if (value instanceof BigInteger)
		 * query.setBigInteger(key, (BigInteger) value); else if (value
		 * instanceof byte[]) query.setBinary(key, (byte[]) value); else if
		 * (value instanceof Boolean) query.setBoolean(key, (Boolean) value);
		 * else if (value instanceof Byte) query.setByte(key, (Byte) value);
		 * else if (value instanceof Calendar) query.setCalendarDate(key,
		 * (Calendar) value); else if (value instanceof Character)
		 * query.setCharacter(key, (Character) value); else if (value instanceof
		 * Date) query.setDate(key, (Date) value); else if (value instanceof
		 * Double) query.setDouble(key, (Double) value); else if (value
		 * instanceof Float) query.setFloat(key, (Float) value); else if (value
		 * instanceof Integer) query.setInteger(key, (Integer) value); else if
		 * (value instanceof Locale) query.setLocale(key, (Locale) value); else
		 * if (value instanceof Long) query.setLong(key, (Long) value); else if
		 * (value instanceof Short) query.setShort(key, (Short) value); else if
		 * (value instanceof String) query.setString(key, (String) value);
		 * 
		 * else query.setParameter(key, value);
		 */
	}

	/**
	 * Return a list of db objects based on specified hql query.
	 * 
	 * @param hql
	 * @return
	 */
	protected List<?> getList(String hql) {
		return getList(hql, null);
	}

	protected List<?> getList(String hql, int noRows) {
		return getList(hql, null, noRows);
	}

	/**
	 * Return a list of db objects based on specified hql query with filter
	 * parameters.
	 * 
	 * @param hql
	 * @param parameters
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected List<?> getList(String hql, Map<String, Object> parameters) {
		List<Object> result = null;
		Session session = getSession();
		try {
			Query query = session.createQuery(hql);
			if (parameters != null)
				for (String key : parameters.keySet())
					addParameter(query, key, parameters.get(key));
			result = query.getResultList();
		} catch (HibernateException e) {
			throw e;
		} finally {
			session.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	protected List<?> getList(String hql, Map<String, Object> parameters, int noRows) {
		List<Object> result = null;
		Session session = getSession();
		try {
			Query query = session.createQuery(hql);
			if (parameters != null)
				for (String key : parameters.keySet())
					addParameter(query, key, parameters.get(key));
			query.setMaxResults(noRows);
			result = query.getResultList();
		} catch (HibernateException e) {
			throw e;
		} finally {
			session.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	protected List<?> getList(String hql, Map<String, Object> parameters, int startingAt, int maxPerPage) {
		List<Object> result = null;
		Session session = getSession();
		try {
			Query query = session.createQuery(hql);
			if (parameters != null)
				for (String key : parameters.keySet())
					addParameter(query, key, parameters.get(key));
			query.setFirstResult(startingAt);
			query.setMaxResults(maxPerPage);
			result = query.getResultList();
		} catch (HibernateException e) {
			throw e;
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * Return a single db object based on specified hql query.
	 * 
	 * @param hql
	 * @return
	 */
	protected Object getUniqueResult(String hql) {
		return getUniqueResult(hql, null);
	}

	/**
	 * Return a single db object based on specified hql query with filter
	 * parameters.
	 * 
	 * @param hql
	 * @param parameters
	 * @return
	 */
	protected Object getUniqueResult(String hql, Map<String, Object> parameters) {
		Object result = null;
		Session session = getSession();
		try {
			Query query = session.createQuery(hql);
			if (parameters != null)
				for (String key : parameters.keySet())
					addParameter(query, key, parameters.get(key));
			result = query.getSingleResult();// uniqueResult();
		} catch (javax.persistence.NoResultException ne) {
			logger.info("No result exception!");
		} catch (HibernateException e) {
			throw e;
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * Return the next value for specified sequence.
	 * 
	 * @param sequence
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	protected long getNextFromSequence(String sequence) throws Exception {
		Session session = getSession();
		try {
			String sql = "select (nextval for " + sequence + ") from users fetch first 1 rows only";
			List<Object> l = session.createSQLQuery(sql).list();
			for (Object o : l) {
				if (o instanceof BigInteger)
					return ((BigInteger) o).longValue();
				if (o instanceof BigDecimal)
					return ((BigDecimal) o).longValue();
				else if (o instanceof Long)
					return ((Long) o).longValue();
				else if (o instanceof Integer)
					return ((Integer) o).longValue();
				else if (o instanceof Short)
					return ((Short) o).longValue();
				else
					throw new UnsupportedDataTypeException("No sequence cast defined for " + o.getClass().toString());
			}
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
		return 0l;
	}

	/**
	 * @Deprecated use AbstractDAO.getNextFromSequence Return next value for
	 *             table key based on max currently.
	 * 
	 * @param hql
	 * @return
	 */

	@SuppressWarnings("rawtypes")
	protected long getLastSeqNr(String hql) {
		long nr = 0;
		try {
			List array = (List) getList(hql);
			for (Object o : array) {
				if (o instanceof Integer)
					try {
						nr = ((Integer) o).longValue();
					} catch (java.lang.NullPointerException np) {
					}
				else if (o instanceof Short)
					try {
						nr = ((Short) o).longValue();
					} catch (java.lang.NullPointerException np) {
					}
				else if (o instanceof Long)
					try {
						nr = ((Long) o).longValue();
					} catch (java.lang.NullPointerException np) {
					}
			}
			nr++;
		} catch (Exception e) {
			logger.fatal(e);
		}
		return nr;
	}

	/**
	 * Return only first db object from specified hql query.
	 * 
	 * @param hql
	 * @return
	 */
	protected Object oneRow(String hql) {
		Object result = null;
		Session session = getSession();
		try {
			Query query = session.createQuery(hql);
			query.setMaxResults(1);
			result = query.getSingleResult();
		} catch (javax.persistence.NoResultException ne) {
			logger.info("No result exception!");
		} catch (HibernateException e) {
			throw e;
		} finally {
			session.close();
		}
		return result;
	}
	public List<?> sortAndFilter(int startingAt, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String hql) {
		if (filters != null) {
			boolean ft = true;
			Map<String, Object> parms = new HashMap<String, Object>();
			String hvar = null;
			for (Entry<String, Object> entry : filters.entrySet()) {
				hvar = entry.getKey();
				if (hvar.contains(".")) {
					hvar = hvar.replaceAll("\\.", "");
					parms.put(hvar, entry.getValue());
				} else {
					parms.put(entry.getKey(), entry.getValue());
				}
				if (ft) {
					hql += " where o." + entry.getKey() + " like " + " :" + hvar;
					ft = false;
				} else {
					hql += " and o." + entry.getKey() + " like " + " :" + hvar;
				}
			}
			filters = parms;
		}

		if (sortField != null) {

			switch (sortOrder) {
				case ASCENDING:
					hql += " order by o." + sortField + " asc ";
					break;
				case DESCENDING:
					hql += " order by o." + sortField + " desc ";
					break;
				default:
					break;
			}
		}
		return getList(hql, filters, startingAt, pageSize);
	}

	/**
	 * Sort and filter.
	 * 
	 * @param startingAt
	 *            the starting at
	 * @param pageSize
	 *            the page size
	 * @param sortField
	 *            the sort field
	 * @param sortOrder
	 *            the sort order
	 * @param filters
	 *            the filters
	 * @param hql
	 *            the hql
	 *
	 * @return the list
	 */
	public List<?> sortAndFilterWhere(int startingAt, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, String hql) {
		if (filters != null) {
			// boolean ft = true;
			Map<String, Object> parms = new HashMap<String, Object>();
			String hvar = null;
			for (Entry<String, Object> entry : filters.entrySet()) {
				hvar = entry.getKey();
				if (hvar.contains(".")) {
					hvar = hvar.replaceAll("\\.", "");
					parms.put(hvar, entry.getValue());
				} else {
					parms.put(entry.getKey(), entry.getValue());
				}
				if (!hql.contains(entry.getKey())) {
					hql += " and o." + entry.getKey() + " like " + " :" + hvar;
				}
			}
			filters = parms;
		}

		if (sortField != null) {

			switch (sortOrder) {
				case ASCENDING:
					hql += " order by o." + sortField + " asc ";
					break;
				case DESCENDING:
					hql += " order by o." + sortField + " desc ";
					break;
				default:
					break;
			}
		}
		return getList(hql, filters, startingAt, pageSize);
	}

	public void nonSelectSql(String sql) throws Exception {
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.createSQLQuery(sql).executeUpdate();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public Long count(Class<?> entity) throws Exception {
		return (Long) getUniqueResult("select count(o) from " + entity.getName() + " o ");
	}

	public int count(Class<?> entity, Map<String, Object> filters) {
		String hql = "select count(o) from " + entity.getName() + " o ";
		if (filters != null) {
			boolean ft = true;
			Map<String, Object> parms = new HashMap<String, Object>();
			String hvar = null;
			for (Entry<String, Object> entry : filters.entrySet()) {
				hvar = entry.getKey();
				if (hvar.contains(".")) {
					hvar = hvar.replaceAll("\\.", "");
					parms.put(hvar, entry.getValue());
				} else {
					parms.put(entry.getKey(), entry.getValue());
				}
				if (ft) {
					hql += " where o." + entry.getKey() + " like " + " :" + hvar;
					ft = false;
				} else {
					hql += " and o." + entry.getKey() + " like " + " :" + hvar;
				}
			}
			filters = parms;
		}
		return ((Long) getUniqueResult(hql, filters)).intValue();
	}

	public List<?> sortAndFilter(Class<?> entity, int startingAt, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters, String hql) {
		if (filters != null) {
			boolean ft = true;
			Map<String, Object> parms = new HashMap<String, Object>();
			String hvar = null;
			for (Entry<String, Object> entry : filters.entrySet()) {
				hvar = entry.getKey();
				if (hvar.contains(".")) {
					hvar = hvar.replaceAll("\\.", "");
					parms.put(hvar, entry.getValue());
				} else {
					parms.put(entry.getKey(), entry.getValue());
				}
				if (ft) {
					hql += " where o." + entry.getKey() + " like " + " :" + hvar;
					ft = false;
				} else {
					hql += " and o." + entry.getKey() + " like " + " :" + hvar;
				}
			}
			filters = parms;
		}

		if (sortField != null) {

			switch (sortOrder) {
			case ASCENDING:
				hql += " order by o." + sortField + " asc ";
				break;
			case DESCENDING:
				hql += " order by o." + sortField + " desc ";
				break;
			default:
				break;
			}
		}
		return getList(hql, filters, startingAt, pageSize);
	}



	public List<?> sortAndFilter(Class<?> entity, int startingAt, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		String hql = "select o from " + entity.getName() + " o ";
		if (filters != null) {
			boolean ft = true;
			Map<String, Object> parms = new HashMap<String, Object>();
			String hvar = null;
			for (Entry<String, Object> entry : filters.entrySet()) {
				hvar = entry.getKey();
				if (hvar.contains(".")) {
					hvar = hvar.replaceAll("\\.", "");
					parms.put(hvar, entry.getValue());
				} else {
					parms.put(entry.getKey(), entry.getValue());
				}
				if (ft) {
					hql += " where o." + entry.getKey() + " like " + " :" + hvar;
					ft = false;
				} else {
					hql += " and o." + entry.getKey() + " like " + " :" + hvar;
				}
			}
			filters = parms;
		}

		if (sortField != null) {

			switch (sortOrder) {
			case ASCENDING:
				hql += " order by o." + sortField + " asc ";
				break;
			case DESCENDING:
				hql += " order by o." + sortField + " desc ";
				break;
			default:
				break;
			}
		}
		return getList(hql, filters, startingAt, pageSize);
	}
	
	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	public <T> List<T> nativeSelectSqlList(String sql, Class<T> entityType) throws Exception {
		List<T> list = null;
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			NativeQuery nq = session.createNativeQuery(sql);
			nq.setResultTransformer(Transformers.aliasToBean(entityType));
			list = nq.getResultList();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
		return list;
	}

	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	public <T> List<T> nativeSelectSqlList(String sql, Class<T> entityType, Map<String, Object> parameters)
			throws Exception {
		List<T> list = null;
		Session session = getSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			NativeQuery nq = session.createNativeQuery(sql);
			nq.setResultTransformer(Transformers.aliasToBean(entityType));
			if (parameters != null)
				for (String key : parameters.keySet())
					addParameter(nq, key, parameters.get(key));
			list = nq.getResultList();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
		return list;
	}
	
	public IDataEntity getByClassAndKey(String className, Long key) {
		String hql = "select o from " + className + " o " + "where o.id = " + key;
		IDataEntity toReturn = (IDataEntity) getUniqueResult(hql);
		return toReturn;
	}
	
	

}
