package haj.com.constants;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.AddressTypeEnum;
import haj.com.entity.UsersLevelEnum;
import haj.com.entity.UsersStatusEnum;
import haj.com.utils.GenericUtility;


public class HAJConstants implements Serializable {

	private static final long serialVersionUID = 1L;
	protected static final Log logger = LogFactory.getLog(HAJConstants.class);
	public static final SimpleDateFormat sdfYYYY = new SimpleDateFormat("yyyy");
	public static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy");
	public static final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
	 public static final int NO_ROWS = 51;
		public static final int           MAX_IMG_WIDHT                 = 100;
		
		/** Field Limits */
		public static final Long MAX_FILE_SIZE                = 4194304L;
		public static final Long MAX_FIRST_NAME_SIZE          = 26L;
		public static final Long MAX_LAST_NAME_SIZE           = 45L;
		public static final Long MAX_EMAIL_SIZE               = 50L;
		public static final Long MAX_COMPANY_NAME_SIZE        = 70L;
		public static final Long MAX_COMPANY_TRADE_NAME_SIZE  = 70L;
		public static final Long MAX_ADDRESS_LINE_SIZE        = 50L;
		public static final Long MAX_ADDRESS_CODE_SIZE        = 4L;
		public static final Long MAX_TAX_NUMBER               = 9L;
		public static final Long MAX_VAX_NUMBER               = 13L;
		public static final Long MAX_RSA_ID_NUMBER            = 13L;
		public static final Long MAX_PASSPORT_NUMBER          = 9L;
		public static final Long MAX_FAX_NUMBER               = 10L;
		public static final Long MAX_NUMBER_OF_EMPLOYEES_SIZE = 10L;
		public static final Long MIN_BANK_ACCOUNT_NUMBER      = 6L;
		public static final Long MAX_BANK_ACCOUNT_NUMBER      = 15L;
		public static final Long MIN_BANK_BRANCH_NUMBER       = 2L;
		public static final Long MAX_BANK_BRANCH_NUMBER       = 6L;
		public static final Long MAX_BANK_HOLDER              = 50L;
		public static final Long MAX_SITE_NUMBER              = 10L;
		public static final Long MAX_SITE_NAME                = 10L;
		
		/** Number Formats */
		public static final String TELPHONE_FORMAT                  = "099 999 9999";
		public static final String FAX_NUMBER_FORMAT                = "099 999 9999";
		public static final String companyRegistrationNumberFormat  = "9999/999999/99";
		public static final String companyAccreditationNumberFormat = "25-aa/ACC/9999/99";
		public static final String companyLevyNumberFormat          = "L999999999";
		public static final String companyNNumberFormat             = "N999999999";
		public static final String companyVatNumberFormat           = "ZA999999999?*9";
		public static final String CELLPHONE_FORMAT                 = "099 999 9999";
		public static final String passportNumberFormat             = "[a-zA-Z]{1}\\d{8}";
		public static final String LatLongDegreesFormat             = "99";
		public static final String LatLongMinuteFormat              = "99";
		public static final String LatLongSecondFormat              = "99.999";
		public static final String allowOnlyNumber                  = "if(event.which &lt; 48 || event.which &gt; 57) return false;";

	 
	 public static String DOC_PATH= ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("docPath");
	 public static String DOC_SERVER= ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("docServer");
	 public static String MAIL_SERVER = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("mailServer");
	 public static boolean MAIL_DEBUG = getMAIL_DEBUG();
	 public static String APP_PATH = (String)System.getProperties().get("TK-APP-PATH");
	 public static String APNS_CERTIFICATE = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("APNS_CERTIFICATE");
	 public static boolean APNS_PROD = getAPNS_PROD();
	  
	 public static String FACEBOOK_URL = "http://www.facebook.com/pages/eezi-Sign/293697487314313?ref=ts";
	  public static String FACEBOOK_APP_CENTER_URL = "http://www.facebook.com/appcenter/eezi-sign?fb_source=appcenter";
	  public static String TWITTER_URL = "https://twitter.com/EeziSign";
	  public static String CHROME_WEBSTORE_URL = "https://chrome.google.com/webstore/detail/allpbmfjdneaahbihagmijcobpmnhfgb?utm_source=gmail";
	  public static String PL_LINK = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("PL");
	  public static String INFO_REQ_MAIL = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("INFO_REQ_MAIL");
	  public static String FOWARD_TO_FRIEND = "";
	  public static String EMAIL_TEMPLATE_COPYWRITE_YEAR = "2013";
	  public static String EMAIL_TEMPLATE_COPYWRITE_COMPANY = "HAJ Consulting";
	  //public static String LEGACY_DOCS = ((java.util.Properties) System.getProperties().get("RC-PROPERTIES")).getProperty("LEGACY_DOCS");
	  
	  public static String EMAIL_MSG = createEmailMSG();
	  public static String EMAIL_MSG_SUBJECT = "Postbox lease expire notification";
	  public static Map<Integer,String> MONTHS = genereateMonthMap();
	
	public static List<SelectItem> userLevelDD = createUserLevelDD();
	public static List<SelectItem> userStatusDD = createUserStatusDD();
	public static List<SelectItem> addrTypeDD = createAddrTypeDD();
	public static String MAIL_FROM = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_mailFrom");
	
	public static String EMAIL_TEMPLATE = getTemplate();
	public static String EMAIL_TEMPLATE_CUSTOM = getTemplateCustom();


	private static List<SelectItem> createUserLevelDD() {
		List<SelectItem> l =new ArrayList<SelectItem>();
		for (UsersLevelEnum val : UsersLevelEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}



	private static boolean getAPNS_PROD() {
		boolean rtnVal = false;
		try {
			rtnVal = Boolean.parseBoolean(((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("APNS_PROD").trim());
		} catch (Exception e) {
		}

		return rtnVal;
	}

	private static List<SelectItem> createAddrTypeDD() {
		List<SelectItem> l =new ArrayList<SelectItem>();
		for (AddressTypeEnum val : AddressTypeEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}

	private static Map<Integer, String> genereateMonthMap() {
		Map<Integer,String> m = new HashMap<Integer,String>();
		m.put(1, "Jan");
		m.put(2, "Feb");
		m.put(3, "March");
		m.put(4, "April");
		m.put(5, "May");
		m.put(6, "Jun");
		m.put(7, "Jul");
		m.put(8, "Aug");
		m.put(9, "Sep");
		m.put(10, "Oct");
		m.put(11, "Nov");
		m.put(12, "Dec");
		
		
		return m;
	}

	private static List<SelectItem> createUserStatusDD() {
		List<SelectItem> l =new ArrayList<SelectItem>();
		for (UsersStatusEnum val : UsersStatusEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	private static String createEmailMSG() {
		String msg = "<p>Dear #NAME,</p>" +
				"<p>The lease of your postbox: <b>#PBOXNR</b> at <b>#COMPANY</b> will expire on <b>#EXP_DATE</b></p> "+
				"<p>Please renew your lease."+			
				"<p>Regards</p>"+
				"<p>The Post Box Manager team</p>"+
				"<br/>";
		return msg;
	}
	
	private static boolean getMAIL_DEBUG() {
		boolean rtnVal = false;
		try {
			rtnVal = Boolean.parseBoolean(((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("MAIL_DEBUG").trim());
		} catch (Exception e) {
		}

		return rtnVal;
}
	
	private static String getTemplate() {
		String html = null;
		try {
			//InputStream inputStream = HAJConstants.class.getResourceAsStream("content.html");
			html =   GenericUtility.readFile(HAJConstants.APP_PATH+"/emailTemplate/content.html");
			//html = IOUtils.toString(inputStream, "UTF8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return html;
	}
	
	private static String getTemplateCustom() {
		String html = null;
		try {
			//InputStream inputStream = CustomPath.class.getResourceAsStream("content.html");
			//html = IOUtils.toString(inputStream, "UTF8");
			html =   GenericUtility.readFile(HAJConstants.APP_PATH+"/emailTemplate/content.html");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return html;
	}
	
	private static boolean getBoolean(String property) {
		boolean rtnVal = false;
		try {
			rtnVal = Boolean.parseBoolean(((java.util.Properties) System.getProperties().get("DD-PROPERTIES")).getProperty(property).trim());
		} catch (Exception e) {
			System.out.println("CANT find property: " + property);
			logger.fatal(e);

		}
		return rtnVal;
	}

}
