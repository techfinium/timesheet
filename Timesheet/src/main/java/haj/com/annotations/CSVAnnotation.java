package haj.com.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CSVAnnotation {

	public String name();
	
	public Class<?> className() default Object.class;
	
	public String datePattern() default "dd/MM/yyyy hh:mm";

}
