package haj.com.bean;

import java.io.Serializable;
import java.util.Locale;

public class AmountBean implements Serializable {
	
	
	private Locale fromLocale;
	private Locale toLocale;
	private Double amount;
	private Double baseAmount;
    private Double exhangeRate;
    
    
	public AmountBean() {
		super();
	}

	public AmountBean(Locale fromLocale, Double amount) {
		super();
		this.fromLocale = fromLocale;
		this.amount = amount;
	}



	public AmountBean(Locale fromLocale, Locale toLocale, Double amount) {
		super();
		this.fromLocale = fromLocale;
		this.toLocale = toLocale;
		this.amount = amount;
	}



	public Locale getFromLocale() {
		return fromLocale;
	}
	public void setFromLocale(Locale fromLocale) {
		this.fromLocale = fromLocale;
	}
	public Locale getToLocale() {
		return toLocale;
	}
	public void setToLocale(Locale toLocale) {
		this.toLocale = toLocale;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getBaseAmount() {
		return baseAmount;
	}
	public void setBaseAmount(Double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Double getExhangeRate() {
		return exhangeRate;
	}
	public void setExhangeRate(Double exhangeRate) {
		this.exhangeRate = exhangeRate;
	}

	@Override
	public String toString() {
		return "AmountBean [fromLocale=" + fromLocale + ", toLocale=" + toLocale + ", amount=" + amount
				+ ", baseAmount=" + baseAmount + ", exhangeRate=" + exhangeRate + "]";
	}


}
