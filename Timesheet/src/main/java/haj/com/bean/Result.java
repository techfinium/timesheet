
package haj.com.bean;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Result {

    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("quality")
    @Expose
    private Integer quality;
    @SerializedName("eastings")
    @Expose
    private Integer eastings;
    @SerializedName("northings")
    @Expose
    private Integer northings;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("nhs_ha")
    @Expose
    private String nhsHa;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("parliamentary_constituency")
    @Expose
    private String parliamentaryConstituency;
    @SerializedName("european_electoral_region")
    @Expose
    private String europeanElectoralRegion;
    @SerializedName("primary_care_trust")
    @Expose
    private String primaryCareTrust;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("lsoa")
    @Expose
    private String lsoa;
    @SerializedName("msoa")
    @Expose
    private String msoa;
    @SerializedName("incode")
    @Expose
    private String incode;
    @SerializedName("outcode")
    @Expose
    private String outcode;
    @SerializedName("admin_district")
    @Expose
    private String adminDistrict;
    @SerializedName("parish")
    @Expose
    private String parish;
    @SerializedName("admin_county")
    @Expose
    private String adminCounty;
    @SerializedName("admin_ward")
    @Expose
    private String adminWard;
    @SerializedName("ccg")
    @Expose
    private String ccg;
    @SerializedName("nuts")
    @Expose
    private String nuts;
    @SerializedName("codes")
    @Expose
    private Codes codes;

    /**
     * 
     * @return
     *     The postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * 
     * @param postcode
     *     The postcode
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * 
     * @return
     *     The quality
     */
    public Integer getQuality() {
        return quality;
    }

    /**
     * 
     * @param quality
     *     The quality
     */
    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    /**
     * 
     * @return
     *     The eastings
     */
    public Integer getEastings() {
        return eastings;
    }

    /**
     * 
     * @param eastings
     *     The eastings
     */
    public void setEastings(Integer eastings) {
        this.eastings = eastings;
    }

    /**
     * 
     * @return
     *     The northings
     */
    public Integer getNorthings() {
        return northings;
    }

    /**
     * 
     * @param northings
     *     The northings
     */
    public void setNorthings(Integer northings) {
        this.northings = northings;
    }

    /**
     * 
     * @return
     *     The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The nhsHa
     */
    public String getNhsHa() {
        return nhsHa;
    }

    /**
     * 
     * @param nhsHa
     *     The nhs_ha
     */
    public void setNhsHa(String nhsHa) {
        this.nhsHa = nhsHa;
    }

    /**
     * 
     * @return
     *     The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * 
     * @param longitude
     *     The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * 
     * @return
     *     The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * 
     * @param latitude
     *     The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * 
     * @return
     *     The parliamentaryConstituency
     */
    public String getParliamentaryConstituency() {
        return parliamentaryConstituency;
    }

    /**
     * 
     * @param parliamentaryConstituency
     *     The parliamentary_constituency
     */
    public void setParliamentaryConstituency(String parliamentaryConstituency) {
        this.parliamentaryConstituency = parliamentaryConstituency;
    }

    /**
     * 
     * @return
     *     The europeanElectoralRegion
     */
    public String getEuropeanElectoralRegion() {
        return europeanElectoralRegion;
    }

    /**
     * 
     * @param europeanElectoralRegion
     *     The european_electoral_region
     */
    public void setEuropeanElectoralRegion(String europeanElectoralRegion) {
        this.europeanElectoralRegion = europeanElectoralRegion;
    }

    /**
     * 
     * @return
     *     The primaryCareTrust
     */
    public String getPrimaryCareTrust() {
        return primaryCareTrust;
    }

    /**
     * 
     * @param primaryCareTrust
     *     The primary_care_trust
     */
    public void setPrimaryCareTrust(String primaryCareTrust) {
        this.primaryCareTrust = primaryCareTrust;
    }

    /**
     * 
     * @return
     *     The region
     */
    public String getRegion() {
        return region;
    }

    /**
     * 
     * @param region
     *     The region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * 
     * @return
     *     The lsoa
     */
    public String getLsoa() {
        return lsoa;
    }

    /**
     * 
     * @param lsoa
     *     The lsoa
     */
    public void setLsoa(String lsoa) {
        this.lsoa = lsoa;
    }

    /**
     * 
     * @return
     *     The msoa
     */
    public String getMsoa() {
        return msoa;
    }

    /**
     * 
     * @param msoa
     *     The msoa
     */
    public void setMsoa(String msoa) {
        this.msoa = msoa;
    }

    /**
     * 
     * @return
     *     The incode
     */
    public String getIncode() {
        return incode;
    }

    /**
     * 
     * @param incode
     *     The incode
     */
    public void setIncode(String incode) {
        this.incode = incode;
    }

    /**
     * 
     * @return
     *     The outcode
     */
    public String getOutcode() {
        return outcode;
    }

    /**
     * 
     * @param outcode
     *     The outcode
     */
    public void setOutcode(String outcode) {
        this.outcode = outcode;
    }

    /**
     * 
     * @return
     *     The adminDistrict
     */
    public String getAdminDistrict() {
        return adminDistrict;
    }

    /**
     * 
     * @param adminDistrict
     *     The admin_district
     */
    public void setAdminDistrict(String adminDistrict) {
        this.adminDistrict = adminDistrict;
    }

    /**
     * 
     * @return
     *     The parish
     */
    public String getParish() {
        return parish;
    }

    /**
     * 
     * @param parish
     *     The parish
     */
    public void setParish(String parish) {
        this.parish = parish;
    }

    /**
     * 
     * @return
     *     The adminCounty
     */
    public String getAdminCounty() {
        return adminCounty;
    }

    /**
     * 
     * @param adminCounty
     *     The admin_county
     */
    public void setAdminCounty(String adminCounty) {
        this.adminCounty = adminCounty;
    }

    /**
     * 
     * @return
     *     The adminWard
     */
    public String getAdminWard() {
        return adminWard;
    }

    /**
     * 
     * @param adminWard
     *     The admin_ward
     */
    public void setAdminWard(String adminWard) {
        this.adminWard = adminWard;
    }

    /**
     * 
     * @return
     *     The ccg
     */
    public String getCcg() {
        return ccg;
    }

    /**
     * 
     * @param ccg
     *     The ccg
     */
    public void setCcg(String ccg) {
        this.ccg = ccg;
    }

    /**
     * 
     * @return
     *     The nuts
     */
    public String getNuts() {
        return nuts;
    }

    /**
     * 
     * @param nuts
     *     The nuts
     */
    public void setNuts(String nuts) {
        this.nuts = nuts;
    }

    /**
     * 
     * @return
     *     The codes
     */
    public Codes getCodes() {
        return codes;
    }

    /**
     * 
     * @param codes
     *     The codes
     */
    public void setCodes(Codes codes) {
        this.codes = codes;
    }

	@Override
	public String toString() {
		return "Result [postcode=" + postcode + ", quality=" + quality + ", eastings=" + eastings + ", northings="
				+ northings + ", country=" + country + ", nhsHa=" + nhsHa + ", longitude=" + longitude + ", latitude="
				+ latitude + ", parliamentaryConstituency=" + parliamentaryConstituency + ", europeanElectoralRegion="
				+ europeanElectoralRegion + ", primaryCareTrust=" + primaryCareTrust + ", region=" + region + ", lsoa="
				+ lsoa + ", msoa=" + msoa + ", incode=" + incode + ", outcode=" + outcode + ", adminDistrict="
				+ adminDistrict + ", parish=" + parish + ", adminCounty=" + adminCounty + ", adminWard=" + adminWard
				+ ", ccg=" + ccg + ", nuts=" + nuts + ", codes=" + codes + "]";
	}

    
}
