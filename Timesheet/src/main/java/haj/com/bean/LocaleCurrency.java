package haj.com.bean;

import java.io.Serializable;
import java.util.Currency;
import java.util.Locale;

public class LocaleCurrency implements Serializable {

	private Locale locale;
	private Currency currency;
	
	
	
	public LocaleCurrency(Locale locale) {
		super();
		this.locale = locale;
	}
	public LocaleCurrency() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Locale getLocale() {
		return locale;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public Currency getCurrency() {
		if (locale!=null) {
			currency = Currency.getInstance(locale);
		}
		return currency;
	}
}
