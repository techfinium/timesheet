package haj.com.bean;

import java.io.Serializable;
import java.util.Date;

public class DashBean implements Serializable {

	private Integer intval;
	private Double doubleVal;
	private Double doubleVal2;
	private Long longVal;
    private Date date;
    
	public DashBean() {
		super();
	}

	public DashBean(Integer intval, Double doubleVal) {
		super();
		this.intval = intval;
		this.doubleVal = doubleVal;
	}

	public DashBean(Date date, Double doubleVal) {
		super();
		this.date = date;
		this.doubleVal = doubleVal;
	}
	
	
	public DashBean(Double doubleVal, Double doubleVal2,Long longVal) {
		super();
		this.longVal = longVal;
		this.doubleVal = doubleVal;
		this.doubleVal2 = doubleVal2;
	}

	
	
	public DashBean(Integer intval, Double doubleVal, Double doubleVal2,
			Long longVal) {
		super();
		this.intval = intval;
		this.doubleVal = doubleVal;
		this.doubleVal2 = doubleVal2;
		this.longVal = longVal;
	}

	public Integer getIntval() {
		return intval;
	}

	public void setIntval(Integer intval) {
		this.intval = intval;
	}

	public Double getDoubleVal() {
		return doubleVal;
	}

	public void setDoubleVal(Double doubleVal) {
		this.doubleVal = doubleVal;
	}

	public Double getDoubleVal2() {
		return doubleVal2;
	}

	public void setDoubleVal2(Double doubleVal2) {
		this.doubleVal2 = doubleVal2;
	}

	public Long getLongVal() {
		return longVal;
	}

	public void setLongVal(Long longVal) {
		this.longVal = longVal;
	}

	@Override
	public String toString() {
		return "DashBean [intval=" + intval + ", doubleVal=" + doubleVal
				+ ", doubleVal2=" + doubleVal2 + ", longVal=" + longVal + "]";
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	
	
	
}
