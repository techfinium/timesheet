package haj.com.bean;
import java.io.Serializable;

public class AttachmentBean implements Serializable {

	private String filename;
	private byte[] file;
	private String ext;
	
	
	
	public AttachmentBean() {
		super();
	}
	
	
	
	public AttachmentBean(String filename, byte[] file) {
		super();
		this.filename = filename;
		this.file = file;
	}

	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public byte[] getFile() {
		return file;
	}
	public void setFile(byte[] file) {
		this.file = file;
	}



	public String getExt() {
		return ext;
	}



	public void setExt(String ext) {
		this.ext = ext;
	}
}
