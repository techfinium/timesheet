package haj.com.bean;

import java.math.BigDecimal;
import java.math.BigInteger;

import haj.com.framework.IDataEntity;

public class ProjectUsersReportBean implements IDataEntity {

	/**
	 * tmd.projects_id as projectID " +
	 * "	, p.description as projectDescription " +
	 * "	, u.first_name as firstName " + "	, u.last_name as lastName " +
	 * "	, sum(tmd.hours) as totalHours " + " , sum(tmd.minutes) as
	 * totalMinutes
	 */
	private BigInteger projectID;
	private String projectCode;
	private String projectDescription;
	private String firstName;
	private String lastName;
	private String fullName;
	private Double percentage;
	private Long userId;
	private Double totalHours;
	private BigDecimal totalMinutes;

	public ProjectUsersReportBean() {
		super();
	}

	/** Getters and setters */
	public BigInteger getProjectID() {
		return projectID;
	}

	public void setProjectID(BigInteger projectID) {
		this.projectID = projectID;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Double totalHours) {
		this.totalHours = totalHours;
	}

	public BigDecimal getTotalMinutes() {
		return totalMinutes;
	}

	public void setTotalMinutes(BigDecimal totalMinutes) {
		this.totalMinutes = totalMinutes;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
}