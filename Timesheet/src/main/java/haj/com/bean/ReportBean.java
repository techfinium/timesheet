package haj.com.bean;

import java.io.Serializable;
import java.util.Date;

import haj.com.entity.CompanyUsers;
import haj.com.entity.Projects;
import haj.com.entity.Tasks;

public class ReportBean implements Serializable {

	private Integer intOne;
	private Long longOne;
	private Double doubleOne;
	private CompanyUsers user;
	private Double hours;
	private Double days;
	private Projects project;
	private Tasks task;
	private Date fromDate;
	private Date toDate;
	private Date detailFromDate;
	private String comment;
	private Integer weekNum;

	public ReportBean() {
		super();
	}

	public ReportBean(Tasks task) {
		super();
		this.task = task;
	}

	public ReportBean(Projects project, Long longOne) {
		super();
		this.longOne = longOne;
		this.project = project;
	}

	public ReportBean(Projects project, Double doubleOne) {
		super();
		this.doubleOne = doubleOne;
		this.project = project;
	}
	//Test
	public ReportBean(Projects project, Double doubleOne, Long longOne) {
		super();
		this.doubleOne = doubleOne;
		this.project = project;
		this.longOne = longOne;
		if (longOne > 60) {
			int h = (int) (longOne / 60);
			this.doubleOne += h;
			this.longOne -= h * 60;
		}
		
	}
	//test
	public ReportBean(CompanyUsers user, Double doubleOne, Long longOne) {
		super();
		this.doubleOne = doubleOne;
		this.user = user;
		this.longOne = longOne;
		if (longOne > 60) {
			int h = (int) (longOne / 60);
			this.doubleOne += h;
			this.longOne -= h * 60;
		}
	}
	//test
	public ReportBean(Double doubleOne, Long longOne,  Integer weekNum, CompanyUsers user ) {
		super();
		this.doubleOne = doubleOne;
		this.user = user;
		this.weekNum = weekNum;
		this.longOne = longOne;
		if (longOne > 60) {
			int h = (int) (longOne / 60);
			this.doubleOne += h;
			this.longOne -= h * 60;
		}
	}
	//Test
	//o.tasks,o.projects,o.comments,o.timesheetDetails.weekNumber,o.timesheetDetails.fromDateTime,o.hours,o.timesheetDetails.timesheet.companyUsers
	public ReportBean(Double doubleOne, Integer longOne ,Tasks task, Projects projects, String comment, Integer weekNum, Date detailFromDate, CompanyUsers user) {
		super();
		this.doubleOne = doubleOne;
		this.user = user;
		this.task = task;
		this.detailFromDate = detailFromDate;
		this.project = projects;
		this.comment = comment;
		this.weekNum = weekNum;
		this.longOne = longOne.longValue();
	}
	

	public ReportBean(CompanyUsers user, Long longOne) {
		super();
		this.longOne = longOne;
		this.user = user;
	}
	
	public ReportBean(CompanyUsers user, Double doubleOne) {
		super();
		this.doubleOne = doubleOne;
		this.user = user;
	}
	
	public ReportBean(Double doubleOne,  Integer weekNum, CompanyUsers user ) {
		super();
		this.doubleOne = doubleOne;
		this.user = user;
		this.weekNum = weekNum;
	}

	public ReportBean(Integer weekNum) {
		super();
		this.weekNum = weekNum;
	}

	public ReportBean(Tasks task, Double doubleOne, CompanyUsers user) {
		super();
		this.doubleOne = doubleOne;
		this.user = user;
		this.task = task;
	}
	
	public ReportBean(Tasks task, Projects projects, String comment, Date detailFromDate ,Double doubleOne, CompanyUsers user ) {
		super();
		this.doubleOne = doubleOne;
		this.user = user;
		this.task = task;
		this.project = projects;
		this.detailFromDate = detailFromDate;
		this.comment = comment;
	}
	
	public ReportBean(Tasks task, Projects projects,String comment, Integer weekNum, Date detailFromDate ,Double doubleOne, CompanyUsers user ) {
		super();
		this.doubleOne = doubleOne;
		this.user = user;
		this.task = task;
		this.detailFromDate = detailFromDate;
		this.project = projects;
		this.comment = comment;
		this.weekNum = weekNum;
	}
	


	public ReportBean(Tasks task, Integer intOne, CompanyUsers user) {
		super();
		this.longOne = intOne.longValue();
		this.user = user;
		this.task = task;
	}


	public ReportBean(Long longOne) {
		super();
		this.longOne = longOne;
	}

	public ReportBean(Double doubleOne) {
		super();
		this.doubleOne = doubleOne;
	}

	public Integer getIntOne() {
		return intOne;
	}

	public void setIntOne(Integer intOne) {
		this.intOne = intOne;
	}

	public Long getLongOne() {
		return longOne;
	}

	public void setLongOne(Long longOne) {
		this.longOne = longOne;
	}

	public Double getDoubleOne() {
		return doubleOne;
	}

	public void setDoubleOne(Double doubleOne) {
		this.doubleOne = doubleOne;
	}

	public Double getHours() {
		return hours;
	}

	public void setHours(Double hours) {
		this.hours = hours;
	}

	public Double getDays() {
		return days;
	}

	public void setDays(Double days) {
		this.days = days;
	}

	@Override
	public String toString() {
		return "ReportBean [intOne=" + intOne + ", longOne=" + longOne + ", doubleOne=" + doubleOne + ", hours=" + hours + ", days=" + days + "]";
	}

	public CompanyUsers getUser() {
		return user;
	}

	public void setUser(CompanyUsers user) {
		this.user = user;
	}

	public Projects getProject() {
		return project;
	}

	public void setProject(Projects project) {
		this.project = project;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Tasks getTask() {
		return task;
	}

	public void setTask(Tasks task) {
		this.task = task;
	}

	public Date getDetailFromDate() {
		return detailFromDate;
	}

	public void setDetailFromDate(Date detailFromDate) {
		this.detailFromDate = detailFromDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getWeekNum() {
		return weekNum;
	}

	public void setWeekNum(Integer weekNum) {
		this.weekNum = weekNum;
	}

}