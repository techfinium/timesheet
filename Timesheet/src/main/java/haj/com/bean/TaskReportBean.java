package haj.com.bean;

import java.io.Serializable;
import java.util.List;

import haj.com.entity.Company;
import haj.com.entity.Tasks;

public class TaskReportBean implements Serializable {

	private List<Tasks> task;
	private String name;
	private String description;
	private Company company;

	public List<Tasks> getTask() {
		return task;
	}

	public void setTask(List<Tasks> task) {
		this.task = task;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}