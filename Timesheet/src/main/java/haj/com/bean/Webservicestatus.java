package haj.com.bean;



public class Webservicestatus {

	private int errorcode;
	private String errdesc;
	private String additional_info;
	
	
	public Webservicestatus() {}
	
	public Webservicestatus(ErrorCodesEnum ec) {
		this.errorcode = ec.getCode();
		this.errdesc = ec.name();
	}
	
	public Webservicestatus(ErrorCodesEnum ec,String additional_info) {
		this.errorcode = ec.getCode();
		this.errdesc = ec.name();
		this.additional_info = additional_info;
	}
	
	public int getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}
	public String getErrdesc() {
		return errdesc;
	}
	public void setErrdesc(String errdesc) {
		this.errdesc = errdesc;
	}
	public String getAdditional_info() {
		return additional_info;
	}
	public void setAdditional_info(String additional_info) {
		this.additional_info = additional_info;
	}

}
