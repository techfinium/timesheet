package haj.com.bean;

import java.util.Date;

import org.primefaces.model.DefaultScheduleEvent;

import haj.com.entity.Timesheet;

public class HlsSheduleEvernt extends DefaultScheduleEvent {

	private Timesheet timesheet;
	private Date fromDateTime;
	private Date toDateTime;

	public HlsSheduleEvernt(Timesheet timesheet, Date fromDateTime, Date toDateTime, String title) {
		super(title, fromDateTime, toDateTime, timesheet);
		this.timesheet = timesheet;
		this.fromDateTime = fromDateTime;
		this.toDateTime = toDateTime;
	}

	public Timesheet getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(Timesheet timesheet) {
		this.timesheet = timesheet;
	}

	public Date getFromDateTime() {
		return fromDateTime;
	}

	public void setFromDateTime(Date fromDateTime) {
		this.fromDateTime = fromDateTime;
	}

	public Date getToDateTime() {
		return toDateTime;
	}

	public void setToDateTime(Date toDateTime) {
		this.toDateTime = toDateTime;
	}

}
