package haj.com.bean;

public enum ErrorCodesEnum {

   Ok(0), Warning(4), Error(8), Severe(12);


   private int code;
   
   private ErrorCodesEnum(int c) {
     code = c;
   }
   
   public int getCode() {
     return code;
   }
}
