
package haj.com.bean;

import javax.annotation.Generated;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Codes {

    @SerializedName("admin_district")
    @Expose
    private String adminDistrict;
    @SerializedName("admin_county")
    @Expose
    private String adminCounty;
    @SerializedName("admin_ward")
    @Expose
    private String adminWard;
    @SerializedName("parish")
    @Expose
    private String parish;
    @SerializedName("ccg")
    @Expose
    private String ccg;
    @SerializedName("nuts")
    @Expose
    private String nuts;

    /**
     * 
     * @return
     *     The adminDistrict
     */
    public String getAdminDistrict() {
        return adminDistrict;
    }

    /**
     * 
     * @param adminDistrict
     *     The admin_district
     */
    public void setAdminDistrict(String adminDistrict) {
        this.adminDistrict = adminDistrict;
    }

    /**
     * 
     * @return
     *     The adminCounty
     */
    public String getAdminCounty() {
        return adminCounty;
    }

    /**
     * 
     * @param adminCounty
     *     The admin_county
     */
    public void setAdminCounty(String adminCounty) {
        this.adminCounty = adminCounty;
    }

    /**
     * 
     * @return
     *     The adminWard
     */
    public String getAdminWard() {
        return adminWard;
    }

    /**
     * 
     * @param adminWard
     *     The admin_ward
     */
    public void setAdminWard(String adminWard) {
        this.adminWard = adminWard;
    }

    /**
     * 
     * @return
     *     The parish
     */
    public String getParish() {
        return parish;
    }

    /**
     * 
     * @param parish
     *     The parish
     */
    public void setParish(String parish) {
        this.parish = parish;
    }

    /**
     * 
     * @return
     *     The ccg
     */
    public String getCcg() {
        return ccg;
    }

    /**
     * 
     * @param ccg
     *     The ccg
     */
    public void setCcg(String ccg) {
        this.ccg = ccg;
    }

    /**
     * 
     * @return
     *     The nuts
     */
    public String getNuts() {
        return nuts;
    }

    /**
     * 
     * @param nuts
     *     The nuts
     */
    public void setNuts(String nuts) {
        this.nuts = nuts;
    }

	@Override
	public String toString() {
		return "Codes [adminDistrict=" + adminDistrict + ", adminCounty=" + adminCounty + ", adminWard=" + adminWard
				+ ", parish=" + parish + ", ccg=" + ccg + ", nuts=" + nuts + "]";
	}
    
    

}
