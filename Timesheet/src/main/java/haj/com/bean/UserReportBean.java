package haj.com.bean;

import java.io.Serializable;
import java.util.List;

import haj.com.entity.Company;
import haj.com.entity.Users;

public class UserReportBean implements Serializable {

	private Company client;
	private int totHours;
	private List<Users> users;
	
	

	public Company getClient() {
		return client;
	}

	public void setClient(Company client) {
		this.client = client;
	}

	public int getTotHours() {
		return totHours;
	}

	public void setTotHours(int totHours) {
		this.totHours = totHours;
	}

	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

}