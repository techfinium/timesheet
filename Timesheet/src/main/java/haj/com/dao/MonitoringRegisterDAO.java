package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.MonitoringRegister;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class MonitoringRegisterDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    MonitoringRegister
 * @return a List of MonitoringRegister objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<MonitoringRegister> allMonitoringRegister() throws Exception {
		return (List<MonitoringRegister>)super.getList("select o from MonitoringRegister o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<MonitoringRegister> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from MonitoringRegister o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<MonitoringRegister>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<MonitoringRegister> byField(long key) throws Exception  {
		String hql = "select o from MonitoringRegister o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<MonitoringRegister>)super.getList(hql, parameters);
	}
*/

	public MonitoringRegister findByKey(Long id) throws Exception {
	 	String hql = "select o from MonitoringRegister o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (MonitoringRegister)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<MonitoringRegister> findByName(String description) throws Exception {
	 	String hql = "select o from MonitoringRegister o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<MonitoringRegister>)super.getList(hql, parameters);
	}
}

