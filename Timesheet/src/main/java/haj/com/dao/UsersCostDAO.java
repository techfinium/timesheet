package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Revinfo;
import haj.com.entity.UsersCost;
import haj.com.entity.UsersCostHist;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class UsersCostDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * Get all UsersCost
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @return List<UsersCost>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<UsersCost> allUsersCost() throws Exception {
		return (List<UsersCost>) super.getList("select o from UsersCost o");
	}

	@SuppressWarnings("unchecked")
	public List<UsersCost> allUsersCost(Long from, int noRows) throws Exception {
		String hql = "select o from UsersCost o ";
		Map<String, Object> parameters = new Hashtable<String, Object>();

		return (List<UsersCost>) super.getList(hql, parameters, from.intValue(), noRows);
	}

	/**
	 * Find object by primary key
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @return UsersCost
	 * @throws Exception
	 */
	public UsersCost findByKey(Long id) throws Exception {
		String hql = "select o from UsersCost o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (UsersCost) super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find UsersCost by description
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @return List<UsersCost>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<UsersCost> findByName(String description) throws Exception {
		String hql = "select o from UsersCost o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<UsersCost>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<UsersCost> findByNames(String description) throws Exception {
		String hql = "select o from UsersCost o where o.users in (select x from Users x where x.firstName like  :description or x.lastName like  :description) ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<UsersCost>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<UsersCost> findByUser(Long userId) throws Exception {
		String hql = "select o from UsersCost o where o.users.uid =  :userId order by o.effectiveDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		return (List<UsersCost>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public Boolean findByUserB(Long userId) throws Exception {
		String hql = "select o from UsersCost o where o.users.uid =  :userId order by o.effectiveDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		return (Boolean) ((super.getList(hql, parameters)).size() > 0);
	}

	@SuppressWarnings("unchecked")
	public UsersCost findByOneUser(Long userId) throws Exception {
		String hql = "select distinct o from UsersCost o where o.users.uid =  :userId order by o.effectiveDate desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		return (UsersCost) super.getUniqueResult(hql, parameters);
	}

	public List<UsersCostHist> findByKeyHist(Long id) throws Exception {
		String hql = "select o from UsersCostHist o where o.id.id = :id order by o.createDate desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<UsersCostHist>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<UsersCostHist> findCostHistoryForUser(Long userId) throws Exception {
		String hql = "select o from UsersCostHist o where o.user.uid = :userId order by o.id.id desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		return (List<UsersCostHist>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public Revinfo findByKeyRev(Integer id) throws Exception {
		String hql = "select o from Revinfo o where o.rev = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Revinfo) super.getUniqueResult(hql, parameters);
	}
}
