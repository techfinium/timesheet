package haj.com.dao;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.bean.ReportBean;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ReportsDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	public ReportBean totalMinutesForProject(Date fromDate, Date toDate, Long companyId) throws Exception {
		String hql = "select new haj.com.bean.ReportBean(sum(o.hours)) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "and o.projects.company.id = :companyId";
		// "group by date(o.timesheet.fromDate), date(o.timesheet.toDate)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("companyId", companyId);
		return (ReportBean) super.getUniqueResult(hql, parameters);
	}
	
	public ReportBean totalMinutesForProjectForUser(Date fromDate, Date toDate, Long userId) throws Exception {
		String hql = "select new haj.com.bean.ReportBean(sum(o.hours)) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "and o.timesheetDetails.timesheet.companyUsers.users.uid = :userId";
		// "group by date(o.timesheet.fromDate), date(o.timesheet.toDate)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("userId", userId);
		return (ReportBean) super.getUniqueResult(hql, parameters);
	}
	
	public ReportBean totalMinutesForProjectDirector(Date fromDate, Date toDate) throws Exception {
		String hql = "select new haj.com.bean.ReportBean(sum(o.hours)) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		return (ReportBean) super.getUniqueResult(hql, parameters);
	}
	
	public ReportBean totalMinutesForProjectDirector(Long projectId, Date fromDate, Date toDate)
			throws Exception {
		String hql = "select new haj.com.bean.ReportBean(sum(o.hours)) from TimesheetDetails o "
				+ "where o.projects.id = :projectId " + "and date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "group by o.projects.id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		return (ReportBean) super.getUniqueResult(hql, parameters);
	}

	public ReportBean totalMinutesForProject(Long projectId, Date fromDate, Date toDate, Long companyId)
			throws Exception {
		String hql = "select new haj.com.bean.ReportBean(sum(o.hours)) from TimesheetDetails o "
				+ "where o.projects.id = :projectId " + "and date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "and o.timesheetDetails.timesheet.companyUsers.company.id = :companyId " 
				+ "group by o.projects.id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("companyId", companyId);
		return (ReportBean) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ReportBean> projectDetails(Long projectId, Date fromDate, Date toDate, Long companyId, Boolean active)
			throws Exception {
		String hql = "select new haj.com.bean.ReportBean(o.timesheetDetails.timesheet.companyUsers,sum(o.hours),sum(o.minutes)) from TimesheetDetails o "
				+ "where o.projects.id = :projectId and o.projects.active = :active " + "and date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "group by o.timesheetDetails.timesheet.companyUsers";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("active", active);
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ReportBean> projectDetailsReport(Long projectId, Date fromDate, Date toDate, Long companyId)
			throws Exception {
		String hql = "select new haj.com.bean.ReportBean(o.timesheetDetails.timesheet.companyUsers,sum(o.hours),sum(o.minutes)) from TimesheetDetails o "
				+ "where o.projects.id = :projectId " + "and date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "group by o.timesheetDetails.timesheet.companyUsers";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ReportBean> projectDetailsDirector(Long projectId, Date fromDate, Date toDate)
			throws Exception {
		String hql = "select new haj.com.bean.ReportBean(o.timesheetDetails.timesheet.companyUsers,sum(o.hours),sum(o.minutes)) from TimesheetDetails o "
				+ "where o.projects.id = :projectId " + "and date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "group by o.timesheetDetails.timesheet.companyUsers";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ReportBean> projectDetails(Long projectId, Date fromDate, Date toDate)
			throws Exception {
		String hql = "select new haj.com.bean.ReportBean(o.timesheetDetails.timesheet.companyUsers,sum(o.hours)) from TimesheetDetails o "
				+ "where o.projects.id = :projectId " + "and date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "group by o.timesheetDetails.timesheet.companyUsers";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ReportBean> projectUserWeek(Long projectId, Date fromDate, Date toDate, Long companyId)
			throws Exception {
		String hql = "select new haj.com.bean.ReportBean(sum(o.hours),sum(o.minutes),o.timesheetDetails.weekNumber,o.timesheetDetails.timesheet.companyUsers) from TimesheetDetails o "
				+ "where o.projects.id = :projectId " + "and date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) and o.timesheetDetails.timesheet.companyUsers.id = :companyId "
				+ "group by o.timesheetDetails.weekNumber";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("companyId", companyId);
		return (List<ReportBean>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ReportBean> projectTasks(Long projectId, Date fromDate, Date toDate, Long companyId, Integer weekNum) throws Exception {
		String hql = "select new haj.com.bean.ReportBean(o.tasks,o.projects,o.comments,o.timesheetDetails.fromDateTime,o.hours,o.timesheetDetails.timesheet.companyUsers) from TimesheetDetails o "
				+ "where o.projects.id = :projectId " + "and date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "and o.timesheetDetails.timesheet.companyUsers.id = :companyId and"
				+ " o.timesheetDetails.weekNumber = :weekNum";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("companyId", companyId);
		parameters.put("weekNum", weekNum);
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ReportBean> userDetailsAll(Date fromDate, Date toDate, Long companyId) throws Exception {	
		String hql = "select new haj.com.bean.ReportBean(o.hours,o.minutes,o.tasks,o.projects,o.comments,o.timesheetDetails.weekNumber,o.timesheetDetails.fromDateTime,o.timesheetDetails.timesheet.companyUsers) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "and o.timesheetDetails.timesheet.companyUsers.id = :companyId "
				+ "order by o.timesheetDetails.fromDateTime asc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("companyId", companyId);
		return (List<ReportBean>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ReportBean> userDetails(Date fromDate, Date toDate, Long companyId, Integer weekNum, Long projectId) throws Exception {	//needs projectId
		String hql = "select new haj.com.bean.ReportBean(o.hours,o.minutes,o.tasks,o.projects,o.comments,o.timesheetDetails.weekNumber,o.timesheetDetails.fromDateTime,o.timesheetDetails.timesheet.companyUsers) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) and o.projects.id = :projectId "
				+ "and o.timesheetDetails.weekNumber = :weekNum "
				+ "and o.timesheetDetails.timesheet.companyUsers.id = :companyId "
				+ "order by o.timesheetDetails.fromDateTime asc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("companyId", companyId);
		parameters.put("weekNum", weekNum);
		parameters.put("projectId", projectId);
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	//o.tasks,o.projects,o.comments,o.timesheetDetails.fromDateTime,o.hours,o.timesheetDetails.timesheet.companyUsers
	// Tasks task, Projects projects, Date detailFromDate ,Double doubleOne,
	// CompanyUsers user
	// TimesheetDetails timeDetail, Double doubleOne, CompanyUsers user
	// o.tasks,o.hours,o.timesheetDetails.timesheet.companyUsers fromDateTime
	// o.tasks,o.hours,o.timesheetDetails.timesheet.companyUsers,o.projects

	@SuppressWarnings("unchecked")
	public List<ReportBean> userDetailsMins(Long timesheetId) throws Exception {
		String hql = "select new haj.com.bean.ReportBean( o.timesheetDetails.timesheet.companyUsers,sum(o.hours)) from TimesheetDetails o "
				+ "where o.timesheetDetails.timesheet.id = :timesheetId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("timesheetId", timesheetId);
		return (List<ReportBean>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ReportBean> projectsFromToDate(Date fromDate, Date toDate, Long companyId) throws Exception {
		String hql = "select new haj.com.bean.ReportBean( o.projects,sum(o.hours),sum(o.minutes)) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "and o.projects.company.id = :companyId group by o.projects.id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("companyId", companyId);
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ReportBean> projectsFromToDateForUser(Date fromDate, Date toDate, Long userId) throws Exception {
		String hql = "select new haj.com.bean.ReportBean( o.projects,sum(o.hours),sum(o.minutes)) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "and o.timesheetDetails.timesheet.companyUsers.users.uid = :userId group by o.projects.id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("userId", userId);
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ReportBean> projectsFromToDateActive(Date fromDate, Date toDate, Long companyId, boolean active) throws Exception {
		String hql = "select new haj.com.bean.ReportBean( o.projects,sum(o.hours),sum(o.minutes)) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "and o.projects.company.id = :companyId and o.projects.active = :active group by o.projects.id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("companyId", companyId);
		parameters.put("active", active); 
		return (List<ReportBean>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ReportBean> projectsFromToDateDirector(Date fromDate, Date toDate) throws Exception {
		String hql = "select new haj.com.bean.ReportBean(o.projects,sum(o.hours),sum(o.minutes)) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) "
				+ "group by o.projects.id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		return (List<ReportBean>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ReportBean> projectsFromToDate(Date fromDate, Date toDate, boolean active) throws Exception {
		String hql = "select new haj.com.bean.ReportBean( o.projects,sum(o.hours)) from TimesheetDetails o "
				+ "where date(o.timesheetDetails.fromDateTime) >= date(:fromDate) "
				+ "and date(o.timesheetDetails.toDateTime) <= date(:toDate) and o.projects.active = :active group by o.projects.id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		parameters.put("active", active);
		return (List<ReportBean>) super.getList(hql, parameters);

	}

}
