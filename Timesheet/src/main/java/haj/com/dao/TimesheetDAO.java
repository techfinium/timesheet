package haj.com.dao;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.ibm.icu.text.SimpleDateFormat;

import haj.com.bean.ProjectUsersReportBean;
import haj.com.entity.Timesheet;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class TimesheetDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Timesheet> allTimesheet() throws Exception {
		return (List<Timesheet>) super.getList("select o from Timesheet o");
	}

	@SuppressWarnings("unchecked")
	public List<Timesheet> byCompany(long companyId) throws Exception {
		String hql = "select o from Timesheet o where o.companyUsers.company.id = :companyId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("companyId", companyId);
		return (List<Timesheet>) super.getList(hql, parameters);
	}
	
	/*
	 * @SuppressWarnings("unchecked") public List<Timesheet> byField(long id)
	 * throws Exception { String hql =
	 * "select o from Timesheet o where o.id = :id"; Map<String, Object>
	 * parameters = new Hashtable<String, Object>(); parameters.put("id", id);
	 * return (List<Timesheet>)super.getList(hql, parameters); }
	 */

	public Timesheet findByKey(Long id) throws Exception {
		String hql = "select o from Timesheet o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Timesheet) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Timesheet> findByName(String description) throws Exception {
		String hql = "select o from Timesheet where o.description like  :description order by o.description ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<Timesheet>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Timesheet> findByCompanyUser(Long companyUserId) throws Exception {
		String hql = "select o from Timesheet o where o.companyUsers.id = :companyUserId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("companyUserId", companyUserId);
		return (List<Timesheet>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Timesheet> findByCompanyUser(Long companyUserId, Date startDate, Date toDate) throws Exception {
		String hql = "select o from Timesheet o where o.companyUsers.id = :companyUserId and date(o.fromDate) >= date(:startDate) and date(o.toDate) <= date(:toDate) order by o.fromDate desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("companyUserId", companyUserId);
		parameters.put("startDate", startDate);
		parameters.put("toDate", toDate);
		return (List<Timesheet>) super.getList(hql, parameters);
	}
	
	public List<ProjectUsersReportBean> locateUsersProjectsReportByDateRange(Date fromDate, Date toDate) throws Exception {	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sql = "select " + 
				"	u.first_name as firstName " + 
				"	, u.last_name as lastName " +
				"	, concat(u.first_name, ' ' ,u.last_name) as fullName " +
				"	, tmd.projects_id as projectID " +
				"	, p.code as projectCode " +
				"	, p.description as projectDescription " + 
				"	, sum(tmd.hours) as totalHours " +
				"	, sum(tmd.minutes) as totalMinutes " + 
				"from timesheet_details tmd " + 
				"left join projects p on p.id = tmd.projects_id " + 
				"left join timesheet_details t on t.id = tmd.timesheet_details_id " + 
				"left join timesheet tm on tm.id = t.timesheet_id " + 
				"left join company_users cu on cu.id = tm.company_users_id " + 
				"left join users u on u.uid = cu.user_id " + 
				"where tmd.projects_id is not null " + 
				"and (t.from_date_time BETWEEN '"+sdf.format(fromDate)+"' AND '"+sdf.format(toDate)+"') " + 
				"group by u.first_name, u.last_name, tmd.projects_id, p.description";
		return (List<ProjectUsersReportBean>) super.nativeSelectSqlList(sql, ProjectUsersReportBean.class, null);
	}

}
