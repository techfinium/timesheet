package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.BankHolidayChild;
import haj.com.entity.BankHolidayParent;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class BankHolidayParentDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<BankHolidayParent> allBankHolidayParent() throws Exception {
		return (List<BankHolidayParent>)super.getList("select o from BankHolidayParent o");
	}


	@SuppressWarnings("unchecked")
	public List<BankHolidayParent> findByCompany(Long companyId) throws Exception {
	 	String hql = "select o from BankHolidayParent o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<BankHolidayParent>)super.getList(hql, parameters);
	}

/*
	@SuppressWarnings("unchecked")
	public List<BankHolidayParent> byField(long key) throws Exception  {
		String hql = "select o from BankHolidayParent o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<BankHolidayParent>)super.getList(hql, parameters);
	}
*/

	public List<BankHolidayChild> findRepeatDates() throws Exception {
		return (List<BankHolidayChild>)super.getList("select o from BankHolidayChild o where o.repeatDates = true");
	}
	
	
	
	public BankHolidayParent findByKey(Long id) throws Exception {
	 	String hql = "select o from BankHolidayParent o where o.bankHolidayYear = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (BankHolidayParent)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<BankHolidayParent> findByName(String description) throws Exception {
	 	String hql = "select o from BankHolidayParent o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<BankHolidayParent>)super.getList(hql, parameters);
	}
}

