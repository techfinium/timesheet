package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.MedicalScreeningForm;
import haj.com.entity.Users;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class MedicalScreeningFormDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    MedicalScreeningForm
 * @return a List of MedicalScreeningForm objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<MedicalScreeningForm> allMedicalScreeningForm() throws Exception {
		return (List<MedicalScreeningForm>)super.getList("select o from MedicalScreeningForm o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<MedicalScreeningForm> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from MedicalScreeningForm o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<MedicalScreeningForm>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<MedicalScreeningForm> byField(long key) throws Exception  {
		String hql = "select o from MedicalScreeningForm o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<MedicalScreeningForm>)super.getList(hql, parameters);
	}
*/

	public MedicalScreeningForm findByKey(Long id) throws Exception {
	 	String hql = "select o from MedicalScreeningForm o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (MedicalScreeningForm)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<MedicalScreeningForm> findByName(String description) throws Exception {
	 	String hql = "select o from MedicalScreeningForm o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<MedicalScreeningForm>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public MedicalScreeningForm findLatestFormByUser(Users users) throws Exception {
	 	String hql = "select o from MedicalScreeningForm o where o.user.id = :uID order by o.createDate desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("uID", users.getUid());
	    List<MedicalScreeningForm> l = (List<MedicalScreeningForm>)super.getList(hql, parameters, 1);
		return (l == null || l.isEmpty()) ? null : l.get(0);
	}
}

