package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.MilestoneStatementOfWork;
import haj.com.entity.StatementOfWork;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class MilestoneStatementOfWorkDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see MilestoneStatementOfWork
	 * @return a List of MilestoneStatementOfWork objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<MilestoneStatementOfWork> allMilestoneStatementOfWork() throws Exception {
		return (List<MilestoneStatementOfWork>) super.getList("select o from MilestoneStatementOfWork o");
	}

	public MilestoneStatementOfWork findByKey(Long id) throws Exception {
		String hql = "select o from MilestoneStatementOfWork o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (MilestoneStatementOfWork) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<MilestoneStatementOfWork> findSowMilestone(Long sowId) throws Exception {
		String hql = "select o from MilestoneStatementOfWork o where o.sow.id = :sowId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowId", sowId);
		return (List<MilestoneStatementOfWork>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<MilestoneStatementOfWork> findSowMilestoneInvoiceNull(StatementOfWork sow) throws Exception {
		String hql = "select o from MilestoneStatementOfWork o where o.sow.id = :id and o.invoice.id is null";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", sow.getId());
		return (List<MilestoneStatementOfWork>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public MilestoneStatementOfWork findSowInvoiceMilestone(Long sowId, Long invoiceId) throws Exception {
		String hql = "select o from MilestoneStatementOfWork o where o.sow.id = :sowId and o.invoice.id = :invoiceId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowId", sowId);
		parameters.put("invoiceId", invoiceId);
		return (MilestoneStatementOfWork) super.getUniqueResult(hql, parameters);
	}
	

	@SuppressWarnings("unchecked")
	public Boolean findSowMilestoneB(Long sow) throws Exception {
		String hql = "select o from MilestoneStatementOfWork o where o.sow.id = :id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", sow);
		return (Boolean) (super.getList(hql, parameters).size() > 0);
	}
}
