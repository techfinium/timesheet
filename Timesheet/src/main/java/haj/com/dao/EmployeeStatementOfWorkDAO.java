package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.EmployeeStatementOfWork;
import haj.com.entity.StatementOfWork;
import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class EmployeeStatementOfWorkDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see EmployeeStatementOfWork
	 * @return a List of EmployeeStatementOfWork objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<EmployeeStatementOfWork> allEmployeeStatementOfWork() throws Exception {
		return (List<EmployeeStatementOfWork>) super.getList("select o from EmployeeStatementOfWork o");
	}

	public EmployeeStatementOfWork findByKey(Long id) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (EmployeeStatementOfWork) super.getUniqueResult(hql, parameters);
	}

	public EmployeeStatementOfWork findByUsers(Long id) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.employee.uid = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (EmployeeStatementOfWork) super.getUniqueResult(hql, parameters);
	}
	
	public EmployeeStatementOfWork findByUsersAndSOW(Long uid, Long sowId) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.employee.uid = :uid and o.sow.id = :sowId ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uid", uid);
		parameters.put("sowId", sowId);
		return (EmployeeStatementOfWork) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<EmployeeStatementOfWork> findBySOW(StatementOfWork sow) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.sow.id = :id and o.employee.id is not null and o.sowTypeEnum = :sowType";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", sow.getId());
		parameters.put("sowType", sow.getSowTypeEnum());
		return (List<EmployeeStatementOfWork>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeStatementOfWork> findSowMilestone(StatementOfWork sow) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.sow.id = :id and o.employee.id is null and o.sowTypeEnum = :sowType";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", sow.getId());
		parameters.put("sowType", sow.getSowTypeEnum());
		return (List<EmployeeStatementOfWork>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<EmployeeStatementOfWork> findBySOWandNull(Long id) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.sow.id = :id and o.employee.id is null";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<EmployeeStatementOfWork>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<EmployeeStatementOfWork> findByName(String description) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<EmployeeStatementOfWork>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeStatementOfWork> findBySOWForInvoice(Long sowId) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.sow.id = :sowId and o.sow.sowTypeEnum = :sowType and o.employee is null";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowId", sowId);
		parameters.put("sowType", SowTypeEnum.Fixed);
		return (List<EmployeeStatementOfWork>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeStatementOfWork> findBySowAndUser(Long sowId, Long userId) throws Exception {
		String hql = "select o from EmployeeStatementOfWork o where o.sow.id = :sowId and o.employee.uid = :userId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowId", sowId);
		parameters.put("userId", userId);
		return (List<EmployeeStatementOfWork>) super.getList(hql, parameters);
	}
}
