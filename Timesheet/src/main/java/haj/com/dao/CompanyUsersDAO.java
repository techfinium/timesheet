package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.CompanyUsers;
import haj.com.entity.UsersLevelEnum;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class CompanyUsersDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<CompanyUsers> allCompanyUsers() throws Exception {
		return (List<CompanyUsers>) super.getList("select o from CompanyUsers o");
	}

	@SuppressWarnings("unchecked")
	public List<CompanyUsers> allCompanyUsersNotAdmin() throws Exception {
		return (List<CompanyUsers>) super.getList(
				"select o from CompanyUsers o where o.level = " + UsersLevelEnum.Normal.ordinal());
	}
	
	@SuppressWarnings("unchecked")
	public List<CompanyUsers> allCompanyUsersExAdmin() throws Exception {
		return (List<CompanyUsers>) super.getList(
				"select o from CompanyUsers o where o.level <> " + UsersLevelEnum.Admin.ordinal());
	}
	
	@SuppressWarnings("unchecked")
	public List<CompanyUsers> allCompanyUsersExAdminDirector() throws Exception {
		return (List<CompanyUsers>) super.getList(
				"select o from CompanyUsers o where o.level <> " + UsersLevelEnum.Admin.ordinal()+ " and o.level <> " + UsersLevelEnum.Director.ordinal());
	}

	@SuppressWarnings("unchecked")
	public List<CompanyUsers> byUser(long uid) throws Exception {
		String hql = "select o from CompanyUsers o where o.users.uid = :uid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uid", uid);
		return (List<CompanyUsers>) super.getList(hql, parameters);
	}

	public CompanyUsers findByKey(Long id) throws Exception {
		String hql = "select o from CompanyUsers o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (CompanyUsers) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CompanyUsers> findByName(String desc) throws Exception {
		String hql = "select o from CompanyUsers where o.desc like  :desc order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("desc", "" + desc.trim() + "%");
		return (List<CompanyUsers>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<CompanyUsers> byCompanyExDirector(Long id) throws Exception {
		String hql = "select o from CompanyUsers o where o.company.id = :id  and o.level = " + UsersLevelEnum.Manager.ordinal();
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<CompanyUsers>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CompanyUsers> byCompany(Long id) throws Exception {
		String hql = "select o from CompanyUsers o where o.company.id = :id and o.active = true ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<CompanyUsers>) super.getList(hql, parameters);
	}

	public CompanyUsers findCompanyUsers(Long userId, Long companyId) throws Exception {
		String hql = "select o from CompanyUsers o where o.users.uid = :userId and o.company.id = :companyId ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		parameters.put("companyId", companyId);
		return (CompanyUsers) super.getUniqueResult(hql, parameters);
	}
	
}
