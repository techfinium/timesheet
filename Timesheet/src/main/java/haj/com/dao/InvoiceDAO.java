package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Invoice;
import haj.com.entity.InvoiceHist;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class InvoiceDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * Get all Invoice
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @return List<Invoice>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Invoice> allInvoice() throws Exception {
		return (List<Invoice>) super.getList("select o from Invoice o  order by o.createDate desc");
	}

	@SuppressWarnings("unchecked")
	public List<Invoice> allInvoice(Long from, int noRows) throws Exception {
		String hql = "select o from Invoice o ";
		Map<String, Object> parameters = new Hashtable<String, Object>();

		return (List<Invoice>) super.getList(hql, parameters, from.intValue(), noRows);
	}

	/**
	 * Find object by primary key
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @return Invoice
	 * @throws Exception
	 */
	public Invoice findByKey(Long id) throws Exception {
		String hql = "select o from Invoice o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Invoice) super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find Invoice by description
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @return List<Invoice>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Invoice> findByName(String description) throws Exception {
		String hql = "select o from Invoice o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<Invoice>) super.getList(hql, parameters);
	}

	/**
	 * Find Invoice by description
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @return List<Invoice>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Double findBySOWInvoiceTotal(Long sowId) throws Exception {
		String hql = "select sum(o.invoiceValue) from Invoice o where o.statementOfWork.id like :sowId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowId", sowId);
		return (Double) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public Boolean findBySOWB(Long sowId) throws Exception {
		String hql = "select o from Invoice o where o.statementOfWork.id like :sowId ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowId", sowId);
		return (Boolean) ((super.getList(hql, parameters)).size() > 0);
	}

	public List<InvoiceHist> findByKeyHist(Long id) throws Exception {
		String hql = "select o from InvoiceHist o where o.id.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<InvoiceHist>) super.getList(hql, parameters);
	}
}
