package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.billing.entity.Accounts;
import haj.com.billing.entity.AcctTransactions;
import haj.com.billing.entity.EventCost;
import haj.com.entity.Users;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class AccountsDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Accounts> allAccounts() throws Exception {
		return (List<Accounts>)super.getList("select o from Accounts o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<Accounts> byField(long key) throws Exception  {
		String hql = "select o from PageText o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<Accounts>)super.getList(hql, parameters);
	}
*/

	public Accounts findByKey(Long id) throws Exception {
	 	String hql = "select o from Accounts o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Accounts)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Accounts> findByName(String desc) throws Exception {
	 	String hql = "select o from Accounts o where o.description like :desc order by o.description " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("desc", ""+desc.trim()+"%");
		return (List<Accounts>)super.getList(hql, parameters);
	}
	
	

	@SuppressWarnings("unchecked")
	public List<AcctTransactions> findAccountTransactions(Long accountId) throws Exception {
	 	String hql = "select o from AcctTransactions o where o.accounts.id = :accountId order by o.tranDate desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("accountId", accountId);
		return (List<AcctTransactions>)super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<EventCost> allEventCost() throws Exception {
	  return ( List<EventCost>)super.getList("select o from EventCost o where o.billingEvent.eventType = 'T' order by o.units");
	}

	public EventCost findEventCostByKey(Long id) {
	 	String hql = "select o from EventCost o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (EventCost)super.getUniqueResult(hql, parameters);
	
	}
	
	@SuppressWarnings("unchecked")
	public List<EventCost> subscriptionEventCost(Integer id) throws Exception {
	String hql = "select o from EventCost o , BillingEvents b , SubscriptionPackagesBillingEvents s " + 
			"where o.billingEvent.id = b.id " + 
			"and  b.id = s.billingEvents.id " + 
			"and s.subscriptionPackages.id = :id";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return  ( List<EventCost>)super.getList(hql, parameters);
	}
	

	@SuppressWarnings("unchecked")
	public List<AcctTransactions> acctTransactions(Long id) throws Exception {
	String hql = "select o from AcctTransactions o  " + 
			"where o.accounts.id = :id order by tranDate desc ";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return  ( List<AcctTransactions>)super.getList(hql, parameters);
	}

	
	@SuppressWarnings("unchecked")
	public List<Users> nightlyNewAccounts() throws Exception {
		return (List<Users>)super.getList("select o from Users o where o.accounts.deductDate is null and o.accounts.unitBalance > 0 order by o.accounts.id asc");
	}

	@SuppressWarnings("unchecked")
	public List<Users> nightlyExistingAccounts() throws Exception {
		return (List<Users>)super.getList("select o from Users o where o.accounts.deductDate is not null and o.accounts.unitBalance > 0 order by o.accounts.id asc");
	}
	
}

