package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Visitor;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class VisitorDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    Visitor
 * @return a List of Visitor objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<Visitor> allVisitor() throws Exception {
		return (List<Visitor>)super.getList("select o from Visitor o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<Visitor> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from Visitor o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<Visitor>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<Visitor> byField(long key) throws Exception  {
		String hql = "select o from Visitor o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<Visitor>)super.getList(hql, parameters);
	}
*/

	public Visitor findByKey(Long id) throws Exception {
	 	String hql = "select o from Visitor o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Visitor)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Visitor> findByName(String description) throws Exception {
	 	String hql = "select o from Visitor o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<Visitor>)super.getList(hql, parameters);
	}
}

