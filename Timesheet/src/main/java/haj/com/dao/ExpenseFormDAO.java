package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Company;
import haj.com.entity.ExpenseForm;
import haj.com.entity.Projects;
import haj.com.entity.Users;
import haj.com.entity.enums.ExpenseFormTypeEnum;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ExpenseFormDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see Company
	 * @return a List of Company objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Company> allCompany() throws Exception {
		return (List<Company>) super.getList("select o from Company o");
	}

	@SuppressWarnings("unchecked")
	public List<Projects> projectsByCompany(Company company) throws Exception {
		String hql = "select o from Projects o where o.company.id = :companyID";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("companyID", company.getId());
		return (List<Projects>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ExpenseForm> findByUserProjectType(Users user, Projects project, ExpenseFormTypeEnum expenseFormType) throws Exception {
		String hql = "select o from ExpenseForm o where o.user.id = :user" 
						+ " and o.project.id = :project"		
						+ " and o.expenseFormType = :expenseFormType and o.status is null";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("user", user.getUid());
		parameters.put("project", project.getId());
		parameters.put("expenseFormType", expenseFormType);
		return (List<ExpenseForm>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<ExpenseForm> findByUser(Users user) throws Exception {
		String hql = "select o from ExpenseForm o where o.user.id = :user";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("user", user.getUid());
		return (List<ExpenseForm>) super.getList(hql, parameters);
	}
	
	

	/*
	 * @SuppressWarnings("unchecked") public List<Company> findByCompany(Integer
	 * companyId) throws Exception { String hql =
	 * "select o from Company o where o.company.id = :companyId" ; Map<String,
	 * Object> parameters = new Hashtable<String, Object>();
	 * parameters.put("companyId", companyId); return
	 * (List<Company>)super.getList(hql, parameters); }
	 * 
	 * 
	 * @SuppressWarnings("unchecked") public List<Company> byField(long key) throws
	 * Exception { String hql = "select o from Company o where o.key = :key";
	 * Map<String, Object> parameters = new Hashtable<String, Object>();
	 * parameters.put("key", key); return (List<Company>)super.getList(hql,
	 * parameters); }
	 */
}
