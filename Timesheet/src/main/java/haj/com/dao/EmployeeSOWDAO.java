package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.EmployeeSOW;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class EmployeeSOWDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see EmployeeSOW
	 * @return a List of EmployeeSOW objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<EmployeeSOW> allEmployeeSOW() throws Exception {
		return (List<EmployeeSOW>) super.getList("select o from EmployeeSOW o");
	}

	public EmployeeSOW findByKey(Long id) throws Exception {
		String hql = "select o from EmployeeSOW o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (EmployeeSOW) super.getUniqueResult(hql, parameters);
	}

	public EmployeeSOW findByUsers(Long id) throws Exception {
		String hql = "select o from EmployeeSOW o where o.employee.uid = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (EmployeeSOW) super.getUniqueResult(hql, parameters);
	}

	public EmployeeSOW findByUsersAndSOW(Long uid, Long sowId) throws Exception {
		String hql = "select o from EmployeeSOW o where o.employee.uid = :uid and o.sow.id = :sowId ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uid", uid);
		parameters.put("sowId", sowId);
		return (EmployeeSOW) super.getUniqueResult(hql, parameters);
	}
	
	public EmployeeSOW findByUsersAndSOWInvoiceNull(Long uid, Long sowId) throws Exception {
		String hql = "select o from EmployeeSOW o where o.employee.uid = :uid and o.sow.id = :sowId and o.invoice.id is null ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uid", uid);
		parameters.put("sowId", sowId);
		return (EmployeeSOW) super.getUniqueResult(hql, parameters);
	}

	public List<EmployeeSOW> findByUsersAndSOWList(Long uid, Long sowId) throws Exception {
		String hql = "select o from EmployeeSOW o where o.employee.uid = :uid and o.sow.id = :sowId ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uid", uid);
		parameters.put("sowId", sowId);
		return (List<EmployeeSOW>) super.getList(hql, parameters);
	}
	
	public Boolean findByUsersAndSOWListB(Long uid, Long sowId) throws Exception {
		String hql = "select o from EmployeeSOW o where o.employee.uid = :uid and o.sow.id = :sowId ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uid", uid);
		parameters.put("sowId", sowId);
		return (Boolean)( super.getList(hql, parameters).size() > 0);
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeSOW> findBySOWandInvoiceNull(Long id) throws Exception {
		String hql = "select o from EmployeeSOW o where o.sow.id = :id and o.invoice.id is null";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<EmployeeSOW>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeSOW> findBySOW(Long sowId) throws Exception {
		String hql = "select o from EmployeeSOW o where o.sow.id = :sowId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowId", sowId);
		return (List<EmployeeSOW>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeSOW> findBySOWInvoice(Long sowId, Long invoiceId) throws Exception {
		String hql = "select o from EmployeeSOW o where o.sow.id = :sowId and o.invoice.id = :invoiceId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowId", sowId);
		parameters.put("invoiceId", invoiceId);
		return (List<EmployeeSOW>) super.getList(hql, parameters);
	}

}
