package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.ManagementStandard;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ManagementStandardDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    ManagementStandard
 * @return a List of ManagementStandard objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<ManagementStandard> allManagementStandard() throws Exception {
		return (List<ManagementStandard>)super.getList("select o from ManagementStandard o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<ManagementStandard> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from ManagementStandard o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<ManagementStandard>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<ManagementStandard> byField(long key) throws Exception  {
		String hql = "select o from ManagementStandard o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<ManagementStandard>)super.getList(hql, parameters);
	}
*/

	public ManagementStandard findByKey(Long id) throws Exception {
	 	String hql = "select o from ManagementStandard o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (ManagementStandard)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<ManagementStandard> findByName(String description) throws Exception {
	 	String hql = "select o from ManagementStandard o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<ManagementStandard>)super.getList(hql, parameters);
	}
}

