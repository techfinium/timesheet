package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.VisitorDeclarationForm;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class VisitorDeclarationFormDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    VisitorDeclarationForm
 * @return a List of VisitorDeclarationForm objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<VisitorDeclarationForm> allVisitorDeclarationForm() throws Exception {
		return (List<VisitorDeclarationForm>)super.getList("select o from VisitorDeclarationForm o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<VisitorDeclarationForm> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from VisitorDeclarationForm o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<VisitorDeclarationForm>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<VisitorDeclarationForm> byField(long key) throws Exception  {
		String hql = "select o from VisitorDeclarationForm o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<VisitorDeclarationForm>)super.getList(hql, parameters);
	}
*/

	public VisitorDeclarationForm findByKey(Long id) throws Exception {
	 	String hql = "select o from VisitorDeclarationForm o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (VisitorDeclarationForm)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<VisitorDeclarationForm> findByName(String description) throws Exception {
	 	String hql = "select o from VisitorDeclarationForm o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<VisitorDeclarationForm>)super.getList(hql, parameters);
	}
}

