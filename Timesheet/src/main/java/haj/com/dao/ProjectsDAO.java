package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Projects;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class ProjectsDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Projects> allProjects() throws Exception {
		return (List<Projects>)super.getList("select o from Projects o");
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> allProjectsActive() throws Exception {
		return (List<Projects>)super.getList("select o from Projects o where o.active = true");
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> allProjectsInActive() throws Exception {
		return (List<Projects>)super.getList("select o from Projects o where o.active = false");
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> allProjectsOverhead() throws Exception {
		return (List<Projects>)super.getList("select o from Projects o where o.overhead = true");
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> allProjectsNotOverhead() throws Exception {
		return (List<Projects>)super.getList("select o from Projects o where o.overhead = false");
	}

/*

	
	@SuppressWarnings("unchecked")
	public List<Projects> byCompany(long companyId) throws Exception  {
		String hql = "select o from Projects o where o.company.id = :companyId";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
	    return (List<Projects>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> byField(long id) throws Exception  {
		String hql = "select o from Projects o where o.id = :id";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
	    return (List<Projects>)super.getList(hql, parameters);
	}
*/

	public Projects findByKey(Long id) throws Exception {
	 	String hql = "select o from Projects o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Projects)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Projects> findByName(String description) throws Exception {
	 	String hql = "select o from Projects where o.description like  :description order by o.description " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<Projects>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> findByCodeName(String code) throws Exception {
	 	String hql = "select o from Projects o where o.code =  :code" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("code", code.trim());
		return (List<Projects>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> findLikeCodeName(String code) throws Exception {
	 	String hql = "select o from Projects o where o.code like  :code" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("code", code.trim() + "%" );
		return (List<Projects>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> findByCodeNameCompany(String code, Long companyId) throws Exception {
	 	String hql = "select o from Projects o where o.code =  :code and o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("code", code.trim());
	    parameters.put("companyId", companyId);
		return (List<Projects>)super.getList(hql, parameters);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Projects> findbyCompany(Long companyId) throws Exception {
	 	String hql = "select o from Projects o where o.company.id = :companyId and o.active = true" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<Projects>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> findbyCompanyReport(Long companyId) throws Exception {
	 	String hql = "select o from Projects o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<Projects>)super.getList(hql, parameters);
	}
}

