package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Doc;
import haj.com.entity.DocByte;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class DocDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	

	
	

	
	@SuppressWarnings("unchecked")
	public List<Doc> search(Long id) throws Exception {
	 	String hql = "select o from Doc o  where o.id = :id ";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (List<Doc>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Doc> searchParent(Long id) throws Exception {
	 	String hql = "select o from Doc o  where o.doc.id = :id ";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (List<Doc>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Doc> getVersions(Doc d) throws Exception {
	 	String hql = "select o from Doc o  where o.doc.id = :id order by o.versionNo desc";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", d.getId());
		return (List<Doc>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Doc> findByUser(Long userId) throws Exception {
	 	String hql = "select o from Doc o where o.user.uid = :userId ";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("userId", userId);
		return (List<Doc>)super.getList(hql, parameters);
	}
	
	public DocByte findDocByteByKey(Long id) throws Exception {
		String hql = "select o from DocByte o join fetch o.doc dc where o.id = :id  ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (DocByte) super.getUniqueResult(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Doc> searchByTargetKeyAndClass(String targetClass, Long targetKey) throws Exception {
		String hql = "select o from Doc o where o.targetClass = :targetClass and o.targetKey = :targetKey and o.doc is null";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("targetClass", targetClass);
		parameters.put("targetKey", targetKey);
		return (List<Doc>) super.getList(hql, parameters);
	}

}

