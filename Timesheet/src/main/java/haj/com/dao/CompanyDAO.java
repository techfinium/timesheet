package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Company;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class CompanyDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Company> allCompany() throws Exception {
		return (List<Company>)super.getList("select o from Company o");
	}
	
	@SuppressWarnings("unchecked")
	public List<Company> allCompanyActive() throws Exception {
		return (List<Company>)super.getList("select o from Company o where o.active = true");
	}
	
	@SuppressWarnings("unchecked")
	public List<Company> allCompanyInActive() throws Exception {
		return (List<Company>)super.getList("select o from Company o where o.active = false");
	}
	
	@SuppressWarnings("unchecked")
	public List<Company> findbyCompany(Long companyId) throws Exception {
	 	String hql = "select o from Company o where o.company.id = :companyId and o.active = true" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<Company>)super.getList(hql, parameters);
	}
/*

	
	@SuppressWarnings("unchecked")
	public List<Company> byCompany(long companyId) throws Exception  {
		String hql = "select o from Company o where o.company.id = :companyId";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
	    return (List<Company>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Company> byField(long id) throws Exception  {
		String hql = "select o from Company o where o.id = :id";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
	    return (List<Company>)super.getList(hql, parameters);
	}
*/

	public Company findByKey(Long id) throws Exception {
	 	String hql = "select o from Company o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Company)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Company> findByName(String description) throws Exception {
	 	String hql = "select o from Company where o.description like  :description order by o.description " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<Company>)super.getList(hql, parameters);
	}
}

