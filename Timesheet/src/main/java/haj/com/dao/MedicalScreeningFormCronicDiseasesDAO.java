package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.MedicalScreeningFormCronicDiseases;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class MedicalScreeningFormCronicDiseasesDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    MedicalScreeningFormCronicDiseases
 * @return a List of MedicalScreeningFormCronicDiseases objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<MedicalScreeningFormCronicDiseases> allMedicalScreeningFormCronicDiseases() throws Exception {
		return (List<MedicalScreeningFormCronicDiseases>)super.getList("select o from MedicalScreeningFormCronicDiseases o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<MedicalScreeningFormCronicDiseases> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from MedicalScreeningFormCronicDiseases o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<MedicalScreeningFormCronicDiseases>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<MedicalScreeningFormCronicDiseases> byField(long key) throws Exception  {
		String hql = "select o from MedicalScreeningFormCronicDiseases o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<MedicalScreeningFormCronicDiseases>)super.getList(hql, parameters);
	}
*/

	public MedicalScreeningFormCronicDiseases findByKey(Long id) throws Exception {
	 	String hql = "select o from MedicalScreeningFormCronicDiseases o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (MedicalScreeningFormCronicDiseases)super.getUniqueResult(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<MedicalScreeningFormCronicDiseases> findByMedicalScreeningFormId(Long id) throws Exception {
		String hql = "select o from MedicalScreeningFormCronicDiseases o where o.medicalScreeningForm.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (List<MedicalScreeningFormCronicDiseases>)super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<MedicalScreeningFormCronicDiseases> findByName(String description) throws Exception {
	 	String hql = "select o from MedicalScreeningFormCronicDiseases o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<MedicalScreeningFormCronicDiseases>)super.getList(hql, parameters);
	}
}

