package haj.com.dao;

import java.util.List;

import haj.com.entity.HAJProperties;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class HAJPropertiesDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<HAJProperties> allProps() throws Exception {
		return (List<HAJProperties>)getList("select o from HAJProperties o");
	}
}
