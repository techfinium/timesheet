package haj.com.dao;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.ibm.icu.text.SimpleDateFormat;

import haj.com.bean.ProjectUsersReportBean;
import haj.com.entity.CompanyUsers;
import haj.com.entity.TimesheetDetails;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class TimesheetDetailsDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<TimesheetDetails> allTimesheetDetails() throws Exception {
		return (List<TimesheetDetails>) super.getList("select o from TimesheetDetails o");
	}

	/*
	 * @SuppressWarnings("unchecked") public List<TimesheetDetails>
	 * byCompany(long companyId) throws Exception { String hql =
	 * "select o from TimesheetDetails o where o.company.id = :companyId";
	 * Map<String, Object> parameters = new Hashtable<String, Object>();
	 * parameters.put("companyId", companyId); return
	 * (List<TimesheetDetails>)super.getList(hql, parameters); }
	 * 
	 * @SuppressWarnings("unchecked") public List<TimesheetDetails> byField(long
	 * id) throws Exception { String hql =
	 * "select o from TimesheetDetails o where o.id = :id"; Map<String, Object>
	 * parameters = new Hashtable<String, Object>(); parameters.put("id", id);
	 * return (List<TimesheetDetails>)super.getList(hql, parameters); }
	 */

	public TimesheetDetails findByKey(Long id) throws Exception {
		String hql = "select o from TimesheetDetails o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (TimesheetDetails) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<TimesheetDetails> findByName(String description) throws Exception {
		String hql = "select o from TimesheetDetails where o.description like  :description order by o.description ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<TimesheetDetails>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<TimesheetDetails> findByTimesheet(Long timesheetId) throws Exception {
		String hql = "select o from TimesheetDetails o where o.timesheet.id = :timesheetId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("timesheetId", timesheetId);
		return (List<TimesheetDetails>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<TimesheetDetails> findByTimesheetBetweenDates(Long timesheetId, Date startDate, Date endDate) throws Exception {
		String hql = "select o from TimesheetDetails o where o.timesheet.id = :timesheetId and date(o.fromDateTime)"
				+ " >= date(:startDate) and date(o.toDateTime) <= date(:endDate) order by o.toDateTime desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("timesheetId", timesheetId);
		parameters.put("startDate", startDate);
		parameters.put("endDate", endDate);
		return (List<TimesheetDetails>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<TimesheetDetails> findByTimesheetBetweenDatesUser(CompanyUsers user, Date startDate, Date endDate) throws Exception {
		String hql = "select o from TimesheetDetails o where o.timesheet.companyUsers = :user and date(o.fromDateTime)"
				+ " >= date(:startDate) and date(o.toDateTime) <= date(:endDate) order by o.toDateTime desc";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("user", user);
		parameters.put("startDate", startDate);
		parameters.put("endDate", endDate);
		return (List<TimesheetDetails>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<TimesheetDetails> findByTimesheetDetail(Long timesheetDetailId) throws Exception {
		String hql = "select o from TimesheetDetails o where  o.timesheetDetails is not null and o.timesheetDetails.id = :timesheetDetailId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("timesheetDetailId", timesheetDetailId);
		return (List<TimesheetDetails>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<TimesheetDetails> findByTimesheetDetailBetweenDatesAndUser(Long userId, Date fromDate, Date toDate) throws Exception {
		String hql = "select o from TimesheetDetails o where o.user.uid = :userId and Date(o.timesheetDetails.toDateTime) between Date(:fromDate) and Date(:toDate)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);
		return (List<TimesheetDetails>) super.getList(hql, parameters);
	}
	
	public List<ProjectUsersReportBean> locateUsersProjectsReportByDateRange(Date fromDate, Date toDate) throws Exception {	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sql = "select " + 
				"	u.first_name as firstName " + 
				"	, u.last_name as lastName " +
				"	, concat(u.first_name, ' ' ,u.last_name) as fullName " +
				"	, tmd.projects_id as projectID " +
				"	, p.code as projectCode " +
				"	, p.description as projectDescription " + 
				"	, sum(tmd.hours) as totalHours " +
				"	, sum(tmd.minutes) as totalMinutes " + 
				"from timesheet_details tmd " + 
				"left join projects p on p.id = tmd.projects_id " + 
				"left join timesheet_details t on t.id = tmd.timesheet_details_id " + 
				"left join timesheet tm on tm.id = t.timesheet_id " + 
				"left join company_users cu on cu.id = tm.company_users_id " + 
				"left join users u on u.uid = cu.user_id " + 
				"where tmd.projects_id is not null " + 
				"and (t.from_date_time BETWEEN '"+sdf.format(fromDate)+"' AND '"+sdf.format(toDate)+"') " + 
				"group by u.first_name, u.last_name, tmd.projects_id, p.description";
		return (List<ProjectUsersReportBean>) super.nativeSelectSqlList(sql, ProjectUsersReportBean.class, null);
	}
}
