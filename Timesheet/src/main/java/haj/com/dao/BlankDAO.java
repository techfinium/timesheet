package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Blank;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class BlankDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    Blank
 * @return a List of Blank objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<Blank> allBlank() throws Exception {
		return (List<Blank>)super.getList("select o from Blank o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<Blank> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from Blank o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<Blank>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<Blank> byField(long key) throws Exception  {
		String hql = "select o from Blank o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<Blank>)super.getList(hql, parameters);
	}
*/

	public Blank findByKey(Long id) throws Exception {
	 	String hql = "select o from Blank o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Blank)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Blank> findByName(String description) throws Exception {
	 	String hql = "select o from Blank o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<Blank>)super.getList(hql, parameters);
	}
}

