package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Address;
import haj.com.entity.AddressTypeEnum;
import haj.com.entity.Users;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class AddressDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
	@SuppressWarnings("unchecked")
	public List<Address> allAddress() throws Exception {
		return (List<Address>)super.getList("select o from Address o");
	}
	

	@SuppressWarnings("unchecked")
	public List<Address> byUserId(Long uid) throws Exception  {
		String hql = "select o from Address o , ParentChildHasAddress p where o.idaddress = p.address.idaddress and p.users.uid = :uid";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("uid", uid);
	    return (List<Address>)super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Address> byChildId(Long idchild) throws Exception  {
		String hql = "select o from Address o , ParentChildHasAddress p where o.idaddress = p.address.idaddress and p.child.idchild = :idchild";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("idchild", idchild);
	    return (List<Address>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Address> byUserCheck(Long uid) throws Exception  {
		String hql = "select o from Address o where o.user.uid = :uid";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("uid", uid);
	    return (List<Address>)super.getList(hql, parameters);
	}

	public Address byUser(Long uid) throws Exception  {
		String hql = "select o from Address o where o.user.uid = :uid";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("uid", uid);
	    return (Address)super.getUniqueResult(hql, parameters);
	}
	
	public Address findByHostingCompanyId(long id) throws Exception {
		String hql = "select o from Address o  left join fetch o.hostingCompany left join fetch o.municipality h where o.hostingCompany.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (Address) super.getUniqueResult(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public Address findByTagetClassTagerKey(String targetClass, Long targetKey) throws Exception {
		String hql = "select o from Address o where o.targetClass = :targetClass and o.targetKey = :targetKey" ;
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("targetClass", targetClass);
		parameters.put("targetKey", targetKey);
		return (Address) super.getUniqueResult(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public Address findOneByTagetClassTagerKey(String targetClass, Long targetKey) throws Exception {
		String hql = "select o from Address o where o.targetClass = :targetClass and o.targetKey = :targetKey" ;
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("targetClass", targetClass);
		parameters.put("targetKey", targetKey);
		return (Address) super.getUniqueResult(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public Address findByTagetClassTagerKeyAndAddressType(String targetClass, Long targetKey, AddressTypeEnum addressTypeEnum) throws Exception {
		String hql = "select o from Address o where o.targetClass = :targetClass and o.targetKey = :targetKey and o.addrType = :addrType" ;
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("targetClass", targetClass);
		parameters.put("targetKey", targetKey);
		parameters.put("addrType", addressTypeEnum);
		return (Address) super.getUniqueResult(hql, parameters);
	}

}

