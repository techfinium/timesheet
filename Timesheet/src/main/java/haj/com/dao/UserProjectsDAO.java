package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Projects;
import haj.com.entity.UserProjects;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class UserProjectsDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}


	/**
	 * Get all UserProjects
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @return List<UserProjects>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<UserProjects> allUserProjects() throws Exception {
		return (List<UserProjects>)super.getList("select o from UserProjects o");
	}


	@SuppressWarnings("unchecked")
	public List<UserProjects> allUserProjects(Long from, int noRows) throws Exception {
	 	String hql = "select o from UserProjects o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<UserProjects>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @return UserProjects 
 	 * @throws Exception
 	 */
	public UserProjects findByKey(Long id) throws Exception {
	 	String hql = "select o from UserProjects o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (UserProjects)super.getUniqueResult(hql, parameters);
	}
	
	/**
	 * Find object by project id
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @return List<UserProjects> 
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<UserProjects> findByProject(Long id) throws Exception {
	 	String hql = "select o from UserProjects o where o.project.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (List<UserProjects>)super.getList(hql, parameters);
	}

	/**
	 * Find UserProjects by description
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @return List<UserProjects>
 	 * @throws Exception
 	 */
	@SuppressWarnings("unchecked")
	public List<UserProjects> findByName(String description) throws Exception {
	 	String hql = "select o from UserProjects o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<UserProjects>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Projects> findProjectsForUser(Long id) throws Exception {
	 	String hql = "select o.project from UserProjects o where o.user.uid = :id and o.project.active = :access and o.accessToProject = :access and o.project.company.active = :access" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
	    parameters.put("access", Boolean.TRUE);
		return (List<Projects>)super.getList(hql, parameters);
	}
}

