package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.CompanyUsers;
import haj.com.entity.Users;
import haj.com.entity.UsersLevelEnum;
import haj.com.entity.UsersStatusEnum;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class UsersDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Users> allUsers() throws Exception {
		return (List<Users>) super.getList("select o from Users o");
	}

	public Users getUserByEmail(String email) throws Exception {
		String hql = "select o from Users o where trim(o.email) = :email ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("email", email.trim());
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public boolean emailAvailable(String email) throws Exception {
		boolean available = false;
		List<Users> l = getAllUserByEmail(email);
		if (l == null || l.size() == 0)
			available = true;
		return available;
	}

	@SuppressWarnings("unchecked")
	public List<Users> getAllUserByEmail(String email) throws Exception {
		String hql = "select o from Users o where upper(o.email) = :email ";
		// + "and (o.status = " + UsersStatusEnum.Active.ordinal() + ")";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("email", email.toUpperCase());
		return (List<Users>) super.getList(hql, parameters);
	}

	public Users getUserByUsername(String username) throws Exception {
		String hql = "select o from Users o where upper(o.username) = :username ";
		// + "and (o.status = " + UsersStatusEnum.Active.ordinal() + ")";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("username", username.toUpperCase());
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users getUserByRSAid(String idNumber) throws Exception {
		String hql = "select o from Users o where o.idNumber = :idNumber";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("idNumber", idNumber);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users getUserByRSAid(String idNumber, Long uid) throws Exception {
		String hql = "select o from Users o where o.idNumber = :idNumber and o.uid <> :uid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("idNumber", idNumber);
		parameters.put("uid", uid);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users checkEmailUsed(String email, Long uid) throws Exception {
		String hql = "select o from Users o where upper (o.email) = :email and o.uid <> :uid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("email", email);
		parameters.put("uid", uid);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users checkUsernameUsed(String username, Long uid) throws Exception {
		String hql = "select o from Users o where upper(o.username) = :username and o.uid <> :uid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("username", username);
		parameters.put("uid", uid);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users findUserByEmailGUID(String emailGuid) throws Exception {
		String hql = "select o from Users o where o.emailGuid = :emailGuid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("emailGuid", emailGuid);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users findByKeyl(Long uid) throws Exception {
		String hql = "select o from Users o where o.uid = :uid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uid", uid);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	public Users findByKey(Long uid) throws Exception {
		String hql = "select o from Users o where o.uid = :uid";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("uid", uid);
		return (Users) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CompanyUsers> findCompanyUsers(Long companyId) throws Exception {
		String hql = "select f from CompanyUsers f " + "where f.company.id = :companyId ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("companyId", companyId);
		return (List<CompanyUsers>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> kidsParents(Long idchild) throws Exception {
		String hql = "select c from Users c , Family f " + "where f.child.idchild = :idchild "
				+ "and f.users.uid = c.uid ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("idchild", idchild);
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> searchByName(String surname, String email) throws Exception {
		String hql = "select o from Users o where o.lastName like :surname and o.email like :email";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("surname", surname);
		parameters.put("email", email);
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> getAllCompanyAdministrors(Long companyId) throws Exception {
		String hql = "select o.users from CompanyUsers o where o.company.id = :companyId and level = :level";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("companyId", companyId);
		parameters.put("level", UsersLevelEnum.Manager.ordinal());
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> findByNameAndCompany(String name, Long companyId) {
		String hql = "select distinct(o) from Users o " + "where (o.lastName like :name or o.firstName like :name) "
				+ "and o.uid not in " + " (select x.users.uid from CompanyUsers x where x.company.id = :companyId)"
				+ "and o.uid not in (select j.users.uid from CompanyUsers j where j.level = :adminLevel)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("name", "%" + name.trim() + "%");
		parameters.put("companyId", companyId);
		parameters.put("adminLevel", UsersLevelEnum.Admin);
		return (List<Users>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Users> findByNameAndCompany(String name) {
		String hql = "select distinct(o) from Users o " + "where (o.lastName like :name or o.firstName like :name) "
				+ "and o.uid not in (select j.users.uid from CompanyUsers j where j.level = :adminLevel)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("name", "%" + name.trim() + "%");
		parameters.put("adminLevel", UsersLevelEnum.Admin);
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> findByNameAndProject(String name, Long projectId) {
		String hql = "select distinct(o) from Users o " + "where (o.lastName like :name or o.firstName like :name) "
				+ "and o.uid not in " + " (select x.user.uid from UserProjects x where x.project.id = :projectId) "
				+ "and o.uid not in (select j.users.uid from CompanyUsers j where j.level = :adminLevel)"
				+ "and o.status = :status ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("name", "%" + name.trim() + "%");
		parameters.put("projectId", projectId);
		parameters.put("adminLevel", UsersLevelEnum.Admin);
		parameters.put("status", UsersStatusEnum.Active);
		return (List<Users>) super.getList(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Users> findByProject(Long projectId) {
		String hql = "select distinct(o) from Users o where o.uid in (select x.user.uid from UserProjects x where x.project.id = :projectId)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		return (List<Users>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Users> findByDirector() {
		String hql = "select distinct(o) from Users o where o.uid in (select j.users.uid from CompanyUsers j where j.level = :directorLevel)";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("directorLevel", UsersLevelEnum.Director);
		return (List<Users>) super.getList(hql, parameters);
	}
}
