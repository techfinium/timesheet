package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.StatementOfWork;
import haj.com.entity.StatementOfWorkHist;
import haj.com.entity.enums.SowStatusEnum;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;


public class StatementOfWorkDAO extends AbstractDAO {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * @author JMRUK
	 * @see StatementOfWork
	 * @return a List of StatementOfWork objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<StatementOfWork> allStatementOfWork() throws Exception {
		return (List<StatementOfWork>) super.getList("select o from StatementOfWork o");
	}

	
	/**
	 * @author JMRUK
	 * @see StatementOfWorkHist
	 * @return a List of StatementOfWorkHist objects
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<StatementOfWorkHist> allStatementOfWorkHist() throws Exception {
		return (List<StatementOfWorkHist>) super.getList("select o from StatementOfWorkHist o");
	}
	
	public List<StatementOfWorkHist> findByKeyHist(Long id) throws Exception {
		String hql = "select o from StatementOfWorkHist o where o.id.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (List<StatementOfWorkHist>) super.getList(hql, parameters);
	}
	/*
	 * @SuppressWarnings("unchecked") public List<Blank> findByCompany(Integer
	 * companyId) throws Exception { String hql =
	 * "select o from Blank o where o.company.id = :companyId" ; Map<String,
	 * Object> parameters = new Hashtable<String, Object>();
	 * parameters.put("companyId", companyId); return
	 * (List<Blank>)super.getList(hql, parameters); }
	 * 
	 * 
	 * @SuppressWarnings("unchecked") public List<Blank> byField(long key)
	 * throws Exception { String hql =
	 * "select o from Blank o where o.key = :key"; Map<String, Object>
	 * parameters = new Hashtable<String, Object>(); parameters.put("key", key);
	 * return (List<Blank>)super.getList(hql, parameters); }
	 */

	public StatementOfWork findByKey(Long id) throws Exception {
		String hql = "select o from StatementOfWork o where o.id = :id ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("id", id);
		return (StatementOfWork) super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<StatementOfWork> findByName(String description) throws Exception {
		String hql = "select o from StatementOfWork o where o.description like  :description order by o.desc ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("description", "" + description.trim() + "%");
		return (List<StatementOfWork>) super.getList(hql, parameters);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<StatementOfWork> findByNumerOrDescription(String description) throws Exception {
		String hql = "select o from StatementOfWork o where( cast(o.sowNumber as string)  like :sowNumber)" +
					 " or (o.title like :title) ";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("sowNumber", "%"+description.trim() + "%" );
		parameters.put("title",  "%"+description.trim() + "%");
		return (List<StatementOfWork>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<StatementOfWork> findByProjectActive(Long projectId) throws Exception {
		String hql = "select distinct(o) from StatementOfWork o where o.projects.id = :projectId and o.sowStatusEnum = :activeEnum";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("projectId", projectId);
		parameters.put("activeEnum", SowStatusEnum.Active);
		return (List<StatementOfWork>) super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public Boolean findByProjectActiveUserAccess(Long sowId, Long userId) throws Exception {
		String hql = "select o from EmployeeSOW o where o.employee.uid = :userId and o.sow.id = :sowId";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("userId", userId);
		parameters.put("sowId", sowId);
		return (Boolean) (super.getList(hql, parameters).size() > 0);
	}
/*
	public void addOrEditObject(StatementOfWork sow) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.saveOrUpdate(sow);
		session.getTransaction().commit();
	}

	public void deleteById(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		StatementOfWork sow = (StatementOfWork) session.get(StatementOfWork.class, id);
		session.delete(sow);
		session.getTransaction().commit();
	}

	public void deleteObject(StatementOfWork sow) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.delete(sow);
		session.getTransaction().commit();
	}

	public StatementOfWork getObject(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		StatementOfWork sow = (StatementOfWork) session.get(StatementOfWork.class, id);
		session.getTransaction().commit();
		return sow;
	}
	*/
	
}
