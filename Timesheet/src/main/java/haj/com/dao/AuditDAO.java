package haj.com.dao;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import haj.com.entity.AuditEvent;
import haj.com.entity.AuditEventParam;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.framework.IDataEntity;
import haj.com.provider.MySQLProvider;



public class AuditDAO extends AbstractDAO
{
  private static final long serialVersionUID = 1L;

  @Override
  public AbstractDataProvider getDataProvider()
  {
    return new MySQLProvider();
  }
  
  @Override
  public void create(IDataEntity entity) throws Exception
  {
    if (entity instanceof AuditEvent)
    {
      Session session = getSession();
      Transaction transaction = null;
      try
      {
        transaction = session.beginTransaction();
        
        AuditEvent event = (AuditEvent)entity;
        //event.setEventId(getNextAuditEventSequence());
        event.setEventDate(new Date());
        create(event, session);
        
        if (event.getParamList()!=null)
          for (AuditEventParam param : event.getParamList())
          {
            param.setEvent(event);
            //param.setEventParamId(getNextAuditEventParamSequence());
            create(param, session);
          }
        
        transaction.commit();
      }
      catch (Exception e)
      {
        if (transaction != null)
          transaction.rollback();
        throw e;
      }
      finally
      {
        session.close();
      }
    }
    else 
      throw new UnsupportedOperationException();
  }
  
  @Override
  public void update(IDataEntity entity) throws Exception
  {
    throw new UnsupportedOperationException();
  }
  
  @Override
  public void delete(IDataEntity entity) throws Exception
  {
    throw new UnsupportedOperationException();
  }

  //public int getNextAuditEventSequence() throws Exception
  //{
  //  return (int)getNextFromSequence("audit_event_seq");
 // }
  
 // public int getNextAuditEventParamSequence() throws Exception
 // {
 //   return (int)getNextFromSequence("audit_event_param_seq");
 // }
  
}
