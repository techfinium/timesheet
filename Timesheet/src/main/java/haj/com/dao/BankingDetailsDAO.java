package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.BankingDetails;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class BankingDetailsDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * Get all BankingDetails
 	 * @author TechFinium 
 	 * @see    BankingDetails
 	 * @return a list of {@link haj.com.entity.BankingDetails}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<BankingDetails> allBankingDetails() throws Exception {
		return (List<BankingDetails>)super.getList("select o from BankingDetails o");
	}

	/**
	 * Get all BankingDetails between from and noRows
 	 * @author TechFinium 
 	 * @param from the from
 	 * @param noRows the no rows
 	 * @see    BankingDetails
 	 * @return a list of {@link haj.com.entity.BankingDetails}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<BankingDetails> allBankingDetails(Long from, int noRows) throws Exception {
	 	String hql = "select o from BankingDetails o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<BankingDetails>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @param id the id
 	 * @see    BankingDetails
 	 * @return a {@link haj.com.entity.BankingDetails}
 	 * @throws Exception global exception
 	 */
	public BankingDetails findByKey(Long id) throws Exception {
	 	String hql = "select o from BankingDetails o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (BankingDetails)super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find BankingDetails by description
 	 * @author TechFinium 
 	 * @param description the description 
 	 * @see    BankingDetails
  	 * @return a list of {@link haj.com.entity.BankingDetails}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<BankingDetails> findByName(String description) throws Exception {
	 	String hql = "select o from BankingDetails o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<BankingDetails>)super.getList(hql, parameters);
	}
}

