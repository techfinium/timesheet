package haj.com.dao;

import java.util.Hashtable;
import java.util.Map;

import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class DashboardDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

  	public Long noClients(Long companyId) throws Exception {
	 	String hql = "select count(o) from Clients o where o.company.id = :companyId " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (Long)super.getUniqueResult(hql, parameters);
	}
	
  	public Long noQuestionaire(Long companyId) throws Exception {
	 	String hql = "select count(o) from Questionaire o where o.company.id = :companyId " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (Long)super.getUniqueResult(hql, parameters);
	}
  	
  	public Long noCompletedQuestionaire(Long companyId) throws Exception {
	 	String hql = "select count(o) from ClientsQuestionaire o "
	 			+ " where o.questionaire.company.id = :companyId and o.completed = true " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (Long)super.getUniqueResult(hql, parameters);
	}
  	
  	public Long noInPorgressQuestionaire(Long companyId) throws Exception {
	 	String hql = "select count(o) from ClientsQuestionaire o "
	 			+ " where o.questionaire.company.id = :companyId and o.completed = false and o.dateStarted is not null " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (Long)super.getUniqueResult(hql, parameters);
	}
}

