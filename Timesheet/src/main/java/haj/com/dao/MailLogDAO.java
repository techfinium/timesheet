package haj.com.dao;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.MailLog;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class MailLogDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<MailLog> allMailLog() throws Exception {
		return (List<MailLog>)super.getList("select o from MailLog o");
	}

	
	@SuppressWarnings("unchecked")
	public List<MailLog> allMailLog(Integer startingAt, int noRows) throws Exception {
		return (List<MailLog>)super.getList("select o from MailLog o order by o.createDate desc ",null,startingAt,noRows);
	}
	
	@SuppressWarnings("unchecked")
	public List<MailLog> findByEmail(String email,Integer startingAt, int noRows) throws Exception {
	 	String hql = "select o from MailLog o where o.email like  :email order by o.createDate desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("email", email);
		return (List<MailLog>)super.getList(hql, parameters,startingAt,noRows);
	}
	
	
	public Long countByEmail(String email) throws Exception {
	 	String hql = "select count(o) from MailLog o where o.email like  :email  " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("email", email);
		return (Long)super.getUniqueResult(hql,parameters);
	}
	
/*
	@SuppressWarnings("unchecked")
	public List<MailLog> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from MailLog o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<MailLog>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<MailLog> byField(long key) throws Exception  {
		String hql = "select o from MailLog o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<MailLog>)super.getList(hql, parameters);
	}
*/

	public MailLog findByKey(Long id) throws Exception {
	 	String hql = "select o from MailLog o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (MailLog)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<MailLog> findByName(String description) throws Exception {
	 	String hql = "select o from MailLog o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<MailLog>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<MailLog> findUser(Long userId) throws Exception {
	 	String hql = "select o from MailLog o where o.user.uid = :userId order by o.createDate desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("userId", userId);
		return (List<MailLog>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<MailLog> allMailLogBetweenDates(Date date, Date endDate) throws Exception {
		String hql = "select o from MailLog o where date(o.sendDate) between date(:date) and date(:endDate) order by o.user.id";
		Map<String, Object> parameters = new Hashtable<String, Object>();
		parameters.put("date", date);
		parameters.put("endDate", endDate);
		return (List<MailLog>) super.getList(hql, parameters);
	}
}

