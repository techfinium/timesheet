package haj.com.dao.lookup;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import haj.com.entity.lookup.EngineType;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

@Named
public class EngineTypeDAO extends AbstractDAO  {
	

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * Get all EngineType
 	 * @author TechFinium 
 	 * @see    EngineType
 	 * @return a list of {@link haj.com.entity.EngineType}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<EngineType> allEngineType() throws Exception {
		return (List<EngineType>)super.getList("select o from EngineType o");
	}

	/**
	 * Get all EngineType between from and noRows
 	 * @author TechFinium 
 	 * @param from the from
 	 * @param noRows the no rows
 	 * @see    EngineType
 	 * @return a list of {@link haj.com.entity.EngineType}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<EngineType> allEngineType(Long from, int noRows) throws Exception {
	 	String hql = "select o from EngineType o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<EngineType>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @param id the id
 	 * @see    EngineType
 	 * @return a {@link haj.com.entity.EngineType}
 	 * @throws Exception global exception
 	 */
	public EngineType findByKey(Long id) throws Exception {
	 	String hql = "select o from EngineType o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (EngineType)super.getUniqueResult(hql, parameters);
	}
	
	



}

