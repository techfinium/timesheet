package haj.com.dao.lookup;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import haj.com.entity.lookup.VehicleVariant;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

@Named
public class VehicleVariantDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * Get all VehicleVariant
 	 * @author TechFinium 
 	 * @see    VehicleVariant
 	 * @return a list of {@link haj.com.entity.VehicleVariant}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<VehicleVariant> allVehicleVariant() throws Exception {
		return (List<VehicleVariant>)super.getList("select o from VehicleVariant o");
	}

	/**
	 * Get all VehicleVariant between from and noRows
 	 * @author TechFinium 
 	 * @param from the from
 	 * @param noRows the no rows
 	 * @see    VehicleVariant
 	 * @return a list of {@link haj.com.entity.VehicleVariant}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<VehicleVariant> allVehicleVariant(Long from, int noRows) throws Exception {
	 	String hql = "select o from VehicleVariant o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<VehicleVariant>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @param id the id
 	 * @see    VehicleVariant
 	 * @return a {@link haj.com.entity.VehicleVariant}
 	 * @throws Exception global exception
 	 */
	public VehicleVariant findByKey(Long id) throws Exception {
	 	String hql = "select o from VehicleVariant o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (VehicleVariant)super.getUniqueResult(hql, parameters);
	}

	/**
	 * Find VehicleVariant by description
 	 * @author TechFinium 
 	 * @param description the description 
 	 * @see    VehicleVariant
  	 * @return a list of {@link haj.com.entity.VehicleVariant}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<VehicleVariant> findByName(String derivative) throws Exception {
	 	String hql = "select o from VehicleVariant o where o.derivative like :derivative"
	 			+ " or o.model like :derivative"
	 			+ " or o.make like :derivative"
	 			+ " order by o.derivative " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("derivative", ""+derivative.trim()+"%");
		return (List<VehicleVariant>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findByMake(String make) throws Exception {
	 	String hql = "select o.make from VehicleVariant o where o.make like :make group by o.make" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("make", ""+make.trim()+"%");
		return (List<String>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findByMakeAndModel(String model, String make) throws Exception {
	 	String hql = "select o.model from VehicleVariant o where o.model like :model and o.make = :make group by o.model" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("model", ""+model.trim()+"%");
	    parameters.put("make",make);
		return (List<String>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findDerivativeByMakeAndModel(String derivative, String model, String make) throws Exception {
	 	String hql = "select o.derivative from VehicleVariant o where o.derivative like :derivative and o.make = :make and o.model = :model group by o.derivative" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("derivative", ""+derivative.trim()+"%");
	    parameters.put("make",make);
	    parameters.put("model",model);
		return (List<String>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> findAllModelYears(Integer year, String make) throws Exception {
	 	String hql = "select o.year from VehicleVariant o where o.make = :make group by o.year order by o.year" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("make",make);
		return (List<Integer>)super.getList(hql, parameters);
	}
}

