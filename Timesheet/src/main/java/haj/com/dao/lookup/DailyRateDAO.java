package haj.com.dao.lookup;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import haj.com.entity.lookup.DailyRate;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

@Named
public class DailyRateDAO extends AbstractDAO  {
	

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * Get all DailyRate
 	 * @author TechFinium 
 	 * @see    DailyRate
 	 * @return a list of {@link haj.com.entity.DailyRate}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<DailyRate> allDailyRate() throws Exception {
		return (List<DailyRate>)super.getList("select o from DailyRate o");
	}

	/**
	 * Get all DailyRate between from and noRows
 	 * @author TechFinium 
 	 * @param from the from
 	 * @param noRows the no rows
 	 * @see    DailyRate
 	 * @return a list of {@link haj.com.entity.DailyRate}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<DailyRate> allDailyRate(Long from, int noRows) throws Exception {
	 	String hql = "select o from DailyRate o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<DailyRate>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @param id the id
 	 * @see    DailyRate
 	 * @return a {@link haj.com.entity.DailyRate}
 	 * @throws Exception global exception
 	 */
	public DailyRate findByKey(Long id) throws Exception {
	 	String hql = "select o from DailyRate o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (DailyRate)super.getUniqueResult(hql, parameters);
	}
	
	public DailyRate findMostRecent() throws Exception {
		String hql = "select o from DailyRate o order by id desc " ;
		return (DailyRate) super.getList(hql,1).get(0);
	}
	
	



}

