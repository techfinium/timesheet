package haj.com.dao.lookup;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.lookup.CronicDiseases;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class CronicDiseasesDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}
	
/**
 * @author JMRUK 
 * @see    CronicDiseases
 * @return a List of CronicDiseases objects
 * @throws Exception
 */
	@SuppressWarnings("unchecked")
	public List<CronicDiseases> allCronicDiseases() throws Exception {
		return (List<CronicDiseases>)super.getList("select o from CronicDiseases o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<CronicDiseases> findByCompany(Integer companyId) throws Exception {
	 	String hql = "select o from CronicDiseases o where o.company.id = :companyId" ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("companyId", companyId);
		return (List<CronicDiseases>)super.getList(hql, parameters);
	}


	@SuppressWarnings("unchecked")
	public List<CronicDiseases> byField(long key) throws Exception  {
		String hql = "select o from CronicDiseases o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<CronicDiseases>)super.getList(hql, parameters);
	}
*/

	public CronicDiseases findByKey(Long id) throws Exception {
	 	String hql = "select o from CronicDiseases o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (CronicDiseases)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<CronicDiseases> findByName(String description) throws Exception {
	 	String hql = "select o from CronicDiseases o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<CronicDiseases>)super.getList(hql, parameters);
	}
}

