package haj.com.dao.lookup;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import haj.com.entity.lookup.AllowanceRate;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

@Named
public class AllowanceRateDAO extends AbstractDAO  {
	

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	/**
	 * Get all AllowanceRate
 	 * @author TechFinium 
 	 * @see    AllowanceRate
 	 * @return a list of {@link haj.com.entity.AllowanceRate}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<AllowanceRate> allAllowanceRate() throws Exception {
		return (List<AllowanceRate>)super.getList("select o from AllowanceRate o");
	}

	/**
	 * Get all AllowanceRate between from and noRows
 	 * @author TechFinium 
 	 * @param from the from
 	 * @param noRows the no rows
 	 * @see    AllowanceRate
 	 * @return a list of {@link haj.com.entity.AllowanceRate}
 	 * @throws Exception global exception
 	 */
	@SuppressWarnings("unchecked")
	public List<AllowanceRate> allAllowanceRate(Long from, int noRows) throws Exception {
	 	String hql = "select o from AllowanceRate o " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    
		return (List<AllowanceRate>)super.getList(hql, parameters,from.intValue(),noRows);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @param id the id
 	 * @see    AllowanceRate
 	 * @return a {@link haj.com.entity.AllowanceRate}
 	 * @throws Exception global exception
 	 */
	public AllowanceRate findByKey(Long id) throws Exception {
	 	String hql = "select o from AllowanceRate o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (AllowanceRate)super.getUniqueResult(hql, parameters);
	}
	
	
	public AllowanceRate findMostRecent() throws Exception {
		String hql = "select o from AllowanceRate o order by id desc " ;
		return (AllowanceRate) super.getList(hql,1).get(0);
	}
	
	
	
	



}

