package haj.com.dao;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.entity.Notes;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class NotesDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	@SuppressWarnings("unchecked")
	public List<Notes> allNotes() throws Exception {
		return (List<Notes>)super.getList("select o from Notes o");
	}

/*
	@SuppressWarnings("unchecked")
	public List<Notes> byField(long key) throws Exception  {
		String hql = "select o from Notes o where o.key = :key";
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("key", key);
	    return (List<Notes>)super.getList(hql, parameters);
	}
*/

	public Notes findByKey(Long id) throws Exception {
	 	String hql = "select o from Notes o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Notes)super.getUniqueResult(hql, parameters);
	}

	@SuppressWarnings("unchecked")
	public List<Notes> findByName(String description) throws Exception {
	 	String hql = "select o from Notes o where o.description like  :description order by o.desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("description", ""+description.trim()+"%");
		return (List<Notes>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Notes> findByCpd(Long attestationId) throws Exception {
	 	String hql = "select o from Notes o where o.cpd.id =  :attestationId order by o.createDate desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("attestationId", attestationId);
		return (List<Notes>)super.getList(hql, parameters);
	}
	
	@SuppressWarnings("unchecked")
	public List<Notes> findByUser(Long userId) throws Exception {
	 	String hql = "select o from Notes o where o.users.uid = :userId order by o.createDate desc " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("userId", userId);
		return (List<Notes>)super.getList(hql, parameters);
	}
}

