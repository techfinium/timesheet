package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import haj.com.entity.lookup.CronicDiseases;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "medica_screening_form_cronic_diseases")
public class MedicalScreeningFormCronicDiseases implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name="cronic_diseases_id")
	private CronicDiseases cronicDiseases;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name="medical_screening_form_id", insertable=true, updatable=true, nullable=true)
	private MedicalScreeningForm medicalScreeningForm;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedicalScreeningFormCronicDiseases other = (MedicalScreeningFormCronicDiseases) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public MedicalScreeningForm getMedicalScreeningForm() {
		return medicalScreeningForm;
	}

	public void setMedicalScreeningForm(MedicalScreeningForm medicalScreeningForm) {
		this.medicalScreeningForm = medicalScreeningForm;
	}

	public CronicDiseases getCronicDiseases() {
		return cronicDiseases;
	}

	public void setCronicDiseases(CronicDiseases cronicDiseases) {
		this.cronicDiseases = cronicDiseases;
	}

}
