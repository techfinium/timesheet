package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import haj.com.framework.IDataEntity;


@Entity
@Table(name = "audit_log")
@DynamicInsert(value=true)
@DynamicUpdate(value=true)
public class AuditLog implements IDataEntity
{
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name="AUDIT_LOG_ID", nullable=false)
  private Date auditLogId;
  
  @Column(name="LOGGED_IP", length=30)
  private String loggedIp;
  
  @Column(name="USER_ID")
  private int userId;
  
  
  @Column(name="DETAILS", length=70)
  private String details;
  
  @Column(name="APP_ID")
  private Integer appId;
  
  public AuditLog()
  {
  }

 

 
  
  @Override
  public String toString()
  {
    Map<String, Object> allVals = new HashMap<String, Object>();
    allVals.put("loggedIp", loggedIp);
    allVals.put("userId", userId);
    allVals.put("details", details);
    allVals.put("appId", appId);
    return allVals.toString();
  }

  public Date getAuditLogId()
  {
    return auditLogId;
  }

  public void setAuditLogId(Date auditLogId)
  {
    this.auditLogId = auditLogId;
  }

  public String getLoggedIp()
  {
    return loggedIp;
  }

  public void setLoggedIp(String loggedIp)
  {
    this.loggedIp = loggedIp;
  }

  public int getUserId()
  {
    return userId;
  }

  public void setUserId(int userId)
  {
    this.userId = userId;
  }


  public String getDetails()
  {
    return details;
  }

  public void setDetails(String details)
  {
    this.details = details;
  }

  public Integer getAppId()
  {
    return appId;
  }

  public void setAppId(Integer appId)
  {
    this.appId = appId;
  }
}
