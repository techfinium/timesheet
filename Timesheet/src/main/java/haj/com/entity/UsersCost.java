package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import haj.com.convertors.LocaleConverter;
import haj.com.framework.IDataEntity;


@Entity
@Table(name = "users_cost")
@AuditTable(value = "users_cost_HIST")
@Audited
public class UsersCost implements IDataEntity
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch=FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name="user_id", insertable=true, updatable=true, nullable=true)
	private Users users; 
	
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="effective_date", length=19)
    private Date effectiveDate;
	
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_date", length=19)
    private Date createDate;
    
    @Column(name = "locale")
    @Convert(converter = LocaleConverter.class)
    private Locale locale;

	@Column(name="exhange_rate")
    private Double exhangeRate;

    @Column(name="gross_salary_per_annum")
    private Double grossSalaryPerAnnum;
    
    @Column(name="ers_ni_per_annum")
    private Double ersNiPerAnnum;
    
    @Column(name="unemployment_fund_per_annum")
    private Double unemploymentFundPerAnnum;
   
    @Column(name="mandatory_tax_pensions_per_annum")
    private Double mandatoryTaxPensionsPerAnnum;
    
    @Column(name="one_time_expense_per_annum")
    private Double oneTimeExpensePerAnnum;
    
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch=FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name="updated_by_user_id", insertable=true, updatable=true, nullable=true)
	private Users updatedBy; 
    
    @Transient
    private Double totalCostOfEmployeePerAnnum;
    
    @Transient
    private Double dayRate;
    
    @Transient
    private Currency currency;

    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}



	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	public UsersCost() {
		super();
	}


    public UsersCost(Double grossSalaryPerAnnum, Double ersNiPerAnnum) {
		super();
		this.grossSalaryPerAnnum = grossSalaryPerAnnum;
		this.ersNiPerAnnum = ersNiPerAnnum;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsersCost other = (UsersCost) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Double getExhangeRate() {
		return exhangeRate;
	}

	public void setExhangeRate(Double exhangeRate) {
		this.exhangeRate = exhangeRate;
	}

	public Double getGrossSalaryPerAnnum() {
		return grossSalaryPerAnnum;
	}

	public void setGrossSalaryPerAnnum(Double grossSalaryPerAnnum) {
		this.grossSalaryPerAnnum = grossSalaryPerAnnum;
	}

	public Double getErsNiPerAnnum() {
		return ersNiPerAnnum;
	}

	public void setErsNiPerAnnum(Double ersNiPerAnnum) {
		this.ersNiPerAnnum = ersNiPerAnnum;
	}

	public Double getUnemploymentFundPerAnnum() {
		return unemploymentFundPerAnnum;
	}

	public void setUnemploymentFundPerAnnum(Double unemploymentFundPerAnnum) {
		this.unemploymentFundPerAnnum = unemploymentFundPerAnnum;
	}

	public Double getMandatoryTaxPensionsPerAnnum() {
		return mandatoryTaxPensionsPerAnnum;
	}

	public void setMandatoryTaxPensionsPerAnnum(Double mandatoryTaxPensionsPerAnnum) {
		this.mandatoryTaxPensionsPerAnnum = mandatoryTaxPensionsPerAnnum;
	}

	public Double getOneTimeExpensePerAnnum() {
		return oneTimeExpensePerAnnum;
	}

	public void setOneTimeExpensePerAnnum(Double oneTimeExpensePerAnnum) {
		this.oneTimeExpensePerAnnum = oneTimeExpensePerAnnum;
	}

	public Double getTotalCostOfEmployeePerAnnum() {
		totalCostOfEmployeePerAnnum = (grossSalaryPerAnnum==null?0:grossSalaryPerAnnum.doubleValue()) 
				+ (mandatoryTaxPensionsPerAnnum==null?0:mandatoryTaxPensionsPerAnnum.doubleValue())
				+ (oneTimeExpensePerAnnum==null?0:oneTimeExpensePerAnnum.doubleValue());
		return totalCostOfEmployeePerAnnum;
	}

	public void setTotalCostOfEmployeePerAnnum(Double totalCostOfEmployeePerAnnum) {
		this.totalCostOfEmployeePerAnnum = totalCostOfEmployeePerAnnum;
	}

	public Double getDayRate() {
		dayRate = getTotalCostOfEmployeePerAnnum()/200;
		return dayRate;
	}

	public void setDayRate(Double dayRate) {
		this.dayRate = dayRate;
	}

	public Currency getCurrency() {
		if (locale!=null) {
			currency =  Currency.getInstance(locale);
		}
		else {
			currency =  Currency.getInstance(Locale.UK);
		}
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Users getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Users updatedBy) {
		this.updatedBy = updatedBy;
	}



}
