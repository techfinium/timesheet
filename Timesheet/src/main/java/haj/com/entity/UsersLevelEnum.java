package haj.com.entity;

public enum UsersLevelEnum {

	Normal("Employee"), Manager("Manager"), Admin("Administrator"), Director("Director"),;

	private String displayName;

	private UsersLevelEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
