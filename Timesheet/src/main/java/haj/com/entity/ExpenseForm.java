package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import haj.com.entity.enums.ApprovalEnum;
import haj.com.entity.enums.CarTypeEnum;
import haj.com.entity.enums.ExpenseFormTypeEnum;
import haj.com.entity.lookup.AllowanceRate;
import haj.com.entity.lookup.DailyRate;
import haj.com.entity.lookup.EngineType;
import haj.com.entity.lookup.Title;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "expense_form")
public class ExpenseForm implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Column(name = "expense_form_date")
	private Date expenseFormdate;

	@Column(name = "desc_of_expense")
	private String descOfExpense;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "daily_rate", nullable = true)
	private DailyRate dailyRate;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "allowance_rate", nullable = true)
	private AllowanceRate allowanceRate;

	@Column(name = "day_number")
	private Integer dayNumber;

	@Column(name = "km_travelled")
	private Double kmTravelled;

	@Column(name = "accomodation_price")
	private Double accomodationPrice;

	@Column(name = "meals")
	private Double meals;

	@Column(name = "parking_fees")
	private Double parking;

	@Column(name = "taxi_train_toll")
	private Double taxiTrainToll;

	@Column(name = "flight_price")
	private Double flightPrice;

	@Column(name = "tel_mobile_dialup_amount")
	private Double telMobileDialupAmount;

	@Column(name = "utilities_amount")
	private Double utilitiesAmount;

	@Column(name = "entertainment_expense_date")
	private Date entertainmentExpensedate;

	@Column(name = "city")
	private String city;

	@Column(name = "meeting_place")
	private String meetingPlace;

	@Column(name = "persons_entertained")
	private String personsEntertained;

	@Column(name = "name")
	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "title", nullable = true)
	private Title title;

	@Column(name = "company")
	private String company;

	@Column(name = "business_purpose")
	private String businessPurpose;

	@Column(name = "total")
	private Double total;

	@Column(name = "miscellaneous_expense_date")
	private Date miscellaneousExpensedate;

	@Column(name = "explanation_of_expenses")
	private String explanationOfExpenses;

	@Column(name = "authorised_by")
	private String authorisedBy;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "rate")
	private Double rate;

	@Column(name = "bank_name")
	private String bank_name;

	@Column(name = "account_holder_name")
	private String accountHolderName;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "sort_code")
	private String sortCode;

	@Column(name = "time_left")
	private Date timeLeft;

	@Column(name = "time_arrived")
	private Date timeArrived;

	@Column(name = "travelling_from")
	private String travellingFrom;

	@Column(name = "travelling_to")
	private String travellingTo;

	@Column(name = "currency")
	private String currency;

	@Column(name = "amount_charged")
	private Double amountCharged;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "project_id", nullable = true)
	private Projects project;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "user_id", nullable = true)
	private Users user;

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "engine_type_id", nullable = true)
	private EngineType engineType;

	@Column(name = "car_type")
	private CarTypeEnum carType;

	@Column(name = "expense_form_type")
	private ExpenseFormTypeEnum expenseFormType;
	
	@Column(name = "status")
	private ApprovalEnum status;

	@Column(name = "consent")
	private Boolean consent;

	@Column(name = "per_diem")
	private Double perDiem;

	@Column(name = "rands_per_km")
	private Double randsPerKm;

	@Column(name = "total_km_expense_form")
	private Double totalKmExpenseForm;

	@Column(name = "total_expense_form")
	private Double totalExpenseForm;

	@Transient
	private List<Doc> docList;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExpenseForm other = (ExpenseForm) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getExpenseFormdate() {
		return expenseFormdate;
	}

	public void setExpenseFormdate(Date expenseFormdate) {
		this.expenseFormdate = expenseFormdate;
	}

	public String getDescOfExpense() {
		return descOfExpense;
	}

	public void setDescOfExpense(String descOfExpense) {
		this.descOfExpense = descOfExpense;
	}

	public DailyRate getDailyRate() {
		return dailyRate;
	}

	public void setDailyRate(DailyRate dailyRate) {
		this.dailyRate = dailyRate;
	}

	public Integer getDayNumber() {
		return dayNumber;
	}

	public void setDayNumber(Integer dayNumber) {
		this.dayNumber = dayNumber;
	}

	public Double getKmTravelled() {
		return kmTravelled;
	}

	public void setKmTravelled(Double kmTravelled) {
		this.kmTravelled = kmTravelled;
	}

	public Double getAccomodationPrice() {
		return accomodationPrice;
	}

	public void setAccomodationPrice(Double accomodationPrice) {
		this.accomodationPrice = accomodationPrice;
	}

	public Double getMeals() {
		return meals;
	}

	public void setMeals(Double meals) {
		this.meals = meals;
	}

	public Double getParking() {
		return parking;
	}

	public void setParking(Double parking) {
		this.parking = parking;
	}

	public Double getTaxiTrainToll() {
		return taxiTrainToll;
	}

	public void setTaxiTrainToll(Double taxiTrainToll) {
		this.taxiTrainToll = taxiTrainToll;
	}

	public Double getFlightPrice() {
		return flightPrice;
	}

	public void setFlightPrice(Double flightPrice) {
		this.flightPrice = flightPrice;
	}

	public Double getTelMobileDialupAmount() {
		return telMobileDialupAmount;
	}

	public void setTelMobileDialupAmount(Double telMobileDialupAmount) {
		this.telMobileDialupAmount = telMobileDialupAmount;
	}

	public Double getUtilitiesAmount() {
		return utilitiesAmount;
	}

	public void setUtilitiesAmount(Double utilitiesAmount) {
		this.utilitiesAmount = utilitiesAmount;
	}

	public Date getEntertainmentExpensedate() {
		return entertainmentExpensedate;
	}

	public void setEntertainmentExpensedate(Date entertainmentExpensedate) {
		this.entertainmentExpensedate = entertainmentExpensedate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMeetingPlace() {
		return meetingPlace;
	}

	public void setMeetingPlace(String meetingPlace) {
		this.meetingPlace = meetingPlace;
	}

	public String getPersonsEntertained() {
		return personsEntertained;
	}

	public void setPersonsEntertained(String personsEntertained) {
		this.personsEntertained = personsEntertained;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getBusinessPurpose() {
		return businessPurpose;
	}

	public void setBusinessPurpose(String businessPurpose) {
		this.businessPurpose = businessPurpose;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Date getMiscellaneousExpensedate() {
		return miscellaneousExpensedate;
	}

	public void setMiscellaneousExpensedate(Date miscellaneousExpensedate) {
		this.miscellaneousExpensedate = miscellaneousExpensedate;
	}

	public String getExplanationOfExpenses() {
		return explanationOfExpenses;
	}

	public void setExplanationOfExpenses(String explanationOfExpenses) {
		this.explanationOfExpenses = explanationOfExpenses;
	}

	public String getAuthorisedBy() {
		return authorisedBy;
	}

	public void setAuthorisedBy(String authorisedBy) {
		this.authorisedBy = authorisedBy;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSortCode() {
		return sortCode;
	}

	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}

	public Date getTimeLeft() {
		return timeLeft;
	}

	public void setTimeLeft(Date timeLeft) {
		this.timeLeft = timeLeft;
	}

	public Date getTimeArrived() {
		return timeArrived;
	}

	public void setTimeArrived(Date timeArrived) {
		this.timeArrived = timeArrived;
	}

	public String getTravellingFrom() {
		return travellingFrom;
	}

	public void setTravellingFrom(String travellingFrom) {
		this.travellingFrom = travellingFrom;
	}

	public String getTravellingTo() {
		return travellingTo;
	}

	public void setTravellingTo(String travellingTo) {
		this.travellingTo = travellingTo;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getAmountCharged() {
		return amountCharged;
	}

	public void setAmountCharged(Double amountCharged) {
		this.amountCharged = amountCharged;
	}

	public Projects getProject() {
		return project;
	}

	public void setProject(Projects project) {
		this.project = project;
	}

	public CarTypeEnum getCarType() {
		return carType;
	}

	public void setCarType(CarTypeEnum carType) {
		this.carType = carType;
	}

	public ExpenseFormTypeEnum getExpenseFormType() {
		return expenseFormType;
	}

	public void setExpenseFormType(ExpenseFormTypeEnum expenseFormType) {
		this.expenseFormType = expenseFormType;
	}

	public Boolean getConsent() {
		return consent;
	}

	public void setConsent(Boolean consent) {
		this.consent = consent;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Double getPerDiem() {
		return perDiem;
	}

	public void setPerDiem(Double perDiem) {
		this.perDiem = perDiem;
	}

	public Double getRandsPerKm() {
		return randsPerKm;
	}

	public void setRandsPerKm(Double randsPerKm) {
		this.randsPerKm = randsPerKm;
	}

	public Double getTotalKmExpenseForm() {
		return totalKmExpenseForm;
	}

	public void setTotalKmExpenseForm(Double totalKmExpenseForm) {
		this.totalKmExpenseForm = totalKmExpenseForm;
	}

	public Double getTotalExpenseForm() {
		return totalExpenseForm;
	}

	public void setTotalExpenseForm(Double totalExpenseForm) {
		this.totalExpenseForm = totalExpenseForm;
	}

	public EngineType getEngineType() {
		return engineType;
	}

	public void setEngineType(EngineType engineType) {
		this.engineType = engineType;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public List<Doc> getDocList() {
		return docList;
	}

	public void setDocList(List<Doc> docList) {
		this.docList = docList;
	}

	public AllowanceRate getAllowanceRate() {
		return allowanceRate;
	}

	public void setAllowanceRate(AllowanceRate allowanceRate) {
		this.allowanceRate = allowanceRate;
	}

}
