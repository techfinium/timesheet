package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.ForeignKey;



@Entity
@Table(name = "audit_event_param")
@DynamicInsert(value=true)
@DynamicUpdate(value=true)
public class AuditEventParam implements haj.com.framework.IDataEntity
{
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name="EVENT_PARAM_ID", nullable=false)
  private Integer eventParamId;

  @ManyToOne(fetch = FetchType.LAZY)
  @ForeignKey(name = "fk_EVENT_ID")
  @JoinColumn(name = "EVENT_ID", insertable = true, updatable = true, nullable = true)
  private AuditEvent event;
 
  @Column(name="PARAM_NAME", length=100)
  private String paramName;
  
  @Column(name="PARAM_VALUE", length=100)
  private String paramValue;

  
  
  public AuditEventParam() {
  }
  
  

public AuditEventParam(AuditEvent event) {
	super();
	this.event = event;
}



public Integer getEventParamId()
  {
    return eventParamId;
  }

  public void setEventParamId(Integer eventParamId)
  {
    this.eventParamId = eventParamId;
  }



  public String getParamName()
  {
    return paramName;
  }

  public void setParamName(String paramName)
  {
    this.paramName = paramName;
  }

  public String getParamValue()
  {
    return paramValue;
  }

  public void setParamValue(String paramValue)
  {
    this.paramValue = paramValue;
  }

public AuditEvent getEvent() {
	return event;
}

public void setEvent(AuditEvent event) {
	this.event = event;
}
  
  
}
