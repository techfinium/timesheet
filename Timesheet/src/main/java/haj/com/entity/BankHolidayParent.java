package haj.com.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import haj.com.framework.IDataEntity;


@Entity
@Table(name = "bank_holiday_parent")
public class BankHolidayParent implements IDataEntity
{
	private static final long serialVersionUID = 1L;
	@Id
    @Column(name="bank_holiday_year",nullable=false)
	private Long bankHolidayYear;
	
	
    @OneToMany(fetch=FetchType.EAGER, mappedBy="bankHolidayParent")
    @OrderBy("bankHoliday ASC")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    private Set<BankHolidayChild> bankHolidayChildrens = new HashSet<BankHolidayChild>(0); 	
	
	
	
	public Long getBankHolidayYear() {
		return bankHolidayYear;
	}
	public void setBankHolidayYear(Long bankHolidayYear) {
		this.bankHolidayYear = bankHolidayYear;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankHolidayYear == null) ? 0 : bankHolidayYear.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankHolidayParent other = (BankHolidayParent) obj;
		if (bankHolidayYear == null) {
			if (other.bankHolidayYear != null)
				return false;
		} else if (!bankHolidayYear.equals(other.bankHolidayYear))
			return false;
		return true;
	}
	
	public Set<BankHolidayChild> getBankHolidayChildrens() {
		return bankHolidayChildrens;
	}
	public void setBankHolidayChildrens(Set<BankHolidayChild> bankHolidayChildrens) {
		this.bankHolidayChildrens = bankHolidayChildrens;
	}

    
	public List<BankHolidayChild> getBankHolidayChildrenList() {
		return new ArrayList<BankHolidayChild>(bankHolidayChildrens);
	}

}
