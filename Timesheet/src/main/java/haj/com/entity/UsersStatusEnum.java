package haj.com.entity;

public enum UsersStatusEnum {

  Active("Active"), InActive("In-Active") , EmailNotConfrimed("Email not confrimed") ;

  private String displayName;

  private UsersStatusEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
