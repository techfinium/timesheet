package haj.com.entity;

public enum AddressTypeEnum {

   Residential("Residential"), Postal("Postal"), Billing("Billing"), Work("Work");

  private String displayName;

  private AddressTypeEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
