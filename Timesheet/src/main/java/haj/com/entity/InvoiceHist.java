package haj.com.entity;

import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import haj.com.convertors.LocaleConverter;
import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "invoice_HIST")
public class InvoiceHist implements IDataEntity {
	private static final long serialVersionUID = 1L;

	private InvoiceHistId id;
	private Byte revtype;
	private Users createdBy;
	private Date createDate;
	private StatementOfWork statementOfWork;
	private MilestoneStatementOfWork milestoneStatementOfWork;
	private String invoiceNumber;
	private String purchaseOrderNumber;
	private Locale locale;
	private Double exhangeRate;
	private Double invoiceValue;
	private Double sowValue;
	private Double employeeValue;
	private Double milestoneValue;
	private Date fromDate;
	private Date toDate;
	private Date invoiceDate;
	private Boolean paid;
	private Date paidDate;
	private Double paidExhangeRate;
	private Currency currency;
	private SowTypeEnum sowTypeEnum;
	private Revinfo revinfo;

	public InvoiceHist() {
	}

	public InvoiceHist(InvoiceHistId id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id", nullable = false)), @AttributeOverride(name = "rev", column = @Column(name = "REV", nullable = false)) })
	public InvoiceHistId getId() {
		return this.id;
	}

	public void setId(InvoiceHistId id) {
		this.id = id;
	}

	@Column(name = "REVTYPE")
	public Byte getRevtype() {
		return this.revtype;
	}

	public void setRevtype(Byte revtype) {
		this.revtype = revtype;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Transient
	public Currency getCurrency() {
		if (locale != null) {
			currency = Currency.getInstance(locale);
		} else {
			currency = Currency.getInstance(Locale.UK);
		}
		return currency;
	}

	@Transient
	public Revinfo getRevinfo() {
		return revinfo;
	}

	public void setRevinfo(Revinfo revinfo) {
		this.revinfo = revinfo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "statement_of_work_id", insertable = true, updatable = true, nullable = true)
	public StatementOfWork getStatementOfWork() {
		return statementOfWork;
	}

	public void setStatementOfWork(StatementOfWork statementOfWork) {
		this.statementOfWork = statementOfWork;
	}

	@Column(name = "locale")
	@Convert(converter = LocaleConverter.class)
	public Locale getLocale() {
		return this.locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@Column(name = "invoice_number", length = 100)
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	@Column(name = "purchase_order_number", length = 100)
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	@Column(name = "exhange_rate")
	public Double getExhangeRate() {
		return exhangeRate;
	}

	public void setExhangeRate(Double exhangeRate) {
		this.exhangeRate = exhangeRate;
	}

	@Column(name = "invoice_value")
	public Double getInvoiceValue() {
		return invoiceValue;
	}

	public void setInvoiceValue(Double invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_date", length = 19)
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date", length = 19)
	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "invoice_date", length = 19)
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	@Column(name = "paid")
	public Boolean getPaid() {
		return paid;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "paid_date", length = 19)
	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	@Column(name = "paid_exhange_rate")
	public Double getPaidExhangeRate() {
		return paidExhangeRate;
	}

	public void setPaidExhangeRate(Double paidExhangeRate) {
		this.paidExhangeRate = paidExhangeRate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "user_id", insertable = true, updatable = true, nullable = true)
	public Users getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Users createdBy) {
		this.createdBy = createdBy;
	}

	@Enumerated
	@Column(name = "sow_type_enum", nullable = true)
	// 0 is TimeMaterials, 1 is Fixed
	public SowTypeEnum getSowTypeEnum() {
		return sowTypeEnum;
	}

	public void setSowTypeEnum(SowTypeEnum sowTypeEnum) {
		this.sowTypeEnum = sowTypeEnum;
	}

	@Column(name = "sow_value")
	public Double getSowValue() {
		return sowValue;
	}

	public void setSowValue(Double sowValue) {
		this.sowValue = sowValue;
	}

	@Column(name = "employee_value")
	public Double getEmployeeValue() {
		return employeeValue;
	}

	public void setEmployeeValue(Double employeeValue) {
		this.employeeValue = employeeValue;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "milestone_statement_of_work_id", insertable = true, updatable = true, nullable = true)
	public MilestoneStatementOfWork getMilestoneStatementOfWork() {
		return milestoneStatementOfWork;
	}

	public void setMilestoneStatementOfWork(MilestoneStatementOfWork milestoneStatementOfWork) {
		this.milestoneStatementOfWork = milestoneStatementOfWork;
	}

	@Column(name = "milestone_value")
	public Double getMilestoneValue() {
		return milestoneValue;
	}

	public void setMilestoneValue(Double milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

}
