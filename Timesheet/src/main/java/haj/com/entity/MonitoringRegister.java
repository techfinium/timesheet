package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import haj.com.entity.enums.CovidPersonRegisterTypeEnum;
import haj.com.entity.enums.VisitorTypeEnum;
import haj.com.entity.enums.YesNoEnum;
import haj.com.framework.IDataEntity;


@Entity
@Table(name = "monotoring_register")
public class MonitoringRegister implements IDataEntity
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_date", length=19)
    private Date createDate;
	
    @Transient
	private Visitor visitor;
    
    @Transient
	private Users user;
   
	@Column(name ="other_sypmtoms")
	private String otherSymptoms;
	
	@Enumerated
	@Column(name = "health_declaration_form_on_file")
	private YesNoEnum healthDeclarationFormOnFile;
	
	@Enumerated
	@Column(name = "medical_surveillance_form_on_file")
	private YesNoEnum medicalSurveillanceFormOnFile;
	
	@Column(name="enter_temperature")
	private Double enterTemperature;
	
	@Column(name="exit_temperature")
	private Double exitTemperature;
	
	@Column(name="enter_time")
	private Date enterTime;
	
	@Column(name = "exit_time")
	private Date exitTime;
	
	@Column(name = "target_key")
	private Long targetKey;
	
	
	@Column(name = "target_class")
	private String targetClass;
	
	@Enumerated
	@Column(name = "register_person_type")
	private CovidPersonRegisterTypeEnum personType;
	
	@Enumerated
	@Transient
	private VisitorTypeEnum visitorType;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonitoringRegister other = (MonitoringRegister) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getOtherSymptoms() {
		return otherSymptoms;
	}

	public void setOtherSymptoms(String otherSymptoms) {
		this.otherSymptoms = otherSymptoms;
	}

	public YesNoEnum getHealthDeclarationFormOnFile() {
		return healthDeclarationFormOnFile;
	}

	public void setHealthDeclarationFormOnFile(YesNoEnum healthDeclarationFormOnFile) {
		this.healthDeclarationFormOnFile = healthDeclarationFormOnFile;
	}

	public YesNoEnum getMedicalSurveillanceFormOnFile() {
		return medicalSurveillanceFormOnFile;
	}

	public void setMedicalSurveillanceFormOnFile(YesNoEnum medicalSurveillanceFormOnFile) {
		this.medicalSurveillanceFormOnFile = medicalSurveillanceFormOnFile;
	}

	public Double getEnterTemperature() {
		return enterTemperature;
	}

	public void setEnterTemperature(Double enterTemperature) {
		this.enterTemperature = enterTemperature;
	}

	public Double getExitTemperature() {
		return exitTemperature;
	}

	public void setExitTemperature(Double exitTemperature) {
		this.exitTemperature = exitTemperature;
	}

	public Date getEnterTime() {
		return enterTime;
	}

	public void setEnterTime(Date enterTime) {
		this.enterTime = enterTime;
	}

	public Date getExitTime() {
		return exitTime;
	}

	public void setExitTime(Date exitTime) {
		this.exitTime = exitTime;
	}

	public Visitor getVisitor() {
		return visitor;
	}

	public void setVisitor(Visitor visitor) {
		this.visitor = visitor;
	}

	public long getTargetKey() {
		return targetKey;
	}

	public void setTargetKey(long targetKey) {
		this.targetKey = targetKey;
	}

	public String getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public void setTargetKey(Long targetKey) {
		this.targetKey = targetKey;
	}

	public CovidPersonRegisterTypeEnum getPersonType() {
		return personType;
	}

	public void setPersonType(CovidPersonRegisterTypeEnum personType) {
		this.personType = personType;
	}

	public VisitorTypeEnum getVisitorType() {
		return visitorType;
	}

	public void setVisitorType(VisitorTypeEnum visitorType) {
		this.visitorType = visitorType;
	}

	
	
}
