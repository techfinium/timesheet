package haj.com.entity;

import java.util.Date;
import java.util.Locale;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import haj.com.convertors.LocaleConverter;
import haj.com.entity.enums.FixedRollingEnum;
import haj.com.entity.enums.SowStatusEnum;
import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "statement_of_work_HIST")
public class StatementOfWorkHist implements IDataEntity, Cloneable {

	private static final long serialVersionUID = 1L;

	private StatementOfWorkHistId id;
	private Byte revtype;
	private Date createDate;
	private Projects projects;
	private String sowNumber;
	private String title;
	private String description;
	private SowTypeEnum sowTypeEnum;
	private String invoiceTerms;
	private String paymentTerms;
	private Double valueSow;
	private Double valueOfSowExhangeRate;
	private Locale locale;
	private SowStatusEnum sowStatusEnum;
	private String note;
	private Users userSow;
	private FixedRollingEnum fixedPeriod;
	private Long paymentMilestones;
	private Revinfo revinfo;

	public StatementOfWorkHist() {

	}

	public StatementOfWorkHist(StatementOfWorkHistId id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id", nullable = false)), @AttributeOverride(name = "rev", column = @Column(name = "REV", nullable = false)) })
	public StatementOfWorkHistId getId() {
		return this.id;
	}

	public void setId(StatementOfWorkHistId id) {
		this.id = id;
	}

	@Column(name = "REVTYPE")
	public Byte getRevtype() {
		return this.revtype;
	}

	public void setRevtype(Byte revtype) {
		this.revtype = revtype;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "locale")
	@Convert(converter = LocaleConverter.class)
	public Locale getLocale() {
		return this.locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@Transient
	public Revinfo getRevinfo() {
		return revinfo;
	}

	public void setRevinfo(Revinfo revinfo) {
		this.revinfo = revinfo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "project_id", nullable = true)
	public Projects getProjects() {
		return projects;
	}

	public void setProjects(Projects projects) {
		this.projects = projects;
	}

	@Column(name = "statement_of_work_number", length  = 100 , nullable = true)
	public String getSowNumber() {
		return sowNumber;
	}

	public void setSowNumber(String sowNumber) {
		this.sowNumber = sowNumber;
	}

	@Column(name = "title", length = 100, nullable = false)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "description", columnDefinition = "LONGTEXT")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Enumerated
	@Column(name = "sow_type_enum", nullable = true)
	public SowTypeEnum getSowTypeEnum() {
		return sowTypeEnum;
	}

	public void setSowTypeEnum(SowTypeEnum sowTypeEnum) {
		this.sowTypeEnum = sowTypeEnum;
	}

	@Column(name = "invoice_terms", length = 100, nullable = false)
	public String getInvoiceTerms() {
		return invoiceTerms;
	}

	public void setInvoiceTerms(String invoiceTerms) {
		this.invoiceTerms = invoiceTerms;
	}

	@Column(name = "payment_terms", length = 100, nullable = false)
	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	@Column(name = "value_of_sow", nullable = true)
	public Double getValueSow() {
		return valueSow;
	}

	public void setValueSow(Double valueSow) {
		this.valueSow = valueSow;
	}

	@Column(name = "value_of_sow_exhange_rate")
	public Double getValueOfSowExhangeRate() {
		return valueOfSowExhangeRate;
	}

	public void setValueOfSowExhangeRate(Double valueOfSowExhangeRate) {
		this.valueOfSowExhangeRate = valueOfSowExhangeRate;
	}

	@Enumerated
	@Column(name = "sow_status_enum", nullable = true)
	public SowStatusEnum getSowStatusEnum() {
		return sowStatusEnum;
	}

	public void setSowStatusEnum(SowStatusEnum sowStatusEnum) {
		this.sowStatusEnum = sowStatusEnum;
	}

	@Column(name = "note", columnDefinition = "LONGTEXT")
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "user_id", nullable = true)
	public Users getUserSow() {
		return userSow;
	}

	public void setUserSow(Users userSow) {
		this.userSow = userSow;
	}

	@Enumerated
	@Column(name = "fixed_period_rolling")
	public FixedRollingEnum getFixedPeriod() {
		return fixedPeriod;
	}

	public void setFixedPeriod(FixedRollingEnum fixedPeriod) {
		this.fixedPeriod = fixedPeriod;
	}

	@Column(name = "payment_milestones", nullable = true)
	public Long getPaymentMilestones() {
		return paymentMilestones;
	}

	public void setPaymentMilestones(Long paymentMilestones) {
		this.paymentMilestones = paymentMilestones;
	}

}
