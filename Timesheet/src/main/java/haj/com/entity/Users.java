package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.hibernate.validator.constraints.Email;

import haj.com.bean.Webservicestatus;
import haj.com.billing.entity.Accounts;
import haj.com.entity.enums.EmployeeTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "users")
public class Users implements IDataEntity {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "uid", unique = true, nullable = false)
	private Long uid;
	
	@Column(name = "admin", nullable = false)
	private Boolean admin;

	@Column(name = "first_name", length = 100, nullable = false)
	private String firstName;

	@Column(name = "last_name", length = 100, nullable = false)
	private String lastName;

	@Column(name = "id_number", length = 13, nullable = true)
	//@CheckID(message = "ID number not valid")
	private String idNumber;

	@Column(name = "passport_number", length = 30, nullable = true)
	private String passportNumber;

	@Column(name = "tel_number", length = 20)
	private String telNumber;

	@Column(name = "cell_number", length = 20)
	private String cellNumber;

	@Column(name = "password", length = 1000, nullable = false)
	private String password;

	@Column(name = "last_login", nullable = true)
	private Date lastLogin;

	@Column(name = "email", length = 100, nullable = true)
	@Email(message = "Please enter a valid Email Address")
	private String email;

	@Column(name = "user_id", length = 15, nullable = true)
	private String userId;

	@Column(name = "username", length = 100, nullable = false)
	private String username;

	@Enumerated
	private UsersStatusEnum status;

	@Column(name = "alternative_contact_name", length = 100, nullable = true)
	private String alternativeContactName;

	@Column(name = "alternative_contact_number", length = 20, nullable = true)
	private String alternativeContactNumber;

	@Column(name = "registration_date", nullable = true)
	private Date registrationDate;

	@Column(name = "email_guid", length = 100, nullable = true)
	public String emailGuid;

	@Column(name = "email_confirm_date", nullable = true)
	private Date emailConfirmDate;

	@Column(name = "internal_cell_number", length = 20, nullable = true)
	private String internalCellNumber;

	@Transient
	private Webservicestatus webservicestatus;

	@Column(name = "phone_id", length = 100)
	private String phoneId;

	@Column(name = "ios_android", length = 1, nullable = true)
	private String iosAndroid;

	@Column(name = "accepted_t_and_c")
	private Boolean acceptedTAndC;

	@Transient
	private boolean chgPwdNow;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "account_id", nullable = true)
	private Accounts accounts;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "users")
//	@Fetch(FetchMode.JOIN)
	@OrderBy("effectiveDate DESC")
	@Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
	private Set<UsersCost> usersCosts = new HashSet<UsersCost>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
//	@Fetch(FetchMode.JOIN)
	private Set<Doc> docs = new HashSet<Doc>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "users")
//	@Fetch(FetchMode.JOIN)
	private Set<Notes> notes = new HashSet<Notes>(0);

	// Ashton
	@Column(name = "location", length = 100)
	private String location;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_of_birth", nullable = true)
	private Date dateOfBirth;

	@Column(name = "recieve_email_task_notification", columnDefinition = "BIT default true")
	private Boolean recieveEmailTaskNotification;

	@Column(name = "ni_number", length = 100)
	private String niNumber;
	
	@Column(name = "department")
	private String department;
	
	@Column(name = "occupation")
	private String occupation;

	@Column(name = "company_number")
	private String companyNumber;

	// @Column(name = "home_address", length = 100)
	// private Address homeAddress;

	@Enumerated
	@Column(name = "employee_type")
	private EmployeeTypeEnum employeeType;

	/** The profile image. */
	@ManyToOne(fetch = FetchType.LAZY)
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "profile_image_id", nullable = true)
	private Images profileImage;

	@Transient
	private Address physicalAddress;
	
	@Column(name = "date_employed")
	private Date dateEmployed;
	
	@Column(name = "date_discharged")
	private Date dateDischarged;
	
	@Column(name = "employment_lenght")
	private String employemntLenght;
	
    @Column(name = "still_employed", columnDefinition = "BIT default true")
    private Boolean stillEmployed;
	
	public Users() {
	}

	public Users(Long uid) {
		this.uid = uid;
	}

	public Users(Webservicestatus webservicestatus) {
		super();
		this.webservicestatus = webservicestatus;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonIgnore
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	@JsonIgnore
	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonIgnore
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@JsonIgnore
	public UsersStatusEnum getStatus() {
		return status;
	}

	public void setStatus(UsersStatusEnum status) {
		this.status = status;
	}

	@JsonIgnore
	public String getAlternativeContactName() {
		return alternativeContactName;
	}

	public void setAlternativeContactName(String alternativeContactName) {
		this.alternativeContactName = alternativeContactName;
	}

	@JsonIgnore
	public String getAlternativeContactNumber() {
		return alternativeContactNumber;
	}

	public void setAlternativeContactNumber(String alternativeContactNumber) {
		this.alternativeContactNumber = alternativeContactNumber;
	}

	@JsonIgnore
	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	@JsonIgnore
	public String getEmailGuid() {
		return emailGuid;
	}

	public void setEmailGuid(String emailGuid) {
		this.emailGuid = emailGuid;
	}

	@JsonIgnore
	public Date getEmailConfirmDate() {
		return emailConfirmDate;
	}

	public void setEmailConfirmDate(Date emailConfirmDate) {
		this.emailConfirmDate = emailConfirmDate;
	}

	public String getInternalCellNumber() {
		return internalCellNumber;
	}

	public void setInternalCellNumber(String internalCellNumber) {
		this.internalCellNumber = internalCellNumber;
	}

	public Webservicestatus getWebservicestatus() {
		return webservicestatus;
	}

	public void setWebservicestatus(Webservicestatus webservicestatus) {
		this.webservicestatus = webservicestatus;
	}

	@JsonIgnore
	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}

	public String getIosAndroid() {
		return iosAndroid;
	}

	public void setIosAndroid(String iosAndroid) {
		this.iosAndroid = iosAndroid;
	}

	@JsonIgnore
	public Boolean getAcceptedTAndC() {
		return acceptedTAndC;
	}

	public void setAcceptedTAndC(Boolean acceptedTAndC) {
		this.acceptedTAndC = acceptedTAndC;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public boolean isChgPwdNow() {
		return chgPwdNow;
	}

	public void setChgPwdNow(boolean chgPwdNow) {
		this.chgPwdNow = chgPwdNow;
	}

	@JsonIgnore
	public Accounts getAccounts() {
		return accounts;
	}

	public void setAccounts(Accounts accounts) {
		this.accounts = accounts;
	}

	public Set<UsersCost> getUsersCosts() {
		return usersCosts;
	}

	public void setUsersCosts(Set<UsersCost> usersCosts) {
		this.usersCosts = usersCosts;
	}

	public List<UsersCost> getUsersCostsList() {
		return new ArrayList<UsersCost>(usersCosts);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (uid == null) {
			if (other.uid != null)
				return false;
		} else if (!uid.equals(other.uid))
			return false;
		return true;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNiNumber() {
		return niNumber;
	}

	public void setNiNumber(String niNumber) {
		this.niNumber = niNumber;
	}

	/*
	 * public Address getHomeAddress() { return homeAddress; }
	 * 
	 * public void setHomeAddress(Address homeAddress) { this.homeAddress =
	 * homeAddress; }
	 */
	public EmployeeTypeEnum getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(EmployeeTypeEnum employeeType) {
		this.employeeType = employeeType;
	}

	public Set<Doc> getDocs() {
		return docs;
	}

	public void setDocs(Set<Doc> docs) {
		this.docs = docs;
	}

	@Transient
	public List<Doc> getDocsList() {
		return new ArrayList<Doc>(docs);
	}

	public Set<Notes> getNotes() {
		return notes;
	}

	public void setNotes(Set<Notes> notes) {
		this.notes = notes;
	}

	@Transient
	public List<Notes> getNotesList() {
		return new ArrayList<Notes>(notes);
	}

	public Images getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(Images profileImage) {
		this.profileImage = profileImage;
	}

	public Boolean getRecieveEmailTaskNotification() {
		return recieveEmailTaskNotification;
	}

	public void setRecieveEmailTaskNotification(Boolean recieveEmailTaskNotification) {
		this.recieveEmailTaskNotification = recieveEmailTaskNotification;
	}

	@Transient
	public String getFormattedName() {
		return firstName + " " + lastName;
	}

	public Address getPhysicalAddress() {
		return physicalAddress;
	}

	public void setPhysicalAddress(Address physicalAddress) {
		this.physicalAddress = physicalAddress;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Date getDateEmployed() {
		return dateEmployed;
	}

	public void setDateEmployed(Date dateEmployed) {
		this.dateEmployed = dateEmployed;
	}

	public Date getDateDischarged() {
		return dateDischarged;
	}

	public void setDateDischarged(Date dateDischarged) {
		this.dateDischarged = dateDischarged;
	}

	public String getEmployemntLenght() {
		return employemntLenght;
	}

	public void setEmployemntLenght(String employemntLenght) {
		this.employemntLenght = employemntLenght;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public Boolean getAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public String getCompanyNumber() {
		return companyNumber;
	}

	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}

	public Boolean getStillEmployed() {
		return stillEmployed;
	}

	public void setStillEmployed(Boolean stillEmployed) {
		this.stillEmployed = stillEmployed;
	}

}
