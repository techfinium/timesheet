package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import haj.com.framework.IDataEntity;



@Entity
@Table(name = "audit_event")
@DynamicInsert(value=true)
@DynamicUpdate(value=true)
public class AuditEvent implements IDataEntity
{
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name="EVENT_ID", nullable=false)
  private Integer eventId;
  
  @Column(name="EVENT_DATE", nullable=false)
  private Date eventDate;
  
  @Column(name="ORIG_IP", length=30)
  private String origIp;
  
  @Column(name="CALLING_CLASS", length=200)
  private String callingClass;
  
  @Column(name="CALLING_METHOD", length=100)
  private String callingMethod;
  
  @Column(name="USER_ID")
  private Integer userId;
  

  @Column(name="COMPANY_ID")
  private Long companyId;
  
  
  @Transient
  List<AuditEventParam> paramList;
  
  public AuditEvent()
  {
    
  }
  
  public AuditEvent(Map<String, Object> map)
  {
    paramList = new ArrayList<AuditEventParam>();
    
    origIp = processStringMapEntry(map, "ip");
    callingClass = processStringMapEntry(map, "callingClass");
    callingMethod = processStringMapEntry(map, "callingMethod");
    userId = processIntegerMapEntry(map, "userId");
    Integer t_companyId = processIntegerMapEntry(map, "companyId");
    if(t_companyId!=null) this.companyId = t_companyId.longValue();
    
    for (String key : map.keySet())
    {
      Object value = map.get(key);
      AuditEventParam param = new AuditEventParam();
      param.setParamName(key);
      if (value!=null)
        param.setParamValue(value.toString());
      paramList.add(param);
    }
  }
  
  private String processStringMapEntry(Map<String, Object> map, String key)
  {
    String dest = null;
    if (map.containsKey(key))
    {
      Object value = map.get(key);
      if (value!=null)
        dest = value.toString();
      map.remove(key);
    }
    return dest;
  }
  
  private Integer processIntegerMapEntry(Map<String, Object> map, String key)
  {
    Integer dest = null;
    if (map.containsKey(key))
    {
      Object value = map.get(key);
      if (value!=null)
      {
        try
        {
          dest = Integer.parseInt(value.toString());
        }
        catch (NumberFormatException e)
        {
          
        }
      }
      map.remove(key);
    }
    return dest;
  }

  public Integer getEventId()
  {
    return eventId;
  }

  public void setEventId(Integer eventId)
  {
    this.eventId = eventId;
  }

  public Date getEventDate()
  {
    return eventDate;
  }

  public void setEventDate(Date eventDate)
  {
    this.eventDate = eventDate;
  }

  public String getOrigIp()
  {
    return origIp;
  }

  public void setOrigIp(String origIp)
  {
    this.origIp = origIp;
  }

  public String getCallingClass()
  {
    return callingClass;
  }

  public void setCallingClass(String callingClass)
  {
    this.callingClass = callingClass;
  }

  public String getCallingMethod()
  {
    return callingMethod;
  }

  public void setCallingMethod(String callingMethod)
  {
    this.callingMethod = callingMethod;
  }

  public Integer getUserId()
  {
    return userId;
  }

  public void setUserId(Integer userId)
  {
    this.userId = userId;
  }



  public List<AuditEventParam> getParamList()
  {
    return paramList;
  }

  public void setParamList(List<AuditEventParam> paramList)
  {
    this.paramList = paramList;
  }


public Long getCompanyId() {
	return companyId;
}

public void setCompanyId(Long companyId) {
	this.companyId = companyId;
}
  
  
 
}
