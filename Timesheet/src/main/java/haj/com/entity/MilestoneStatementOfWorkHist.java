package haj.com.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "milestone_statement_of_work_HIST")
public class MilestoneStatementOfWorkHist implements IDataEntity, Cloneable {

	private static final long serialVersionUID = 1L;
	private MilestoneStatementOfWorkHistId id;
	private Byte revtype;
	private Date createDate;
	private Users updatedBy;
	private StatementOfWork sow;
	private Invoice invoice;
	private String milestoneDescription;
	private Date milestoneDate;
	private Double milestoneValue;
	private Long milestoneNum;
	private SowTypeEnum sowTypeEnum;
	private Revinfo revinfo;

	public MilestoneStatementOfWorkHist() {

	}

	public MilestoneStatementOfWorkHist(MilestoneStatementOfWorkHistId id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id", nullable = false)), @AttributeOverride(name = "rev", column = @Column(name = "REV", nullable = false)) })
	public MilestoneStatementOfWorkHistId getId() {
		return this.id;
	}

	public void setId(MilestoneStatementOfWorkHistId id) {
		this.id = id;
	}

	@Column(name = "REVTYPE")
	public Byte getRevtype() {
		return this.revtype;
	}

	public void setRevtype(Byte revtype) {
		this.revtype = revtype;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Transient
	public Revinfo getRevinfo() {
		return revinfo;
	}

	public void setRevinfo(Revinfo revinfo) {
		this.revinfo = revinfo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "sow_id", nullable = true)
	public StatementOfWork getSow() {
		return sow;
	}

	public void setSow(StatementOfWork sow) {
		this.sow = sow;
	}

	@Column(name = "milestone_description", columnDefinition = "LONGTEXT")
	public String getMilestoneDescription() {
		return milestoneDescription;
	}

	public void setMilestoneDescription(String milestoneDescription) {
		this.milestoneDescription = milestoneDescription;
	}

	@Column(name = "milestone_value")
	public Double getMilestoneValue() {
		return milestoneValue;
	}

	public void setMilestoneValue(Double milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

	@Enumerated
	@Column(name = "sow_type_enum", nullable = true)
	public SowTypeEnum getSowTypeEnum() {
		return sowTypeEnum;
	}

	public void setSowTypeEnum(SowTypeEnum sowTypeEnum) {
		this.sowTypeEnum = sowTypeEnum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "updated_by_id", nullable = true)
	public Users getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Users updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "milestone_date", length = 19)

	public Date getMilestoneDate() {
		return milestoneDate;
	}

	public void setMilestoneDate(Date milestoneDate) {
		this.milestoneDate = milestoneDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "invoice_id", nullable = true)
	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	@Column(name = "milestone_number")
	public Long getMilestoneNum() {
		return milestoneNum;
	}

	public void setMilestoneNum(Long milestoneNum) {
		this.milestoneNum = milestoneNum;
	}

}
