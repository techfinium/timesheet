package haj.com.entity.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.Visitor;
import haj.com.service.VisitorService;

public class VisitorDatamodel extends LazyDataModel<Visitor> {

	private static final long serialVersionUID = 1L;
	private List<Visitor> retorno = new ArrayList<Visitor>();
	private VisitorService service = new VisitorService();

	public VisitorDatamodel() {
		super();
	}

	@Override
	public List<Visitor> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		try {
			retorno = service.allVisitor(Visitor.class, first, pageSize, sortField, sortOrder, filters);
			setRowCount(service.count(Visitor.class, filters));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public Object getRowKey(Visitor obj) {
		return obj.getId();
	}

	@Override
	public Visitor getRowData(String rowKey) {
		for (Visitor obj : retorno) {
			if (obj.getId().equals(Long.valueOf(rowKey))) return obj;
		}
		return null;
	}

}
