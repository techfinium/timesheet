package haj.com.entity.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.MedicalScreeningForm;
import haj.com.service.MedicalScreeningFormService;

public class MedicalScreeningFormDatamodel extends LazyDataModel<MedicalScreeningForm> {

	private static final long serialVersionUID = 1L;
	private List<MedicalScreeningForm> retorno = new ArrayList<MedicalScreeningForm>();
	private MedicalScreeningFormService service = new MedicalScreeningFormService();

	public MedicalScreeningFormDatamodel() {
		super();
	}

	@Override
	public List<MedicalScreeningForm> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		try {
			retorno = service.allMedicalScreeningForm(MedicalScreeningForm.class, first, pageSize, sortField, sortOrder, filters);
			setRowCount(service.count(MedicalScreeningForm.class, filters));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public Object getRowKey(MedicalScreeningForm obj) {
		return obj.getId();
	}

	@Override
	public MedicalScreeningForm getRowData(String rowKey) {
		for (MedicalScreeningForm obj : retorno) {
			if (obj.getId().equals(Long.valueOf(rowKey))) return obj;
		}
		return null;
	}

}
