package haj.com.entity.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.VisitorDeclarationForm;
import haj.com.service.VisitorDeclarationFormService;

public class VisitorDeclarationFormDatamodel extends LazyDataModel<VisitorDeclarationForm> {

	private static final long serialVersionUID = 1L;
	private List<VisitorDeclarationForm> retorno = new ArrayList<VisitorDeclarationForm>();
	private VisitorDeclarationFormService service = new VisitorDeclarationFormService();

	public VisitorDeclarationFormDatamodel() {
		super();
	}

	@Override
	public List<VisitorDeclarationForm> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		try {
			retorno = service.allVisitorDeclarationForm(VisitorDeclarationForm.class, first, pageSize, sortField, sortOrder, filters);
			setRowCount(service.count(VisitorDeclarationForm.class, filters));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public Object getRowKey(VisitorDeclarationForm obj) {
		return obj.getId();
	}

	@Override
	public VisitorDeclarationForm getRowData(String rowKey) {
		for (VisitorDeclarationForm obj : retorno) {
			if (obj.getId().equals(Long.valueOf(rowKey))) return obj;
		}
		return null;
	}

}
