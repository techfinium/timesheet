package haj.com.entity.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.lookup.CronicDiseases;
import haj.com.service.lookup.CronicDiseasesService;

public class CronicDiseasesDatamodel extends LazyDataModel<CronicDiseases> {

	private static final long serialVersionUID = 1L;
	private List<CronicDiseases> retorno = new ArrayList<CronicDiseases>();
	private CronicDiseasesService service = new CronicDiseasesService();

	public CronicDiseasesDatamodel() {
		super();
	}

	@Override
	public List<CronicDiseases> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		try {
			retorno = service.allCronicDiseases(CronicDiseases.class, first, pageSize, sortField, sortOrder, filters);
			setRowCount(service.count(CronicDiseases.class, filters));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public Object getRowKey(CronicDiseases obj) {
		return obj.getId();
	}

	@Override
	public CronicDiseases getRowData(String rowKey) {
		for (CronicDiseases obj : retorno) {
			if (obj.getId().equals(Long.valueOf(rowKey))) return obj;
		}
		return null;
	}

}
