package haj.com.entity.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.MedicalScreeningFormCronicDiseases;
import haj.com.service.MedicalScreeningFormCronicDiseasesService;

public class MedicalScreeningFormCronicDiseasesDatamodel extends LazyDataModel<MedicalScreeningFormCronicDiseases> {

	private static final long serialVersionUID = 1L;
	private List<MedicalScreeningFormCronicDiseases> retorno = new ArrayList<MedicalScreeningFormCronicDiseases>();
	private MedicalScreeningFormCronicDiseasesService service = new MedicalScreeningFormCronicDiseasesService();

	public MedicalScreeningFormCronicDiseasesDatamodel() {
		super();
	}

	@Override
	public List<MedicalScreeningFormCronicDiseases> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		try {
			retorno = service.allMedicalScreeningFormCronicDiseases(MedicalScreeningFormCronicDiseases.class, first, pageSize, sortField, sortOrder, filters);
			setRowCount(service.count(MedicalScreeningFormCronicDiseases.class, filters));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public Object getRowKey(MedicalScreeningFormCronicDiseases obj) {
		return obj.getId();
	}

	@Override
	public MedicalScreeningFormCronicDiseases getRowData(String rowKey) {
		for (MedicalScreeningFormCronicDiseases obj : retorno) {
			if (obj.getId().equals(Long.valueOf(rowKey))) return obj;
		}
		return null;
	}

}
