package haj.com.entity.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.MonitoringRegister;
import haj.com.service.MonitoringRegisterService;

public class MonitoringRegisterDatamodel extends LazyDataModel<MonitoringRegister> {

	private static final long serialVersionUID = 1L;
	private List<MonitoringRegister> retorno = new ArrayList<MonitoringRegister>();
	private MonitoringRegisterService service = new MonitoringRegisterService();

	public MonitoringRegisterDatamodel() {
		super();
	}

	@Override
	public List<MonitoringRegister> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		try {
			retorno = service.allMonitoringRegister(MonitoringRegister.class, first, pageSize, sortField, sortOrder, filters);
			setRowCount(service.count(MonitoringRegister.class, filters));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public Object getRowKey(MonitoringRegister obj) {
		return obj.getId();
	}

	@Override
	public MonitoringRegister getRowData(String rowKey) {
		for (MonitoringRegister obj : retorno) {
			if (obj.getId().equals(Long.valueOf(rowKey))) return obj;
		}
		return null;
	}

}
