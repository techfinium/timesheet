package haj.com.entity.datamodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.ManagementStandard;
import haj.com.service.ManagementStandardService;

public class ManagementStandardDatamodel extends LazyDataModel<ManagementStandard> {

	private static final long serialVersionUID = 1L;
	private List<ManagementStandard> retorno = new ArrayList<ManagementStandard>();
	private ManagementStandardService service = new ManagementStandardService();

	public ManagementStandardDatamodel() {
		super();
	}

	@Override
	public List<ManagementStandard> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

		try {
			retorno = service.allManagementStandard(ManagementStandard.class, first, pageSize, sortField, sortOrder, filters);
			setRowCount(service.count(ManagementStandard.class, filters));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	@Override
	public Object getRowKey(ManagementStandard obj) {
		return obj.getId();
	}

	@Override
	public ManagementStandard getRowData(String rowKey) {
		for (ManagementStandard obj : retorno) {
			if (obj.getId().equals(Long.valueOf(rowKey))) return obj;
		}
		return null;
	}

}
