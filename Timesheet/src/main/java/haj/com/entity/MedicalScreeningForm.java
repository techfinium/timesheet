package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import haj.com.entity.enums.HealthStatusEnum;
import haj.com.entity.enums.YesNoEnum;
import haj.com.entity.lookup.CronicDiseases;
import haj.com.framework.IDataEntity;


@Entity
@Table(name = "medical_screening_form")
public class MedicalScreeningForm implements IDataEntity
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_date", length=19)
    private Date createDate;
	
/*	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="company_id", nullable=true)
	private Company company;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "order", cascade = CascadeType.ALL)
	private Set<OrderItems> orderItemses = new HashSet<OrderItems>(0);
	*/

    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id", nullable=false)
	private Users user;
    
    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="assessor_id", nullable=false)
	private Users assessor;
    

    @Column(name = "temperature")
    private Double temperature;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "ever_had_occupational_accident_or_disease")
    private YesNoEnum everHadOccupationalAccidentOrDisease;
    
    @Column(name = "discribe_occupational_accident_or_disease", length = 500)
    private String discribeOccupationalAccidentOrDisease;
    
    @Column(name = "suffer_from_croncic_disease")
    private Boolean sufferFromCronicDisease;
    
//    @Column(name = "hypertension")
//    private Boolean hypertension;
//    
//    @Column(name = "diabetes")
//    private Boolean diabetes;
//    
//    @Column(name = "epilipsy")
//    private Boolean epilipsy;
//    
//    @Column(name = "asthma")
//    private Boolean asthma;
//    
//    @Column(name = "tb")
//    private Boolean tb;
//    
//    @Column(name = "hiv_aids")
//    private Boolean hivAids;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "take_any_medication")
    private YesNoEnum takeAnyMedication;
    
    @Column(name = "discribe_medication", length = 500)
    private String discribeMedication;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "fever")
    private YesNoEnum fever;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "cough")
    private YesNoEnum cough;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "sore_throat")
    private YesNoEnum soreThroat;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "shotness_of_breath")
    private YesNoEnum shortnessOfBreath;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "contact_with_person_diagnosed_with_covid")
    private YesNoEnum contactWithPersonDiagnosedWithCovid;
    
    @Enumerated
    private HealthStatusEnum status;
    
    @Column(name = "employee_signature")
    private Boolean employeeSignature;
   
    
    @Transient
    private String lengthOfService;
    
    @Transient
    private List<MedicalScreeningFormCronicDiseases> medicalScreeningFormcronicDiseasesList;
    
    @Transient
    private MedicalScreeningFormCronicDiseases medicalScreeningFormCronicDiseases;
    
    @Transient
    private List<CronicDiseases> cronicDiseasesList;
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedicalScreeningForm other = (MedicalScreeningForm) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public YesNoEnum getEverHadOccupationalAccidentOrDisease() {
		return everHadOccupationalAccidentOrDisease;
	}

	public void setEverHadOccupationalAccidentOrDisease(YesNoEnum everHadOccupationalAccidentOrDisease) {
		this.everHadOccupationalAccidentOrDisease = everHadOccupationalAccidentOrDisease;
	}

	public String getDiscribeOccupationalAccidentOrDisease() {
		return discribeOccupationalAccidentOrDisease;
	}

	public void setDiscribeOccupationalAccidentOrDisease(String discribeOccupationalAccidentOrDisease) {
		this.discribeOccupationalAccidentOrDisease = discribeOccupationalAccidentOrDisease;
	}

	public YesNoEnum getTakeAnyMedication() {
		return takeAnyMedication;
	}

	public void setTakeAnyMedication(YesNoEnum takeAnyMedication) {
		this.takeAnyMedication = takeAnyMedication;
	}

	public String getDiscribeMedication() {
		return discribeMedication;
	}

	public void setDiscribeMedication(String discribeMedication) {
		this.discribeMedication = discribeMedication;
	}

	public YesNoEnum getFever() {
		return fever;
	}

	public void setFever(YesNoEnum fever) {
		this.fever = fever;
	}

	public YesNoEnum getCough() {
		return cough;
	}

	public void setCough(YesNoEnum cough) {
		this.cough = cough;
	}

	public YesNoEnum getSoreThroat() {
		return soreThroat;
	}

	public void setSoreThroat(YesNoEnum soreThroat) {
		this.soreThroat = soreThroat;
	}

	public YesNoEnum getShortnessOfBreath() {
		return shortnessOfBreath;
	}

	public void setShortnessOfBreath(YesNoEnum shortnessOfBreath) {
		this.shortnessOfBreath = shortnessOfBreath;
	}

	public YesNoEnum getContactWithPersonDiagnosedWithCovid() {
		return contactWithPersonDiagnosedWithCovid;
	}

	public void setContactWithPersonDiagnosedWithCovid(YesNoEnum contactWithPersonDiagnosedWithCovid) {
		this.contactWithPersonDiagnosedWithCovid = contactWithPersonDiagnosedWithCovid;
	}

	public HealthStatusEnum getStatus() {
		return status;
	}

	public void setStatus(HealthStatusEnum status) {
		this.status = status;
	}

	public Boolean getEmployeeSignature() {
		return employeeSignature;
	}

	public void setEmployeeSignature(Boolean employeeSignature) {
		this.employeeSignature = employeeSignature;
	}


	public String getLengthOfService() {
		return lengthOfService;
	}

	public void setLengthOfService(String lengthOfService) {
		this.lengthOfService = lengthOfService;
	}

	public Boolean getSufferFromCronicDisease() {
		return sufferFromCronicDisease;
	}

	public void setSufferFromCronicDisease(Boolean sufferFromCronicDisease) {
		this.sufferFromCronicDisease = sufferFromCronicDisease;
	}

	public Users getAssessor() {
		return assessor;
	}

	public void setAssessor(Users assessor) {
		this.assessor = assessor;
	}

	public List<MedicalScreeningFormCronicDiseases> getMedicalScreeningFormcronicDiseasesList() {
		return medicalScreeningFormcronicDiseasesList;
	}

	public void setMedicalScreeningFormcronicDiseasesList(List<MedicalScreeningFormCronicDiseases> medicalScreeningFormcronicDiseasesList) {
		this.medicalScreeningFormcronicDiseasesList = medicalScreeningFormcronicDiseasesList;
	}

	public List<CronicDiseases> getCronicDiseasesList() {
		return cronicDiseasesList;
	}

	public void setCronicDiseasesList(List<CronicDiseases> cronicDiseasesList) {
		this.cronicDiseasesList = cronicDiseasesList;
	}

	public MedicalScreeningFormCronicDiseases getMedicalScreeningFormCronicDiseases() {
		return medicalScreeningFormCronicDiseases;
	}

	public void setMedicalScreeningFormCronicDiseases(MedicalScreeningFormCronicDiseases medicalScreeningFormCronicDiseases) {
		this.medicalScreeningFormCronicDiseases = medicalScreeningFormCronicDiseases;
	}

}
