package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import haj.com.convertors.LocaleConverter;
import haj.com.entity.enums.FixedRollingEnum;
import haj.com.entity.enums.SowStatusEnum;
import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "statement_of_work")
@AuditTable(value = "statement_of_work_HIST")
@Audited
public class StatementOfWork implements IDataEntity, Cloneable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "project_id", nullable = true)
	private Projects projects;

	@Column(name = "statement_of_work_number", length = 100 , nullable = true)
	private String sowNumber;

	@Column(name = "title", length = 100, nullable = false)
	private String title;

	@Column(name = "description", columnDefinition = "LONGTEXT")
	private String description;

	@Enumerated
	@Column(name = "sow_type_enum", nullable = true)
	// 0 is TimeMaterials, 1 is Fixed
	private SowTypeEnum sowTypeEnum;

	@Column(name = "invoice_terms", length = 100, nullable = false)
	private String invoiceTerms;

	@Column(name = "payment_terms", length = 100, nullable = false)
	private String paymentTerms;

	@Column(name = "value_of_sow", nullable = true)
	private Double valueSow;
	
	@Column(name = "value_of_sow_exhange_rate")
	private Double valueOfSowExhangeRate;

	@Column(name = "locale")
	@Convert(converter = LocaleConverter.class)
	private Locale locale;

	@Enumerated
	@Column(name = "sow_status_enum", nullable = true)
	// 0 is Active, 1 is Inactive
	private SowStatusEnum sowStatusEnum;

	@NotAudited
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sow")
	@Fetch(FetchMode.JOIN)
	private Set<Doc> docs = new HashSet<Doc>(0);

	@NotAudited
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sow")
	@Fetch(FetchMode.JOIN)
	private Set<Notes> notes = new HashSet<Notes>(0);

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "user_id", nullable = true)
	private Users userSow;

	/** Time and Material */
	// 0 is Fixed, 1 is Rolling
	@Enumerated
	@Column(name = "fixed_period_rolling")
	private FixedRollingEnum fixedPeriod;

	/** Fixed Price */
	@Column(name = "payment_milestones", nullable = true)
	private Long paymentMilestones;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatementOfWork other = (StatementOfWork) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Projects getProjects() {
		return projects;
	}

	public void setProjects(Projects projects) {
		this.projects = projects;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SowTypeEnum getSowTypeEnum() {
		return sowTypeEnum;
	}

	public void setSowTypeEnum(SowTypeEnum sowTypeEnum) {
		this.sowTypeEnum = sowTypeEnum;
	}

	public String getInvoiceTerms() {
		return invoiceTerms;
	}

	public void setInvoiceTerms(String invoiceTerms) {
		this.invoiceTerms = invoiceTerms;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public SowStatusEnum getSowStatusEnum() {
		return sowStatusEnum;
	}

	public void setSowStatusEnum(SowStatusEnum sowStatusEnum) {
		this.sowStatusEnum = sowStatusEnum;
	}

	public Double getValueSow() {
		return valueSow;
	}

	public void setValueSow(Double valueSow) {
		this.valueSow = valueSow;
	}

	public FixedRollingEnum getFixedPeriod() {
		return fixedPeriod;
	}

	public void setFixedPeriod(FixedRollingEnum fixedPeriod) {
		this.fixedPeriod = fixedPeriod;
	}

	public Long getPaymentMilestones() {
		return paymentMilestones;
	}

	public void setPaymentMilestones(Long paymentMilestones) {
		this.paymentMilestones = paymentMilestones;
	}

	public Double getValueOfSowExhangeRate() {
		return valueOfSowExhangeRate;
	}

	public void setValueOfSowExhangeRate(Double valueOfSowExhangeRate) {
		this.valueOfSowExhangeRate = valueOfSowExhangeRate;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Set<Doc> getDocs() {
		return docs;
	}

	public void setDocs(Set<Doc> docs) {
		this.docs = docs;
	}

	@Transient
	public List<Doc> getDocsList() {
		return new ArrayList<Doc>(docs);
	}

	public Set<Notes> getNotes() {
		return notes;
	}

	public void setNotes(Set<Notes> notes) {
		this.notes = notes;
	}

	@Transient
	public List<Notes> getNotesList() {
		return new ArrayList<Notes>(notes);
	}

	public String getSowNumber() {
		return sowNumber;
	}

	public void setSowNumber(String sowNumber) {
		this.sowNumber = sowNumber;
	}

	public Users getUserSow() {
		return userSow;
	}

	public void setUserSow(Users userSow) {
		this.userSow = userSow;
	}

}
