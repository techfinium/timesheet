package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "employee_statement_of_work")
@AuditTable(value = "employee_statement_of_work_HIST")
@Audited
public class EmployeeStatementOfWork implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "employee_id", nullable = true)
	private Users employee;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "updated_by_id", nullable = true)
	private Users updatedBy;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "sow_id", nullable = true)
	private StatementOfWork sow;
	/** Time and Material */

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date", length = 19)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date", length = 19)
	private Date endDate;

	@Column(name = "charge_rate")
	private Double chargeRate;
	/** Time and Material */

	/** Fixed Price */

	@Column(name = "payment_milestone_pos", nullable = true)
	private Long paymentMilestonePos;

	@Column(name = "payment_milestone_tot", nullable = true)
	private Long paymentMilestoneTot;

	@Column(name = "milestone_description", columnDefinition = "LONGTEXT")
	private String milestoneDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fixed_price_date", length = 19)
	private Date fixedPriceDate;

	@Column(name = "milestone_value")
	private Double milestoneValue;

	@Enumerated
	@Column(name = "sow_type_enum", nullable = true)
	// 0 is TimeMaterials, 1 is Fixed
	private SowTypeEnum sowTypeEnum;

	/** Fixed Price */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeStatementOfWork other = (EmployeeStatementOfWork) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Users getEmployee() {
		return employee;
	}

	public void setEmployee(Users employee) {
		this.employee = employee;
	}

	public StatementOfWork getSow() {
		return sow;
	}

	public void setSow(StatementOfWork sow) {
		this.sow = sow;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getChargeRate() {
		return chargeRate;
	}

	public void setChargeRate(Double chargeRate) {
		this.chargeRate = chargeRate;
	}

	public String getMilestoneDescription() {
		return milestoneDescription;
	}

	public void setMilestoneDescription(String milestoneDescription) {
		this.milestoneDescription = milestoneDescription;
	}

	public Date getFixedPriceDate() {
		return fixedPriceDate;
	}

	public void setFixedPriceDate(Date fixedPriceDate) {
		this.fixedPriceDate = fixedPriceDate;
	}

	public Double getMilestoneValue() {
		return milestoneValue;
	}

	public void setMilestoneValue(Double milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

	public SowTypeEnum getSowTypeEnum() {
		return sowTypeEnum;
	}

	public void setSowTypeEnum(SowTypeEnum sowTypeEnum) {
		this.sowTypeEnum = sowTypeEnum;
	}

	public Long getPaymentMilestonePos() {
		return paymentMilestonePos;
	}

	public void setPaymentMilestonePos(Long paymentMilestonePos) {
		this.paymentMilestonePos = paymentMilestonePos;
	}

	public Long getPaymentMilestoneTot() {
		return paymentMilestoneTot;
	}

	public void setPaymentMilestoneTot(Long paymentMilestoneTot) {
		this.paymentMilestoneTot = paymentMilestoneTot;
	}

	public Users getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Users updatedBy) {
		this.updatedBy = updatedBy;
	}

}
