package haj.com.entity.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum ConfigDocProcessEnum.
 */
public enum ConfigDocProcessEnum {
	/** The sdf. 0 */
	Expenses("expenses") {
		@Override
		public String getType() {
			return "expenses";
		}

		@Override
		public String getRegistrationName() {
			return "expenses";
		}

		@Override
		public String getRedirectPage() {
			return "/pages/expensesapprovals.jsf";
		}
		

		@Override
		public String getTaskDescription() {
			return "sdf.task.description";
		}

		@Override
		public String getTaskTags() {
			return "#FIRST_NAME# #LAST_NAME# #IDENTITY_NUMBER# #EMAIL#";
		}
	};
	

	/** The display name. */
	private String displayName;

	/**
	 * Instantiates a new config doc process enum.
	 *
	 * @param displayNameX
	 *            the display name X
	 */
	private ConfigDocProcessEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/**
	 * Gets the friendly name.
	 *
	 * @return the friendly name
	 */
	public String getFriendlyName() {
		return displayName;
	}

	/**
	 * Gets the registration name.
	 *
	 * @return the registration name
	 */
	public String getRegistrationName() {
		return displayName;
	}

	/**
	 * Gets the redirect page.
	 *
	 * @return the redirect page
	 */
	public String getRedirectPage() {
		return displayName;
	}

	/**
	 * Gets the task description.
	 *
	 * @return the task description
	 */
	public String getTaskDescription() {
		return displayName;

	}

	public String getTaskTags() {
		return displayName;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return displayName;
	}

	public static final ConfigDocProcessEnum getConfigDocProcessEnumByValue(int value) {
		for (ConfigDocProcessEnum status : ConfigDocProcessEnum.values()) {
			if (status.ordinal() == value) return status;
		}
		return null;
	}
}
