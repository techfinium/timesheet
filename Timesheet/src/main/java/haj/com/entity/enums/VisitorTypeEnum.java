package haj.com.entity.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum RagEnum.
 */
public enum VisitorTypeEnum {

	/** The Red N. */
	New("New"){
		public String getRegistrationName() {
			return "new";
		}
   } , 
	
	/** The Amber N. */
	Existing("Existing"){
		public String getRegistrationName() {
			return "exsisting";
		}
   } ;

	/** The display name. */
	private String displayName;

	/**
	 * Instantiates a new rag enum.
	 *
	 * @param displayNameX the display name X
	 */
	private VisitorTypeEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/**
	 * Gets the friendly name.
	 *
	 * @return the friendly name
	 */
	public String getFriendlyName() {
		return toString();
	}
	
	/**
	 * Gets the registration name.
	 *
	 * @return the registration name
	 */
	public String getRegistrationName() {
		return displayName;
	}

}
