package haj.com.entity.enums;

public enum SowStatusEnum {

	Active("Active"), Inactive("Inactive");

	private String displayName;

	private SowStatusEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
