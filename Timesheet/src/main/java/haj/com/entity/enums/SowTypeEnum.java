package haj.com.entity.enums;

public enum SowTypeEnum {

	TimeMaterials("Time and Materials"), Fixed("Fixed Price");

	private String displayName;

	private SowTypeEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
