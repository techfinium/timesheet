package haj.com.entity.enums;

public enum EmployeeTypeEnum {

	Internal("Internal"), External("External");

  private String displayName;

  private EmployeeTypeEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
