package haj.com.entity.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum TaskStatusEnum.
 */
public enum TaskStatusEnum {

	/** The Not started. */
	NotStarted("Not Started") {
		public String getRegistrationName() {
			return "notstarted";
		}
	},

	/** The Underway. */
	Underway("Underway") {
		public String getRegistrationName() {
			return "underway";
		}
	},

	/** The Completed. */
	Completed("Completed") {
		public String getRegistrationName() {
			return "completed";
		}
	},

	/** The Closed. */
	Closed("Closed") {
		public String getRegistrationName() {
			return "closed";
		}
	},

	/** The Overdue. */
	Overdue("Overdue") {
		public String getRegistrationName() {
			return "overdue";
		}
	},

	/** The All. */
	/* For Reporting */
	All("All") {
		public String getRegistrationName() {
			return "all";
		}
	},

	ERROR("ERROR") {
		public String getRegistrationName() {
			return "error";
		}
	};

	/** The display name. */
	private String displayName;

	/**
	 * Instantiates a new task status enum.
	 *
	 * @param displayNameX
	 *            the display name X
	 */
	private TaskStatusEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/**
	 * Gets the friendly name.
	 *
	 * @return the friendly name
	 */
	public String getFriendlyName() {
		return displayName;
	}

	/**
	 * Gets the registration name.
	 *
	 * @return the registration name
	 */
	public String getRegistrationName() {
		return displayName;
	}
}
