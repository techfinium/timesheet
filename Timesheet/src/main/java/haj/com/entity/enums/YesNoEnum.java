package haj.com.entity.enums;

// TODO: Auto-generated Javadoc
/**
 * The Enum RagEnum.
 */
public enum YesNoEnum {

	/** The Red N. */
	Yes("Yes"){
		public String getRegistrationName() {
			return "yes";
		}
   } , 
	
	/** The Amber N. */
	No("No"){
		public String getRegistrationName() {
			return "no";
		}
   } ;

	/** The display name. */
	private String displayName;

	/**
	 * Instantiates a new rag enum.
	 *
	 * @param displayNameX the display name X
	 */
	private YesNoEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/**
	 * Gets the friendly name.
	 *
	 * @return the friendly name
	 */
	public String getFriendlyName() {
		return toString();
	}
	
	/**
	 * Gets the registration name.
	 *
	 * @return the registration name
	 */
	public String getRegistrationName() {
		return displayName;
	}

}
