package haj.com.entity.enums;

public enum EmployeeLevelEnum {

  GeneralUser("General User"), Manager("Manager"), Director("Director") ,Admin("Administrator");

  private String displayName;

  private EmployeeLevelEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
