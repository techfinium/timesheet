package haj.com.entity.enums;

public enum CarTypeEnum {

  Petrol("Petrol")
  , Diesel("Diesel")
  , Electric("Electric");

  private String displayName;

  private CarTypeEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
