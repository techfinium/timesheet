package haj.com.entity.enums;

public enum FixedRollingEnum {

	Fixed("Fixed Period"), Rolling("Rolling");

	private String displayName;

	private FixedRollingEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return displayName;
	}

	public String getFriendlyName() {
		return toString();
	}
}
