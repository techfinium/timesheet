package haj.com.entity.enums;

public enum ExpenseFormTypeEnum {

  ExpenseForm("Expense Form")
  , Entertainment("Entertainment Expenses")
  , Miscellaneous("Miscellaneous Expenses")
	, Travel("Travel Expenses");

  private String displayName;

  private ExpenseFormTypeEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
