package haj.com.entity.enums;

public enum DocumentTrackerEnum {

  Upload("Uploaded"), Downloaded("Downloaded"), UploadVersion("Uploaded a new version"), Viewed("Viewed");

  private String displayName;

  private DocumentTrackerEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
}
