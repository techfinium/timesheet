package haj.com.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "employee_statement_of_work_HIST")
public class EmployeeStatementOfWorkHist implements IDataEntity, Cloneable {

	private static final long serialVersionUID = 1L;
	private EmployeeStatementOfWorkHistId id;
	private Byte revtype;
	private Date createDate;
	private Users employee;
	private Users updatedBy;
	private StatementOfWork sow;
	private Date startDate;
	private Date endDate;
	private Double chargeRate;
	private Long paymentMilestonePos;
	private Long paymentMilestoneTot;
	private String milestoneDescription;
	private Date fixedPriceDate;
	private Double milestoneValue;
	private SowTypeEnum sowTypeEnum;
	private Revinfo revinfo;

	public EmployeeStatementOfWorkHist() {

	}

	public EmployeeStatementOfWorkHist(EmployeeStatementOfWorkHistId id) {
		this.id = id;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id", nullable = false)), @AttributeOverride(name = "rev", column = @Column(name = "REV", nullable = false)) })
	public EmployeeStatementOfWorkHistId getId() {
		return this.id;
	}

	public void setId(EmployeeStatementOfWorkHistId id) {
		this.id = id;
	}

	@Column(name = "REVTYPE")
	public Byte getRevtype() {
		return this.revtype;
	}

	public void setRevtype(Byte revtype) {
		this.revtype = revtype;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Transient
	public Revinfo getRevinfo() {
		return revinfo;
	}

	public void setRevinfo(Revinfo revinfo) {
		this.revinfo = revinfo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "employee_id", nullable = true)
	public Users getEmployee() {
		return employee;
	}

	public void setEmployee(Users employee) {
		this.employee = employee;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "sow_id", nullable = true)
	public StatementOfWork getSow() {
		return sow;
	}

	public void setSow(StatementOfWork sow) {
		this.sow = sow;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date", length = 19)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date", length = 19)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "charge_rate")
	public Double getChargeRate() {
		return chargeRate;
	}

	public void setChargeRate(Double chargeRate) {
		this.chargeRate = chargeRate;
	}

	@Column(name = "payment_milestone_pos", nullable = true)
	public Long getPaymentMilestonePos() {
		return paymentMilestonePos;
	}

	public void setPaymentMilestonePos(Long paymentMilestonePos) {
		this.paymentMilestonePos = paymentMilestonePos;
	}

	@Column(name = "payment_milestone_tot", nullable = true)
	public Long getPaymentMilestoneTot() {
		return paymentMilestoneTot;
	}

	public void setPaymentMilestoneTot(Long paymentMilestoneTot) {
		this.paymentMilestoneTot = paymentMilestoneTot;
	}

	@Column(name = "milestone_description", columnDefinition = "LONGTEXT")
	public String getMilestoneDescription() {
		return milestoneDescription;
	}

	public void setMilestoneDescription(String milestoneDescription) {
		this.milestoneDescription = milestoneDescription;
	}

	public Date getFixedPriceDate() {
		return fixedPriceDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fixed_price_date", length = 19)
	public void setFixedPriceDate(Date fixedPriceDate) {
		this.fixedPriceDate = fixedPriceDate;
	}

	@Column(name = "milestone_value")
	public Double getMilestoneValue() {
		return milestoneValue;
	}

	public void setMilestoneValue(Double milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

	@Enumerated
	@Column(name = "sow_type_enum", nullable = true)
	public SowTypeEnum getSowTypeEnum() {
		return sowTypeEnum;
	}

	public void setSowTypeEnum(SowTypeEnum sowTypeEnum) {
		this.sowTypeEnum = sowTypeEnum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "updated_by_id", nullable = true)
	public Users getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Users updatedBy) {
		this.updatedBy = updatedBy;
	}

}
