package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import haj.com.entity.enums.VisitorTypeEnum;
import haj.com.entity.enums.YesNoEnum;
import haj.com.framework.IDataEntity;


@Entity
@Table(name = "visitor_declaration_form")
public class VisitorDeclarationForm implements IDataEntity
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="create_date", length=19)
    private Date createDate;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="visitor_id", nullable=true)
	private Visitor visitor;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="assessor_id", nullable=true)
	private Users assessor;

    @Enumerated(EnumType.STRING)
    @Column(name = "contact_with_person_diagnosed_with_covid")
    private YesNoEnum contactWithPersonDiagnosedWithCovid;

    @Enumerated(EnumType.STRING)
    @Column(name = "traveled")
    private YesNoEnum traveled;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "contact_with_person_traveled")
    private YesNoEnum contactWithPersonTraveled;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "fever")
    private YesNoEnum fever;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "cough")
    private YesNoEnum cough;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "sore_throat")
    private YesNoEnum soreThroat;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "shotness_of_breath")
    private YesNoEnum shortnessOfBreath;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "runny_nose")
    private YesNoEnum runnyNose;
    
    @Column(name = "acknoladgement" )
    private Boolean acknoleedgement;
    
	@Enumerated
	@Transient
	private VisitorTypeEnum visitorType;
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitorDeclarationForm other = (VisitorDeclarationForm) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public YesNoEnum getContactWithPersonDiagnosedWithCovid() {
		return contactWithPersonDiagnosedWithCovid;
	}

	public void setContactWithPersonDiagnosedWithCovid(YesNoEnum contactWithPersonDiagnosedWithCovid) {
		this.contactWithPersonDiagnosedWithCovid = contactWithPersonDiagnosedWithCovid;
	}

	public YesNoEnum getTraveled() {
		return traveled;
	}

	public void setTraveled(YesNoEnum traveled) {
		this.traveled = traveled;
	}

	public YesNoEnum getContactWithPersonTraveled() {
		return contactWithPersonTraveled;
	}

	public void setContactWithPersonTraveled(YesNoEnum contactWithPersonTraveled) {
		this.contactWithPersonTraveled = contactWithPersonTraveled;
	}

	public YesNoEnum getFever() {
		return fever;
	}

	public void setFever(YesNoEnum fever) {
		this.fever = fever;
	}

	public YesNoEnum getCough() {
		return cough;
	}

	public void setCough(YesNoEnum cough) {
		this.cough = cough;
	}

	public YesNoEnum getSoreThroat() {
		return soreThroat;
	}

	public void setSoreThroat(YesNoEnum soreThroat) {
		this.soreThroat = soreThroat;
	}

	public YesNoEnum getShortnessOfBreath() {
		return shortnessOfBreath;
	}

	public void setShortnessOfBreath(YesNoEnum shortnessOfBreath) {
		this.shortnessOfBreath = shortnessOfBreath;
	}

	public YesNoEnum getRunnyNose() {
		return runnyNose;
	}

	public void setRunnyNose(YesNoEnum runnyNose) {
		this.runnyNose = runnyNose;
	}

	public Visitor getVisitor() {
		return visitor;
	}

	public void setVisitor(Visitor visitor) {
		this.visitor = visitor;
	}

	public Boolean getAcknoleedgement() {
		return acknoleedgement;
	}

	public void setAcknoleedgement(Boolean acknoleedgement) {
		this.acknoleedgement = acknoleedgement;
	}

	public Users getAssessor() {
		return assessor;
	}

	public void setAssessor(Users assessor) {
		this.assessor = assessor;
	}

	public VisitorTypeEnum getVisitorType() {
		return visitorType;
	}

	public void setVisitorType(VisitorTypeEnum visitorType) {
		this.visitorType = visitorType;
	}



}
