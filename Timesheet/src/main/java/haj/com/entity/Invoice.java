package haj.com.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import haj.com.convertors.LocaleConverter;
import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.IDataEntity;

@Entity
@Table(name = "invoice")
@AuditTable(value = "invoice_HIST")
@Audited
public class Invoice implements IDataEntity {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "user_id", insertable = true, updatable = true, nullable = true)
	private Users createdBy;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;

	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "statement_of_work_id", insertable = true, updatable = true, nullable = true)
	private StatementOfWork statementOfWork;
	
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	@ManyToOne(fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	@JoinColumn(name = "milestone_statement_of_work_id", insertable = true, updatable = true, nullable = true)
	private MilestoneStatementOfWork milestoneStatementOfWork;

	@Column(name = "invoice_number", length = 100)
	private String invoiceNumber;

	@Column(name = "purchase_order_number", length = 100)
	private String purchaseOrderNumber;

	@Column(name = "locale")
	@Convert(converter = LocaleConverter.class)
	private Locale locale;

	@Column(name = "exhange_rate")
	private Double exhangeRate;

	@Column(name = "invoice_value")
	private Double invoiceValue;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "from_date", length = 19)
	private Date fromDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "to_date", length = 19)
	private Date toDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "invoice_date", length = 19)
	private Date invoiceDate;

	@Column(name = "paid")
	private Boolean paid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "paid_date", length = 19)
	private Date paidDate;

	@Column(name = "paid_exhange_rate")
	private Double paidExhangeRate;
	
	@Column(name = "sow_value")
	private Double sowValue;
	
	@Column(name = "employee_value")
	private Double employeeValue;
	
	@Column(name = "milestone_value")
	private Double milestoneValue;

	@NotAudited
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
	@Fetch(FetchMode.JOIN)
	private Set<Doc> docs = new HashSet<Doc>(0);

	@NotAudited
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
	@Fetch(FetchMode.JOIN)
	private Set<Notes> notes = new HashSet<Notes>(0);

	@Transient
	private Currency currency;
	
	@Enumerated
	@Column(name = "sow_type_enum", nullable = true)
	// 0 is TimeMaterials, 1 is Fixed
	private SowTypeEnum sowTypeEnum;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invoice other = (Invoice) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Users getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Users createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public StatementOfWork getStatementOfWork() {
		return statementOfWork;
	}

	public void setStatementOfWork(StatementOfWork statementOfWork) {
		this.statementOfWork = statementOfWork;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Double getExhangeRate() {
		return exhangeRate;
	}

	public void setExhangeRate(Double exhangeRate) {
		this.exhangeRate = exhangeRate;
	}

	public Double getInvoiceValue() {
		return invoiceValue;
	}

	public void setInvoiceValue(Double invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Boolean getPaid() {
		return paid;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public Double getPaidExhangeRate() {
		return paidExhangeRate;
	}

	public void setPaidExhangeRate(Double paidExhangeRate) {
		this.paidExhangeRate = paidExhangeRate;
	}

	public Set<Doc> getDocs() {
		return docs;
	}

	public void setDocs(Set<Doc> docs) {
		this.docs = docs;
	}

	@Transient
	public List<Doc> getDocsList() {
		return new ArrayList<Doc>(docs);
	}

	public Set<Notes> getNotes() {
		return notes;
	}

	public void setNotes(Set<Notes> notes) {
		this.notes = notes;
	}

	@Transient
	public List<Notes> getNotesList() {
		return new ArrayList<Notes>(notes);
	}

	public Currency getCurrency() {
		if (locale != null) {
			currency = Currency.getInstance(locale);
		} else {
			currency = Currency.getInstance(Locale.UK);
		}
		return currency;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public SowTypeEnum getSowTypeEnum() {
		return sowTypeEnum;
	}

	public void setSowTypeEnum(SowTypeEnum sowTypeEnum) {
		this.sowTypeEnum = sowTypeEnum;
	}

	public Double getSowValue() {
		return sowValue;
	}

	public void setSowValue(Double sowValue) {
		this.sowValue = sowValue;
	}

	public Double getEmployeeValue() {
		return employeeValue;
	}

	public void setEmployeeValue(Double employeeValue) {
		this.employeeValue = employeeValue;
	}

	public MilestoneStatementOfWork getMilestoneStatementOfWork() {
		return milestoneStatementOfWork;
	}

	public void setMilestoneStatementOfWork(MilestoneStatementOfWork milestoneStatementOfWork) {
		this.milestoneStatementOfWork = milestoneStatementOfWork;
	}

	public Double getMilestoneValue() {
		return milestoneValue;
	}

	public void setMilestoneValue(Double milestoneValue) {
		this.milestoneValue = milestoneValue;
	}

}
