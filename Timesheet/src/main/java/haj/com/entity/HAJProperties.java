package haj.com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import haj.com.framework.IDataEntity;

@Entity
@Table(name = "haj_properties")
public class HAJProperties  implements IDataEntity
{
	private static final long serialVersionUID = 1L;

  
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @Column(name="e_property_id")
  private Long ePropertyId;
  
  @Column(name="e_property", length=100, nullable=false)
  private String eProperty;

  @Column(name="e_value", length=100, nullable=true)
  private String eValue;
  
  
  public HAJProperties() {}


public Long getePropertyId() {
	return ePropertyId;
}


public void setePropertyId(Long ePropertyId) {
	this.ePropertyId = ePropertyId;
}


public String geteProperty() {
	if (eProperty==null) eProperty ="";
	else eProperty = eProperty.trim();
	return eProperty;
}


public void seteProperty(String eProperty) {
	this.eProperty = eProperty;
}


public String geteValue() {
	if (eValue==null) eValue ="";
	else eValue = eValue.trim();
	return eValue;
}


public void seteValue(String eValue) {
	this.eValue = eValue;
}

 
  
  
}
