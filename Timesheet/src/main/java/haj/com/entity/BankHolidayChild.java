package haj.com.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import haj.com.framework.IDataEntity;


@Entity
@Table(name = "bank_holiday_child")
public class BankHolidayChild implements IDataEntity
{
	private static final long serialVersionUID = 1L;
	@Id
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="bank_holiday", length=19)
    private Date bankHoliday;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="bank_holiday_parent", nullable=true)
	private BankHolidayParent bankHolidayParent;
	
	@Column(name="holiday_names")
	private String holidayNames;
	
	@Column(name="repeat_dates")
	private Boolean repeatDates;
	
	@Transient
	private String cssStyle;
	
	
	public Date getBankHoliday() {
		return bankHoliday;
	}

	public void setBankHoliday(Date bankHoliday) {
		this.bankHoliday = bankHoliday;
	}

	public BankHolidayParent getBankHolidayParent() {
		return bankHolidayParent;
	}

	public void setBankHolidayParent(BankHolidayParent bankHolidayParent) {
		this.bankHolidayParent = bankHolidayParent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankHoliday == null) ? 0 : bankHoliday.hashCode());
		result = prime * result + ((bankHolidayParent == null) ? 0 : bankHolidayParent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankHolidayChild other = (BankHolidayChild) obj;
		if (bankHoliday == null) {
			if (other.bankHoliday != null)
				return false;
		} else if (!bankHoliday.equals(other.bankHoliday))
			return false;
		if (bankHolidayParent == null) {
			if (other.bankHolidayParent != null)
				return false;
		} else if (!bankHolidayParent.equals(other.bankHolidayParent))
			return false;
		return true;
	}

	public String getCssStyle() {
		return cssStyle;
	}

	public void setCssStyle(String cssStyle) {
		this.cssStyle = cssStyle;
	}

	public String getHolidayNames() {
		return holidayNames;
	}

	public void setHolidayNames(String holidayNames) {
		this.holidayNames = holidayNames;
	}

	public Boolean getRepeatDates() {
		return repeatDates;
	}

	public void setRepeatDates(Boolean repeatDates) {
		this.repeatDates = repeatDates;
	}
	
	
	


}
