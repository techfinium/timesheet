package haj.com.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class SecurityCheckFilter implements Filter
{
  private final static String FILTER_APPLIED = "_security_filter_applied";
  private static final Logger logger = Logger.getLogger(SecurityCheckFilter.class);

  @Override
  public void init(FilterConfig filterConfig) throws ServletException
  {

  }

  @Override
  public void destroy()
  {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
  {
    if (request.getAttribute(FILTER_APPLIED) != null)
    {
      chain.doFilter(request, response);
      return;
    }

    HttpServletRequest hreq = (HttpServletRequest) request;
    HttpServletResponse hres = (HttpServletResponse) response;
    HttpSession session = hreq.getSession();
    String contextPath = hreq.getContextPath();
    String checkforloginpage = hreq.getRequestURI();
    
	//No Caching.
	hres.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
	hres.setHeader("Pragma", "no-cache"); // HTTP 1.0.
	hres.setDateHeader("Expires", 0); // Proxies.

	if(session.getAttribute("logMeOff") != null){
		if((Boolean)session.getAttribute("logMeOff")){
			session.setAttribute("uobj", null);
			session.setAttribute("logMeOff", null);				
		}
	}
	
	if (request.getAttribute(FILTER_APPLIED) != null){
		chain.doFilter(request, response);
		return;
	}
    
    if ((request.getAttribute(FILTER_APPLIED) == null) 
        && (!checkforloginpage.endsWith("login.jsf")) 
     //   && (!checkforloginpage.endsWith("registration.jsf"))
        && (!checkforloginpage.endsWith("welcome.jsf"))
    //    && (!checkforloginpage.endsWith("momentumValidate.jsf")) 
    //    && (!checkforloginpage.endsWith("momentumWeb.jsf"))
    //    && (!checkforloginpage.endsWith("momentumConfirm.jsf")) 
    //    && (!checkforloginpage.endsWith("registeredMomentum.jsf"))
        && (checkforloginpage.endsWith(".jsf")))
    {
      request.setAttribute(FILTER_APPLIED, Boolean.TRUE);

      // If the session bean is not null get the session bean property username.
      Object user = null;
      user = session.getAttribute("uobj");

		if(user==null) {
			//Handles AJAX redirect
			if ("partial/ajax".equals(hreq.getHeader("Faces-Request"))) {
				hres.setContentType("text/xml");
				hres.getWriter()
			        .append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
			        .printf("<partial-response><redirect url=\"%s\"></redirect></partial-response>", contextPath+"/login.jsf");
				return;
			}
			//Normal Redirect
			else{
				//hreq.getRequestDispatcher("logon.jsf").forward(request, response);
				logger.debug("authorization breeched");
				if (checkforloginpage.contains("mobile")) hres.sendRedirect(contextPath+"/mobile/index.jsp");
				else	
				hres.sendRedirect(contextPath+"/login.jsf");
				//hres.sendRedirect("http://www.metra.co.za");
			return;
			}
		}	

    }
    // deliver request to next filter
    chain.doFilter(request, response);
  }

}
