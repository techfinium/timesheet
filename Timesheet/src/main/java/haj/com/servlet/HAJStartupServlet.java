package haj.com.servlet;

import java.util.List;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.dao.HAJPropertiesDAO;
import haj.com.entity.HAJProperties;
import haj.com.jobs.HAJScheduler;



public class HAJStartupServlet extends HttpServlet
{
  private static final long serialVersionUID = 1L;

  protected final Log logger = LogFactory.getLog(this.getClass());

  @Override
  public void init(ServletConfig config) throws ServletException
  {
	    // init super
	    super.init(config);
	      System.getProperties().put("TK-APP-PATH",super.getServletContext().getRealPath("/"));
	  	 System.getProperties().put("TK-SERVER-TYPE",super.getServletContext().getServerInfo());
		     java.util.Properties prop = new java.util.Properties();
		    loadpropsFromDB(prop);
		 System.getProperties().put("TK-PROPERTIES", prop);
	    loadConstants();
	    startScheduler();
	 
	    // cleanup file system
	    //cleanupFileSystem();
  }



private void loadpropsFromDB(Properties prop) {
	try {
		List<HAJProperties> list =  new HAJPropertiesDAO().allProps();
		for (HAJProperties p : list) {
			try {
				prop.put(p.geteProperty().trim(), p.geteValue().trim());
			} catch (Exception e) {
				logger.fatal(e);
			}
			
		}
		
	} catch (Exception e) {
		logger.fatal(e);
	}
	
}
  protected void loadConstants()
  {
    logger.info("in loadConstants()");
   
  }
  
  private void startScheduler() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				
				try {
					logger.info("HAJStartupServlet: starting scheduler");
					new HAJScheduler().run();
					logger.info("HAJStartupServlet: scheduler started");
				} catch (Exception e) {
				  logger.fatal(e);
				}
			}
		}).start();	
	
}
}
