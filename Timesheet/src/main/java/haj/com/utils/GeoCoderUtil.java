package haj.com.utils;

import java.io.Serializable;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.LatLng;

public class GeoCoderUtil implements Serializable {
	
	public static final Geocoder geocoder = new Geocoder();
	
	public static LatLng calcLatLng(String address) throws Exception {
		LatLng ll = null;
		address += ", South Africa";
		
		//via Proxy
	/*	final AdvancedGeoCoder advancedGeoCoder = new AdvancedGeoCoder();
		advancedGeoCoder.getHttpClient().getHostConfiguration().setProxy("172.17.4.37", 8080);
		GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(address).setLanguage("en").getGeocoderRequest();
		GeocodeResponse geocoderResponse = advancedGeoCoder.geocode(geocoderRequest);
	*/	
		GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(address).setLanguage("en").getGeocoderRequest();
		GeocodeResponse geocoderResponse = geocoder.geocode(geocoderRequest);
		for (GeocoderResult result : geocoderResponse.getResults()){
			ll = result.getGeometry().getLocation();
		}
		return ll;
	}
}
