package haj.com.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibm.icu.text.SimpleDateFormat;

import haj.com.annotations.CSVAnnotation;

public class CSVUtil {

	private final Log logger = LogFactory.getLog(CSVUtil.class);
	private String cvsSplitBy = ",";
	private final char DEFAULT_SEPARATOR = ',';
	private final char DEFAULT_QUOTE = '"';

	private Map<String, Map<String, Object>> readCSV(InputStream inputStream) {
		Map<String, Map<String, Object>> values = new LinkedHashMap<String, Map<String, Object>>();
		List<String> headings = new LinkedList<>();
		BufferedReader br = null;
		String line = "";
		try {
			boolean heading = true;
			int count = 1;
			br = new BufferedReader(new InputStreamReader(inputStream));
			while ((line = br.readLine()) != null) {
				int col = 0;
				if (heading) {
					String[] strings =line.split(cvsSplitBy); 
					for (String string : strings) {
						headings.add(string.trim());
					}
					heading = false;
				} else {
					Map<String, Object> csvValue = new HashMap<>();
					for (String string : parseLine(line, cvsSplitBy.charAt(0), DEFAULT_QUOTE)) {
						csvValue.put(headings.get(col), string);
						col++;
					}
					values.put("object" + count, csvValue);
					count++;
				}
			}
			logger.info(values);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return values;
	}

	public List<Object> getObjects(Class<?> outputClass, InputStream inputStream, String delimeter) {
		List<Object> policies = new ArrayList<Object>();
		if (!delimeter.isEmpty())
			cvsSplitBy = delimeter;
		try {
			Map<String, Map<String, Object>> values = readCSV(inputStream);
			for (Entry<String, Map<String, Object>> entry : values.entrySet()) {
				Object policy = outputClass.newInstance();
				List<Field> fields = Arrays.asList(outputClass.getDeclaredFields());
				for (Field field : fields) {
					if (field.isAnnotationPresent(CSVAnnotation.class)) {
						field.setAccessible(true);
						Annotation annotation = field.getAnnotation(CSVAnnotation.class);
						CSVAnnotation testerInfo = (CSVAnnotation) annotation;
						Object objs = entry.getValue().get(testerInfo.name());
						if (testerInfo.name().equals("PolicyHolder")) {
							System.out.println();
						}
						if (objs == null || ("" + objs).isEmpty())
							field.set(policy, null);
						else if (testerInfo.className().equals(Date.class))
							field.set(policy, new SimpleDateFormat(testerInfo.datePattern()).parse("" + objs));
						else if (testerInfo.className().equals(Double.class))
							field.set(policy, Double.parseDouble("" + objs));
						else if (testerInfo.className().equals(Integer.class))
							field.set(policy, Integer.parseInt("" + objs));
						else if (testerInfo.className().equals(Long.class))
							field.set(policy, Long.parseLong("" + objs));
						else
							field.set(policy, testerInfo.className().cast(objs));
					}
				}
				policies.add(policy);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return policies;
	}

	public List<String> parseLine(String cvsLine, char separators, char customQuote) {
		List<String> result = new ArrayList<>();
		// if empty, return!
		if (cvsLine == null || cvsLine.isEmpty()) {
			return result;
		}
		if (customQuote == ' ') {
			customQuote = DEFAULT_QUOTE;
		}
		if (separators == ' ') {
			separators = DEFAULT_SEPARATOR;
		}
		StringBuffer curVal = new StringBuffer();
		boolean inQuotes = false;
		boolean startCollectChar = false;
		boolean doubleQuotesInColumn = false;
		char[] chars = cvsLine.toCharArray();
		for (char ch : chars) {
			if (inQuotes) {
				startCollectChar = true;
				if (ch == customQuote) {
					inQuotes = false;
					doubleQuotesInColumn = false;
				} else {
					// Fixed : allow "" in custom quote enclosed
					if (ch == '\"') {
						if (!doubleQuotesInColumn) {
							curVal.append(ch);
							doubleQuotesInColumn = true;
						}
					} else {
						curVal.append(ch);
					}

				}
			} else {
				if (ch == customQuote) {
					inQuotes = true;
					// Fixed : allow "" in empty quote enclosed
					if (chars[0] != '"' && customQuote == '\"') {
						curVal.append('"');
					}
					// double quotes in column will hit this!
					if (startCollectChar) {
						curVal.append('"');
					}

				} else if (ch == separators) {
					result.add(curVal.toString());
					curVal = new StringBuffer();
					startCollectChar = false;
				} else if (ch == '\r') {
					// ignore LF characters
					continue;
				} else if (ch == '\n') {
					// the end, break!
					break;
				} else {
					curVal.append(ch);
				}
			}

		}
		result.add(curVal.toString());
		return result;
	}

}
