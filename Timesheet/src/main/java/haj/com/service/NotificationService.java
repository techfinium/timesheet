package haj.com.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import haj.com.constants.HAJConstants;
import haj.com.dao.CompanyUsersDAO;
import haj.com.dao.TimesheetDetailsDAO;
import haj.com.entity.CompanyUsers;
import haj.com.entity.TimesheetDetails;
import haj.com.framework.AbstractService;
import haj.com.utils.GenericUtility;

public class NotificationService extends AbstractService {

	private CompanyUsersDAO companyusersdao = new CompanyUsersDAO();
	private TimesheetDetailsDAO tsdetailsdao = new TimesheetDetailsDAO();
	private List<CompanyUsers> users = null;

	public List<CompanyUsers> allCompanyUsers() throws Exception {
		return companyusersdao.allCompanyUsers();
	}

	public List<CompanyUsers> allCompanyUsersNotAdmin() throws Exception {
		return companyusersdao.allCompanyUsersNotAdmin();
	}

	public List<TimesheetDetails> findByTimesheetBetweenDatesUser(CompanyUsers user, Date startDate, Date endDate)
			throws Exception {
		return tsdetailsdao.findByTimesheetBetweenDatesUser(user, startDate, endDate);
	}

	public void populateUsers() {
		try {
			users = allCompanyUsersNotAdmin();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void checkReminder() {

		Date lastweek = GenericUtility.deductDaysFromDate(new Date(), 7);
		Date beginDate = GenericUtility.getFirstDayOfWeek(lastweek);
		Date endDate = GenericUtility.getLastDayOfWeek(lastweek);

		try {
			
			for (CompanyUsers companyUsers : users) {
				
				List<TimesheetDetails> timesheetDetails = findByTimesheetBetweenDatesUser(companyUsers, beginDate,
						endDate);
				boolean complete = true;
				
				for (TimesheetDetails tsDetails : timesheetDetails) {					
//					To be changed when there is a complete status added
					System.out.println(tsDetails.getId());
					if(tsDetails.getHours()==0 || tsDetails.getHours() == null)
					{
						complete = false;
					}
				}
				
				if(!complete)
				{
					sendReminderEmail(companyUsers);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendReminderEmail(CompanyUsers compUser) {

		String email = compUser.getUsers().getEmail();
		String name = compUser.getUsers().getFirstName() + " " + compUser.getUsers().getLastName();

		String message = "<p>Dear #NAME#,</p>" + "<br/>"
				+ "<p>It appears you have not completed your Timesheet for last week.</p>" + "<p>Please " + "<a href=\""
				+ HAJConstants.PL_LINK + "\">Login</a> to complete it.</p>" + "<p>Regards</p>"
				+ "<p>The Timesheet team</p>" + "<br/>";
		message = message.replaceAll("#NAME#", name);

		GenericUtility.sendMail(email, "Timesheet incomplete notification", message);
	}

	public void sendWeeklyEmail() {
		Date date = new Date();
		if (date == GenericUtility.getLastDayOfWeek(date) || date == GenericUtility.getLasttDayOfMonth(date)) {
			for (CompanyUsers companyUsers : users) {
				String email = companyUsers.getUsers().getEmail();
				String name = companyUsers.getUsers().getFirstName() + " " + companyUsers.getUsers().getLastName();

				String message = "<p>Dear #NAME#,</p>" + "<br/>"
						+ "<p>This is a reminder to complete your Timesheet for this week.</p>" + "<p>Please "
						+ "<a href=\"" + HAJConstants.PL_LINK + "\">Login</a> to complete it.</p>" + "<p>Regards</p>"
						+ "<p>The Timesheet team</p>" + "<br/>";
				message = message.replaceAll("#NAME#", name);

				GenericUtility.sendMail(email, "Timesheet reminder", message);
			}
		}
	}
	
	public Integer getWeekNumber(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.WEEK_OF_MONTH);
	}

	public void run() {
		populateUsers();
		sendWeeklyEmail();
		checkReminder();
	}

}
