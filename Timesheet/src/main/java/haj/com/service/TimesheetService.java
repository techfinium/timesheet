package haj.com.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import haj.com.dao.TimesheetDAO;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Timesheet;
import haj.com.entity.TimesheetDetails;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;
import haj.com.utils.GenericUtility;

public class TimesheetService extends AbstractService {

	private TimesheetDetailsService service = new TimesheetDetailsService();
	private TimesheetDAO dao = new TimesheetDAO();
	private UsersService usersService = new UsersService();
	private CompanyUsersService companyUsersService = new CompanyUsersService();

	public List<Timesheet> allTimesheet() throws Exception {
		return totHours(dao.allTimesheet());
	}

	public List<Timesheet> byCompany(long companyId) throws Exception {
		return totHours(dao.byCompany(companyId));
	}

	/*
	 * public List<Timesheet> byCompany(Company company) throws Exception {
	 * return dao.byCompany(company.getId()); }
	 */

	public void create(Timesheet entity) throws Exception {
		this.dao.create(entity);
	}

	public void update(Timesheet entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(Timesheet entity) throws Exception {
		this.dao.delete(entity);
	}

	public Timesheet findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<Timesheet> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}


	public List<Timesheet> findByCompanyUserCal(CompanyUsers companyUser) throws Exception {
		return dao.findByCompanyUser(companyUser.getId());

	}
	
	public List<Timesheet> findByCompanyUser(CompanyUsers companyUser) throws Exception {
		return totHours(dao.findByCompanyUser(companyUser.getId()));

	}

	public List<Timesheet> findByCompanyUser(CompanyUsers companyUser, Date startDate, Date toDate) throws Exception {
		return totHours(dao.findByCompanyUser(companyUser.getId()));

	}

	public List<Timesheet> findByCompanyUserByDate(CompanyUsers companyUser, Date startDate, Date toDate)
			throws Exception {
		return totHours(dao.findByCompanyUser(companyUser.getId(), startDate, toDate));
	}

	private List<Timesheet> totHours(List<Timesheet> timesheets) throws Exception {
		for (Timesheet timesheet : timesheets) {
			List<TimesheetDetails> timesheetdetailsList = service.findByTimesheet(timesheet);
			for (TimesheetDetails timesheetDetails : timesheetdetailsList) {
				timesheet.setTotHours(timesheet.getTotHours() + timesheetDetails.getHours());
			}
		}
		return timesheets;

	}

	public void genTimesheetForMonth() throws Exception {
		Date createDate = new Date();
		List<IDataEntity> entities = new ArrayList<IDataEntity>();
		Timesheet timesheet = null;
		List<CompanyUsers> companyUsers = companyUsersService.allCompanyUsersExAdmin();
		for (CompanyUsers companyUser : companyUsers) {
			Date firstDay = GenericUtility.getFirstDayOfMonth(getSynchronizedDate());
			Date lastDay = GenericUtility.getLasttDayOfMonth(getSynchronizedDate());
			List<Timesheet> timesheets = findByCompanyUserByDate(companyUser, firstDay, lastDay);
			if (timesheets.size() == 0) {
				timesheet = new Timesheet(firstDay, lastDay, companyUser);
				timesheet.setCreateDate(createDate);
				entities.add(timesheet);
				int amountDays = GenericUtility.getDaysBetweenDates(firstDay, lastDay);
				for (int i = 0; i <= amountDays; i++) {
					TimesheetDetails timesheetDetails = new TimesheetDetails(timesheet,
							GenericUtility.getStartOfDay(GenericUtility.addDaysToDate(firstDay, i)),
							GenericUtility.getEndOfDay(GenericUtility.addDaysToDate(firstDay, i)));
					timesheetDetails
							.setWeekNumber(GenericUtility.weekOfMonth(GenericUtility.addDaysToDate(firstDay, i)));
					timesheetDetails.setCreateDate(createDate);
					entities.add(timesheetDetails);
				}
			}
		}
		dao.createBatch(entities);
	}

}
