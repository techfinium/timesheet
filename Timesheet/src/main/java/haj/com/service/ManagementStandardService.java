package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.ManagementStandardDAO;
import haj.com.entity.ManagementStandard;
import haj.com.framework.AbstractService;

public class ManagementStandardService extends AbstractService {

	private ManagementStandardDAO dao = new ManagementStandardDAO();

	public List<ManagementStandard> allManagementStandard() throws Exception {
	  	return dao.allManagementStandard();
	}


	public void create(ManagementStandard entity) throws Exception  {
	   if (entity.getId() ==null)
		 this.dao.create(entity);
		else
		 this.dao.update(entity);
	}

	public void update(ManagementStandard entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(ManagementStandard entity) throws Exception  {
		this.dao.delete(entity);
	}

	public ManagementStandard findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<ManagementStandard> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	  /**
     * Lazy load ManagementStandard with pagination, filter, sorting
     * @author TechFinium 
     * @param class1 ManagementStandard class
     * @param first the first
     * @param pageSize the page size
     * @param sortField the sort field
     * @param sortOrder the sort order
     * @param filters the filters
     * @return  a list of {@link haj.com.entity.ManagementStandard} containing data
     * @throws Exception the exception
     */	
	@SuppressWarnings("unchecked")
	public List<ManagementStandard> allManagementStandard(Class<ManagementStandard> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<ManagementStandard>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * Get count of ManagementStandard for lazy load with filters
     * @author TechFinium 
     * @param entity ManagementStandard class
     * @param filters the filters
     * @return Number of rows in the ManagementStandard entity
     * @throws Exception the exception     
     */	
	public int count(Class<ManagementStandard> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}

/*
	public List<ManagementStandard> findByCompany(haj.com.entity.Company company) throws Exception {
		return dao.findByCompany(company.getId());
	}
*/	
}
