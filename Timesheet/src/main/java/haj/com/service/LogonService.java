package haj.com.service;

import java.util.Date;
import java.util.List;

import org.jasypt.util.password.StrongPasswordEncryptor;

import haj.com.dao.UsersDAO;
import haj.com.entity.Users;
import haj.com.entity.UsersStatusEnum;
import haj.com.framework.AbstractService;
import haj.com.utils.GenericUtility;

public class LogonService extends AbstractService {

	private UsersDAO dao = new UsersDAO();
	
	public String encryptPassword(String pwd) throws Exception {
		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
		return passwordEncryptor.encryptPassword(pwd);
	}

	public Users logon(String email, String inputPassword) throws Exception {
		Users u = dao.getUserByEmail(email);
		if (u==null) throw new Exception("User with email address: "+email +" is not registered on the system! If you typed in the correct email please contact support.");
  
		else if (u.getStatus() == UsersStatusEnum.InActive) throw new Exception("Your profile is not active. Please contact support!");
		else if (u.getStatus() == UsersStatusEnum.EmailNotConfrimed) throw new Exception("You have not confirmed your email address. Please check your mail box");

		
		else {
    		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
    		if (!passwordEncryptor.checkPassword(inputPassword.trim(), u.getPassword().trim())) {
    			throw new Exception("Invalid password for user id: " + email);
    			} 
    	}
		if (u.getLastLogin()==null) u.setChgPwdNow(true);
		else u.setChgPwdNow(false);
		
		u.setLastLogin(new Date());
		
		dao.update(u);
		return u;
	}
	
	
	public Users logonByUsername(String username, String inputPassword) throws Exception {
		Users u = dao.getUserByUsername(username);
		if (u==null) throw new Exception("User with username : "+username +" is not registered on the system! If you typed in the correct email please contact support.");
  
		else if (u.getStatus() == UsersStatusEnum.InActive) throw new Exception("Your profile is not active. Please contact support!");
		else if (u.getStatus() == UsersStatusEnum.EmailNotConfrimed) throw new Exception("You have not confirmed your email address. Please check your mail box");

		
		else {
    		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
    		if (!passwordEncryptor.checkPassword(inputPassword.trim(), u.getPassword().trim())) {
    			throw new Exception("Invalid password for username: " + username);
    			} 
    	}
		if (u.getLastLogin()==null) {
			u.setChgPwdNow(true);
		}
		else {
		 u.setLastLogin(new Date());
		 u.setChgPwdNow(false);
		 dao.update(u);
		}
		
		return u;
	}
	
	public Users logon(String email, String inputPassword,String phoneId, String iosAndroid) throws Exception {
		Users u = logon(email, inputPassword);
		u.setPhoneId(phoneId);
		u.setIosAndroid(iosAndroid);
		dao.update(u);
		return u;
	}
	
	public Users changePassword(String email, String password, String newPassword) throws Exception {
		Users u = new UsersDAO().getUserByEmail(email);
		if (u==null) throw new Exception("User with email address: "+email +" is not registered on the system! If you typed in the correct email please contact support.");
    	else {
    		StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
    		if (!passwordEncryptor.checkPassword(password.trim(), u.getPassword().trim())) {
    			throw new Exception("Invalid password for user id: " + email);
    			} 
    	}
		u.setPassword(encryptPassword(newPassword));
	    dao.update(u);
		return u;
    }
	


	public Users changePassword(Users u,String pwd) throws Exception {
	    u.setPassword(encryptPassword(pwd));
	    u.setLastLogin(new java.util.Date());
	    dao.update(u);
		return u;
    }	
	
	public Users changePassword(Users u) throws Exception {
	    u.setPassword(encryptPassword(u.getPassword()));
	    dao.update(u);
		return u;
    }
	
	
	
	public void notifyUserAboutUsernames(String email) throws Exception {
		List<Users> ul = dao.getAllUserByEmail(email);
		if (ul==null  || ul.size()==0) throw new Exception("User with email address: "+email +" is not registered on the system! If you typed in the correct email please contact support.");

		String welcome = "<p>Dear client,</p>"+	
				"<br/>" +
		        "<p>You have the following usernames on our system.</p>"+
		        "<table style='border:1px solid gray' cellspacing=\"0\" cellpadding=\"1\">"+
		        "<tr><th style=\"border-right:1px solid gray;\">Username/s</th><th>Name and Surname</th></tr>";
		for (Users u : ul) {
			welcome +="<tr><td style='border-top:1px solid gray;border-right:1px solid gray;'> "+u.getUsername()+"</td><td style='border-top:1px solid gray;'> " +u.getFirstName()+ " " +u.getLastName() +"</td></tr>";
		}
				
		
			welcome +=	 "</table><p>Regards</p>"+
				"<p>The Timesheet team</p>"+
				"<br/>";
		
			GenericUtility.sendMail(email, "Timesheet usernames", welcome);
		
	}
	
	
	public void notifyUserNewPassword(String username) throws Exception {
		Users ul = dao.getUserByUsername(username);
		if (ul==null) throw new Exception("User with username : "+username +" is not registered on the system! If you typed in the correct username please contact support.");
		notifyUserNewPassword(ul);
	}
	
	public void notifyUserNewPasswordEmail(String email) throws Exception {
		Users ul = dao.getUserByEmail(email);
		if (ul==null) throw new Exception("User with email : "+ email +" is not registered on the system! If you typed in the correct username please contact support.");
		notifyUserNewPassword(ul);
	}
	
	public void notifyUserNewPassword(Users u) throws Exception {
		String pwd = GenericUtility.genPassord(); 
		 u.setPassword(encryptPassword(pwd));
		 dao.update(u);
		String welcome = "<p>Dear #NAME#,</p>"+	
				"<br/>" +
				"<p>This is your new password: <b>"+pwd+ "</b> for email: <b>"+u.getEmail()+"</b></p>"+
				"<p>You can change it after you have logged in.</p>"+
				"<p>Regards</p>"+
				"<p>The Timesheet team</p>"+
				"<br/>";
		welcome = welcome.replaceAll("#NAME#",u.getFirstName());
		GenericUtility.sendMail(u.getEmail(), "Timesheet password reset", welcome);
	}
}