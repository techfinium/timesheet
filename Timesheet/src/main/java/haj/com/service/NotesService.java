package haj.com.service;

import java.util.List;

import haj.com.dao.NotesDAO;
import haj.com.entity.Notes;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;

public class NotesService extends AbstractService {

	private NotesDAO dao = new NotesDAO();

	public List<Notes> allNotes() throws Exception {
	  	return dao.allNotes();
	}


	public void create(Notes entity) throws Exception  {
		if (entity.getId() ==null) {
		  entity.setCreateDate(new java.util.Date());
		  this.dao.create(entity);
		}
		else update(entity);
	}

	public void update(Notes entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(Notes entity) throws Exception  {
		this.dao.delete(entity);
	}

	public Notes findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<Notes> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	/**
	 * JMB 01 09 2017
	 * 
	 * Returns a list of notes by user uid
	 * 
	 * @param user
	 * @return List<Notes>
	 * @throws Exception
	 */
	public List<Notes> findByUser(Users user) throws Exception {
		return dao.findByUser(user.getUid());
	}


}
