package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.VisitorDAO;
import haj.com.entity.Visitor;
import haj.com.framework.AbstractService;

public class VisitorService extends AbstractService {

	private VisitorDAO dao = new VisitorDAO();

	public List<Visitor> allVisitor() throws Exception {
	  	return dao.allVisitor();
	}


	public void create(Visitor entity) throws Exception  {
	   if (entity.getId() ==null)
		 this.dao.create(entity);
		else
		 this.dao.update(entity);
	}

	public void update(Visitor entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(Visitor entity) throws Exception  {
		this.dao.delete(entity);
	}

	public Visitor findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<Visitor> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	  /**
     * Lazy load Visitor with pagination, filter, sorting
     * @author TechFinium 
     * @param class1 Visitor class
     * @param first the first
     * @param pageSize the page size
     * @param sortField the sort field
     * @param sortOrder the sort order
     * @param filters the filters
     * @return  a list of {@link haj.com.entity.Visitor} containing data
     * @throws Exception the exception
     */	
	@SuppressWarnings("unchecked")
	public List<Visitor> allVisitor(Class<Visitor> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<Visitor>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * Get count of Visitor for lazy load with filters
     * @author TechFinium 
     * @param entity Visitor class
     * @param filters the filters
     * @return Number of rows in the Visitor entity
     * @throws Exception the exception     
     */	
	public int count(Class<Visitor> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}

/*
	public List<Visitor> findByCompany(haj.com.entity.Company company) throws Exception {
		return dao.findByCompany(company.getId());
	}
*/	
}
