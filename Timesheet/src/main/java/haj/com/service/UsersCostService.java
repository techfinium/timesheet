package haj.com.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.UsersCostDAO;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Users;
import haj.com.entity.UsersCost;
import haj.com.entity.UsersCostHist;
import haj.com.framework.AbstractService;

public class UsersCostService extends AbstractService {

	private UsersCostDAO dao = new UsersCostDAO();

	/**
	 * Get all UsersCost
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @return List<UsersCost>
	 * @throws Exception
	 */
	public List<UsersCost> allUsersCost() throws Exception {
		return dao.allUsersCost();
	}

	/**
	 * Create or update UsersCost
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @throws Exception
	 */
	public void create(UsersCost entity) throws Exception {
		if (entity.getId() == null) {
			entity.setCreateDate(new java.util.Date());
			this.dao.create(entity);
		} else {
			entity.setCreateDate(new java.util.Date());
			this.dao.update(entity);
		}
	}

	/**
	 * Update UsersCost
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @throws Exception
	 */
	public void update(UsersCost entity) throws Exception {
		this.dao.update(entity);
	}

	/**
	 * Delete UsersCost
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @throws Exception
	 */
	public void delete(UsersCost entity) throws Exception {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @return a UsersCost object
	 * @throws Exception
	 */
	public UsersCost findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	/**
	 * Find UsersCost by description
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @return List<UsersCost>
	 * @throws Exception
	 */
	public List<UsersCost> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<UsersCost> findByNames(String desc) throws Exception {
		return dao.findByNames(desc);
	}

	/**
	 * Lazy load UsersCost
	 * 
	 * @param first
	 *            from key
	 * @param pageSize
	 *            no of rows to fetch
	 * @return List<UsersCost>
	 * @throws Exception
	 */
	public List<UsersCost> allUsersCost(int first, int pageSize) throws Exception {
		return dao.allUsersCost(Long.valueOf(first), pageSize);
	}

	/**
	 * @author TechFinium
	 * @return Number of rows in the UsersCost
	 * @throws Exception
	 */
	public Long count() throws Exception {
		return dao.count(UsersCost.class);
	}

	/**
	 * @author TechFinium
	 * @param class1
	 *            UsersCost class
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return List<UsersCost> containing data
	 */
	@SuppressWarnings("unchecked")
	public List<UsersCost> allUsersCost(Class<UsersCost> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<UsersCost>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	/**
	 * 
	 * @param entity
	 *            UsersCost class
	 * @param filters
	 * @return Number of rows in the UsersCost entity
	 */
	public int count(Class<UsersCost> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	public List<UsersCost> findByUser(Users user) throws Exception {
		return dao.findByUser(user.getUid());
	}
	
	public Boolean findByUserB(Users user) throws Exception {
			return dao.findByUserB(user.getUid());
		}
	public UsersCost findByOneUser(Users user) throws Exception {
		return dao.findByOneUser(user.getUid());
	}

	public List<UsersCostHist> findCostHistoryForUser(Users user) throws Exception {
		return resolveTimeStamp(dao.findCostHistoryForUser(user.getUid()));
	}

	private List<UsersCostHist> resolveTimeStamp(List<UsersCostHist> findCostHistoryForUser) throws Exception {
		for (UsersCostHist uh : findCostHistoryForUser) {
			uh.setRevinfo(dao.findByKeyRev(uh.getId().getRev()));
		}
		return findCostHistoryForUser;
	}

	public List<UsersCostHist> findByKeyHist(long id) throws Exception {
		return dao.findByKeyHist(id);
	}
	/**
	 * JMB 01 09 2017
	 * 
	 * Finds and sets company users cost code
	 * 
	 * @param companyUser
	 * @return CompanyUsers
	 * @throws Exception
	 */
	public CompanyUsers findAndSetUserCost(CompanyUsers companyUser) throws Exception{
		companyUser.getUsers().setUsersCosts(new HashSet<UsersCost>(dao.findByUser(companyUser.getUsers().getUid())));
		return companyUser;
	} 
}
