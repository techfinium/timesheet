package haj.com.service;

import java.util.List;

import haj.com.dao.MilestoneStatementOfWorkDAO;
import haj.com.entity.Invoice;
import haj.com.entity.MilestoneStatementOfWork;
import haj.com.entity.StatementOfWork;
import haj.com.framework.AbstractService;

public class MilestoneStatementOfWorkService extends AbstractService {

	private MilestoneStatementOfWorkDAO dao = new MilestoneStatementOfWorkDAO();

	public List<MilestoneStatementOfWork> allMilestoneStatementOfWork() throws Exception {
		return dao.allMilestoneStatementOfWork();
	}

	public void create(MilestoneStatementOfWork entity) throws Exception {
		if (entity.getId() == null) {
			entity.setCreateDate(new java.util.Date());
			this.dao.create(entity);
		} else {
			entity.setCreateDate(new java.util.Date());
			this.dao.update(entity);
		}
	}

	public void update(MilestoneStatementOfWork entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(MilestoneStatementOfWork entity) throws Exception {
		this.dao.delete(entity);
	}

	public MilestoneStatementOfWork findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<MilestoneStatementOfWork> findSowMilestone(StatementOfWork sow) throws Exception {
		return dao.findSowMilestone(sow.getId());
	}

	public List<MilestoneStatementOfWork> findSowMilestoneInvoiceNull(StatementOfWork sow) throws Exception {
		return dao.findSowMilestoneInvoiceNull(sow);
	}

	public MilestoneStatementOfWork findSowInvoiceMilestone(StatementOfWork sow, Invoice invoice) throws Exception {
		return dao.findSowInvoiceMilestone(sow.getId(), invoice.getId());
	}

	public Boolean findSowMilestoneB(Long sow) throws Exception {
		return dao.findSowMilestoneB(sow);
	}

}
