package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.InvoiceDAO;
import haj.com.entity.Invoice;
import haj.com.entity.InvoiceHist;
import haj.com.framework.AbstractService;

public class InvoiceService extends AbstractService {

	private InvoiceDAO dao = new InvoiceDAO();

	/**
	 * Get all Invoice
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @return List<Invoice>
	 * @throws Exception
	 */
	public List<Invoice> allInvoice() throws Exception {
		return dao.allInvoice();
	}

	/**
	 * Create or update Invoice
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @throws Exception
	 */
	public void create(Invoice entity) throws Exception {

		if (entity.getId() == null) {
			entity.setCreateDate(new java.util.Date());
			this.dao.create(entity);
		} else {
			entity.setCreateDate(new java.util.Date());
			this.dao.update(entity);

		}
	}

	/**
	 * Update Invoice
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @throws Exception
	 */
	public void update(Invoice entity) throws Exception {
		entity.setCreateDate(new java.util.Date());
		this.dao.update(entity);
	}

	/**
	 * Delete Invoice
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @throws Exception
	 */
	public void delete(Invoice entity) throws Exception {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @return a Invoice object
	 * @throws Exception
	 */
	public Invoice findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	/**
	 * Find Invoice by description
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @return List<Invoice>
	 * @throws Exception
	 */
	public List<Invoice> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public Double findBySOWInvoiceTotal(Long sowId) throws Exception {
		return dao.findBySOWInvoiceTotal(sowId);
	}

	public Boolean findBySOWB(Long sowId) throws Exception {
		return dao.findBySOWB(sowId);
	}

	public List<InvoiceHist> findByKeyHist(long id) throws Exception {
		return dao.findByKeyHist(id);
	}

	/**
	 * Lazy load Invoice
	 * 
	 * @param first
	 *            from key
	 * @param pageSize
	 *            no of rows to fetch
	 * @return List<Invoice>
	 * @throws Exception
	 */
	public List<Invoice> allInvoice(int first, int pageSize) throws Exception {
		return dao.allInvoice(Long.valueOf(first), pageSize);
	}

	/**
	 * @author TechFinium
	 * @return Number of rows in the Invoice
	 * @throws Exception
	 */
	public Long count() throws Exception {
		return dao.count(Invoice.class);
	}

	/**
	 * @author TechFinium
	 * @param class1
	 *            Invoice class
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return List<Invoice> containing data
	 */
	@SuppressWarnings("unchecked")
	public List<Invoice> allInvoice(Class<Invoice> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<Invoice>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	/**
	 * 
	 * @param entity
	 *            Invoice class
	 * @param filters
	 * @return Number of rows in the Invoice entity
	 */
	public int count(Class<Invoice> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}
}
