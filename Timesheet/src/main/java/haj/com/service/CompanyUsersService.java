package haj.com.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.CompanyUsersDAO;
import haj.com.entity.Company;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Doc;
import haj.com.entity.Notes;
import haj.com.entity.Users;
import haj.com.entity.UsersCost;
import haj.com.framework.AbstractService;

public class CompanyUsersService extends AbstractService {

	private CompanyUsersDAO dao = new CompanyUsersDAO();
	private NotesService notesService = new NotesService();
	private DocService docService = new DocService();
	private UsersCostService usersCostService = new UsersCostService();
	


	public List<CompanyUsers> allCompanyUsers() throws Exception {
		return resolveDocAndNotesList(dao.allCompanyUsers());
//		return dao.allCompanyUsers();
	}

	public List<CompanyUsers> allCompanyUsersExAdmin() throws Exception {
		return resolveDocAndNotesList(dao.allCompanyUsersExAdmin());
	}
	
	public List<CompanyUsers> allCompanyUsersExAdminDirector() throws Exception {
		return resolveDocAndNotesList(dao.allCompanyUsersExAdminDirector());
	}

	public void create(CompanyUsers entity) throws Exception  {
		this.dao.create(entity);
	}

	public void update(CompanyUsers entity) throws Exception  {
		this.dao.update(entity.getUsers());
		this.dao.update(entity);
	}

	public void delete(CompanyUsers entity) throws Exception  {
		this.dao.delete(entity);
	}

	public CompanyUsers findByKey(long id) throws Exception {
       return resolveDocAndNotes(dao.findByKey(id));
	}

	public List<CompanyUsers> findByName(String desc) throws Exception {
		return resolveDocAndNotesList(dao.findByName(desc));
	}

	public List<CompanyUsers> byUser(Users user) throws Exception  { 
		return resolveDocAndNotesList(dao.byUser(user.getUid()));
	}
	
	
	public List<CompanyUsers> byCompany(Company company) throws Exception  { 
		return resolveDocAndNotesList(dao.byCompany(company.getId()));
	}
	
	public List<CompanyUsers> byCompanyExDirector(Company company) throws Exception {
		return resolveDocAndNotesList(dao.byCompanyExDirector(company.getId()));
	}
	
	public List<CompanyUsers> allCompanyUsersNotAdmin() throws Exception {
		return resolveDocAndNotesList(dao.allCompanyUsersNotAdmin());
	}
	
    /**
     * @author TechFinium 
     * @param class1 SOW class
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<SOW> containing data
     */	
	@SuppressWarnings("unchecked")
	public List<CompanyUsers> allInCompanyUsers(Class<CompanyUsers> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return resolveDocAndNotesList((List<CompanyUsers>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters));
	}
	
    /**
     * 
     * @param entity SOW class
     * @param filters
     * @return Number of rows in the SOW entity
     */	
	public int count(Class<CompanyUsers> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
	
	/**
	 * JMB 01 09 2017
	 * 
	 * Sets docs and notes set for company users
	 * 
	 * @param companyUsersList
	 * @return List<CompanyUsers>
	 * @throws Exception
	 */
	private List<CompanyUsers> resolveDocAndNotesList(List<CompanyUsers> companyUsersList) throws Exception{
		for (CompanyUsers companyUser : companyUsersList) {
			// locates notes
			companyUser.getUsers().setNotes(new HashSet<Notes>(notesService.findByUser(companyUser.getUsers())));
			// locates uploaded documents
			companyUser.getUsers().setDocs(new HashSet<Doc>(docService.findDocsByUser(companyUser.getUsers())));
			// locates users cost code
			companyUser.getUsers().setUsersCosts(new HashSet<UsersCost>(usersCostService.findByUser(companyUser.getUsers())));
		}
		return companyUsersList;
	}
	
	/**
	 * JMB 01 09 2017
	 * 
	 * Sets docs and notes set for company user
	 * 
	 * @param companyUser
	 * @return CompanyUsers
	 * @throws Exception
	 */
	private CompanyUsers resolveDocAndNotes(CompanyUsers companyUser) throws Exception{
		// locates notes
		companyUser.getUsers().setNotes(new HashSet<Notes>(notesService.findByUser(companyUser.getUsers())));
		// locates uploaded documents
		companyUser.getUsers().setDocs(new HashSet<Doc>(docService.findDocsByUser(companyUser.getUsers())));
		// locates users cost code
		companyUser.getUsers().setUsersCosts(new HashSet<UsersCost>(usersCostService.findByUser(companyUser.getUsers())));
		return companyUser;
	}
	

}
