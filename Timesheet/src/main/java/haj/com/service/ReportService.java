package haj.com.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import haj.com.bean.ProjectUsersReportBean;
import haj.com.bean.ReportBean;
import haj.com.dao.ReportsDAO;
import haj.com.entity.Company;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Projects;
import haj.com.entity.Timesheet;
import haj.com.entity.TimesheetDetails;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;
import haj.com.utils.GenericUtility;

@SuppressWarnings("serial")
public class ReportService extends AbstractService {

	private ReportsDAO dao = new ReportsDAO();
	private TimesheetService service = new TimesheetService();
	private TimesheetDetailsService detailService = new TimesheetDetailsService();

	public ReportBean totalMinutesForProject(Date fromDate, Date toDate, Company company) throws Exception {
		ReportBean rep = dao.totalMinutesForProject(fromDate, toDate, company.getId());
		if (rep != null) {
			calcHours(rep);
		}
		return rep;
	}
	
	public ReportBean totalMinutesForProjectForUser(Date fromDate, Date toDate, Users user) throws Exception {
		ReportBean rep = dao.totalMinutesForProjectForUser(fromDate, toDate, user.getUid());
		if (rep != null) {
			calcHours(rep);
		}
		return rep;
	}

	public ReportBean totalMinutesForProject(Projects project, Date fromDate, Date toDate, Company company)
			throws Exception {
		ReportBean rep = dao.totalMinutesForProject(project.getId(), fromDate, toDate, company.getId());
		if (rep != null) {
			calcHours(rep);
		}
		return rep;
	}

	public ReportBean totalMinutesForProjectDirector(Date fromDate, Date toDate) throws Exception {
		ReportBean rep = dao.totalMinutesForProjectDirector(fromDate, toDate);
		if (rep != null) {
			calcHours(rep);
		}
		return rep;
	}

	public ReportBean totalMinutesForProjectDirector(Projects project, Date fromDate, Date toDate) throws Exception {
		ReportBean rep = dao.totalMinutesForProjectDirector(project.getId(), fromDate, toDate);
		if (rep != null) {
			calcHours(rep);
		}
		return rep;
	}

	private void calcHours(ReportBean rep) {
		if (rep.getDoubleOne() != null) {
			Double hours = rep.getDoubleOne();
			Integer totHours = hours.intValue();
			Long mins = rep.getLongOne();
			if (mins != null && mins >= 60) {
				int h = (int) (mins / 60);
				hours += h;
				mins -= h * 60;
			}
			rep.setHours(hours);
			rep.setLongOne(mins);
			hours = hours - totHours;
//			rep.setHours(rep.getHours() + (hours * 60) / 100);
			Double days = rep.getHours() / 7.5;
			Integer day = days.intValue();
			days = ((days - day) * 7.5) / 100;
			rep.setDays(day + days);
		}
	}

	public List<ReportBean> projectDetails(Projects project, Date fromDate, Date toDate, Company company)
			throws Exception {
		boolean active = true;
		List<ReportBean> repList = dao.projectDetails(project.getId(), fromDate, toDate, company.getId(), active);
		for (ReportBean rep : repList) {
			rep.setProject(project);
			calcHours(rep);
		}
		return repList;
	}
	
	public List<ReportBean> projectDetailsReport(Projects project, Date fromDate, Date toDate, Company company)
			throws Exception {
		List<ReportBean> repList = dao.projectDetailsReport(project.getId(), fromDate, toDate, company.getId());
		for (ReportBean rep : repList) {
			rep.setProject(project);
			calcHours(rep);
		}
		return repList;
	}

	public List<ReportBean> projectDetailsDirector(Projects project, Date fromDate, Date toDate) throws Exception {
		List<ReportBean> repList = dao.projectDetailsDirector(project.getId(), fromDate, toDate);
		for (ReportBean rep : repList) {
			rep.setProject(project);
			calcHours(rep);
		}
		return repList;
	}

	public List<ReportBean> projectDetails(Projects project, Date fromDate, Date toDate) throws Exception {
		List<ReportBean> repList = dao.projectDetails(project.getId(), fromDate, toDate);
		for (ReportBean rep : repList) {
			rep.setProject(project);
			calcHours(rep);
		}
		return repList;
	}

	public List<ReportBean> projectTasks(Projects project, Date fromDate, Date toDate, CompanyUsers companyUsers)
			throws Exception {
		int week = 1;
		List<ReportBean> repList = dao.projectTasks(project.getId(), fromDate, toDate, companyUsers.getId(), week);
		for (ReportBean rep : repList) {
			calcHours(rep);
		}
		return repList;
	}

	public List<ReportBean> projectUsersWeek(Projects project, Date fromDate, Date toDate, CompanyUsers companyUsers)
			throws Exception {
		List<ReportBean> repList = dao.projectUserWeek(project.getId(), fromDate, toDate, companyUsers.getId());
		for (ReportBean rep : repList) {
			calcHours(rep);
		}
		return repList;
	}

	public List<ReportBean> UserDetailsAll(Company company, Date fromDate, Date toDate, CompanyUsers companyUsers)
			throws Exception {
		List<ReportBean> repList = dao.userDetailsAll(fromDate, toDate, companyUsers.getId());

		List<ReportBean> beans2 = new ArrayList<>();
		beans2.addAll(repList);
		for (ReportBean reportBean : beans2) {
			if (!reportBean.getProject().getCompany().equals(company)) {
				repList.remove(reportBean);
			}
		}

		for (ReportBean rep : repList) {
			calcHours(rep);
		}

		return repList;
	}

	public List<ReportBean> UserDetails(Company company, Date fromDate, Date toDate, CompanyUsers companyUsers,
			Integer weekNum, Projects project) throws Exception {
		List<ReportBean> repList = dao.userDetails(fromDate, toDate, companyUsers.getId(), weekNum, project.getId());

		List<ReportBean> beans2 = new ArrayList<>();
		beans2.addAll(repList);
		// for (ReportBean reportBean : beans2) {
		// if (!reportBean.getProject().getCompany().equals(company)) {
		// repList.remove(reportBean);
		// }
		// }

		for (ReportBean rep : repList) {
			calcHours(rep);
		}

		return repList;
	}

	public List<ReportBean> UserDetailsAllDirector(Date fromDate, Date toDate, CompanyUsers companyUsers)
			throws Exception {
		List<ReportBean> repList = dao.userDetailsAll(fromDate, toDate, companyUsers.getId());

		List<ReportBean> beans2 = new ArrayList<>();
		beans2.addAll(repList);

		for (ReportBean rep : repList) {
			calcHours(rep);
		}

		return repList;
	}

	public List<ReportBean> UserDetailsDirector(Date fromDate, Date toDate, CompanyUsers companyUsers, Integer weekNum,
			Projects project) throws Exception {
		List<ReportBean> repList = dao.userDetails(fromDate, toDate, companyUsers.getId(), weekNum, project.getId());
		List<ReportBean> beans2 = new ArrayList<>();
		beans2.addAll(repList);
		for (ReportBean rep : repList) {
			calcHours(rep);
		}
		return repList;
	}

	public List<ReportBean> userDetailsMins(Company company, CompanyUsers companyUser, Date fromDate, Date toDate)
			throws Exception {
		List<Timesheet> timesheetList = service.findByCompanyUserByDate(companyUser,
				GenericUtility.getFirstDayOfMonth(fromDate), GenericUtility.getLasttDayOfMonth(toDate));
		// Double hours = 0.0;
		// List<Timesheet> timesheetList =
		// service.findByCompanyUserByDate(companyUser, fromDate, toDate);
		List<ReportBean> beans = new ArrayList<>();
		for (Timesheet timesheet : timesheetList) {
			List<TimesheetDetails> detail = detailService.findByForCompUserTimesheetBetweenDates(company, timesheet,
					fromDate, toDate);
			Double hours = 0.0;
			Integer mins = 0;
			for (TimesheetDetails timesheetDetails : detail) {
				hours += timesheetDetails.getHours();
				mins += timesheetDetails.getMinutes();
			}
			if (mins >= 60) {
				int h = (int) (mins / 60);
				hours += h;
				mins -= h * 60;
			}
			ReportBean reportBean = new ReportBean();
			reportBean.setLongOne(mins.longValue());
			reportBean.setHours(hours);
			reportBean.setFromDate(timesheet.getFromDate());
			reportBean.setToDate(timesheet.getToDate());
			Double days = hours / 7.5;
			reportBean.setDays(days);
			reportBean.setUser(companyUser);
			beans.add(reportBean);
			// beans.addAll(dao.userDetailsMins(timesheet.getId()));
		}

		return beans;

	}

	public List<ReportBean> userDetailsMinsDirector(CompanyUsers companyUser, Date fromDate, Date toDate)
			throws Exception {
		List<Timesheet> timesheetList = service.findByCompanyUserByDate(companyUser,
				GenericUtility.getFirstDayOfMonth(fromDate), GenericUtility.getLasttDayOfMonth(toDate));
		// Double hours = 0.0;
		// List<Timesheet> timesheetList =
		// service.findByCompanyUserByDate(companyUser, fromDate, toDate);
		List<ReportBean> beans = new ArrayList<>();
		for (Timesheet timesheet : timesheetList) {
			Double hours = 0.0;
			Integer mins = 0;
			List<TimesheetDetails> detail = detailService.findByForCompUserTimesheetBetweenDatesDirector(timesheet,
					fromDate, toDate);
			for (TimesheetDetails timesheetDetails : detail) {
				hours += timesheetDetails.getHours();
				mins += timesheetDetails.getMinutes();

			}
			if (mins >= 60) {
				int h = (int) (mins / 60);
				hours += h;
				mins -= h * 60;
			}
			ReportBean reportBean = new ReportBean();
			reportBean.setHours(hours);
			reportBean.setLongOne(mins.longValue());
			reportBean.setFromDate(timesheet.getFromDate());
			reportBean.setToDate(timesheet.getToDate());
			Double days = hours / 7.5;
			reportBean.setDays(days);
			reportBean.setUser(companyUser);
			beans.add(reportBean);
			// beans.addAll(dao.userDetailsMins(timesheet.getId()));
		}

		return beans;

	}

	public List<ReportBean> projectsFromToDate(Date fromDate, Date toDate) throws Exception {
		boolean active = true;
		List<ReportBean> repList = dao.projectsFromToDate(fromDate, toDate, active);
		for (ReportBean rep : repList) {
			calcHours(rep);
		}
		return repList;
	}

	public List<ReportBean> projectsFromToDate(Date fromDate, Date toDate, Company company) throws Exception {
		boolean active = true;
		List<ReportBean> repList = dao.projectsFromToDateActive(fromDate, toDate, company.getId(), active);
		for (ReportBean rep : repList) {
			calcHours(rep);
		}
		return repList;
	}
	
	public List<ReportBean> projectsFromToDateReport(Date fromDate, Date toDate, Company company) throws Exception {
		List<ReportBean> repList = dao.projectsFromToDate(fromDate, toDate, company.getId());
		for (ReportBean rep : repList) {
			calcHours(rep);
		}
		return repList;
	}
	
/**
 * 	Report for specifc user
 * @param fromDate
 * @param toDate
 * @param user
 * @return
 * @throws Exception
 */
	public List<ReportBean> projectsFromToDateUserReport(Date fromDate, Date toDate, Users user) throws Exception {
		List<ReportBean> repList = dao.projectsFromToDateForUser(fromDate, toDate, user.getUid());
		for (ReportBean rep : repList) {
			calcHours(rep);
		}
		return repList;
	}

	public List<ReportBean> projectsFromToDateDirector(Date fromDate, Date toDate) throws Exception {
		List<ReportBean> repList = dao.projectsFromToDateDirector(fromDate, toDate);
		for (ReportBean rep : repList) {
			calcHours(rep);
		}
		return repList;
	}
	
	public List<ProjectUsersReportBean> locateProjectsUsersReportByDateRange(Date fromDate, Date toDate) throws Exception{
		List<ProjectUsersReportBean> resultList = detailService.locateUsersProjectsReportByDateRange(fromDate, toDate);
		return resultList;
	}

}
