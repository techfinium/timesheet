package haj.com.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import haj.com.billing.BillingConstants;
import haj.com.billing.BillingHelper;
import haj.com.billing.entity.Accounts;
import haj.com.constants.HAJConstants;
import haj.com.dao.CompanyUsersDAO;
import haj.com.dao.UsersDAO;
import haj.com.entity.Address;
import haj.com.entity.AddressTypeEnum;
import haj.com.entity.Company;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Users;
import haj.com.entity.UsersLevelEnum;
import haj.com.entity.UsersStatusEnum;
import haj.com.exception.NotifyUserException;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;
import haj.com.utils.GenericUtility;
import haj.com.utils.PhoneNumberUtils;

public class RegisterService extends AbstractService {
	private UsersDAO dao = new UsersDAO();
	private AddressService addressService = new AddressService();
	private AccountsService accountsService = new AccountsService();
	private CompanyUsersDAO companyUsersDAO = new CompanyUsersDAO();
	private Date createDate = new Date();

	public void register(Users u) throws Exception {
		// if (!u.getAcceptedTAndC()) throw new Exception("You need to accept
		// the Terms And Conditions before you can register");
		if (dao.getUserByEmail(u.getEmail()) != null)
			throw new Exception("Another user is already registered with this email address");
		u.setPassword(new LogonService().encryptPassword(u.getPassword()));
		u.setLastLogin(null);
		if (u.getCellNumber() != null && u.getCellNumber().trim().length() > 0) {
			u.setInternalCellNumber(PhoneNumberUtils.convertNumberToInternalFormat(u.getCellNumber(), "ZA"));
		}
		u.setStatus(UsersStatusEnum.EmailNotConfrimed);
		u.setEmailGuid(UUID.randomUUID().toString());
		u.setRegistrationDate(new java.util.Date());

		// u.setUserId(("" + u.getFirstName().toUpperCase().charAt(0) +
		// u.getLastName().toUpperCase().charAt(0) + String.format("%05d",
		// cntr)).toUpperCase());
		if (u.getUid() == null) {
			dao.create(u);
		} else {
			dao.update(u);
		}
		notifyUser(u);
	}

	public void register(Company company, Users u, Address address) throws Exception {
		if (dao.getUserByUsername(u.getUsername()) != null)
			throw new Exception("Another user is already registered with this username");
		if (!dao.emailAvailable(u.getEmail())) {
			new LogonService().notifyUserAboutUsernames(u.getEmail());
			throw new Exception(
					"Another user is already registered with this email. A notification with username has been send to "
							+ u.getEmail());
		}
		if (u.getPassword() == null || u.getPassword().trim().length() < 5)
			throw new Exception("Please enter a password. The password must be at least 5 characters");
		u.setPassword(new LogonService().encryptPassword(u.getPassword()));
		u.setLastLogin(null);
		if (u.getCellNumber() != null && u.getCellNumber().trim().length() > 0) {
			u.setInternalCellNumber(PhoneNumberUtils.convertNumberToInternalFormat(u.getCellNumber(), "UK"));
		}
		u.setStatus(UsersStatusEnum.EmailNotConfrimed);
		u.setEmailGuid(UUID.randomUUID().toString());
		u.setRegistrationDate(new java.util.Date());
		List<IDataEntity> list = new ArrayList<IDataEntity>();
		company.setCreateDate(new java.util.Date());
		if (company.getCompanyRegNumber() == null)
			company.setCompanyRegNumber("");
		if (company.getVatNumber() == null)
			company.setVatNumber("");
		if (company.getIncomeTaxNumber() == null)
			company.setIncomeTaxNumber("");

		list.add(company);
		list.add(u);
		CompanyUsers cu = new CompanyUsers();
		cu.setCompany(company);
		cu.setUsers(u);
		cu.setCreateDate(new java.util.Date());
		cu.setLevel(UsersLevelEnum.Manager);
		list.add(cu);
		dao.createBatch(list);
		notifyUser(u);
		accountsService.createDefaultAccount(u, address);
	}

	/*
	 * public void register(ClientUsers u) throws NotifyUserException,Exception
	 * { if (dao.getUserByUsername(u.getUsers().getUsername()) != null) throw
	 * new Exception("Another user is already registered with this username");
	 * Users existing = dao.getUserByEmail(u.getUsers().getEmail()); if
	 * (existing!=null){ //new
	 * LogonService().notifyUserAboutUsernames(u.getUsers().getEmail());
	 * u.setUsers(existing); dao.create(u); throw new NotifyUserException(
	 * "Existing has been added."); } // if (u.getPassword()==null ||
	 * u.getPassword().trim().length()<5) throw new Exception(
	 * "Please enter a password. The password must be at least 5 characters");
	 * String pwd = GenericUtility.genPassord(); u.getUsers().setPassword(pwd);
	 * u.getUsers().setPassword(new
	 * LogonService().encryptPassword(u.getUsers().getPassword()));
	 * u.getUsers().setLastLogin(null); if (u.getUsers().getCellNumber()!=null
	 * && u.getUsers().getCellNumber().trim().length()>0) {
	 * u.getUsers().setInternalCellNumber(PhoneNumberUtils.
	 * convertNumberToInternalFormat(u.getUsers().getCellNumber(), "UK")); }
	 * u.getUsers().setStatus(UsersStatusEnum.EmailNotConfrimed);
	 * u.getUsers().setEmailGuid(UUID.randomUUID().toString());
	 * u.getUsers().setRegistrationDate(new java.util.Date()); List<IDataEntity>
	 * list = new ArrayList<IDataEntity>();
	 * 
	 * 
	 * list.add(u.getUsers()); list.add(u); list.add(u);
	 * 
	 * dao.createBatch(list); notifyUser(u,pwd);
	 * 
	 * }
	 * 
	 */
	/**
	 * When company level billing is enabled
	 * 
	 * @param company
	 * @param address
	 * @throws Exception
	 */
	public void createDefaultAccount(Company company, Address address) throws Exception {
		Accounts accounts = new Accounts();
		accounts.setUnitBalance(0L);
		Address billingAddress = null;
		if (address != null) {
			billingAddress = (Address) address.clone();
			billingAddress.setIdaddress(null);
			billingAddress.setAddrType(AddressTypeEnum.Billing);
			dao.create(billingAddress);

		}
		accounts.setBillingAddress(billingAddress);

		accounts.setAccountName(company.getCompanyName() + " Ref:" + company.getId());
		accounts.setAccountOpenDate(new java.util.Date());
		dao.create(accounts);
		company.setAccounts(accounts);
		dao.update(company);

		BillingHelper.processTransaction(company,
				accountsService.findEventCostByKey(BillingConstants.FREE_UNIT).getBillingEvent().getId(),
				"One Free unit");

	}
	
	public void linkCreateUpdateAddress (Address homeAddress) throws Exception{
		if (homeAddress.getIdaddress() == null) {
			addressService.create(homeAddress);
		} else {
			addressService.update(homeAddress);
		}
	}

	public void register(Company company, Users u, UsersLevelEnum level, Users inviter)
			throws NotifyUserException, Exception {
		Users tu = dao.getUserByEmail(u.getEmail());// dao.getUserByUsername(u.getUsername());

		CompanyUsers compUsers = null;
		if (tu != null) {
			compUsers = companyUsersDAO.findCompanyUsers(tu.getUid(), company.getId());
			if (compUsers != null) {
				if (tu.getUid().longValue() == inviter.getUid().longValue())
					throw new NotifyUserException("You can't invite yourself!");
				notifyUserAboutJoinCompany(company, tu, level, inviter);
				throw new NotifyUserException(
						"Another user is already registered with this username. They will be notified to join your company");
			} else {
				u = tu;
			}

		}

		List<Users> ul = dao.getAllUserByEmail(u.getEmail());
		if (ul != null && ul.size() > 0) {
			for (Users users : ul) {
				tu = users;
				break;
			}
			if (compUsers != null) {
				if (tu.getUid().longValue() == inviter.getUid().longValue())
					throw new NotifyUserException("You can't invite yourself!");

				notifyUserAboutJoinCompany(company, tu, level, inviter);
				throw new NotifyUserException(
						"Another user is already registered with this email. They will be notified to join your company");
			}
		}
		// u.setPassword(new LogonService().encryptPassword(u.getPassword()));
		List<IDataEntity> list = new ArrayList<IDataEntity>();
		String pwd = GenericUtility.genPassord();
		if (tu == null) {
			u.setLastLogin(null);

			if (u.getCellNumber() != null && u.getCellNumber().trim().length() > 0) {
				u.setInternalCellNumber(PhoneNumberUtils.convertNumberToInternalFormat(u.getCellNumber(), "UK"));
			}
			
			u.setPassword(new LogonService().encryptPassword(pwd));

			u.setStatus(UsersStatusEnum.EmailNotConfrimed);
			u.setEmailGuid(UUID.randomUUID().toString());
			u.setUsername("");
			u.setRegistrationDate(new java.util.Date());
			
			list.add(u);
		}

		CompanyUsers cu = new CompanyUsers();
		cu.setCompany(company);
		cu.setUsers(u);
		cu.setActive(true);
		cu.setCreateDate(createDate);
		cu.setLevel(level);
		list.add(cu);
		dao.createBatch(list);
		if (tu == null) {
			notifyUserNewPassword(u, pwd);
		}
		
		
	}

	private void notifyUserAboutJoinCompany(Company company, Users u, UsersLevelEnum level, Users inviter)
			throws Exception {
		/*
		 * TempCompanyUsers cu = companyUsersDAO.findTempInvites(u.getUid(),
		 * company.getId()); if (cu==null) { cu = new TempCompanyUsers();
		 * cu.setCompany(company); cu.setUsers(u); cu.setLevel(level);
		 * cu.setCreateDate(new Date()); cu.setInviter(inviter); dao.create(cu);
		 * } else { cu.setCreateDate(new Date()); cu.setInviter(inviter);
		 * dao.update(cu); }
		 * 
		 * String welcome = "<p>Dear #NAME#,</p>"+ "<br/>" +
		 * "<p>You have been invited by <b>"+inviter.getFirstName() + " " +
		 * inviter.getLastName() + "</b> to join company <b>"
		 * +company.getCompanyName() +"</b></p>"+
		 * "<p>Please login to infoFINIUM and confirm that you would like to join <b>"
		 * +company.getCompanyName() +"</b></p>"+ "<p>Regards</p>"+
		 * "<p>The infoFINIUM team</p>"+ "<br/>"; welcome =
		 * welcome.replaceAll("#NAME#",u.getFirstName());
		 * GenericUtility.sendMail(u.getEmail(), "infoFINIUM invitation",
		 * welcome);
		 */
	}

	public void register(Users user, Address address, Address postalAddress) throws Exception {

		List<IDataEntity> entityList = new ArrayList<IDataEntity>();
		register(user);
		entityList.add(user);
		addressService.lookupLongitudeLatitude(address);
		entityList.add(address);
		entityList.add(postalAddress);

		/*
		 * ParentChildHasAddress pca = new ParentChildHasAddress();
		 * pca.setAddress(address); pca.setUsers(user);
		 * pca.setPrimaryAddr(true); entityList.add(pca);
		 * 
		 * ParentChildHasAddress pca2 = new ParentChildHasAddress();
		 * pca2.setAddress(postalAddress); pca2.setUsers(user);
		 * pca2.setPrimaryAddr(true); entityList.add(pca2);
		 * 
		 * dao.createBatch(entityList);
		 */
		notifyUser(user);

	}

	public Users logonBarcode(Long barcode) throws Exception {
		Users u = dao.findByKeyl(barcode);
		if (u == null)
			throw new Exception("Barcode is invalid");
		if (u.getEmail() != null)
			throw new Exception("You need to logon with your email address! ");
		return u;
	}

	public void notifyUser(Users u) throws Exception {
		// welcome message to the user

		String welcome = "<p>Dear #NAME#,</p>" + "<br/>" + "<p>Thank you for joining Timesheet</p>" + "<p>Please "
				+ "<a href=\"" + HAJConstants.PL_LINK + "confirmemail.jsf?uuid=" + u.getEmailGuid().trim()
				+ "\">confirm</a> your email address.</p>" + "<p>Regards</p>" + "<p>The Timesheet team</p>" + "<br/>";
		welcome = welcome.replaceAll("#NAME#", u.getFirstName());
		GenericUtility.sendMail(u.getEmail(), "Complete Timesheet registration", welcome);
	}

	/*
	 * public void notifyUser(ClientUsers u, String pwd) throws Exception { //
	 * welcome message to the user
	 * 
	 * String welcome = "<p>Dear #NAME#,</p>"+ "<br/>" +
	 * "<p>You have been invited to joining TechFINIUM LTD Q/A</p>"+ "<p>Please "+
	 * "<a href=\""+HAJConstants.PL_LINK +
	 * "confirmemail.jsf?uuid="+u.getUsers().getEmailGuid().trim()+
	 * "\">confirm</a> your email address.</p>"+ "<p>Your username is: <b>"
	 * +u.getUsers().getUsername()+"</b> and your password is: <b>"+pwd+
	 * "</b></p>"+ "<p>You can change it after you have logged in.</p>"+
	 * "<p>Regards</p>"+ "<p>The TechFINIUM LTD team</p>"+ "<br/>"; welcome =
	 * welcome.replaceAll("#NAME#",u.getUsers().getFirstName());
	 * GenericUtility.sendMail(u.getUsers().getEmail(),
	 * "Complete infoFINIUM registration", welcome); }
	 */
	public void confirmEmail(Users u) throws Exception {
		u.setEmailConfirmDate(new java.util.Date());
		u.setStatus(UsersStatusEnum.Active);
		dao.update(u);

	}

	public void notifyUserNewPassword(Users u, String pwd) throws Exception {

		String welcome = "<p>Dear #NAME#,</p>" + "<br/>" + "<p>Please " + "<a href=\"" + HAJConstants.PL_LINK
				+ "confirmemail.jsf?uuid=" + u.getEmailGuid().trim() + "\">confirm</a> your email address.</p>"
				+ "<p>Your username is: <b>" + u.getUsername() + "</b> and your password is: <b>" + pwd + "</b></p>"
				+ "<p>You can change it after you have logged in.</p>" + "<p>Regards</p>" + "<p>The Timesheet team</p>"
				+ "<br/>";
		welcome = welcome.replaceAll("#NAME#", u.getFirstName());
		GenericUtility.sendMail(u.getEmail(), "Timesheet new password", welcome);
	}

	public List<CompanyUsers> registerCompany(Company company, Users u, Address address) throws Exception {
		List<IDataEntity> list = new ArrayList<IDataEntity>();
		company.setCreateDate(new java.util.Date());
		if (company.getCompanyRegNumber() == null)
			company.setCompanyRegNumber("");
		if (company.getVatNumber() == null)
			company.setVatNumber("");
		if (company.getIncomeTaxNumber() == null)
			company.setIncomeTaxNumber("");

		list.add(company);

		CompanyUsers cu = new CompanyUsers();
		cu.setCompany(company);
		cu.setUsers(u);
		cu.setCreateDate(new java.util.Date());
		cu.setLevel(UsersLevelEnum.Manager);
		list.add(cu);
		dao.createBatch(list);
		return companyUsersDAO.byUser(u.getUid());

	}

}
