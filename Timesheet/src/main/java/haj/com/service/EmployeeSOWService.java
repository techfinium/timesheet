package haj.com.service;

import java.util.List;

import haj.com.dao.EmployeeSOWDAO;
import haj.com.entity.EmployeeSOW;
import haj.com.entity.Invoice;
import haj.com.entity.StatementOfWork;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;

public class EmployeeSOWService extends AbstractService {

	private EmployeeSOWDAO dao = new EmployeeSOWDAO();

	public List<EmployeeSOW> allEmployeeSOW() throws Exception {
		return dao.allEmployeeSOW();
	}

	public void create(EmployeeSOW entity) throws Exception {
		if (entity.getId() == null) {
			entity.setCreateDate(new java.util.Date());
			this.dao.create(entity);
		} else {
			entity.setCreateDate(new java.util.Date());
			this.dao.update(entity);
		}
	}

	public void update(EmployeeSOW entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(EmployeeSOW entity) throws Exception {
		this.dao.delete(entity);
	}

	public EmployeeSOW findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public EmployeeSOW findByUser(Users u) throws Exception {
		return dao.findByUsers(u.getUid());
	}

	public EmployeeSOW findByUsersAndSOW(Users u, StatementOfWork sow) throws Exception {
		return dao.findByUsersAndSOW(u.getUid(), sow.getId());
	}

	public EmployeeSOW findByUsersAndSOWInvoiceNull(Users u, StatementOfWork sow) throws Exception {
		return dao.findByUsersAndSOWInvoiceNull(u.getUid(), sow.getId());
	}

	public List<EmployeeSOW> findByUsersAndSOWList(Users u, StatementOfWork sow) throws Exception {
		return dao.findByUsersAndSOWList(u.getUid(), sow.getId());
	}

	public Boolean findByUsersAndSOWListB(Long u, Long sow) throws Exception {
		return dao.findByUsersAndSOWListB(u, sow);
	}

	public List<EmployeeSOW> findBySOWandInvoiceNull(StatementOfWork sow) throws Exception {
		return dao.findBySOWandInvoiceNull(sow.getId());
	}

	public List<EmployeeSOW> findBySOW(StatementOfWork sow) throws Exception {
		return dao.findBySOW(sow.getId());
	}

	public List<EmployeeSOW> findBySOWInvoice(StatementOfWork sow, Invoice invoice) throws Exception {
		return dao.findBySOWInvoice(sow.getId(), invoice.getId());
	}
}
