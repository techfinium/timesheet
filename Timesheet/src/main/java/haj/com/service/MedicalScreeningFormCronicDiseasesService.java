package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.MedicalScreeningFormCronicDiseasesDAO;
import haj.com.entity.MedicalScreeningFormCronicDiseases;
import haj.com.framework.AbstractService;

public class MedicalScreeningFormCronicDiseasesService extends AbstractService {

	private MedicalScreeningFormCronicDiseasesDAO dao = new MedicalScreeningFormCronicDiseasesDAO();

	public List<MedicalScreeningFormCronicDiseases> allMedicalScreeningFormCronicDiseases() throws Exception {
	  	return dao.allMedicalScreeningFormCronicDiseases();
	}


	public void create(MedicalScreeningFormCronicDiseases entity) throws Exception  {
	   if (entity.getId() ==null)
		 this.dao.create(entity);
		else
		 this.dao.update(entity);
	}

	public void update(MedicalScreeningFormCronicDiseases entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(MedicalScreeningFormCronicDiseases entity) throws Exception  {
		this.dao.delete(entity);
	}

	public MedicalScreeningFormCronicDiseases findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}
	
	public List<MedicalScreeningFormCronicDiseases> findByMedicalScreeningFormId(Long id) throws Exception {
		return dao.findByMedicalScreeningFormId(id);
	}

	public List<MedicalScreeningFormCronicDiseases> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	  /**
     * Lazy load MedicalScreeningFormCronicDiseases with pagination, filter, sorting
     * @author TechFinium 
     * @param class1 MedicalScreeningFormCronicDiseases class
     * @param first the first
     * @param pageSize the page size
     * @param sortField the sort field
     * @param sortOrder the sort order
     * @param filters the filters
     * @return  a list of {@link haj.com.entity.MedicalScreeningFormCronicDiseases} containing data
     * @throws Exception the exception
     */	
	@SuppressWarnings("unchecked")
	public List<MedicalScreeningFormCronicDiseases> allMedicalScreeningFormCronicDiseases(Class<MedicalScreeningFormCronicDiseases> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<MedicalScreeningFormCronicDiseases>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * Get count of MedicalScreeningFormCronicDiseases for lazy load with filters
     * @author TechFinium 
     * @param entity MedicalScreeningFormCronicDiseases class
     * @param filters the filters
     * @return Number of rows in the MedicalScreeningFormCronicDiseases entity
     * @throws Exception the exception     
     */	
	public int count(Class<MedicalScreeningFormCronicDiseases> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}

/*
	public List<MedicalScreeningFormCronicDiseases> findByCompany(haj.com.entity.Company company) throws Exception {
		return dao.findByCompany(company.getId());
	}
*/	
}
