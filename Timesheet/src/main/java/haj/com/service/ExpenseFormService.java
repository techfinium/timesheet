package haj.com.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.ExpenseFormDAO;
import haj.com.entity.Company;
import haj.com.entity.Doc;
import haj.com.entity.ExpenseForm;
import haj.com.entity.Projects;
import haj.com.entity.Users;
import haj.com.entity.enums.ExpenseFormTypeEnum;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class ExpenseFormService extends AbstractService {

	private ExpenseFormDAO dao = new ExpenseFormDAO();
	private DocService docService = new DocService();

	@SuppressWarnings("unchecked")
	public List<Company> allcompanies(Class<Company> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<Company>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);
	}

	public void create(ExpenseForm entity) throws Exception {
		Date date = new Date();
		entity.setCreateDate(date);
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else {
			this.dao.update(entity);
		}
	}

	public void update(ExpenseForm entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(ExpenseForm entity) throws Exception {
		this.dao.delete(entity);
	}

	public int countAllcompanies(Class<Company> class1) throws Exception {
		Long long1 = (Long) dao.count(class1);
		return long1.intValue();
	}

	public List<Projects> projectsByCompany(Company company) throws Exception {
		return dao.projectsByCompany(company);
	}

	public List<ExpenseForm> findByUserProjectType(Users user, Projects project, ExpenseFormTypeEnum expenseFormType) throws Exception {
		return dao.findByUserProjectType(user, project, expenseFormType);

	}

	public void saveExpenseForm(ExpenseForm entity) throws Exception {
		Integer count = 0;
		if (entity.getAccomodationPrice() != null) {
			count++;
		}
		if (entity.getMeals() != null) {
			count++;
		}
		if (entity.getTaxiTrainToll() != null) {
			count++;
		}
		if (entity.getParking() != null) {
			count++;
		}
		if (entity.getFlightPrice() != null) {
			count++;
		}
		if (entity.getTelMobileDialupAmount() != null) {
			count++;
		}
		if (entity.getUtilitiesAmount() != null) {
			count++;
		}

		if (!count.equals(entity.getDocList().size())) {
			throw new Exception("Please upload all supporting documents");
		}
		this.dao.create(entity);

		for (Doc doc : entity.getDocList()) {
			if (doc.getId() == null) {
				doc.setTargetClass(entity.getClass().getName());
				doc.setTargetKey(entity.getId());
				docService.create(doc);
			}
		}
	}

	public void storeExpensform(ExpenseForm expenseForm) throws Exception {
		List<IDataEntity> updateList = new ArrayList<>();
		expenseFormCalcs(expenseForm);
		create(expenseForm);
		//TasksService.instance().findNextInProcessAndCreateTask(user, tasks, users, false);
		for (Doc doc : expenseForm.getDocList()) {
			doc.setTargetKey(expenseForm.getId());
			doc.setTargetClass(expenseForm.getClass().getName());
			updateList.add(doc);
		}
		dao.updateBatch(updateList);
	}

	public void expenseFormCalcs(ExpenseForm expenseForm) {
		if (expenseForm.getDailyRate() != null && expenseForm.getDayNumber() != null) {
			expenseForm.setPerDiem(expenseForm.getDailyRate().getValue() * expenseForm.getDayNumber());
		}
		if (expenseForm.getKmTravelled() != null) {
			expenseForm.setTotalKmExpenseForm(expenseForm.getKmTravelled() * expenseForm.getAllowanceRate().getValue());
		}
		expenseForm.setTotalExpenseForm((expenseForm.getPerDiem() == null ? 0.0 : expenseForm.getPerDiem()) + (expenseForm.getTotalKmExpenseForm() == null ? 0.0 : expenseForm.getTotalKmExpenseForm()) + (expenseForm.getAccomodationPrice() == null ? 0.0 : expenseForm.getAccomodationPrice()) + (expenseForm.getMeals() == null ? 0.0 : expenseForm.getMeals()) + (expenseForm.getParking() == null ? 0.0 : expenseForm.getParking()) + (expenseForm.getTaxiTrainToll() == null ? 0.0 : expenseForm.getTaxiTrainToll()) + (expenseForm.getFlightPrice() == null ? 0.0 : expenseForm.getFlightPrice()) + (expenseForm.getTelMobileDialupAmount() == null ? 0.0 : expenseForm.getTelMobileDialupAmount()) + (expenseForm.getUtilitiesAmount() == null ? 0.0 : expenseForm.getUtilitiesAmount()));
	}
	public List<ExpenseForm> findByUser(Users user) throws Exception {
		return dao.findByUser(user);
	}

}
