package haj.com.service;

import java.util.Date;
import java.util.List;

import haj.com.dao.ProjectsDAO;
import haj.com.entity.Company;
import haj.com.entity.Projects;
import haj.com.framework.AbstractService;

public class ProjectsService extends AbstractService {

	private ProjectsDAO dao = new ProjectsDAO();

	public List<Projects> allProjects() throws Exception {
	  	return dao.allProjects();
	}
	
	public List<Projects> allProjectsActive() throws Exception {
	  	return dao.allProjectsActive();
	}
	
	public List<Projects> allProjectsInActive() throws Exception {
	  	return dao.allProjectsInActive();
	}
	
	public List<Projects> allProjectsOverhead() throws Exception {
	  	return dao.allProjectsOverhead();
	}
	
	public List<Projects> allProjectsNotOverhead() throws Exception {
	  	return dao.allProjectsNotOverhead();
	}
 
 /*
  	public List<Projects> byCompany(Company company) throws Exception {
	  	return dao.byCompany(company.getId());
	}
 */

	public void create(Projects entity) throws Exception  {
		Date date = new Date();
		entity.setCreateDate(date);
//		System.out.println(entity.getActive());
		this.dao.create(entity);
	}

	public void update(Projects entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(Projects entity) throws Exception  {
		this.dao.delete(entity);
	}

	public Projects findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<Projects> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	public List<Projects> findByCompany(Company company) throws Exception {
		return dao.findbyCompany(company.getId());
	}
	
	public List<Projects> findByCompanyReport(Company company) throws Exception {
		return dao.findbyCompanyReport(company.getId());
	}
	
	public List<Projects> findByCodeName(String code) throws Exception {
		return dao.findByCodeName(code);
	}
	
	public List<Projects> findLikeCodeName(String code) throws Exception {
		return dao.findLikeCodeName(code);
	}
	
	public List<Projects> findByCodeNameCompany(String code, long id) throws Exception {
		return dao.findByCodeNameCompany(code , id);
	}

}
