package haj.com.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import haj.com.dao.BankHolidayParentDAO;
import haj.com.entity.BankHolidayChild;
import haj.com.entity.BankHolidayParent;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class BankHolidayParentService extends AbstractService {

	private BankHolidayParentDAO dao = new BankHolidayParentDAO();

	private SimpleDateFormat SDFD = new SimpleDateFormat("dd");

	private SimpleDateFormat SDFM = new SimpleDateFormat("MMMM");

	private SimpleDateFormat SDFDMY = new SimpleDateFormat("dd MMMM yyyy");

	public BankHolidayParentService() {
		super();
	}



	public List<BankHolidayParent> allBankHolidayParent() throws Exception {
		return clcCss(dao.allBankHolidayParent());
	}

	public List<BankHolidayChild> findRepeatDates() throws Exception {
		return dao.findRepeatDates();
	}

	private List<BankHolidayParent> clcCss(List<BankHolidayParent> bankHolidayParents) {
		for (BankHolidayParent bankHolidayParent : bankHolidayParents) {
			for (BankHolidayChild bankHolidayChild : bankHolidayParent.getBankHolidayChildrenList()) {
				if (bankHolidayChild.getBankHoliday().before(getSynchronizedDate())) {
					bankHolidayChild.setCssStyle("light gray");
				} else {
					bankHolidayChild.setCssStyle("green ");
				}
			}

		}
		return bankHolidayParents;
	}

	public void create(IDataEntity entity) throws Exception {
		this.dao.create(entity);
	}


	public void create(BankHolidayParent entity) throws Exception {
		this.dao.create(entity);

	
	}

	public void createRepeat(BankHolidayParent entity) throws Exception {
		List<BankHolidayChild> bankHolidayChilds = findRepeatDates();
		List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();
		dataEntities.add(entity);
		BankHolidayChild newChild;
		String months = null;
		String days = null;
		String years = entity.getBankHolidayYear() + "";
		for (BankHolidayChild bankHolidayChild : bankHolidayChilds) {
			months = SDFM.format(bankHolidayChild.getBankHoliday());
			days = SDFD.format(bankHolidayChild.getBankHoliday());
			newChild = new BankHolidayChild();
			newChild.setBankHolidayParent(entity);
			newChild.setBankHoliday(SDFDMY.parse(days+" "+months+" "+years));
			newChild.setHolidayNames(bankHolidayChild.getHolidayNames());
			newChild.setRepeatDates(bankHolidayChild.getRepeatDates());
			dataEntities.add(newChild);
		}
		this.dao.createBatch(dataEntities);

	}

	public void update(BankHolidayParent entity) throws Exception {
		this.dao.update(entity);

	}

	public void delete(IDataEntity entity) throws Exception {
		this.dao.delete(entity);

	}

	public BankHolidayParent findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<BankHolidayParent> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<BankHolidayParent> findByCompany(haj.com.entity.Company company) throws Exception {
		return dao.findByCompany(company.getId());
	}
}
