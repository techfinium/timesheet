package haj.com.service;

import java.util.Date;
import java.util.List;

import haj.com.dao.CompanyDAO;
import haj.com.entity.Company;
import haj.com.framework.AbstractService;

public class CompanyService extends AbstractService {

	private CompanyDAO dao = new CompanyDAO();

	public List<Company> allCompany() throws Exception {
	  	return dao.allCompany();
	}
 
 /*
  	public List<Company> byCompany(Company company) throws Exception {
	  	return dao.byCompany(company.getId());
	}
 */

	public void create(Company entity) throws Exception  {
		Date date = new Date();
		entity.setCreateDate(date);
		this.dao.create(entity);
	}

	public void update(Company entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(Company entity) throws Exception  {
		this.dao.delete(entity);
	}

	public Company findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<Company> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	public List<Company> allCompanyActive() throws Exception {
	  	return dao.allCompanyActive();
	}
	public List<Company> allCompanyInActive() throws Exception {
	  	return dao.allCompanyInActive();
	}
	

	
}
