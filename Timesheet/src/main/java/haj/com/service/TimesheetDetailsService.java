package haj.com.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import haj.com.bean.ProjectUsersReportBean;
import haj.com.dao.TimesheetDetailsDAO;
import haj.com.entity.Company;
import haj.com.entity.Timesheet;
import haj.com.entity.TimesheetDetails;
import haj.com.framework.AbstractService;
import haj.com.utils.GenericUtility;

public class TimesheetDetailsService extends AbstractService {

	private TimesheetDetailsDAO dao = new TimesheetDetailsDAO();

	public List<TimesheetDetails> allTimesheetDetails() throws Exception {
		return dao.allTimesheetDetails();
	}

	/*
	 * public List<TimesheetDetails> byCompany(Company company) throws Exception
	 * { return dao.byCompany(company.getId()); }
	 */

	public void create(TimesheetDetails entity) throws Exception {
		calcMinutesUpd(entity);
		this.dao.create(entity);
	}

	// private void calcMinutes(TimesheetDetails tsd) throws Exception {
	// int minutes;
	// int hours;
	// if (tsd.getMinutes() == null) {
	// if (tsd.getHours() != null) {
	// hours = tsd.getHours().intValue();
	// minutes = calcCorrectMinutes(hours, tsd.getHours() - hours).intValue();
	// tsd.setMinutes(minutes);
	// tsd.setHours((double) hours);
	// }
	// }
	//
	// }

	private Double calcCorrectMinutes(Integer hours, Double minutes) {
		Integer minute;
		minutes = (minutes * 60);
		minute = minutes.intValue();
		if (minute == 60) {
			hours++;
		}
		return minutes;
	}

	private void calcMinutesUpd(TimesheetDetails tsd) throws Exception {
		int minutes;
		int hours;
		if (tsd.getHours() != null) {
			hours = tsd.getHours().intValue();
			minutes = calcCorrectMinutes(hours, tsd.getHours() - hours).intValue();
			if (tsd.getMinutes() + minutes >= 60) {
				int h = (tsd.getMinutes() + minutes) / 60;
				hours += h;
				tsd.setMinutes((tsd.getMinutes() + minutes) - (h * 60));
			} else {
				tsd.setMinutes(tsd.getMinutes() + minutes);
			}

			tsd.setHours((double) hours);
		}
	}

	public void update(TimesheetDetails entity) throws Exception {
		calcMinutesUpd(entity);
		this.dao.update(entity);
	}

	public void delete(TimesheetDetails entity) throws Exception {
		this.dao.delete(entity);
	}

	public TimesheetDetails findByKeyChild(long id) throws Exception {
		return dao.findByKey(id);
	}

	public TimesheetDetails findByKey(long id) throws Exception {
		return getDetailInfo(dao.findByKey(id));
	}

	public List<TimesheetDetails> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<TimesheetDetails> findByTimesheet(Timesheet timesheet) throws Exception {
		return getDetailInfo(dao.findByTimesheet(timesheet.getId()));
	}

	public List<TimesheetDetails> findByTimesheetBetweenDates(Timesheet timesheet) throws Exception {
		return getDetailInfo(dao.findByTimesheetBetweenDates(timesheet.getId(),
				GenericUtility.getFirstDayOfMonth(getSynchronizedDate()), getSynchronizedDate()));
	}

	public List<TimesheetDetails> findByForCompUserTimesheetBetweenDates(Timesheet timesheet, Date fromDate, Date toDate) throws Exception {
		return getDetailInfo(dao.findByTimesheetBetweenDates(timesheet.getId(), fromDate, toDate));
	}

	public List<TimesheetDetails> findByForCompUserTimesheetBetweenDates(Company company, Timesheet timesheet,
			Date fromDate, Date toDate) throws Exception {
		return getDetailInfo(company, dao.findByTimesheetBetweenDates(timesheet.getId(), fromDate, toDate));
	}

	public List<TimesheetDetails> findByForCompUserTimesheetBetweenDatesDirector(Timesheet timesheet, Date fromDate,
			Date toDate) throws Exception {
		return getDetailInfo(dao.findByTimesheetBetweenDates(timesheet.getId(), fromDate, toDate));
	}

	public List<TimesheetDetails> findByTimesheetDetail(TimesheetDetails timesheetDetail) throws Exception {
		return dao.findByTimesheetDetail(timesheetDetail.getId());
	}

	private TimesheetDetails getDetailInfo(TimesheetDetails timesheetDetails) throws Exception {
		timesheetDetails.setTimesheetDetailList(findByTimesheetDetail(timesheetDetails));
		calcHours(timesheetDetails);
		return timesheetDetails;
	}

	private List<TimesheetDetails> getDetailInfo(Company company, List<TimesheetDetails> timesheetDetails)
			throws Exception {
		for (TimesheetDetails timesheetDetail : timesheetDetails) {
			timesheetDetail.setTimesheetDetailList(findByTimesheetDetail(timesheetDetail));
			calcHours(company, timesheetDetail);
		}
		return timesheetDetails;
	}

	public void calcHours(Company company, TimesheetDetails details) {
		Double totHours = 0.0;
		Integer totMins = 0;
		for (TimesheetDetails timesheetDetails : details.getTimesheetDetailList()) {
			if (timesheetDetails.getProjects().getCompany().equals(company)) {
				totHours += timesheetDetails.getHours();
				totMins += timesheetDetails.getMinutes();
			}
		}
		if (totMins > 60) {
			int h = (int) (totMins / 60);
			totHours += h;
			totMins -= h * 60;
		}
		details.setMinutes(totMins);
		details.setHours(totHours);
	}

	private List<TimesheetDetails> getDetailInfo(List<TimesheetDetails> timesheetDetails) throws Exception {
		for (TimesheetDetails timesheetDetail : timesheetDetails) {
			timesheetDetail.setTimesheetDetailList(findByTimesheetDetail(timesheetDetail));
			calcHours(timesheetDetail);
		}
		return timesheetDetails;
	}

	public void calcHours(TimesheetDetails details) {
		try {
			Double totHours = 0.0;
			Integer totMins = 0;
			if (details.getTimesheetDetailList() == null) {
				details.setTimesheetDetailList(findByTimesheetDetail(details));
			}
			for (TimesheetDetails timesheetDetails : details.getTimesheetDetailList()) {
				totHours += timesheetDetails.getHours();
				totMins += timesheetDetails.getMinutes();
			}
			if (totMins > 60) {
				int h = (int) (totMins / 60);
				totHours += h;
				totMins -= h * 60;
			}
			details.setMinutes(totMins);
			details.setHours(totHours);
		} catch (Exception e) {
			logger.error(e.getCause(), e);
		}
	}

	public boolean calcTot(TimesheetDetails details, TimesheetDetails detailsChild) {
		try {
			Double totalMinutes = 0.0;
			int totMins = 0;

			if (details.getTimesheetDetailList() == null) {
				details.setTimesheetDetailList(findByTimesheetDetail(details));
			}
			if (details.getTimesheetDetailList().contains(detailsChild)) {
				details.getTimesheetDetailList().remove(detailsChild);
			}
			for (TimesheetDetails timesheetDetails : details.getTimesheetDetailList()) {
				totalMinutes += timesheetDetails.getHours();
				totMins += timesheetDetails.getMinutes();
			}
			totalMinutes += detailsChild.getHours();
			totMins += detailsChild.getMinutes();
			totalMinutes += (double) totMins / 100;
			totalMinutes = totalMinutes * 60;

			if (totalMinutes > 1440) {
				return false;

			}
		} catch (Exception e) {
			logger.error(e.getCause(), e);
		}
		return true;
	}

	public boolean checkAllDay(TimesheetDetails details, TimesheetDetails detailsChild) {
		boolean allDay = false;
		TimesheetDetails timesheetDetail = new TimesheetDetails();
		for (TimesheetDetails timesheetDetails : details.getTimesheetDetailList()) {
			if (timesheetDetails.getAllDayEvent() != null && timesheetDetails.getAllDayEvent()) {
				allDay = true;
				timesheetDetail = timesheetDetails;
				break;
			}
		}
		if (!allDay) {
			if (detailsChild.getAllDayEvent() != null && detailsChild.getAllDayEvent() && timesheetDetail.getId() != null) {
				return true;
			} else {
				return allDay;
			}
		} else {
			
			if (detailsChild.equals(timesheetDetail)) {
				return false;
			}
			return allDay;
		}

	}

	private List<TimesheetDetails> calcHours(List<TimesheetDetails> list) {
		double hrs = 60.0d;
		for (TimesheetDetails tsd : list) {
			if (tsd.getMinutes() != null) {
				double min = tsd.getMinutes().doubleValue();
				double hours = min / hrs;
				tsd.setHours(hours);
			}
		}
		return list;
	}
	
	public List<ProjectUsersReportBean> locateUsersProjectsReportByDateRange(Date fromDate, Date toDate) throws Exception {	
		return populateAdditionalInformationListProjectUsersReportBean(dao.locateUsersProjectsReportByDateRange(fromDate, toDate));
	}
	
	public List<ProjectUsersReportBean> populateAdditionalInformationListProjectUsersReportBean(List<ProjectUsersReportBean> projectUsersReportBeanList) throws Exception{
		for (ProjectUsersReportBean projectUsersReportBean : projectUsersReportBeanList) {
			populateAdditionalInformationProjectUsersReportBean(projectUsersReportBean);
		}
		return projectUsersReportBeanList;
	}
	
	
	public ProjectUsersReportBean populateAdditionalInformationProjectUsersReportBean(ProjectUsersReportBean bean) throws Exception{
		calculateHours(bean);
		return bean;
	}

	/**
	 * Calculate hours and minutes for ProjectUsersReportBean result
	 * @param bean
	 * @see ProjectUsersReportBean
	 * @throws Exception
	 */
	public void calculateHours(ProjectUsersReportBean bean) throws Exception{
		if (bean.getTotalHours() == null) {
			bean.setTotalHours((double) 0);
		}
		if (bean.getTotalMinutes() == null) {
			bean.setTotalMinutes(BigDecimal.valueOf((long) 0));
		}
		Double hours = bean.getTotalHours();
		Long mins = bean.getTotalMinutes().longValue();
		if (mins != null && mins >= 60) {
			int h = (int) (mins / 60);
			hours += h;
			mins -= h * 60;
		}
		bean.setTotalHours(hours);
		bean.setTotalMinutes(BigDecimal.valueOf(mins));
	}
	
	public List<TimesheetDetails> findByTimesheetDetailBetweenDatesAndUser(Long userId, Date fromDate, Date toDate) throws Exception {
		return dao.findByTimesheetDetailBetweenDatesAndUser(userId, fromDate, toDate);
	}
}
