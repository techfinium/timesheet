package haj.com.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.dao.MailLogDAO;
import haj.com.entity.Doc;
import haj.com.entity.DocByte;
import haj.com.entity.MailLog;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class MailLogService extends AbstractService {

	private static final MailLogDAO dao = new MailLogDAO();
	protected static final Log logger = LogFactory.getLog(MailLogService.class);
	private UsersService usersService = new UsersService();

	public List<MailLog> allMailLog() throws Exception {
		return dao.allMailLog();
	}

	public static void create(MailLog entity) {
		try {
			entity.setCreateDate(new Date());
			entity.setFailed(Boolean.FALSE);
			if (entity.getUser() != null) {
				entity.setEmail(entity.getUser().getEmail());
			}
			dao.create(entity);
		} catch (Exception e) {
			logger.fatal(e);
		}
	}

	public static void update(MailLog entity) {
		try {
			if (entity != null && entity.getId() != null) {
				entity.setSendDate(new Date());
				dao.update(entity);
			}
		} catch (Exception e) {
			logger.fatal(e);
		}
	}

	public static void update(MailLog entity, Exception error) {
		try {
			if (entity != null && entity.getId() != null) {
				entity.setErrorMsg(error.getMessage());
				entity.setFailed(Boolean.TRUE);
				dao.update(entity);
			}
		} catch (Exception e) {
			logger.fatal(e);
		}
	}
	
	public static void update(MailLog mailLog, String attachmentName, String mime, byte[] attachment) throws Exception {
		update(mailLog);
		List<IDataEntity> entityList = new ArrayList<IDataEntity>();
		Doc doc = new Doc();
		doc.setMailLog(mailLog);
		doc.setOriginalFname(attachmentName);
		if (mime.indexOf('/') > -1) {
			mime = mime.substring(mime.indexOf("/")+1);
		}
		doc.setExtension(mime);
		entityList.add(doc);
		entityList.add(new DocByte(attachment, doc));
		dao.createBatch(entityList);
	}
	

	public void delete(MailLog entity) throws Exception {
		dao.delete(entity);
	}

	public MailLog findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<MailLog> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<MailLog> findUser(Users user) throws Exception {
		return dao.findUser(user.getUid());
	}

	/*
	 * public List<MailLog> findByCompany(haj.com.entity.Company company) throws
	 * Exception { return dao.findByCompany(company.getId()); }
	 */

	public List<MailLog> allMailLog(Integer startingAt, int noRows) throws Exception {
		return dao.allMailLog(startingAt, noRows);
	}

	public Long countMailLog() throws Exception {
		return dao.count(MailLog.class);
	}

	public List<MailLog> findByEmail(String email, Integer startingAt, int noRows) throws Exception {
		return dao.findByEmail('%' + email.trim() + '%', startingAt, noRows);
	}

	public Long countByEmail(String email) throws Exception {
		return dao.countByEmail('%' + email.trim() + '%');
	}
	
	public List<MailLog> allMailLogBetweenDates(Date startDate, Date endDate) throws Exception {
		return dao.allMailLogBetweenDates(startDate, endDate);
	}

//	public void populateUsersMailLog() throws Exception {
//		// gathers all emails in database
//		List<MailLog> mailList = dao.allMailLog();
//		// loops through all emails
//		for (MailLog mailLog : mailList) {
//			if (mailLog.getUser() == null) {
//				// finds user via email, email is unique in database
//				mailLog.setUser(usersService.getUserByEmail(mailLog.getEmail()));
//				update(mailLog);
//			}
//		}
//
//	}
}
