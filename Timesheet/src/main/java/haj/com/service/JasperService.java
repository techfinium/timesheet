package haj.com.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.axis.utils.ByteArrayOutputStream;
import org.apache.commons.io.FileUtils;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import haj.com.constants.HAJConstants;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.framework.AbstractService;
import haj.com.provider.MySQLProvider;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

public class JasperService extends AbstractService {
	private static final long serialVersionUID = 1L;

	private static final String path = HAJConstants.APP_PATH;
	private static final String sub_path = "/reports/";
	private static final String _pdfContent = "application/pdf";
	private static final String _excelContent = "application/vnd.ms-excel";
	// private static final String _wordContent = "application/msword";
	private static final String workPath = "/Users/hendrik/Downloads/";

	public static void createReportFromXMLtoServletOutputStream(String report, Map<String, Object> params, String xml, String filename) throws Exception {
		byte b[] = createReportFromXMLtoByteArray(report, params, xml);
		convertByteArrayToServletOutputStream(b, filename);
	}

	@SuppressWarnings("serial")
	public void createReportFromDBtoServletOutputStream(String report, Map<String, Object> params, String filename) throws Exception {
		try {
			params.put("PATH", path);
			params.put("REPORT_LOCALE", new Locale("en", "US"));
			params.put("SUBREPORT_DIR", path + sub_path);
			Session session = new AbstractDAO() {

				@Override
				public AbstractDataProvider getDataProvider() {
					// TODO Auto-generated method stub
					return new MySQLProvider();
				}
			}.getSession();

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			InputStream reportStream = new BufferedInputStream(new FileInputStream(path + sub_path + report));
			session.doWork(new RunReportToPdfStreamHelper(reportStream, out, params));
			// JasperRunManager.runReportToPdfStream(reportStream, out, params);
			byte[] b = out.toByteArray();
			session.close();
			out.close();
			out.flush();

			convertByteArrayToServletOutputStream(b, filename);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	/**
	 * Generic method to write a byte array to a Servlet Output Stream
	 * 
	 * @param bytes
	 * @param filename
	 * @throws Exception
	 */
	private static void convertByteArrayToServletOutputStream(byte[] bytes, String filename) throws Exception {
		FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
		Object resp = context.getExternalContext().getResponse();
		if (resp instanceof HttpServletResponse) {
			HttpServletResponse response = createRespObj((HttpServletResponse) resp, _pdfContent, filename);
			ServletOutputStream out = response.getOutputStream();
			out.write(bytes);
			out.flush();
			out.close();
			context.responseComplete();
		}
	}

	private static void convertByteArrayToServletOutputStreamExcel(byte[] bytes, String filename) throws Exception {
		FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
		Object resp = context.getExternalContext().getResponse();
		if (resp instanceof HttpServletResponse) {
			HttpServletResponse response = createRespObj((HttpServletResponse) resp, _excelContent, filename);
			ServletOutputStream out = response.getOutputStream();
			out.write(bytes);
			out.flush();
			out.close();
			context.responseComplete();
		}
	}

	/**
	 * Generic method to create a Jasper report to a byteArray
	 * 
	 * @param report
	 * @param params
	 * @param xml
	 * @return byte[] array containing the report
	 * @throws Exception
	 */
	private static byte[] createReportFromXMLtoByteArray(String report, Map<String, Object> params, String xml) throws Exception {
		params.put("SUBREPORT_DIR", path + sub_path);
		params.put("PATH", path);
		params.put("REPORT_LOCALE", new Locale("en", "ZA"));
		InputStream reportStream = new BufferedInputStream(new FileInputStream(path + sub_path + report));
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder(); // throws
																// ParserConfigurationException
		StringReader reader = new StringReader(xml);
		InputSource inputData = new InputSource(reader);
		Document inputDocument = builder.parse(inputData);

		params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, inputDocument);
		params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
		params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
		params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.ENGLISH);
		params.put(JRParameter.REPORT_LOCALE, Locale.ENGLISH);
		JasperRunManager.runReportToPdfStream(reportStream, out, params);
		byte[] rtn = out.toByteArray();
		out.close();
		return rtn;
	}

	private static HttpServletResponse createRespObj(HttpServletResponse response, String contentType, String fileName) {
		response.setContentType(contentType);
		response.setHeader("Content-disposition", "attachment; filename=" + fileName);
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		return response;
	}

	@SuppressWarnings("serial")
	public void createReportFromDBtoFile(String report, Map<String, Object> params, String filenameOut, String filename) throws Exception {
		try {
			params.put("PATH", path);
			params.put("REPORT_LOCALE", new Locale("en", "ZA"));
			params.put("SUBREPORT_DIR", path + sub_path);
			Session session = new AbstractDAO() {

				@Override
				public AbstractDataProvider getDataProvider() {
					// TODO Auto-generated method stub
					return new MySQLProvider();
				}
			}.getSession();

			// java.sql.Connection conn =
			// HibernateUtil.getSessionFactory().getCurrentSession().connection();
			// String filename = "pn_" + filenameOut + ".pdf";
			File f = new File(workPath + filename);
			OutputStream out = new FileOutputStream(f);
			InputStream reportStream = new BufferedInputStream(new FileInputStream(path + sub_path + report));
			session.doWork(new RunReportToPdfStreamHelper(reportStream, out, params));
			out.flush();
			out.close();

			convertByteArrayToServletOutputStream(FileUtils.readFileToByteArray(new File(workPath + filename)), filenameOut);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private String formatArray(Address[] arr) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			sb.append(arr[i].toString());
			if (i < arr.length - 1)
				sb.append(", ");
		}
		return sb.toString();
	}

	class RunReportToPdfStreamHelper implements Work {
		private InputStream inputStream;
		private OutputStream outputStream;
		private Map<?, ?> parameters;

		public RunReportToPdfStreamHelper(InputStream inputStream, OutputStream outputStream, Map<?, ?> parameters) {
			this.inputStream = inputStream;
			this.outputStream = outputStream;
			this.parameters = parameters;
		}

		@Override
		public void execute(Connection conn) throws SQLException {
			try {
				JasperRunManager.runReportToPdfStream(inputStream, outputStream, (Map<String, Object>) parameters, conn);
			} catch (JRException e) {
				throw new SQLException(e);
			}
		}

	}

	public void createReportFromXML(String report, Map<String, Object> params, String xml, boolean draft, String filename) throws Exception {
		FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
		Object resp = context.getExternalContext().getResponse();
		if (resp instanceof HttpServletResponse) {
			HttpServletResponse response = createRespObj((HttpServletResponse) resp, _pdfContent, filename);
			InputStream reportStream = new BufferedInputStream(new FileInputStream(path + sub_path + report));
			ServletOutputStream out = response.getOutputStream();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder(); // throws
																	// ParserConfigurationException
			StringReader reader = new StringReader(xml);
			InputSource inputData = new InputSource(reader);
			Document inputDocument = builder.parse(inputData);

			params.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT, inputDocument);
			params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "yyyy-MM-dd");
			params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
			params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.ENGLISH);
			params.put(JRParameter.REPORT_LOCALE, Locale.ENGLISH);
			JasperRunManager.runReportToPdfStream(reportStream, out, params);
			out.flush();
			out.close();
			context.responseComplete();
			// out.close();
		}

	}

	public void exportToExcel(String report, Map<String, Object> params, String filename) throws Exception {
		try {
			
			Session session = new AbstractDAO() {

				@Override
				public AbstractDataProvider getDataProvider() {
					// TODO Auto-generated method stub
					return new MySQLProvider();
				}
			}.getSession();
			session.doWork(
				    new Work() {
				        public void execute(Connection connection) throws SQLException 
				        { 
				        	try {
								JasperPrint jasperPrint = JasperFillManager.fillReport(path + sub_path + report, params, connection);
								
								JRXlsxExporter Xlsxexporter = new JRXlsxExporter();
								Xlsxexporter.setExporterInput(new SimpleExporterInput(jasperPrint));
								ByteArrayOutputStream out = new ByteArrayOutputStream();
								Xlsxexporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
								SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
								configuration.setDetectCellType(true);
								configuration.setCollapseRowSpan(false);
								Xlsxexporter.exportReport();
								byte[] b = out.toByteArray();
								out.close();
								out.flush();
								convertByteArrayToServletOutputStreamExcel(b, filename);
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				        }
				    }
				);
			session.close();
			

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
