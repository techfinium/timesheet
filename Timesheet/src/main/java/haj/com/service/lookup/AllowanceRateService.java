package haj.com.service.lookup;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.lookup.AllowanceRateDAO;
import haj.com.entity.lookup.AllowanceRate;
import haj.com.framework.AbstractService;

public class AllowanceRateService extends AbstractService {
	/** The dao. */

	private AllowanceRateDAO dao = new AllowanceRateDAO();

	/**
	 * Get all AllowanceRate
	 * 
	 * @author TechFinium
	 * @see AllowanceRate
	 * @return a list of {@link haj.com.entity.AllowanceRate}
	 * @throws Exception the exception
	 */
	public List<AllowanceRate> allAllowanceRate() throws Exception {
		return dao.allAllowanceRate();
	}

	/**
	 * Create or update AllowanceRate.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see AllowanceRate
	 */
	public void create(AllowanceRate entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else
			this.dao.update(entity);
	}

	/**
	 * Update AllowanceRate.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see AllowanceRate
	 */
	public void update(AllowanceRate entity) throws Exception {
		this.dao.update(entity);
	}

	/**
	 * Delete AllowanceRate.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see AllowanceRate
	 */
	public void delete(AllowanceRate entity) throws Exception {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key.
	 *
	 * @author TechFinium
	 * @param id the id
	 * @return a {@link haj.com.entity.AllowanceRate}
	 * @throws Exception the exception
	 * @see AllowanceRate
	 */
	public AllowanceRate findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	@SuppressWarnings("unchecked")
	public List<AllowanceRate> allAllowanceRate(Class<AllowanceRate> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<AllowanceRate>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	public int count(Class<AllowanceRate> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	public AllowanceRate findMostRecent() throws Exception {
		return dao.findMostRecent();
	}

}
