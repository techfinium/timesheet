package haj.com.service.lookup;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.SortOrder;

import haj.com.dao.lookup.VehicleVariantDAO;
import haj.com.entity.lookup.VehicleVariant;
import haj.com.framework.AbstractService;

@Named
public class VehicleVariantService extends AbstractService {
	/** The dao. */
	@Inject
	private VehicleVariantDAO dao;

	/**
	 * Get all VehicleVariant
 	 * @author TechFinium 
 	 * @see   VehicleVariant
 	 * @return a list of {@link haj.com.entity.VehicleVariant}
	 * @throws Exception the exception
 	 */
	public List<VehicleVariant> allVehicleVariant() throws Exception {
	  	return dao.allVehicleVariant();
	}


	/**
	 * Create or update VehicleVariant.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see     VehicleVariant
	 */
	public void create(VehicleVariant entity) throws Exception  {
	   if (entity.getId() ==null) {
		 this.dao.create(entity);
	   }
		else
		 this.dao.update(entity);
	}


	/**
	 * Update  VehicleVariant.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see    VehicleVariant
	 */
	public void update(VehicleVariant entity) throws Exception  {
		this.dao.update(entity);
	}

	/**
	 * Delete  VehicleVariant.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see    VehicleVariant
	 */
	public void delete(VehicleVariant entity) throws Exception  {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key.
	 *
	 * @author TechFinium
	 * @param id the id
	 * @return a {@link haj.com.entity.VehicleVariant}
	 * @throws Exception the exception
	 * @see    VehicleVariant
	 */
	public VehicleVariant findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	/**
	 * Find VehicleVariant by description.
	 *
	 * @author TechFinium
	 * @param desc the desc
	 * @return a list of {@link haj.com.entity.VehicleVariant}
	 * @throws Exception the exception
	 * @see    VehicleVariant
	 */
	public List<VehicleVariant> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	public List<String> findByMake(String make) throws Exception {
		return dao.findByMake(make);
	}
	
	public List<String> findByMakeAndModel(String make, String model) throws Exception {
		return dao.findByMakeAndModel(make, model);
	}
	
	public List<String> findDerivativeByMakeAndModel(String derivative, String model, String make) throws Exception {
		return dao.findDerivativeByMakeAndModel(derivative, model, make);
	}
	
	public List<Integer> findAllModelYears(Integer year, String make) throws Exception {
		return dao.findAllModelYears(year, make);
	}
	
	/**
	 * Lazy load VehicleVariant
	 * @param first from key
	 * @param pageSize no of rows to fetch
	 * @return a list of {@link haj.com.entity.VehicleVariant}
	 * @throws Exception the exception
	 */
	public List<VehicleVariant> allVehicleVariant(int first, int pageSize) throws Exception {
		return dao.allVehicleVariant(Long.valueOf(first), pageSize);
	}
		
	
	/**
	 * Get count of VehicleVariant for lazy load
	 * @author TechFinium 
	 * @return Number of rows in the VehicleVariant
	 * @throws Exception the exception
	 */
	public Long count() throws Exception {
	       return dao.count(VehicleVariant.class);
	}
	
    /**
     * Lazy load VehicleVariant with pagination, filter, sorting
     * @author TechFinium 
     * @param class1 VehicleVariant class
     * @param first the first
     * @param pageSize the page size
     * @param sortField the sort field
     * @param sortOrder the sort order
     * @param filters the filters
     * @return  a list of {@link haj.com.entity.VehicleVariant} containing data
     * @throws Exception the exception
     */	
	@SuppressWarnings("unchecked")
	public List<VehicleVariant> allVehicleVariant(Class<VehicleVariant> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<VehicleVariant>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * Get count of VehicleVariant for lazy load with filters
     * @author TechFinium 
     * @param entity VehicleVariant class
     * @param filters the filters
     * @return Number of rows in the VehicleVariant entity
     * @throws Exception the exception     
     */	
	public int count(Class<VehicleVariant> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
}
