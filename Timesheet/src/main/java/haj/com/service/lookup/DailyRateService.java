package haj.com.service.lookup;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.lookup.DailyRateDAO;
import haj.com.entity.lookup.DailyRate;
import haj.com.framework.AbstractService;

public class DailyRateService extends AbstractService {
	/** The dao. */
	
	private DailyRateDAO dao = new DailyRateDAO();

	/**
	 * Get all DailyRate
 	 * @author TechFinium 
 	 * @see   DailyRate
 	 * @return a list of {@link haj.com.entity.DailyRate}
	 * @throws Exception the exception
 	 */
	public List<DailyRate> allDailyRate() throws Exception {
	  	return dao.allDailyRate();
	}


	/**
	 * Create or update DailyRate.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see     DailyRate
	 */
	public void create(DailyRate entity) throws Exception  {
	   if (entity.getId() ==null) {
		 this.dao.create(entity);
	   }
		else
		 this.dao.update(entity);
	}


	/**
	 * Update  DailyRate.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see    DailyRate
	 */
	public void update(DailyRate entity) throws Exception  {
		this.dao.update(entity);
	}

	/**
	 * Delete  DailyRate.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see    DailyRate
	 */
	public void delete(DailyRate entity) throws Exception  {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key.
	 *
	 * @author TechFinium
	 * @param id the id
	 * @return a {@link haj.com.entity.DailyRate}
	 * @throws Exception the exception
	 * @see    DailyRate
	 */
	public DailyRate findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}
	
	public DailyRate findMostRecent() throws Exception {
		return dao.findMostRecent();
	}
	
	@SuppressWarnings("unchecked")
	public List<DailyRate> allDailyRate(Class<DailyRate> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<DailyRate>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}
	public int count(Class<DailyRate> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

}
