package haj.com.service.lookup;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.lookup.CronicDiseasesDAO;
import haj.com.entity.lookup.CronicDiseases;
import haj.com.framework.AbstractService;

public class CronicDiseasesService extends AbstractService {

	private CronicDiseasesDAO dao = new CronicDiseasesDAO();

	public List<CronicDiseases> allCronicDiseases() throws Exception {
	  	return dao.allCronicDiseases();
	}


	public void create(CronicDiseases entity) throws Exception  {
	   if (entity.getId() ==null)
		 this.dao.create(entity);
		else
		 this.dao.update(entity);
	}

	public void update(CronicDiseases entity) throws Exception  {
		this.dao.update(entity);
	}

	public void delete(CronicDiseases entity) throws Exception  {
		this.dao.delete(entity);
	}

	public CronicDiseases findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<CronicDiseases> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	  /**
     * Lazy load CronicDiseases with pagination, filter, sorting
     * @author TechFinium 
     * @param class1 CronicDiseases class
     * @param first the first
     * @param pageSize the page size
     * @param sortField the sort field
     * @param sortOrder the sort order
     * @param filters the filters
     * @return  a list of {@link haj.com.entity.CronicDiseases} containing data
     * @throws Exception the exception
     */	
	@SuppressWarnings("unchecked")
	public List<CronicDiseases> allCronicDiseases(Class<CronicDiseases> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<CronicDiseases>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * Get count of CronicDiseases for lazy load with filters
     * @author TechFinium 
     * @param entity CronicDiseases class
     * @param filters the filters
     * @return Number of rows in the CronicDiseases entity
     * @throws Exception the exception     
     */	
	public int count(Class<CronicDiseases> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}

/*
	public List<CronicDiseases> findByCompany(haj.com.entity.Company company) throws Exception {
		return dao.findByCompany(company.getId());
	}
*/	
}
