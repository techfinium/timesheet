package haj.com.service.lookup;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.lookup.EngineTypeDAO;
import haj.com.entity.lookup.EngineType;
import haj.com.framework.AbstractService;

public class EngineTypeService extends AbstractService {
	/** The dao. */
	
	private EngineTypeDAO dao = new EngineTypeDAO();

	/**
	 * Get all EngineType
 	 * @author TechFinium 
 	 * @see   EngineType
 	 * @return a list of {@link haj.com.entity.EngineType}
	 * @throws Exception the exception
 	 */
	public List<EngineType> allEngineType() throws Exception {
	  	return dao.allEngineType();
	}


	/**
	 * Create or update EngineType.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see     EngineType
	 */
	public void create(EngineType entity) throws Exception  {
	   if (entity.getId() ==null) {
		 this.dao.create(entity);
	   }
		else
		 this.dao.update(entity);
	}


	/**
	 * Update  EngineType.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see    EngineType
	 */
	public void update(EngineType entity) throws Exception  {
		this.dao.update(entity);
	}

	/**
	 * Delete  EngineType.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see    EngineType
	 */
	public void delete(EngineType entity) throws Exception  {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key.
	 *
	 * @author TechFinium
	 * @param id the id
	 * @return a {@link haj.com.entity.EngineType}
	 * @throws Exception the exception
	 * @see    EngineType
	 */
	public EngineType findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}
	@SuppressWarnings("unchecked")
	public List<EngineType> allEngineType(Class<EngineType> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<EngineType>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}
	public int count(Class<EngineType> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

}
