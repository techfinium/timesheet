package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.MonitoringRegisterDAO;
import haj.com.entity.AddressTypeEnum;
import haj.com.entity.MonitoringRegister;
import haj.com.entity.Users;
import haj.com.entity.Visitor;
import haj.com.entity.VisitorDeclarationForm;
import haj.com.entity.enums.CovidPersonRegisterTypeEnum;
import haj.com.entity.enums.VisitorTypeEnum;
import haj.com.framework.AbstractService;

public class MonitoringRegisterService extends AbstractService {

	private MonitoringRegisterDAO dao = new MonitoringRegisterDAO();
	private VisitorService visitorService = new VisitorService();
	private AddressService addressService = new AddressService();
	private UsersService usersService = new UsersService();

	public List<MonitoringRegister> allMonitoringRegister() throws Exception {
		return dao.allMonitoringRegister();
	}

	public void create(MonitoringRegister entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}
	
	public void createRegisterForm(MonitoringRegister monitoringRegister) throws Exception {
		if (monitoringRegister.getPersonType().equals(CovidPersonRegisterTypeEnum.Visitor)) {
			createRegisterFormVisitor(monitoringRegister);
		} else if (monitoringRegister.getPersonType().equals(CovidPersonRegisterTypeEnum.Employee)) {
			createRegisterFormUser(monitoringRegister);
		}
	}

	public void createRegisterFormVisitor(MonitoringRegister monitoringRegister) throws Exception {
		monitoringRegister.setTargetClass((monitoringRegister.getVisitor().getClass().getName()));
		monitoringRegister.setTargetKey(monitoringRegister.getVisitor().getId());
		create(monitoringRegister);
	}

	public void createRegisterFormUser(MonitoringRegister monitoringRegister) throws Exception {
		monitoringRegister.setTargetClass((monitoringRegister.getUser().getClass().getName()));
		monitoringRegister.setTargetKey(monitoringRegister.getUser().getUid());
		create(monitoringRegister);
	}

	public void update(MonitoringRegister entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(MonitoringRegister entity) throws Exception {
		this.dao.delete(entity);
	}

	public MonitoringRegister findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<MonitoringRegister> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	/**
	 * Lazy load MonitoringRegister with pagination, filter, sorting
	 * 
	 * @author TechFinium
	 * @param class1    MonitoringRegister class
	 * @param first     the first
	 * @param pageSize  the page size
	 * @param sortField the sort field
	 * @param sortOrder the sort order
	 * @param filters   the filters
	 * @return a list of {@link haj.com.entity.MonitoringRegister} containing data
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public List<MonitoringRegister> allMonitoringRegister(Class<MonitoringRegister> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return resolveTransientValues((List<MonitoringRegister>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters));

	}

	/**
	 * Get count of MonitoringRegister for lazy load with filters
	 * 
	 * @author TechFinium
	 * @param entity  MonitoringRegister class
	 * @param filters the filters
	 * @return Number of rows in the MonitoringRegister entity
	 * @throws Exception the exception
	 */
	public int count(Class<MonitoringRegister> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	/*
	 * public List<MonitoringRegister> findByCompany(haj.com.entity.Company company)
	 * throws Exception { return dao.findByCompany(company.getId()); }
	 */

	public List<MonitoringRegister> resolveTransientValues(List<MonitoringRegister> l) throws Exception {
		for (MonitoringRegister monitoringRegister : l) {
			if (monitoringRegister.getTargetClass().equals(Users.class.getName())) {
				monitoringRegister.setUser(usersService.findByKey(monitoringRegister.getTargetKey()));
				monitoringRegister.getUser().setPhysicalAddress(addressService.findByTagetClassTagerKeyAndAddressType(monitoringRegister.getUser().getClass().getName(), monitoringRegister.getUser().getUid(), AddressTypeEnum.Residential));
			} else if (monitoringRegister.getTargetClass().equals(Visitor.class.getName())) {
				monitoringRegister.setVisitor(visitorService.findByKey(monitoringRegister.getTargetKey()));
				monitoringRegister.getVisitor().setPhysicalAddress(addressService.findByTagetClassTagerKeyAndAddressType(monitoringRegister.getVisitor().getClass().getName(), monitoringRegister.getVisitor().getId(), AddressTypeEnum.Residential));
			}
		}
		return l;
	}

	public void populateUserData(MonitoringRegister monitoringRegister) throws Exception {
		monitoringRegister.getUser().setPhysicalAddress(addressService.findByTagetClassTagerKeyAndAddressType(monitoringRegister.getUser().getClass().getName(), monitoringRegister.getUser().getUid(), AddressTypeEnum.Residential));
	}

	public void populateVisitorData(MonitoringRegister monitoringRegister) throws Exception {
		monitoringRegister.getVisitor().setPhysicalAddress(addressService.findByTagetClassTagerKeyAndAddressType(monitoringRegister.getVisitor().getClass().getName(), monitoringRegister.getVisitor().getId(), AddressTypeEnum.Residential));
	}

}
