package haj.com.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;

import org.apache.commons.lang3.time.DateUtils;
import org.primefaces.model.SortOrder;

import haj.com.bean.TaskReportBean;
import haj.com.dao.TasksDAO;
import haj.com.entity.Company;
import haj.com.entity.ExpenseForm;
import haj.com.entity.HostingCompany;
import haj.com.entity.HostingCompanyEmployees;
import haj.com.entity.HostingCompanyProcess;
import haj.com.entity.MailDef;
import haj.com.entity.ProcessRoles;
import haj.com.entity.TaskUsers;
import haj.com.entity.Tasks;
import haj.com.entity.Users;
import haj.com.entity.UsersRole;
import haj.com.entity.enums.ConfigDocProcessEnum;
import haj.com.entity.enums.RagEnum;
import haj.com.entity.enums.TaskStatusEnum;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;
import haj.com.service.lookup.MailTemplatesService;
import haj.com.utils.GenericUtility;

// TODO: Auto-generated Javadoc
/**
 * The Class TasksService.
 */
public class TasksService extends AbstractService {

	private TasksDAO                     dao                          = new TasksDAO();
	private UsersService                 usersService                 = new UsersService();
	private HostingCompanyProcessService hostingCompanyProcessService = new HostingCompanyProcessService();
	private ProcessRolesService          processRolesService          = new ProcessRolesService();
	private UsersRoleService             usersRoleService             = new UsersRoleService();
	private CompanyUsersService          companyUsersService          = new CompanyUsersService();
	private static TasksService          tasksService                 = null;

	/**
	 * Instance.
	 * @return the tasks service
	 */
	public static synchronized TasksService instance() {
		if (tasksService == null) {
			tasksService = new TasksService();
		}
		return tasksService;
	}

	/**
	 * Instantiates a new tasks service.
	 */
	public TasksService() {
		super();
	}

	/**
	 * Instantiates a new tasks service.
	 * @param auditlog
	 * the auditlog
	 * @param resourceBundle
	 * the resource bundle
	 */
	public TasksService(Map<String, Object> auditlog, ResourceBundle resourceBundle) {
		super(auditlog, resourceBundle);
	}

	/**
	 * Instantiates a new tasks service.
	 * @param auditlog
	 * the auditlog
	 */
	public TasksService(Map<String, Object> auditlog) {
		super(auditlog);
	}

	/**
	 * Instantiates a new tasks service.
	 * @param resourceBundle
	 * the resource bundle
	 */
	public TasksService(ResourceBundle resourceBundle) {
		super(resourceBundle);
	}

	/**
	 * Get all Tasks.
	 * @author TechFinium
	 * @return a list of {@link haj.com.entity.Tasks}
	 * @throws Exception
	 * the exception
	 * @see Tasks
	 */
	public List<Tasks> allTasks() throws Exception {
		return dao.allTasks();
	}

	/**
	 * Create or update Tasks.
	 * @author TechFinium
	 * @param entity
	 * the entity
	 * @throws Exception
	 * the exception
	 * @see Tasks
	 */
	public void create(Tasks entity) throws Exception {
		if (entity.getId() == null) {
			this.dao.create(entity);
		} else this.dao.update(entity);
	}

	/**
	 * Update Tasks.
	 * @author TechFinium
	 * @param entity
	 * the entity
	 * @throws Exception
	 * the exception
	 * @see Tasks
	 */
	public void update(Tasks entity) throws Exception {
		this.dao.update(entity);
	}

	/**
	 * Delete Tasks.
	 * @author TechFinium
	 * @param entity
	 * the entity
	 * @throws Exception
	 * the exception
	 * @see Tasks
	 */
	public void delete(Tasks entity) throws Exception {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key.
	 * @author TechFinium
	 * @param id
	 * the id
	 * @return a {@link haj.com.entity.Tasks}
	 * @throws Exception
	 * the exception
	 * @see Tasks
	 */
	public Tasks findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public Tasks findByGuid(String id) throws Exception {
		return dao.findByGuid(id);
	}

	public long findUserCountForTask(ConfigDocProcessEnum docProcessEnum, Long userId) throws Exception {
		return dao.findUserCountForTask(docProcessEnum, userId);
	}

	/**
	 * Find Tasks by description.
	 * @author TechFinium
	 * @param desc
	 * the desc
	 * @return a list of {@link haj.com.entity.Tasks}
	 * @throws Exception
	 * the exception
	 * @see Tasks
	 */
	public List<Tasks> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	/**
	 * Lazy load Tasks.
	 * @param first
	 * from key
	 * @param pageSize
	 * no of rows to fetch
	 * @return a list of {@link haj.com.entity.Tasks}
	 * @throws Exception
	 * the exception
	 */
	public List<Tasks> allTasks(int first, int pageSize) throws Exception {
		return dao.allTasks(Long.valueOf(first), pageSize);
	}

	/**
	 * Get count of Tasks for lazy load.
	 * @author TechFinium
	 * @return Number of rows in the Tasks
	 * @throws Exception
	 * the exception
	 */
	public Long count() throws Exception {
		return dao.count(Tasks.class);
	}

	/**
	 * Find tasks by user incomplete.
	 * @param user
	 * the user
	 * @return the list
	 * @throws Exception
	 * the exception
	 */
	public List<Tasks> findTasksByUserIncomplete(Users user) throws Exception {
		return dao.calcRag(dao.findTasksByUserIncomplete(user.getUid()));
	}

	/**
	 * Find tasks by user incomplete count.
	 * @param user
	 * the user
	 * @return the long
	 * @throws Exception
	 * the exception
	 */
	public long findTasksByUserIncompleteCount(Users user) throws Exception {
		return dao.findTasksByUserIncompleteCount(user.getUid());
	}

	public void checkRag(Tasks task) {
		if (task.getTaskStatus() == TaskStatusEnum.Completed) task.setRag(RagEnum.Green);
		else if (task.getDueDate() == null) task.setRag(RagEnum.Green);
		else if (DateUtils.isSameDay(task.getDueDate(), getSynchronizedDate())) task.setRag(RagEnum.Amber);
		else if (task.getDueDate().after(getSynchronizedDate())) task.setRag(RagEnum.Green);
		else task.setRag(RagEnum.Red);
	}

	/**
	 * Lazy load Tasks with pagination, filter, sorting.
	 * @author TechFinium
	 * @param class1
	 * Tasks class
	 * @param first
	 * the first
	 * @param pageSize
	 * the page size
	 * @param sortField
	 * the sort field
	 * @param sortOrder
	 * the sort order
	 * @param filters
	 * the filters
	 * @return a list of {@link haj.com.entity.Tasks} containing data
	 * @throws Exception
	 * the exception
	 */
	@SuppressWarnings("unchecked")
	public List<Tasks> allTasks(Class<Tasks> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		String hql = null;
		if (sortField == null && filters.isEmpty()) {
			// hql = "select o from Tasks o left join fetch o.createUser left join fetch
			// o.actionUser where o.taskStatus <> :completed order by o.createDate";
			hql = "select o from Tasks o left join fetch o.createUser left join fetch o.actionUser left join fetch o.processRole where o.taskStatus <> :completed and o.taskStatus <> :taskStatusError order by datediff(o.startDate,o.createDate) desc";

		} else hql = "select o from Tasks o left join fetch o.createUser left join fetch o.actionUser where o.taskStatus <> :completed and o.taskStatus <> :taskStatusError ";
		filters.put("completed", TaskStatusEnum.Completed);
		filters.put("taskStatusError", TaskStatusEnum.ERROR);
		return dao.calcRag((List<Tasks>) dao.sortAndFilterWhere(first, pageSize, sortField, sortOrder, filters, hql));

	}

	@SuppressWarnings("unchecked")
	public List<Tasks> allTasksNoSort(Class<Tasks> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		String hql = null;
		hql = "select o from Tasks o left join fetch o.createUser left join fetch o.actionUser where o.taskStatus <> :completed and o.taskStatus <> :taskStatusError";
		filters.put("completed", TaskStatusEnum.Completed);
		filters.put("taskStatusError", TaskStatusEnum.ERROR);
		return dao.calcRag((List<Tasks>) dao.sortAndFilterWhere(first, pageSize, sortField, sortOrder, filters, hql));

	}

	@SuppressWarnings("unchecked")
	public List<Tasks> allTasksNoSortWithCompleted(Class<Tasks> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		String hql = null;
		hql = "select o from Tasks o left join fetch o.createUser left join fetch o.actionUser where o.taskStatus <> :taskStatusError";
		filters.put("taskStatusError", TaskStatusEnum.ERROR);
		return dao.calcRag((List<Tasks>) dao.sortAndFilterWhere(first, pageSize, sortField, sortOrder, filters, hql));

	}

	@SuppressWarnings("unchecked")
	public List<Tasks> findTasksByUserIncomplete(Class<Tasks> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, Users user) throws Exception {
		String hql = "select distinct(o.task) from TaskUsers o left join fetch o.task.actionUser au where o.user.id  = :userId " + "and o.task.taskStatus <> :taskStatusComplete and o.task.taskStatus <> :taskStatusClosed and o.task.taskStatus <> :taskStatusError";
		filters.put("userId", user.getUid());
		filters.put("taskStatusComplete", TaskStatusEnum.Completed);
		filters.put("taskStatusClosed", TaskStatusEnum.Closed);
		filters.put("taskStatusError", TaskStatusEnum.ERROR);
		return dao.calcRag((List<Tasks>) dao.sortAndFilterWhere(first, pageSize, sortField, sortOrder, filters, hql));
	}

	public int findTasksByUserIncompleteCount(Class<Tasks> entity, Map<String, Object> filters) throws Exception {
		String hql = "select count(distinct o.task) from TaskUsers o where o.user.id  = :userId and o.task.taskStatus <> :taskStatusComplete and o.task.taskStatus <> :taskStatusClosed and o.task.taskStatus <> :taskStatusError";
		return dao.countWhere(filters, hql);
	}

	public int countWithCompleted(Class<Tasks> entity, Map<String, Object> filters) throws Exception {
		String hql = "select count(o) from Tasks o where o.taskStatus <> :taskStatusError";
		return dao.countWhere(filters, hql);
	}

	public int countNotCompleted(Class<Tasks> entity, Map<String, Object> filters) throws Exception {
		String hql = "select count(o) from Tasks o where o.taskStatus <> :completed and o.taskStatus <> :taskStatusError";
		return dao.countWhere(filters, hql);
	}

	/**
	 * Find tasks by type and key.
	 * @param docProcessEnum
	 * the doc process enum
	 * @param targetKey
	 * the target key
	 * @return the list
	 * @throws Exception
	 * the exception
	 */
	public List<Tasks> findTasksByTypeAndKey(ConfigDocProcessEnum docProcessEnum, Long targetKey) throws Exception {
		return dao.calcRag(dao.findTasksByTypeAndKey(docProcessEnum, targetKey));
	}

	public List<Tasks> findTasksByTypeAndKeyLast(ConfigDocProcessEnum docProcessEnum, Long targetKey) throws Exception {
		return dao.findTasksByTypeAndKeyLast(docProcessEnum, targetKey);
	}

	public List<Tasks> allTasksByType(Class<Tasks> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters, boolean employee, TaskStatusEnum taskStatusEnum) throws Exception {

		return dao.allTasksByType(class1, first, pageSize, sortField, sortOrder, filters, employee, taskStatusEnum);
	}

	public int countByType(Class<Tasks> entity, Map<String, Object> filters, Boolean employee, TaskStatusEnum taskStatusEnum) throws Exception {

		return dao.countByType(entity, filters, employee, taskStatusEnum);
	}

	/**
	 * Get count of Tasks for lazy load with filters.
	 * @author TechFinium
	 * @param entity
	 * Tasks class
	 * @param filters
	 * the filters
	 * @return Number of rows in the Tasks entity
	 * @throws Exception
	 * the exception
	 */
	public int count(Class<Tasks> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	/**
	 * Find first in process and create task.
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param targetClass
	 * the target class
	 * @param sendMail
	 * the send mail
	 * @param processEnum
	 * the process enum
	 * @param users
	 * TODO
	 * @throws Exception
	 * the exception
	 */
	public void findFirstInProcessAndCreateTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, ConfigDocProcessEnum processEnum, MailDef mailDef, List<Users> users) throws Exception {
		HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.allHostingCompanyProcess(HostingCompanyService.instance().findByKey(1l), processEnum);
		if (hostingCompanyProcess != null) {
			List<ProcessRoles> first = processRolesService.firstProcessRoles(hostingCompanyProcess);
			if (first.size() > 0) {
				if (first.get(0).getNote() != null && !first.get(0).getNote().isEmpty()) {
					description = first.get(0).getNote();
				}
				description = replaceStrings(targetClass, description, targetKey, createUser);

				if (users != null && users.size() > 0) createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, users, mailDef, null);
				else if (first.get(0).getCompanyUserType() == null) createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, mailDef, null);
				else {
					users = new ArrayList<>();
					getUsers(targetKey, targetClass, users, processEnum);
					createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, users, mailDef, null);
				}

			} else {
				throw new Exception("Hosting company setup error. No process roles assigned to Hosting company.");
			}
		}

	}

	public void findFirstInProcessAndCreateTask(Users createUser, Long targetKey, String targetClass, ConfigDocProcessEnum processEnum, boolean sendMail) throws Exception {
		HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.allHostingCompanyProcess(HostingCompanyService.instance().findByKey(1L), processEnum);
		String                description           = "";
		if (hostingCompanyProcess != null) {
			List<ProcessRoles> first = processRolesService.firstProcessRoles(hostingCompanyProcess);
			if (first.size() > 0) {
				if (first.get(0).getNote() != null && !first.get(0).getNote().isEmpty()) {
					description = first.get(0).getNote();
				}
				description = replaceStrings(targetClass, description, targetKey, createUser);
				if (first.get(0).getCompanyUserType() == null) createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, null, null);
				else {
					List<Users> users = new ArrayList<>();
					getUsers(targetKey, targetClass, users, processEnum);
					createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, users, null, null);
				}
			} else {
				throw new Exception("Hosting company setup error. No process roles assigned to Hosting company.");
			}
		}

	}

	public void findFirstInProcessAndCreateTask(Users createUser, Long targetKey, String targetClass, ConfigDocProcessEnum processEnum) throws Exception {
		HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.allHostingCompanyProcess(HostingCompanyService.instance().findByKey(1L), processEnum);
		String                description           = "";
		if (hostingCompanyProcess != null) {
			List<ProcessRoles> first = processRolesService.firstProcessRoles(hostingCompanyProcess);
			if (first.size() > 0) {
				if (first.get(0).getNote() != null && !first.get(0).getNote().isEmpty()) {
					description = first.get(0).getNote();
				}
				description = replaceStrings(targetClass, description, targetKey, createUser);
				if (first.get(0).getCompanyUserType() == null) createWorkFlowTask(description, createUser, targetKey, targetClass, true, true, first.get(0), processEnum, hostingCompanyProcess, null, null);
				else {
					List<Users> users = new ArrayList<>();
					getUsers(targetKey, targetClass, users, processEnum);
					createWorkFlowTask(description, createUser, targetKey, targetClass, true, true, first.get(0), processEnum, hostingCompanyProcess, users, null, null);
				}
			} else {
				throw new Exception("Hosting company setup error. No process roles assigned to Hosting company.");
			}
		}

	}

	private void getUsers(Long targetKey, String targetClass, List<Users> users, ConfigDocProcessEnum configDocProcessEnum) throws Exception {
		if (targetClass.equals(Users.class.getName())) {
			users.add(usersService.findByKey(targetKey));
		} 
	}

	public void findByPositionAndCreateTask(int position, String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, HostingCompany hostingCompany, ConfigDocProcessEnum processEnum, MailDef mailDef, String extraInfo, boolean useDesc) throws Exception {
		HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.allHostingCompanyProcess(hostingCompany, processEnum);
		List<ProcessRoles>    first                 = processRolesService.findByPosition(hostingCompanyProcess, position);
		if (first.size() > 0) {
			if (first.get(0).getNote() != null && !first.get(0).getNote().isEmpty() && useDesc) {
				description = replaceStrings(targetClass, first.get(0).getNote(), targetKey, createUser);
			}

			if (first.get(0).getCompanyUserType() == null) {
				createWorkFlowTask(description + extraInfo, createUser, targetKey, targetClass, sendMail, false, first.get(0), processEnum, hostingCompanyProcess, mailDef, null);
			} else {
				List<Users> users = new ArrayList<>();
				getUsers(targetKey, targetClass, users, processEnum);
				createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, false, first.get(0), processEnum, hostingCompanyProcess, users, mailDef, null);
			}

		} else {
			throw new Exception("Hosting company setup error. No process roles assigned to Hosting company.");
		}

	}

	public void findNextInProcessAndCreateTask(Users createUser, Tasks tasks, List<Users> users) throws Exception {
		List<ProcessRoles> first = processRolesService.findNextProcessRoles(tasks.getProcessRole());
		if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess().getNextProcess() != null) {
			HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findByKey(tasks.getProcessRole().getHostingCompanyProcess().getId());
			first = processRolesService.firstProcessRoles(hostingCompanyProcess.getNextProcess());
		}
		createProcess("", createUser, tasks.getTargetKey(), tasks.getTargetClass(), true, false, tasks, first, false, null, "", users);
		completeTask(tasks);
	}

	public void findPreviousInProcessAndCreateTask(Users createUser, Tasks tasks, List<Users> users) throws Exception {
		if (!tasks.getFirstInProcess()) {
			boolean            firstInProcess = true;
			List<ProcessRoles> first          = processRolesService.findPreviousProcessRoles(tasks.getProcessRole());
			if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess() != null) {
				HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findPreviousProcess(tasks.getProcessRole().getHostingCompanyProcess());
				first = processRolesService.lastProcessRoles(hostingCompanyProcess);
				firstInProcess = false;
			} else {
				firstInProcess = processRolesService.findPreviousProcessRolesCount(first.get(0)) == 0;
			}
			createProcess("", createUser, tasks.getTargetKey(), tasks.getTargetClass(), true, firstInProcess, tasks, first, true, null, "", users);
			completeTask(tasks);
		}
	}

	public void findFirstInProcessAndCreateTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, HostingCompany hostingCompany, ConfigDocProcessEnum processEnum, MailDef mailDef, String extraInfo) throws Exception {
		HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.allHostingCompanyProcess(hostingCompany, processEnum);
		if (hostingCompanyProcess != null) {
			List<ProcessRoles> first = processRolesService.firstProcessRoles(hostingCompanyProcess);
			if (first.size() > 0) {
				if (first.get(0).getNote() != null && !first.get(0).getNote().isEmpty()) {
					description = first.get(0).getNote();
				}
				description = replaceStrings(targetClass, description, targetKey, createUser);
				createWorkFlowTask(description + extraInfo, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, mailDef, null);
			} else {
				throw new Exception("Hosting company setup error. No process roles assigned to Hosting company.");
			}
		}

	}

	/**
	 * Find next in process and create task.
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param targetClass
	 * the target class
	 * @param sendMail
	 * the send mail
	 * @param tasks
	 * the tasks
	 * @throws Exception
	 * the exception
	 */
	public void findNextInProcessAndCreateTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, Tasks tasks, MailDef mailDef, String extraInfo) throws Exception {
		List<ProcessRoles> first = processRolesService.findNextProcessRoles(tasks.getProcessRole());
		if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess().getNextProcess() != null) {
			HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findByKey(tasks.getProcessRole().getHostingCompanyProcess().getId());
			first = processRolesService.firstProcessRoles(hostingCompanyProcess.getNextProcess());
		}
		// createProcess(description, createUser, targetKey, targetClass, sendMail,
		// false, tasks, first, false, mailDef, extraInfo);
		createProcess(description, createUser, targetKey, targetClass, sendMail, false, tasks, first, false, mailDef, extraInfo, null);
		completeTask(tasks);
	}

	/**
	 * Find previous in process and create task.
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param targetClass
	 * the target class
	 * @param sendMail
	 * the send mail
	 * @param tasks
	 * the tasks
	 * @throws Exception
	 * the exception
	 */
	public void findPreviousInProcessAndCreateTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, Tasks tasks, MailDef mailDef, String extraInfo) throws Exception {

		if (!tasks.getFirstInProcess()) {
			boolean            firstInProcess = true;
			List<ProcessRoles> first          = processRolesService.findPreviousProcessRoles(tasks.getProcessRole());
			if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess() != null) {
				HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findPreviousProcess(tasks.getProcessRole().getHostingCompanyProcess());
				first = processRolesService.lastProcessRoles(hostingCompanyProcess);
				firstInProcess = false;
			} else {
				firstInProcess = processRolesService.findPreviousProcessRolesCount(first.get(0)) == 0;
			}
			createProcess(description, createUser, targetKey, targetClass, sendMail, firstInProcess, tasks, first, true, mailDef, extraInfo, null);
			completeTask(tasks);
		}
	}

	public void findPreviousInProcessAndCreateTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, Tasks tasks, MailDef mailDef) throws Exception {
		String extraInfo = null;

		if (!tasks.getFirstInProcess()) {
			boolean            firstInProcess = true;
			List<ProcessRoles> first          = processRolesService.findPreviousProcessRoles(tasks.getProcessRole());
			if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess() != null) {
				HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findPreviousProcess(tasks.getProcessRole().getHostingCompanyProcess());
				first = processRolesService.lastProcessRoles(hostingCompanyProcess);
				firstInProcess = false;
			} else {
				firstInProcess = processRolesService.findPreviousProcessRolesCount(first.get(0)) == 0;
			}
			createProcess(description, createUser, targetKey, targetClass, sendMail, firstInProcess, tasks, first, true, mailDef, extraInfo, null);
			completeTask(tasks);
		}
	}

	public void findNextInProcessAndCreateTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, Tasks tasks, MailDef mailDef, List<Users> users) throws Exception {
		List<ProcessRoles> first = processRolesService.findNextProcessRoles(tasks.getProcessRole());
		if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess().getNextProcess() != null) {
			HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findByKey(tasks.getProcessRole().getHostingCompanyProcess().getId());
			first = processRolesService.firstProcessRoles(hostingCompanyProcess.getNextProcess());
		}
		createProcess(description, createUser, targetKey, targetClass, sendMail, false, tasks, first, false, mailDef, "", users);
		completeTask(tasks);
	}

	/**
	 * Find previous in process and create task.
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param targetClass
	 * the target class
	 * @param sendMail
	 * the send mail
	 * @param tasks
	 * the tasks
	 * @param users
	 * TODO
	 * @throws Exception
	 * the exception
	 */
	public void findPreviousInProcessAndCreateTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, Tasks tasks, MailDef mailDef, List<Users> users) throws Exception {

		if (!tasks.getFirstInProcess()) {
			boolean            firstInProcess = true;
			List<ProcessRoles> first          = processRolesService.findPreviousProcessRoles(tasks.getProcessRole());
			if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess() != null) {
				HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findPreviousProcess(tasks.getProcessRole().getHostingCompanyProcess());
				first = processRolesService.lastProcessRoles(hostingCompanyProcess);
				firstInProcess = false;
			} else {
				firstInProcess = processRolesService.findPreviousProcessRolesCount(first.get(0)) == 0;
			}
			createProcess(description, createUser, targetKey, targetClass, sendMail, firstInProcess, tasks, first, true, mailDef, "", users);
			completeTask(tasks);
		}
	}

	/**
	 * Creates the process.
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param targetClass
	 * the target class
	 * @param sendMail
	 * the send mail
	 * @param firstInProcess
	 * the first in process
	 * @param tasks
	 * the tasks
	 * @param first
	 * the first
	 * @param useRejectMessage
	 * the use reject message
	 * @throws Exception
	 * the exception
	 */
	private void createProcess(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, boolean firstInProcess, Tasks tasks, List<ProcessRoles> first, boolean useRejectMessage, MailDef mailDef, String extraInfo, List<Users> users) throws Exception {
		if (first.size() > 0) {

			if (useRejectMessage) {
				if (first.get(0).getRejectMessage() != null && !first.get(0).getRejectMessage().isEmpty()) {
					description = first.get(0).getRejectMessage();
				}
				description += extraInfo;
			} else {
				if (first.size() > 0 && first.get(0).getNote() != null && !first.get(0).getNote().isEmpty()) {
					description = first.get(0).getNote();
				}
				description += extraInfo;
			}

			description = replaceStrings(targetClass, description, targetKey, (tasks != null) ? tasks.getActionUser() : null);

			HostingCompanyProcess hostingCompanyProcess = first.get(0).getHostingCompanyProcess();

			if (hostingCompanyProcess.getWorkflowProcess() == null) hostingCompanyProcess.setWorkflowProcess(tasks.getWorkflowProcess());

			if (users != null && users.size() > 0) {
				createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, firstInProcess, first.get(0), tasks.getWorkflowProcess(), hostingCompanyProcess, users, mailDef, tasks);
			} else if (first.get(0).getCompanyUserType() == null) {
				createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, firstInProcess, first.get(0), tasks.getWorkflowProcess(), hostingCompanyProcess, mailDef, tasks);
			} else {
				users = new ArrayList<>();
				getUsers(targetKey, targetClass, users, tasks.getWorkflowProcess());
				createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, firstInProcess, first.get(0), tasks.getWorkflowProcess(), hostingCompanyProcess, users, mailDef, tasks);
			}
		}
	}

	/**
	 * Send mail.
	 * @param users
	 * the to
	 * @param subject
	 * the subject
	 * @param processEnum
	 * the msg
	 */
	private void sendMail(Users user, String subject, MailDef mailDef, String description) {
		if (user.getRecieveEmailTaskNotification() != null && user.getRecieveEmailTaskNotification()) {
			// Code required
			if (mailDef == null) {
				GenericUtility.sendMail(user, subject, description, null);
			} else {
				MailTemplatesService.instance().sendMail(user, subject, mailDef, description);
			}
		}

	}

	private void sendMail(String email, String subject, String description) {
		GenericUtility.sendMail(email, subject, description, null);

	}

	/**
	 * Creates the work flow task.
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param targetClass
	 * the target class
	 * @param sendMail
	 * the send mail
	 * @param firstInProcess
	 * the first in process
	 * @param processRoles
	 * the process roles
	 * @param processEnum
	 * the process enum
	 * @param hostingCompanyProcess
	 * the hosting company process
	 * @throws Exception
	 * the exception
	 */
	private void createWorkFlowTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, boolean firstInProcess, ProcessRoles processRoles, ConfigDocProcessEnum processEnum, HostingCompanyProcess hostingCompanyProcess, MailDef mailDef, Tasks task) throws Exception {

		if (checkCanCreateTask(targetClass, targetKey, processRoles)) {
			List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();
			// Task information
			int numbeOfDays = 5;
			// if (processRoles.getNumberOfDays() != null) numbeOfDays =
			// processRoles.getNumberOfDays();

			Tasks tasks = new Tasks(description, createUser, targetKey, targetClass, TaskStatusEnum.NotStarted, GenericUtility.addDaysToDateExcludeWeekends(getSynchronizedDate(), numbeOfDays), UUID.randomUUID().toString());

			tasks.setProcessRole(processRoles);
			tasks.setWorkflowProcess(processEnum);
			tasks.setFirstInProcess(firstInProcess);
			tasks.setHostingCompanyProcess(hostingCompanyProcess);
			tasks.setPreviousTask(task);
			dataEntities.add(tasks);
			if (processRoles.getCompanyUserType() == null) {
				// Users for workflow process
				List<UsersRole> usersRoles = usersRoleService.findByProcess(processRoles);
				for (UsersRole usersRole : usersRoles) {

					dataEntities.add(new TaskUsers(usersRole.getUsers(), tasks));
					if (sendMail) {
						this.sendMail(usersRole.getUsers(), "New task created on NTA VETERP portal", mailDef, description);
					}
				}
			} else {
				List<Users> users = new ArrayList<>();
				getUsers(targetKey, targetClass, users, processEnum);
				if (sendMail) {
					for (Users u : users) {
						dataEntities.add(new TaskUsers(u, tasks));
						this.sendMail(u, "New task created on NTA VETERP portal", mailDef, description);
					}
				}
			}
			this.dao.createBatch(dataEntities);
		} else {
			System.out.println("No Task created");
		}
	}

	/**
	 * Creates the work flow task.
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param targetClass
	 * the target class
	 * @param sendMail
	 * the send mail
	 * @param firstInProcess
	 * the first in process
	 * @param processRoles
	 * the process roles
	 * @param processEnum
	 * the process enum
	 * @param hostingCompanyProcess
	 * the hosting company process
	 * @param users
	 * the users
	 * @throws Exception
	 * the exception
	 */
	private void createWorkFlowTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, boolean firstInProcess, ProcessRoles processRoles, ConfigDocProcessEnum processEnum, HostingCompanyProcess hostingCompanyProcess, List<Users> users, MailDef mailDef, Tasks previousTask) throws Exception {

		if (checkCanCreateTask(targetClass, targetKey, processRoles)) {

			List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();
			// Task information
			Tasks tasks = new Tasks(description, createUser, targetKey, targetClass, TaskStatusEnum.NotStarted, GenericUtility.add3DaysCountingFromStartDate(getSynchronizedDate()), UUID.randomUUID().toString());
			tasks.setProcessRole(processRoles);
			tasks.setWorkflowProcess(processEnum);
			tasks.setFirstInProcess(firstInProcess);
			tasks.setHostingCompanyProcess(hostingCompanyProcess);
			tasks.setPreviousTask(previousTask);
			dataEntities.add(tasks);
			for (Users user : users) {
				dataEntities.add(new TaskUsers(user, tasks));
				if (sendMail) {
					this.sendMail(user, "New task created on NTA VETERP portal", mailDef, description);
				}
			}
			this.dao.createBatch(dataEntities);

		} else {
			System.out.println("No Task created");
		}
	}
	/**
	 * Creates the task.
	 * @param comp
	 * the comp
	 * @param subject
	 * the subject
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param sendMail
	 * the send mail
	 * @param createTask
	 * the create task
	 * @param task
	 * the task
	 * @param user
	 * the user
	 * @param configDocProcessEnum
	 * the config doc process enum
	 * @return the tasks
	 * @throws Exception
	 * the exception
	 */
	public Tasks createTask(Company comp, String subject, String description, Users createUser, Long targetKey, boolean sendMail, boolean createTask, Tasks task, Users user, ConfigDocProcessEnum configDocProcessEnum, MailDef mailDef) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();
		Tasks             tasks        = null;
		if (createTask) {
			tasks = new Tasks(description, createUser, targetKey, Company.class.toString(), TaskStatusEnum.NotStarted, GenericUtility.add3DaysCountingFromStartDate(getSynchronizedDate()), UUID.randomUUID().toString());
			tasks.setWorkflowProcess(configDocProcessEnum);
			tasks.setPreviousTask(task);
			dataEntities.add(tasks);
			dataEntities.add(new TaskUsers(user, tasks));
		}
		this.sendMail(user, "New task created on NTA VETERP portal", mailDef, description);
		completeTask(task);
		this.dao.createBatch(dataEntities);
		return tasks;
	}

	/**
	 * Creates the task.
	 * @param targetClass
	 * the target class
	 * @param comp
	 * the comp
	 * @param emailMessage
	 * the email message
	 * @param subject
	 * the subject
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param sendMail
	 * the send mail
	 * @param createTask
	 * the create task
	 * @param task
	 * the task
	 * @param users
	 * the users
	 * @param configDocProcessEnum
	 * the config doc process enum
	 * @return the tasks
	 * @throws Exception
	 * the exception
	 */
	public Tasks createTask(String targetClass, Company comp, String emailMessage, String subject, String description, Users createUser, Long targetKey, boolean sendMail, boolean createTask, Tasks task, List<Users> users, ConfigDocProcessEnum configDocProcessEnum, MailDef mailDef) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();
		Tasks             tasks        = null;
		if (createTask) {
			tasks = new Tasks(description, createUser, targetKey, targetClass, TaskStatusEnum.NotStarted, GenericUtility.add3DaysCountingFromStartDate(getSynchronizedDate()), UUID.randomUUID().toString());
			tasks.setWorkflowProcess(configDocProcessEnum);
			tasks.setPreviousTask(task);
			dataEntities.add(tasks);
		}
		for (Users user : users) {
			if (createTask) {
				dataEntities.add(new TaskUsers(user, tasks));
			}
			this.sendMail(user, "New task created on NTA VETERP portal", mailDef, description);
		}
		completeTask(task);
		this.dao.createBatch(dataEntities);
		return tasks;
	}

	public Tasks createTaskEachUser(String targetClass, Company comp, String emailMessage, String subject, String description, Users createUser, Long targetKey, boolean sendMail, boolean createTask, Tasks task, List<Users> users, ConfigDocProcessEnum configDocProcessEnum, MailDef mailDef) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();
		Tasks             tasks        = null;
		for (Users user : users) {
			if (createTask) {
				tasks = new Tasks(description, createUser, targetKey, targetClass, TaskStatusEnum.NotStarted, GenericUtility.add3DaysCountingFromStartDate(getSynchronizedDate()), UUID.randomUUID().toString());
				tasks.setWorkflowProcess(configDocProcessEnum);
				tasks.setPreviousTask(task);
				dataEntities.add(tasks);
				dataEntities.add(new TaskUsers(user, tasks));
			}
			this.sendMail(user, "New task created on NTA VETERP portal", mailDef, description);
		}
		completeTask(task);
		this.dao.createBatch(dataEntities);
		return tasks;
	}

	
	public void createTaskUser(List<Users> users, String targetClass, Long targetKey, String description, Users createUser, boolean sendMail, boolean createTask, Tasks task, ConfigDocProcessEnum configDocProcessEnum, boolean firstInProcess) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();

		for (Users user : users) {
			Tasks tasks = null;
			if (createTask) {
				description = replaceStrings(targetClass, description, targetKey, createUser);
				tasks = new Tasks(description, createUser, targetKey, targetClass, TaskStatusEnum.NotStarted, GenericUtility.add3DaysCountingFromStartDate(getSynchronizedDate()), UUID.randomUUID().toString());
				tasks.setWorkflowProcess(configDocProcessEnum);
				tasks.setPreviousTask(task);
				tasks.setFirstInProcess(firstInProcess);
				dataEntities.add(tasks);
				dataEntities.add(new TaskUsers(user, tasks));
			}
			if (sendMail) {
				this.sendMail(user, "New task created on NTA VETERP portal", null, description);
			}
		}
		completeTask(task);
		this.dao.createBatch(dataEntities);
		// return tasks;
	}

	public void createTaskUser(Users user, String targetClass, Long targetKey, String description, Users createUser, boolean sendMail, boolean createTask, Tasks task, ConfigDocProcessEnum configDocProcessEnum, boolean firstInProcess) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();

		Tasks tasks = null;
		if (createTask) {
			description = replaceStrings(targetClass, description, targetKey, createUser);
			tasks = new Tasks(description, createUser, targetKey, targetClass, TaskStatusEnum.NotStarted, GenericUtility.add3DaysCountingFromStartDate(getSynchronizedDate()), UUID.randomUUID().toString());
			tasks.setWorkflowProcess(configDocProcessEnum);
			tasks.setPreviousTask(task);
			tasks.setFirstInProcess(firstInProcess);
			dataEntities.add(tasks);
			dataEntities.add(new TaskUsers(user, tasks));
		}
		if (sendMail) {
			this.sendMail(user, "New task created on NTA VETERP portal", null, description);
		}
		completeTask(task);
		this.dao.createBatch(dataEntities);
		// return tasks;
	}

	/**
	 * Complete task.
	 * @param task
	 * the task
	 * @throws Exception
	 * the exception
	 */
	public void completeTask(Tasks task) throws Exception {
		if (task != null) {
			task.setTaskStatus(TaskStatusEnum.Completed);
			update(task);
		}
	}

	/**
	 * Complete task by target key.
	 * @param docProcessEnum
	 * the doc process enum
	 * @param targetKey
	 * the target key
	 * @throws Exception
	 * the exception
	 */
	public void completeTaskByTargetKey(ConfigDocProcessEnum docProcessEnum, Long targetKey) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();
		List<Tasks>       t            = findTasksByTypeAndKey(docProcessEnum, targetKey);
		for (Tasks tasks : t) {
			tasks.setTaskStatus(TaskStatusEnum.Completed);
			dataEntities.add(tasks);
		}
		dao.updateBatch(dataEntities);
	}

	private String replaceStrings(String targetClass, String description, long targetKey, Users previousUser) throws Exception {
		if (description != null) {
			IDataEntity target = dao.getByClassAndKey(targetClass, targetKey);
			if (previousUser != null) {
				description = description.replace("#PREVIOUS_FIRST_NAME#", previousUser.getFirstName());
				description = description.replace("#PREVIOUS_LAST_NAME#", previousUser.getLastName());
				description = description.replace("#PREVIOUS_EMAIL#", previousUser.getEmail());
			}

			if (targetClass.equals(ExpenseForm.class.getName())) {
				description = replaceStringsUsers((ExpenseForm) target, description);
			}
		}
		return description;
	}
	
	private String replaceStringsUsers(ExpenseForm u, String description) {
		String toReturn = description;
		toReturn = toReturn.replace("#FIRST_NAME#", u.getUser().getFirstName());
		toReturn = toReturn.replace("#LAST_NAME#", u.getUser().getLastName());
		toReturn = toReturn.replace("#EMAIL#", u.getUser().getEmail());
		toReturn = toReturn.replace("#IDENTITY_NUMBER#", u.getUser().getIdNumber());
		return toReturn;
	}



	public void completeTaskByTargetKey(String targetClass, Long targetKey) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();
		List<Tasks>       t            = findTasksByTypeAndKey(targetClass, targetKey);
		for (Tasks tasks : t) {
			tasks.setTaskStatus(TaskStatusEnum.Completed);
			dataEntities.add(tasks);
		}
		dao.updateBatch(dataEntities);
	}

	public void assignUsersToTask(List<UsersRole> usersRoles, Tasks tasks, boolean sendMail) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<>();
		for (UsersRole usersRole : usersRoles) {
			dataEntities.add(new TaskUsers(usersRole.getUsers(), tasks));
			if (sendMail) {
				this.sendMail(usersRole.getUsers(), "New task created on NTA VETERP portal", null, tasks.getDescription());
			}
		}
		dao.createBatch(dataEntities);
	}

	public void assignUserListToTask(List<Users> users, Tasks tasks, boolean sendMail) throws Exception {
		List<IDataEntity> dataEntities = new ArrayList<>();
		for (Users user : users) {
			dataEntities.add(new TaskUsers(user, tasks));
			if (sendMail) {
				this.sendMail(user, "New task created on Techfinium timesheet", null, tasks.getDescription());
			}
		}
		dao.createBatch(dataEntities);
	}

	public List<TaskReportBean> findTaskBySDFCompanyforCompany(Company company) throws Exception {
		if (company == null || company.getId() == null) throw new Exception("Need company details to run this report");

		return dao.findTaskBySDFCompanyforCompany(company.getId());
	}


	public List<TaskReportBean> findTaskByUser(Users user) throws Exception {
		return dao.findTaskByUser(user.getUid());
	}

	public List<TaskReportBean> findTaskForCompany(Company company) throws Exception {
		return dao.findTaskForCompany(company.getId());
	}

	public List<TaskReportBean> allTasksForCompany(Company company) throws Exception {
		List<TaskReportBean> l = findTaskBySDFCompanyforCompany(company);
		l.addAll(findTaskForCompany(company));
		return (l);
	}


	public List<TaskUsers> findOpenTaskUsers(ConfigDocProcessEnum workflwoProcess) throws Exception {
		return dao.findOpenTaskUsers(workflwoProcess);
	}

	public List<Tasks> findTasksByTypeAndKey(String targetClass, Long targetKey) throws Exception {
		return dao.findTasksByTypeAndKey(targetClass, targetKey);
	}

	public boolean checkCanCreateTask(String targetClass, Long targetKey, ProcessRoles pr) {
		if (pr.getTargetClass() != null && !pr.getTargetClass().isEmpty() && pr.getTargetMethod() != null && !pr.getTargetMethod().isEmpty()) {
			try {
				IDataEntity target = dao.getByClassAndKey(targetClass, targetKey);
				Object      obj    = Class.forName(pr.getTargetClass()).newInstance();
				Method      m      = obj.getClass().getDeclaredMethod(pr.getTargetMethod(), Class.forName(targetClass), ProcessRoles.class);
				return (boolean) m.invoke(obj, target, pr);
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/**
	 * Locates a list of tasks assigned to employees
	 * @see Tasks
	 * @see HostingCompanyEmployees
	 * @throws Exception
	 */

	public void findFirstInProcessAndCreateTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, ConfigDocProcessEnum processEnum) throws Exception {
		ArrayList<Users>      users                 = null;
		HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.allHostingCompanyProcess(HostingCompanyService.instance().findByKey(1L), processEnum);
		if (hostingCompanyProcess != null) {
			List<ProcessRoles> first = processRolesService.firstProcessRoles(hostingCompanyProcess);
			if (first.size() > 0) {
				if (first.get(0).getNote() != null && !first.get(0).getNote().isEmpty()) {
					description = first.get(0).getNote();
				}
				// description = FixTaskDataService.instance().replaceStrings(targetClass,
				// description, targetKey, createUser);

				if (users != null && users.size() > 0) createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, users, null, null, false);
				else if (first.get(0).getCompanyUserType() == null) createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, null, null);
				else {
					users = new ArrayList<>();
					/// FixTaskDataService.instance().getUsers(targetKey, targetClass, users,
					/// processEnum, true, first.get(0), createUser);
					createWorkFlowTask(description, createUser, targetKey, targetClass, true, true, first.get(0), processEnum, hostingCompanyProcess, users, null, null, false);
				}

			} else {
				throw new Exception("Hosting company setup error. No process roles assigned to Hosting company.");
			}
		}

	}

	public void findNextInProcessAndCreateTaskNullUsers(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, Tasks tasks, MailDef mailDef) throws Exception {

		List<Users>        users = null;
		List<ProcessRoles> first = processRolesService.findNextProcessRoles(tasks.getProcessRole());
		if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess().getNextProcess() != null) {
			HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findByKey(tasks.getProcessRole().getHostingCompanyProcess().getId());
			first = processRolesService.firstProcessRoles(hostingCompanyProcess.getNextProcess());
		}
		createProcess(description, createUser, targetKey, targetClass, sendMail, false, tasks, first, false, mailDef, "", users, false);
		completeTask(tasks);
	}

	public void findFirstInProcessAndCreateTaskNullUsers(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, ConfigDocProcessEnum processEnum, MailDef mailDef) throws Exception {
		List<Users>           users                 = null;
		HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.allHostingCompanyProcess(HostingCompanyService.instance().findByKey(1L), processEnum);
		if (hostingCompanyProcess != null) {
			List<ProcessRoles> first = processRolesService.firstProcessRoles(hostingCompanyProcess);
			if (first.size() > 0) {
				if (first.get(0).getNote() != null && !first.get(0).getNote().isEmpty()) {
					description = first.get(0).getNote();
				}
				// description = FixTaskDataService.instance().replaceStrings(targetClass,
				// description, targetKey, createUser);

				if (users != null && users.size() > 0) createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, users, mailDef, null, false);
				else if (first.get(0).getCompanyUserType() == null) createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, true, first.get(0), processEnum, hostingCompanyProcess, mailDef, null);
				else {
					users = new ArrayList<>();
					// FixTaskDataService.instance().getUsers(targetKey, targetClass, users,
					// processEnum, true, first.get(0), createUser);
					createWorkFlowTask(description, createUser, targetKey, targetClass, true, true, first.get(0), processEnum, hostingCompanyProcess, users, null, null, false);
				}

			} else {
				throw new Exception("Hosting company setup error. No process roles assigned to Hosting company.");
			}
		}

	}

	private void createWorkFlowTask(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, boolean firstInProcess, ProcessRoles processRoles, ConfigDocProcessEnum processEnum, HostingCompanyProcess hostingCompanyProcess, List<Users> users, MailDef mailDef, Tasks previousTask, boolean createSeparateTasks) throws Exception {
		if (checkCanCreateTask(targetClass, targetKey, processRoles)) {
			List<IDataEntity> dataEntities = new ArrayList<IDataEntity>();

			int numbeOfDays = 5;
			if (processRoles.getNumberOfDays() != null) numbeOfDays = processRoles.getNumberOfDays();
			// Task information
			if (!createSeparateTasks) {
				Tasks tasks = new Tasks(description, createUser, targetKey, targetClass, TaskStatusEnum.NotStarted, GenericUtility.addDaysToDateExcludeWeekends(getSynchronizedDate(), numbeOfDays), UUID.randomUUID().toString());
				tasks.setProcessRole(processRoles);
				tasks.setWorkflowProcess(processEnum);
				tasks.setFirstInProcess(firstInProcess);
				tasks.setHostingCompanyProcess(hostingCompanyProcess);
				tasks.setPreviousTask(previousTask);
				dataEntities.add(tasks);
				for (Users user : users) {
					dataEntities.add(new TaskUsers(user, tasks));
					this.sendMail(user, "New task created on merSETA NSDMS portal", mailDef, description);
				}
			} else {
				for (Users user : users) {
					Tasks tasks = new Tasks(description, createUser, targetKey, targetClass, TaskStatusEnum.NotStarted, GenericUtility.addDaysToDateExcludeWeekends(getSynchronizedDate(), numbeOfDays), UUID.randomUUID().toString());
					tasks.setProcessRole(processRoles);
					tasks.setWorkflowProcess(processEnum);
					tasks.setFirstInProcess(firstInProcess);
					tasks.setHostingCompanyProcess(hostingCompanyProcess);
					tasks.setPreviousTask(previousTask);
					dataEntities.add(tasks);
					dataEntities.add(new TaskUsers(user, tasks));
					this.sendMail(user, "New task created on merSETA NSDMS portal", mailDef, description);
				}
			}
			this.dao.createBatch(dataEntities);
		} else {
			logger.info("NO TASK NEEDED TO BE CREATED FOR:\t" + targetClass + "\t" + targetKey + "\t" + processEnum);
		}

	}

	/**
	 * Creates the process.
	 * @param description
	 * the description
	 * @param createUser
	 * the create user
	 * @param targetKey
	 * the target key
	 * @param targetClass
	 * the target class
	 * @param sendMail
	 * the send mail
	 * @param firstInProcess
	 * the first in process
	 * @param tasks
	 * the tasks
	 * @param first
	 * the first
	 * @param useRejectMessage
	 * the use reject message
	 * @param createSeperateTasks
	 * TODO
	 * @throws Exception
	 * the exception
	 */
	private void createProcess(String description, Users createUser, Long targetKey, String targetClass, boolean sendMail, boolean firstInProcess, Tasks tasks, List<ProcessRoles> first, boolean useRejectMessage, MailDef mailDef, String extraInfo, List<Users> users, boolean createSeperateTasks) throws Exception {
		if (first.size() > 0) {

			if (useRejectMessage) {
				if (first.get(0).getRejectMessage() != null && !first.get(0).getRejectMessage().isEmpty()) {
					description = first.get(0).getRejectMessage();
				}
				description += extraInfo;
			} else {
				if (first.size() > 0 && first.get(0).getNote() != null && !first.get(0).getNote().isEmpty()) {
					description = first.get(0).getNote();
				}
				description += extraInfo;
			}

			// description = FixTaskDataService.instance().replaceStrings(targetClass,
			// description, targetKey, (tasks != null) ? tasks.getActionUser() : null);

			HostingCompanyProcess hostingCompanyProcess = first.get(0).getHostingCompanyProcess();

			if (hostingCompanyProcess.getWorkflowProcess() == null) hostingCompanyProcess.setWorkflowProcess(tasks.getWorkflowProcess());

			if (users != null && users.size() > 0) {
				createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, firstInProcess, first.get(0), tasks.getWorkflowProcess(), hostingCompanyProcess, users, mailDef, tasks, createSeperateTasks);
			} else if (first.get(0).getCompanyUserType() == null) {
				createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, firstInProcess, first.get(0), tasks.getWorkflowProcess(), hostingCompanyProcess, mailDef, tasks);
			} else {
				users = new ArrayList<>();
				// FixTaskDataService.instance().getUsers(targetKey, targetClass, users,
				// tasks.getWorkflowProcess(), false, first.get(0), createUser);
				createWorkFlowTask(description, createUser, targetKey, targetClass, sendMail, firstInProcess, first.get(0), tasks.getWorkflowProcess(), hostingCompanyProcess, users, mailDef, tasks, createSeperateTasks);
			}
		}
	}

	public long findTasksByTypeAndKeyOpen(ConfigDocProcessEnum docProcessEnum, String targetClass, Long targetKey) throws Exception {
		return dao.findTasksByTypeAndKeyOpen(docProcessEnum, targetClass, targetKey);
	}

	public void findNextInProcessAndCreateTask(Users createUser, Tasks tasks, List<Users> users, boolean createIndividualTasks) throws Exception {
		List<ProcessRoles> first = processRolesService.findNextProcessRoles(tasks.getProcessRole());
		if (first.size() == 0 && tasks.getProcessRole().getHostingCompanyProcess().getNextProcess() != null) {
			HostingCompanyProcess hostingCompanyProcess = hostingCompanyProcessService.findByKey(tasks.getProcessRole().getHostingCompanyProcess().getId());
			first = processRolesService.firstProcessRoles(hostingCompanyProcess.getNextProcess());
		}
		createProcess("", createUser, tasks.getTargetKey(), tasks.getTargetClass(), true, false, tasks, first, false, null, "", users, createIndividualTasks);
		completeTask(tasks);
	}

}
