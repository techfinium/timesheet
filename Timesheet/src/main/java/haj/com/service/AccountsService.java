package haj.com.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import haj.com.billing.BillingConstants;
import haj.com.billing.BillingHelper;
import haj.com.billing.entity.Accounts;
import haj.com.billing.entity.AcctTransactions;
import haj.com.billing.entity.EventCost;
import haj.com.dao.AccountsDAO;
import haj.com.entity.Address;
import haj.com.entity.AddressTypeEnum;
import haj.com.entity.Company;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;
import haj.com.utils.GenericUtility;

public class AccountsService extends AbstractService {

	private AccountsDAO dao = new AccountsDAO();
	private UsersService usersService = new UsersService();
	private CompanyService companyService = new CompanyService();

	public List<Accounts> allAccounts() throws Exception {
	  	return dao.allAccounts();
	}

	public void createDefaultAccounts() throws Exception {
		 List<Users> l = usersService.allUsers();
		 for (Users u : l) {
			if (u.getAccounts()==null) {
				createDefaultAccount(u);
			}
		}
	}

	public void create(Accounts entity) throws Exception  {
		this.dao.create(entity);
	}

	
	public void create(IDataEntity entity) throws Exception  {
		this.dao.create(entity);
	}
	
	public void update(IDataEntity entity) throws Exception  {
		this.dao.update(entity);
	}
	public void update(Accounts entity) throws Exception  {
		if (entity.getBillingAddress().getIdaddress()==null) dao.create(entity.getBillingAddress());
		else  dao.update(entity.getBillingAddress());
		this.dao.update(entity);
		
	}

	public void delete(Accounts entity) throws Exception  {
		this.dao.delete(entity);
	}

	public Accounts findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	public List<Accounts> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<AcctTransactions> findAccountTransactions(Long accountId) throws Exception {
		return dao.findAccountTransactions(accountId);
	}
	
	public List<EventCost> allEventCost() throws Exception {
		return dao.allEventCost();
	}

	public EventCost findEventCostByKey(Long id) throws Exception {
		return dao.findEventCostByKey(id);
	}

	public Company topUpUnits(Company company, EventCost eventCost,String remark) throws Exception  { 
		BillingHelper.processTransaction( company, findEventCostByKey(eventCost.getId()).getBillingEvent().getId(), remark);
		return companyService.findByKey(company.getId());
	}
	
	public Users topUpUnits(Users user, EventCost eventCost,String remark) throws Exception  { 
		BillingHelper.processTransaction( user, findEventCostByKey(eventCost.getId()).getBillingEvent().getId(), remark);
		return usersService.findByKeyl(user.getUid());
	}
	
	
	public List<EventCost> subscriptionEventCost(Integer subsId) throws Exception { 
		return dao.subscriptionEventCost(subsId);
	}
	
/*

	public void sendDebitOrder(Users user) throws Exception {
		String msg = "Dear #NAME#" + 
				"<br/>" + "<br/>" + 
				"I would like to thank you for selecting Protecting Lives. It is our commitment to provide your child with the best and reliable 24-hour emergency assistance.<br/>" + 
				"<br/>" + 
				"Please find enclosed our debit order authorisation letter for your agreement. Please sign and return it marked for our accounts department.<br/>" + 
				"<br/>" + 
				"If you have any further questions or queries, please do not hesitate to contact me directly or our accounts department via accounts@protectinglives.co.za.<br/>" + 
				"<br/>" + 
				"Sincerely," + 
				"<br/>"+
				"<p>Carol Pirie</p>";
		
		msg = msg.replaceAll("#NAME#",user.getFirstName().trim());
		GenericUtility.sendMail(user.getEmail(), "Protecting Lives Debit Order Form", msg, HAJConstants.APP_PATH+"forms/DebitOrderForm.pdf");
		
		notifyAdmin(user,"debit order form");
	}

	public void sendEFT(Users user) throws Exception { 
		String msg = "Dear #NAME#" + 
				"<br/>" + "<br/>" + 
				"I would like to thank you for selecting Protecting Lives. It is our commitment to provide your child with the best and reliable 24-hour emergency assistance.<br/>" + 
				" <br/>" + 
				"Please find listed below our banking details for your Electronic Funds Transfer (EFT). Please use your ID number as reference.<br/>" + 
				" <br/>" + 
				"Proof of payment can be sent directly to our accounts department in order to activate your account.  <br/>" + 
				" <br/>" + 
				"First National Bank<br/>" + 
				"Karaglen Branch (252442)<br/>" + 
				"Protecting Lives Cheque account 62490986927<br/>" + 
				"Ref: Your ID Number<br/>" + 
				" <br/>" + 
				"If you have any further questions or queries, please do not hesitate to contact me directly or our accounts department via accounts@protectinglives.co.za.<br/>" + 
				" <br/>" + 
				"Sincerely,"+
				"<br/>"+
				"<p>Carol Pirie</p>";
				msg = msg.replaceAll("#NAME#",user.getFirstName().trim());
				GenericUtility.sendMail(user.getEmail(), "Protecting Lives EFT Details",msg);
				notifyAdmin(user,"EFT request");
	}

	private void notifyAdmin(Users user,String type) throws Exception {
		   String msg =		"<p>The following person requested a #TYPE# from the site</p>"+
					"<table>"+
					"<tr>"+
					"<td><b>Name:</b></td><td>#NAME#</td>"+
					"</tr>"+
					"<tr>"+
					"<td><b>Email:</b></td><td>#EMAIL#</td>"+
					"</tr>"+	
					"<tr>"+
					"<td><b>Mobile:</b></td><td>#CELL#</td>"+
					"</tr>"+	
					"</table>"+				
					"<p>Regards</p>"+
					"<p>The infoFINIUM  team</p>"+
					"<br/>";
			msg = msg.replaceAll("#NAME#",user.getFirstName().trim()+ " "+user.getLastName().trim());
			msg = msg.replaceAll("#EMAIL#",user.getEmail().trim());
			msg = msg.replaceAll("#CELL#",user.getCellNumber());
			msg = msg.replaceAll("#TYPE#",type);
		
			GenericUtility.sendMail(HAJConstants.INFO_REQ_MAIL, "Debit Order request", msg);
	}
	*/
	
	private void notifyAdmin(Exception e)  {
		   String msg =		"<p>The nightly run crashed with error</p>"+
					"<p> #ERR# </p>"+
					"<p>Regards</p>"+
					"<p>The Timesheet team</p>"+
					"<br/>";
			msg = msg.replaceAll("#ERR#", e.getMessage());
			
		
			GenericUtility.sendMail("hendrik@hlspro.com", "Timesheet error", msg);
	}
	
	public void nightlyRun() {
		try {
			// do all the acounts where expire not filled in (i.e. still in free month
			List<Users> nnew = dao.nightlyNewAccounts();
			Date now = new Date();
			Map<Long,Long> m = new HashMap<Long,Long>();
			for (Users user : nnew) {
			  if (user.getAccounts()!=null && user.getAccounts().getAccountOpenDate()!=null) {	
				Date expireD = GenericUtility.addMonthsToDate(user.getAccounts().getAccountOpenDate(), 1);
				if (expireD.before(now)) {
					BillingHelper.processTransaction(user, BillingConstants.DEDUCT_UNIT, "Deduct for montly usage");
					m.put(user.getAccounts().getId(), user.getAccounts().getId());
				}
			  }
			}
			now = new Date();
			List<Users> existing = dao.nightlyExistingAccounts();
			for (Users user : existing) {
				  if (user.getAccounts()!=null && user.getAccounts().getAccountOpenDate()!=null) {	
					Date expireD = GenericUtility.addMonthsToDate(user.getAccounts().getDeductDate(), 1);
					if (expireD.before(now) && user.getAccounts().getExpireDate().before(now)) {
					  if (!m.containsKey(user.getAccounts().getId())) {
						BillingHelper.processTransaction(user, BillingConstants.DEDUCT_UNIT, "Deduct for montly usage");
					  }
					}
			   }
			}
			
		} catch (Exception e) {
			logger.fatal(e);
			notifyAdmin(e);
		}
		
	}
	
	
	public void createDefaultAccount(Users user) throws Exception {
		Address address = new Address();
		address.setAddressLine1("N/A");
		address.setAddrType(AddressTypeEnum.Billing);
		address.setPostcode("N/A");
		createDefaultAccount(user, address);
	}
	
	public void createDefaultAccount(Users user, Address address) throws Exception {
		Accounts accounts = new Accounts();
		accounts.setUnitBalance(0L);
		Address billingAddress = null;
		if (address!=null) {
			 billingAddress = (Address)address.clone();
			 billingAddress.setIdaddress(null);
			 billingAddress.setAddrType(AddressTypeEnum.Billing);
			 dao.create(billingAddress);
			 
		}		
		   accounts.setBillingAddress(billingAddress);
		 
			accounts.setAccountName(user.getFirstName() + " "+ user.getLastName() + " Ref:"+user.getUid());
			accounts.setAccountOpenDate(new java.util.Date());
			dao.create(accounts);
			user.setAccounts(accounts);
			dao.update(user);
		
			BillingHelper.processTransaction( user, findEventCostByKey(BillingConstants.FREE_UNIT).getBillingEvent().getId(), "One Free unit");
		
	}	
	
	public List<AcctTransactions> acctTransactions(Accounts account) throws Exception { 
		return dao.acctTransactions(account.getId());
	}
}
