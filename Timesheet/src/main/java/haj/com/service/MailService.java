package haj.com.service;

import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.HtmlEmail;

import haj.com.constants.HAJConstants;
import haj.com.framework.AbstractService;





public class MailService extends AbstractService {

	private static final String mailServer = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver");
	private static final String portS = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_port");
	
	

	public static void mail(String from,String to, String subject,  String body) {
		  try {
			  HtmlEmail email = new HtmlEmail();
				  // Create the email message
			  
			  email.setHostName(mailServer);
			  email.setSmtpPort(Integer.valueOf(portS.trim()));
			  email.setAuthenticator(new DefaultAuthenticator("hendrik.strydom@jmruk.com", "sapiens02$"));
			  email.setStartTLSRequired(true);
			  email.setDebug(HAJConstants.MAIL_DEBUG);
			  email.addTo(to);
			  email.setFrom(from);
			  email.setSubject(subject);
			  email.setHtmlMsg(createHtml(email,  body));
			
			  /*
			   
			    		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.office365.com");
		props.put("mail.smtp.port", "587");
			    
			   

			   */
				  
			  // send the email
			   email.send();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	



	
	public static void mailWithAttachement(String from,String to, String subject, String body, String attachmentPath, String attachmentDescription, String attachmentName) {
		  try {
			  
				  EmailAttachment attachment = new EmailAttachment();
			  attachment.setPath(attachmentPath);

			  attachment.setDisposition(EmailAttachment.ATTACHMENT);
			  attachment.setDescription(attachmentDescription);
			  attachment.setName(attachmentName);

			  // Create the email message
			  HtmlEmail email = new HtmlEmail();
			  email.setHostName(mailServer);
			  email.setSmtpPort(Integer.valueOf(portS.trim()));
			  email.setDebug(HAJConstants.MAIL_DEBUG);
			  email.addTo(to);
			  email.setFrom(from);
			  email.setSubject(subject);
			  email.setHtmlMsg(createHtml(email,body));

			  // add the attachment
			 email.attach(attachment);


			  // send the email
			  email.send();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String createHtml(HtmlEmail email,String body) {
		String html =  HAJConstants.EMAIL_TEMPLATE;
			   html = html.replace("#BODY#", body);
		 try {
			 
			 html  =  html.replace("#IMAGEHS1#",   email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("image-1.png"), "image/png"),"image/png"));
			 html  =  html.replace("#BG#",  email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("background.jpg"), "image/jpg"),"image1a/jpg"));

			 html  =  html.replace("#BG0#",  email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("background-1.jpg"), "image/jpg"),"image1/jpg"));

			 html  =  html.replace("#BG1#",   email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("box-bg-1.jpg"), "image/jpg"),"image/jpg"));
			 html  =  html.replace("#BG2#",   email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("box-bg-2.jpg"), "image/jpg"),"image-2/jpg"));
			 html  =  html.replace("#BG3#",   email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("box-bg-3.jpg"), "image/jpg"),"image-3/jpg"));

			 html  =  html.replace("#BG2A#",  email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("background-2.jpg"), "image/jpg"),"image2/jpg"));
		
			 html  =  html.replace("#PG00#",  email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("page-bg.jpg"), "image/jpg"),"image3/jpg"));
			} catch (Exception e) {
			e.printStackTrace();
		}  
	return html;
	}

	public static void mailWithAttachement(String from,String to, String subject, String body, byte[] attachment, String attachmentDescription, String attachmentName,String mime) {
		  try {
			  // Create the email message
			  HtmlEmail email = new HtmlEmail();
			  email.setHostName(mailServer);
			  email.setSmtpPort(Integer.valueOf(portS.trim()));
			  email.setDebug(HAJConstants.MAIL_DEBUG);
			  email.addTo(to);
			//  email.addBcc(from);
			  email.setFrom(from);
			  email.setSubject(subject);
			  email.setHtmlMsg(createHtml(email,body));

			  // add the attachment
			
			  email.attach(new ByteArrayDataSource(attachment, mime),
					  attachmentName, attachmentDescription,
				       EmailAttachment.ATTACHMENT);
/*
 			  email.attach(new ByteArrayDataSource(FileUtils.readFileToByteArray(new File("/Users/hendrik/Desktop/Daniel/scan.pdf") ), "application/pdf"),
				      "document.pdf", "Document description",
				       EmailAttachment.ATTACHMENT);
 */
			  // send the email
			  email.send();
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	

	public static void main(String[] args) { 
		//MailService.sendMailCommons("Hendrik.Strydom@momentum.co.za", "Hello", "Hello World");
	    System.out.println("Start");
		//MailService.mailWithAttachement("Hendrik.Strydom@momentum.co.za", "Hendrik.Strydom@momentum.co.za", "Test Subject", "Test heading", "Test body", "/Users/hendrik/Desktop/Daniel/scan.pdf", "attachmentDescription", "attachmentName.pdf");;
		MailService.mail("hendrik.strydom@jmruk.com", "hendrik@hlspro.com", "My subject",  "Content of email");
		System.out.println("Done");
		System.exit(0);
	}


}
