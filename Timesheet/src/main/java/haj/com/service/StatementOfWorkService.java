package haj.com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.StatementOfWorkDAO;
import haj.com.entity.Projects;
import haj.com.entity.StatementOfWork;
import haj.com.entity.StatementOfWorkHist;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;

public class StatementOfWorkService extends AbstractService {

	private StatementOfWorkDAO dao = new StatementOfWorkDAO();

	public List<StatementOfWork> allStatementOfWork() throws Exception {
		return dao.allStatementOfWork();
	}

	public void create(StatementOfWork entity) throws Exception {
		if (entity.getId() == null) {
			entity.setCreateDate(new java.util.Date());
			this.dao.create(entity);
		} else {
			entity.setCreateDate(new java.util.Date());
			this.dao.update(entity);
		}
	}


	public void update(StatementOfWork entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(StatementOfWork entity) throws Exception {
		this.dao.delete(entity);
	}

	public StatementOfWork findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<StatementOfWork> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<StatementOfWorkHist> findByKeyHist(long id) throws Exception {
		return dao.findByKeyHist(id);
	}

	public List<StatementOfWorkHist> allStatementOfWorkHist() throws Exception {
		return dao.allStatementOfWorkHist();
	}

	/*
	 * public List<StatementOfWork> findByCompany(haj.com.entity.Company
	 * company) throws Exception { return dao.findByCompany(company.getId()); }
	 */
	/*
	 * public void addOrEditObject(StatementOfWork sow) {
	 * this.dao.addOrEditObject(sow); }
	 * 
	 * public void deleteById(StatementOfWork sow) {
	 * this.dao.deleteById(sow.getId()); }
	 * 
	 * public void deleteObject(StatementOfWork sow) {
	 * this.dao.deleteObject(sow); }
	 * 
	 * public StatementOfWork getObject(StatementOfWork sow) { return
	 * this.dao.getObject(sow.getId()); }
	 */

	public List<StatementOfWork> findByNumerOrDescription(String description) throws Exception {
		return dao.findByNumerOrDescription(description);
	}

	public List<StatementOfWork> findByProjectActive(Projects project, Users user , Boolean directorLevel) throws Exception {
		List<StatementOfWork> allSow = dao.findByProjectActive(project.getId());
		List<StatementOfWork> filteredList = new ArrayList<>();
		if (directorLevel == true) {
			filteredList.addAll(allSow);
		} else {
			for (StatementOfWork statementOfWork : allSow) {
				Boolean employeeAssigned = false;
				employeeAssigned = dao.findByProjectActiveUserAccess(statementOfWork.getId(), user.getUid());
				if (employeeAssigned) {
					filteredList.add(statementOfWork);
				}
			}
		}
		return filteredList;
	}

	/**
	 * @author TechFinium
	 * @param class1
	 *            SOW class
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return List<SOW> containing data
	 */
	@SuppressWarnings("unchecked")
	public List<StatementOfWork> allInStatementsOfWork(Class<StatementOfWork> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<StatementOfWork>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	/**
	 * 
	 * @param entity
	 *            SOW class
	 * @param filters
	 * @return Number of rows in the SOW entity
	 */
	public int count(Class<StatementOfWork> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}
}
