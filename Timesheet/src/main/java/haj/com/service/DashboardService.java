package haj.com.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;

import haj.com.bean.ReportBean;
import haj.com.dao.DashboardDAO;
import haj.com.entity.Company;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Projects;
import haj.com.entity.Timesheet;
import haj.com.entity.TimesheetDetails;
import haj.com.framework.AbstractService;
import haj.com.utils.GenericUtility;

public class DashboardService extends AbstractService {

	private DashboardDAO dao = new DashboardDAO();
	private List<TimesheetDetails> timesheetDetails;
	private ProjectsService projectsService = new ProjectsService();
	private ReportService reportService = new ReportService();
	private TimesheetDetailsService service = new TimesheetDetailsService();
	private TimesheetService timesheetService = new TimesheetService();
	private SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");

	public Long noClients(Company company) throws Exception {
		return dao.noClients(company.getId());
	}

	public Long noQuestionaire(Company company) throws Exception {
		return dao.noQuestionaire(company.getId());
	}

	public Long noCompletedQuestionaire(Company company) throws Exception {
		return dao.noCompletedQuestionaire(company.getId());
	}

	public Long noInPorgressQuestionaire(Company company) throws Exception {
		return dao.noInPorgressQuestionaire(company.getId());
	}

	public TimesheetDetails getLatestTimesheet(CompanyUsers companyUsers) throws Exception {
		List<Timesheet> timesheet = timesheetService.findByCompanyUserByDate(companyUsers,
				GenericUtility.getFirstDayOfMonth(getSynchronizedDate()),
				GenericUtility.getLasttDayOfMonth(getSynchronizedDate()));
		if (timesheet.size() == 0) {
			return null;
		}
		timesheetDetails = service.findByTimesheetBetweenDates(timesheet.get(0));
		if (timesheetDetails.size() == 0) {
			return null;
		}
		return timesheetDetails.get(0);
	}

	public PieChartModel createPieModel1(PieChartModel pieModel1, Company comapny) throws Exception {
		pieModel1 = new PieChartModel();
		List<ReportBean> reportBeanList = reportService.projectsFromToDate(
				GenericUtility.getFirstDayOfMonth(GenericUtility.deductDaysFromDate(getSynchronizedDate(), 30)),
				getSynchronizedDate(), comapny);
		for (ReportBean reportBean : reportBeanList) {
			pieModel1.set(reportBean.getProject().getCode(), reportBean.getHours());
		}
		pieModel1.setLegendPosition("w");
		pieModel1.setExtender("skinBar");
		pieModel1.setShowDataLabels(true);
		return pieModel1;
	}

	public PieChartModel createPieModel1(PieChartModel pieModel1) throws Exception {
		pieModel1 = new PieChartModel();
		List<ReportBean> reportBeanList = reportService.projectsFromToDate(
				GenericUtility.getFirstDayOfMonth(GenericUtility.deductDaysFromDate(getSynchronizedDate(), 30)),
				getSynchronizedDate());
		for (ReportBean reportBean : reportBeanList) {
			pieModel1.set(reportBean.getProject().getCode(), reportBean.getHours());
		}
		pieModel1.setLegendPosition("w");
		pieModel1.setExtender("skinBar");
		pieModel1.setShowDataLabels(true);
		return pieModel1;
	}

	public List<BarChartModel> createPieModel2(Company comapny) throws Exception {
		BarChartModel model = new BarChartModel();
		List<BarChartModel> models = new ArrayList<>();
		List<Projects> projects = projectsService.findByCompany(comapny);
		List<ReportBean> reportBeanList = new ArrayList<>();
		ChartSeries boys = new ChartSeries();
		for (Projects project : projects) {
			int totMins = 0;
			int userMins = 0;
			Map<Object, Number> data = new HashMap<>();
			model = new BarChartModel();
			reportBeanList = reportService.projectDetails(project,
					GenericUtility.getFirstDayOfMonth(GenericUtility.deductDaysFromDate(getSynchronizedDate(), 30)),
					getSynchronizedDate(), comapny);
			for (ReportBean reportBean : reportBeanList) {
				userMins = (int) ((int) (reportBean.getHours() * 60) + (reportBean.getLongOne()));
				String name = reportBean.getUser().getUsers().getFirstName() + " "
						+ reportBean.getUser().getUsers().getLastName() ;
//				+ " : " + userMins + " Min"
				data.put(name, reportBean.getHours()); 
				totMins += reportBean.getHours() * 60;
				totMins += reportBean.getLongOne();
			}
			if (reportBeanList.size() > 0) {
				boys = new ChartSeries();
				boys.setData(data);
				model.addSeries(boys);
				model.setTitle(project.getCode());	// + " (Total Minutes: " + totMins + ")"
				model.setExtender("skinBar");
				model.setAnimate(true);
				models.add(model);
			}
		}
		return models;
	}

	public List<BarChartModel> createPieModel2() throws Exception {
		
		BarChartModel model = new BarChartModel();
		List<BarChartModel> models = new ArrayList<>();
		List<Projects> projects = projectsService.allProjectsActive();
		List<ReportBean> reportBeanList = new ArrayList<>();
		ChartSeries boys = new ChartSeries();
		for (Projects project : projects) {
			int totMins = 0;
			int userMins = 0;
			Map<Object, Number> data = new HashMap<>();
			model = new BarChartModel();
			reportBeanList = reportService.projectDetailsDirector(project,
					GenericUtility.getFirstDayOfMonth(GenericUtility.deductDaysFromDate(getSynchronizedDate(), 30)),
					getSynchronizedDate());
			for (ReportBean reportBean : reportBeanList) {
				userMins = (int) ((int) (reportBean.getHours() * 60) + (reportBean.getLongOne()));
				String name = reportBean.getUser().getUsers().getFirstName() + " "
						+ reportBean.getUser().getUsers().getLastName();	// + " : " + userMins + " Min"
				data.put(name, reportBean.getHours());
				totMins += reportBean.getHours() * 60;
				totMins += reportBean.getLongOne();
			}
			if (reportBeanList.size() > 0) {
				boys = new ChartSeries();
				boys.setData(data);
				model.addSeries(boys);
				model.setTitle( project.getCode() + " (" + project.getCompany().getCompanyName() + ")");	// Minutes: " + totMins
				model.setExtender("skinBar");
				model.setAnimate(true);
				models.add(model);
			}
		}
		return models;
	}

	public BarChartModel initBarModel(CompanyUsers companyUsers) throws Exception {
		BarChartModel model = new BarChartModel();
		List<Timesheet> timesheets = timesheetService.findByCompanyUserByDate(companyUsers,
				GenericUtility.getFirstDayOfMonth(getSynchronizedDate()),
				GenericUtility.getLasttDayOfMonth(getSynchronizedDate()));
		Map<Object, Number> data = new HashMap<>();
		ChartSeries boys = new ChartSeries();
		for (Timesheet timesheet : timesheets) {
			timesheetDetails = service.findByTimesheetBetweenDates(timesheet);
			for (TimesheetDetails timesheetDetails : timesheetDetails) {
				if (timesheetDetails.getHours() != null && timesheetDetails.getHours() > 0) {
					data.put(sdf.format(timesheetDetails.getFromDateTime()), timesheetDetails.getHours());
				}
			}
		}
		boys.setData(data);
		model.addSeries(boys);
		model.setAnimate(true);
		model.setTitle("Total hours per day");
		model.setExtender("skinBar");
		Axis xAxis = model.getAxis(AxisType.X);
		Axis yAxis = model.getAxis(AxisType.Y);

		yAxis.setMin(0);
		yAxis.setMax(24);
		yAxis.setLabel("Hours");
		xAxis.setTickAngle(-30);
		return model;
	}
}
