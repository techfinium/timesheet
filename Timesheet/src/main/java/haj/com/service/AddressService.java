package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import com.google.code.geocoder.model.LatLng;

import haj.com.dao.AddressDAO;
import haj.com.entity.Address;
import haj.com.entity.AddressTypeEnum;
import haj.com.entity.MonitoringRegister;
import haj.com.entity.Users;
import haj.com.entity.VisitorDeclarationForm;
import haj.com.framework.AbstractService;
import haj.com.utils.GeoCoderUtil;

public class AddressService extends AbstractService {

	private UsersService userService = new UsersService();
	private VisitorService visitorService = new VisitorService();

	private AddressDAO dao = new AddressDAO();

	public List<Address> allAddress() throws Exception {
		return dao.allAddress();
	}

	public void create(Address entity) throws Exception {
		 if (entity.getIdaddress() ==null)
			 this.dao.create(entity);
			else
			 this.dao.update(entity);
	}	
	
	public void createAddress(Address address, String targetClass, Long targetKey) throws Exception {
		address.setTargetClass(targetClass);
		address.setTargetKey(targetKey);
		create(address);

	}
	
	public void createResidentialAddress(Address address, String targetClass, Long targetKey) throws Exception {
		address.setTargetClass(targetClass);
		address.setTargetKey(targetKey);
		address.setAddrType(AddressTypeEnum.Residential);
		create(address);

	}
	
	public void createWorkAddress(Address address, String targetClass, Long targetKey) throws Exception {
		address.setTargetClass(targetClass);
		address.setTargetKey(targetKey);
		address.setAddrType(AddressTypeEnum.Work);
		create(address);

	}

	public void update(Address entity) throws Exception {
		lookupLongitudeLatitude(entity);
		this.dao.update(entity);
	}

	public void delete(Address entity) throws Exception {
		this.dao.delete(entity);
	}

	public void createUpdate(Address entity) throws Exception {
		if (entity.getIdaddress() != null) {
			this.dao.update(entity);
		} else {
			this.dao.create(entity);
		}
	}

	public List<Address> byUserCheck(Users user) throws Exception {
		return dao.byUserCheck(user.getUid());
	}

	public Address byUser(Users user) throws Exception {
		return dao.byUser(user.getUid());
	}

	public List<Address> byUserId(Long uid) throws Exception {
		return dao.byUserId(uid);
	}

	public List<Address> byChildId(Long idchild) throws Exception {
		return dao.byChildId(idchild);
	}

	

	public Address childResiAddr(Long idchild) throws Exception {
		Address addr = null;
		List<Address> l = byChildId(idchild);
		for (Address address : l) {
			if (address.getAddrType() == AddressTypeEnum.Residential)
				addr = address;
		}
		return addr;
	}

	public Address findByHostingCompanyId(long id) throws Exception {
		return dao.findByHostingCompanyId(id);
	}

	public void lookupLongitudeLatitude(Address addr) {
		try {
			String address = addr.getAddressLine1() + " " + addr.getAddressLine2() + " " + addr.getAddressLine3() + " " + addr.getAddressLine4() + " " + addr.getPostcode();
			if (address.trim().length() > 3) {
				LatLng ll = GeoCoderUtil.calcLatLng(address.trim());
				if (ll != null) {
					addr.setLatitude(ll.getLat().doubleValue());
					addr.setLongitude(ll.getLng().doubleValue());
				}
			}
		} catch (Exception e) {
			logger.fatal(e);
		}

	}

	/**
	 * Lazy load MonitoringRegister with pagination, filter, sorting
	 * 
	 * @author TechFinium
	 * @param class1    MonitoringRegister class
	 * @param first     the first
	 * @param pageSize  the page size
	 * @param sortField the sort field
	 * @param sortOrder the sort order
	 * @param filters   the filters
	 * @return a list of {@link haj.com.entity.MonitoringRegister} containing data
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public List<Address> allAddresses(Class<Address> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<Address>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	/**
	 * Get count of MonitoringRegister for lazy load with filters
	 * 
	 * @author TechFinium
	 * @param entity  MonitoringRegister class
	 * @param filters the filters
	 * @return Number of rows in the MonitoringRegister entity
	 * @throws Exception the exception
	 */
	public int count(Class<Address> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	/*
	 * public List<MonitoringRegister> findByCompany(haj.com.entity.Company company)
	 * throws Exception { return dao.findByCompany(company.getId()); }
	 */
	
	public Address findByTagetClassTagerKeyAndAddressType(String targetClass, Long targetKey, AddressTypeEnum addressTypeEnum) throws Exception {
		return dao.findByTagetClassTagerKeyAndAddressType(targetClass, targetKey, addressTypeEnum);
	}
	
	public Address findByTagetClassTagerKey(String targetClass, Long targetKey) throws Exception {
		return dao.findByTagetClassTagerKey(targetClass, targetKey);
	}

}