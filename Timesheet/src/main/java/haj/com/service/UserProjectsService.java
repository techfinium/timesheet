package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.UserProjectsDAO;
import haj.com.entity.Projects;
import haj.com.entity.UserProjects;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;

public class UserProjectsService extends AbstractService {

	private UserProjectsDAO dao = new UserProjectsDAO();

	/**
	 * Get all UserProjects
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @return List<UserProjects>
 	 * @throws Exception
 	 */
	public List<UserProjects> allUserProjects() throws Exception {
	  	return dao.allUserProjects();
	}

	/**
	 * Create or update  UserProjects
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @throws Exception
 	 */
	public void create(UserProjects entity) throws Exception  {
	   if (entity.getId() ==null) {
		 entity.setCreateDate(new java.util.Date());
		 this.dao.create(entity);
	   }
		else
		 this.dao.update(entity);
	}

	/**
	 * Update  UserProjects
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @throws Exception
 	 */
	public void update(UserProjects entity) throws Exception  {
		this.dao.update(entity);
	}

	/**
	 * Delete  UserProjects
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @throws Exception
 	 */
	public void delete(UserProjects entity) throws Exception  {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @return a UserProjects object
 	 * @throws Exception
 	 */
	public UserProjects findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	/**
	 * Find UserProjects by description
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @return List<UserProjects>
 	 * @throws Exception
 	 */
	public List<UserProjects> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	/**
	 * Lazy load UserProjects
	 * @param first from key
	 * @param pageSize no of rows to fetch
	 * @return List<UserProjects>
	 * @throws Exception
	 */
	public List<UserProjects> allUserProjects(int first, int pageSize) throws Exception {
		return dao.allUserProjects(Long.valueOf(first), pageSize);
	}
		
	
	/**
	 * @author TechFinium 
	 * @return Number of rows in the UserProjects
	 * @throws Exception
	 */
	public Long count() throws Exception {
	       return dao.count(UserProjects.class);
	}
	
    /**
     * @author TechFinium 
     * @param class1 UserProjects class
     * @param first
     * @param pageSize
     * @param sortField
     * @param sortOrder
     * @param filters
     * @return List<UserProjects> containing data
     */	
	@SuppressWarnings("unchecked")
	public List<UserProjects> allUserProjects(Class<UserProjects> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<UserProjects>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * 
     * @param entity UserProjects class
     * @param filters
     * @return Number of rows in the UserProjects entity
     */	
	public int count(Class<UserProjects> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
	
	/**
	 * Find user projects by project
	 *
	 * @param project
	 * @return List<UserProjects>
	 * @throws Exception
	 */
	public List<UserProjects> findByProject(Projects project) throws Exception {
		return dao.findByProject(project.getId());
	}
	
	/**
	 * Find Projects for user
	 * 
	 * @param user
	 * @return List<Projects>
	 * @throws Exception
	 */
	public List<Projects> findProjectsForUser(Users user) throws Exception {
		return dao.findProjectsForUser(user.getUid());
	}
}
