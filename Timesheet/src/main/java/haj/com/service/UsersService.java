package haj.com.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;

import haj.com.dao.UsersDAO;
import haj.com.entity.Address;
import haj.com.entity.Company;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Projects;
import haj.com.entity.Users;
import haj.com.entity.UsersStatusEnum;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class UsersService extends AbstractService {

	private UsersDAO dao = new UsersDAO();
	
	public void create(Users entity) throws Exception  {
		this.dao.create(entity);
	}

	public List<Users> allUsers() throws Exception {
		return dao.allUsers();
	}

	public UsersDAO getDao() {
		return dao;
	}

	public void setDao(UsersDAO dao) {
		this.dao = dao;
	}

	public void endUserChecks(Users u) throws Exception {
		if (u.getIdNumber() != null && u.getIdNumber().length() > 0) {
			try {
				if (u.getIdNumber() != null && u.getIdNumber().trim().length() > 0 && dao.getUserByRSAid(u.getIdNumber()) != null)
					throw new Exception("User with this id number already on database.");
			} catch (HibernateException e) {
				logger.fatal(e);
				throw new Exception("User with this id number already on database.");
			}
		}
		if (u.getEmail() != null && u.getEmail().length() > 0) {
			try {
				if (dao.getUserByEmail(u.getEmail()) != null)
					throw new Exception("Another is already registered with this email address.");
			} catch (HibernateException e) {
				logger.fatal(e);
				throw new Exception("Another is already registered with this email address.");
			}
		}
		/*
		 * if (u.getNiNumber() != null && u.getNiNumber().length() > 0) { try {
		 * if (u.getNiNumber() != null && u.getNiNumber().trim().length() > 0 &&
		 * dao.getUserNiNumber(u.getNiNumber()) != null) throw new
		 * Exception("User with this NI number already on database."); } catch
		 * (HibernateException e) { logger.fatal(e); throw new
		 * Exception("User with this NI number already on database."); } }
		 */

	}

	public void insertNewEndUser(Users u) throws Exception {
		endUserChecks(u);
		u.setStatus(UsersStatusEnum.Active);
		u.setPassword(new LogonService().encryptPassword(u.getPassword()));
		dao.create(u);
	}
	
	public Users getUserByEmail(String email) throws Exception {
		return dao.getUserByEmail(email);
	}


	public void updateEndUser(CompanyUsers cu) throws Exception {
		// if (dao.checkUsernameUsed(cu.getUsers().getUsername(),
		// cu.getUsers().getUid()) != null) throw new Exception("Another user
		// has already registered with this username.");
		List<IDataEntity> l = new ArrayList<IDataEntity>();
		l.add(cu.getUsers());
		l.add(cu);
		dao.updateBatch(l);

	}

	public void updateEndUser(Users u) throws Exception {
		if (dao.checkUsernameUsed(u.getUsername(), u.getUid()) != null)
			throw new Exception("Another is already registered with this username.");
		dao.update(u);
	}

	public Users findUserByEmailGUID(String emailGuid) throws Exception {
		return dao.findUserByEmailGUID(emailGuid);
	}

	public Users checkEmailUsed(String email, Long uid) throws Exception {
		return dao.checkEmailUsed(email, uid);
	}

	public void emailUser(Users u) throws Exception {
		if (checkEmailUsed(u.getEmail(), u.getUid()) != null)
			throw new Exception("Email: " + u.getEmail() + " is already taken! Please use another email.");
		dao.update(u);

	}

	public Users checkUsernameUsed(String username, Long uid) throws Exception {
		return dao.checkUsernameUsed(username, uid);
	}

	public void update(Users u) throws Exception {
		if (!u.getUsername().isEmpty())
			if (checkUsernameUsed(u.getUsername(), u.getUid()) != null)
				throw new Exception("Username: " + u.getUsername() + " is already taken! Please use another username.");

		dao.update(u);
	}

	public Users findByKeyl(Long uid) throws Exception {
		return dao.findByKeyl(uid);
	}

	public Users findByKey(Long uid) throws Exception {
		return dao.findByKey(uid);
	}

	public List<Users> kidsParents(Long idchild) throws Exception {
		return dao.kidsParents(idchild);
	}

	public List<CompanyUsers> findCompanyUsers(Company company) throws Exception {
		return dao.findCompanyUsers(company.getId());
	}

	public List<Users> searchByName(String searchString, String email) throws Exception {
		if (searchString == null && email == null)
			throw new Exception("Enter Surname and/or email");
		if (searchString == null)
			searchString = "";
		if (email == null)
			email = "";
		if (searchString.trim().length() == 0 && email.trim().length() == 0)
			throw new Exception("Enter Surname and/or email");
		return dao.searchByName('%' + searchString.trim() + '%', '%' + email.trim() + '%');
	}

	public List<Users> getAllCompanyAdministrors(Company company) throws Exception {
		if (company == null)
			return null;
		return dao.getAllCompanyAdministrors(company.getId());
	}

	public List<Users> findByNameLastname(String name, Company company) throws Exception {
		return dao.findByNameAndCompany(name, company.getId());
	}

	public List<Users> findByNameLastname(String name) throws Exception {
		return dao.findByNameAndCompany(name);
	}

	public List<Users> findByNameLastnameUsersProject(String name, Projects project) throws Exception {
		return dao.findByNameAndProject(name, project.getId());
	}

	public List<Users> findByProject(Projects project) throws Exception {
		List<Users> userList = new ArrayList<Users>();
		userList.addAll(dao.findByProject(project.getId()));
		userList.addAll(dao.findByDirector());
		return userList;
	}

}
