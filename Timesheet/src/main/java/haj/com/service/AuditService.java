package haj.com.service;

import java.util.Map;

import haj.com.dao.AuditDAO;
import haj.com.entity.AuditEvent;
import haj.com.framework.AbstractService;



public class AuditService extends AbstractService
{
  private static final long serialVersionUID = 1L;
  
  public long auditEvent(Map<String, Object> map)
  {
    try
    {
      AuditDAO dao = new AuditDAO();
      AuditEvent event = new AuditEvent(map);
      dao.create(event);
      logger.info("Audit Event " + event.getEventId() + " created.");
      return event.getEventId();
    }
    catch (Exception e)
    {
      logger.error("Failed to create Audit Event", e);
      return -1;
    }
  }
 
}
