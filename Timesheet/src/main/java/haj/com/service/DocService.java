package haj.com.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.sql.Template;

import haj.com.dao.DocDAO;
import haj.com.entity.Doc;
import haj.com.entity.DocByte;
import haj.com.entity.Users;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.framework.AbstractService;

public class DocService extends AbstractService {

	private DocDAO dao = new DocDAO();
	private UsersService usersService = new UsersService();

	public void create(Doc entity) throws Exception {
		Date date = new Date();
		entity.setCreateDate(date);
		this.dao.create(entity);
	}

	public List<Doc> search(Doc doc) throws Exception {
		if (doc == null || doc.getId() == null)
			return null;
		if (doc.getDoc() != null) {
			return resolveVersions(dao.search(doc.getDoc().getId()), false);
		} else {
			return resolveVersions(dao.search(doc.getId()), false);
		}
	}

	public List<Doc> resolveVersions(List<Doc> search, boolean clearcontents) {
		List<Doc> result = new ArrayList<Doc>();
		try {
			for (Doc doc : search) {
				if (clearcontents)
					doc.setFileContent(null);
				resolveVersionCommon(result, doc, clearcontents);
			}
		} catch (Exception e) {
			logger.fatal(e);
		}
		return result;
	}

	private void resolveVersionCommon(List<Doc> result, Doc doc, boolean clearcontents) throws Exception {

		List<Doc> t = getVersions(doc);

		if (t != null && t.size() > 0) {
			Doc x = t.get(0);
			List<Doc> versions = new ArrayList<Doc>();

			for (Doc doc2 : t) {
				if (clearcontents)
					doc2.setFileContent(null);
				if (doc2.getId() != x.getId())
					versions.add(doc2);
			}

			versions.add(doc);
			x.setDocVerions(versions);
			result.add(x);
		} else {
			result.add(doc);
		}

	}

	public DocByte findDocByteByKey(Long id) throws Exception {
		return resolvePossiblePathDoc(dao.findDocByteByKey(id));
	}

	private DocByte resolvePossiblePathDoc(DocByte docByte) {
		if ((docByte.getData() == null || docByte.getData().length == 0) && !StringUtils.isEmpty(docByte.getDocPath())) {
			try {
				docByte.setData(FileUtils.readFileToByteArray(new File(docByte.getDocPath().trim() + docByte.getDoc().getOriginalFname())));
			} catch (IOException e) {
				logger.fatal(e);
			}
		}
		return docByte;
	}

	public List<Doc> getVersions(Doc d) throws Exception {
		return dao.getVersions(d);
	}

	public void save(Doc doc, byte[] contents, String fileName, Users user) throws Exception {
		doc.setFileContent(contents);
		doc.setOriginalFname(fileName);
		doc.setExtension(FilenameUtils.getExtension(fileName));
		doc.setAddDate(new java.util.Date());
		doc.setCreateDate(new Date());
		doc.setVersionNo(1);
		doc.setUser(user);
		doc.setSearch(true);
		dao.create(doc);
		dao.create(new DocByte(doc.getFileContent(), doc));
		DocumentTrackerService.create(user, doc, DocumentTrackerEnum.Upload);
	}

	private String createRef(Template template) {
		if (template == null)
			return "";
		else {
			String ref = null;
			if (ref.length() > 200) {
				ref = ref.substring(0, 198);
			}
			return ref;
		}
	}

	public Doc getRootDoc(List<Doc> docs, Doc originalDoc) {
		Doc t = null;
		if (docs == null || docs.size() == 0) {
			t = originalDoc;
		} else {
			for (Doc doc : docs) {
				if (doc.getDoc() == null)
					t = doc;
			}
		}
		return t;
	}

	public void save(Doc doc, byte[] contents, String fileName, Users user, List<Doc> docs) throws Exception {
		if (docs == null || docs.size() == 0) {
			save(doc, contents, fileName, user);
		} else {
			doc.setFileContent(contents);
			doc.setOriginalFname(fileName);
			doc.setCreateDate(new Date());
			doc.setExtension(FilenameUtils.getExtension(fileName));
			doc.setAddDate(new java.util.Date());
			doc.setUser(user);
			Doc orig = docs.get(0);
			doc.setDoc(getRootDoc(orig.getDocVerions(), docs.get(0)));
			doc.setVersionNo(orig.getVersionNo() + 1);
			doc.setKeyword("");
			doc.setSearch(false);
			dao.create(doc);
			dao.create(new DocByte(doc.getFileContent(), doc));
			DocumentTrackerService.create(user, doc.getDoc(), DocumentTrackerEnum.UploadVersion);
		}

	}

	/**
	 * 
	 * JMB 01 09 2017
	 * 
	 * Finds docs by user id
	 * 
	 * @param user
	 * @return List<Doc>
	 * @throws Exception
	 */
	public List<Doc> findDocsByUser(Users user) throws Exception {
		return resolveVersions(dao.findByUser(user.getUid()), false);
	}

	public List<Doc> searchByTargetKeyAndClass(String targetClass, Long id) throws Exception {
		return resolveVersions(dao.searchByTargetKeyAndClass(targetClass, id), false);
	}

}
