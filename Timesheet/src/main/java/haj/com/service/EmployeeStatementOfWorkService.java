package haj.com.service;

import java.util.List;

import haj.com.dao.EmployeeStatementOfWorkDAO;
import haj.com.entity.EmployeeStatementOfWork;
import haj.com.entity.StatementOfWork;
import haj.com.entity.Users;
import haj.com.framework.AbstractService;

public class EmployeeStatementOfWorkService extends AbstractService {

	private EmployeeStatementOfWorkDAO dao = new EmployeeStatementOfWorkDAO();

	public List<EmployeeStatementOfWork> allEmployeeStatementOfWork() throws Exception {
		return dao.allEmployeeStatementOfWork();
	}

	public void create(EmployeeStatementOfWork entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

	public void update(EmployeeStatementOfWork entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(EmployeeStatementOfWork entity) throws Exception {
		this.dao.delete(entity);
	}

	public EmployeeStatementOfWork findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public EmployeeStatementOfWork findByUser(Users u) throws Exception {
		return dao.findByUsers(u.getUid());
	}

	public EmployeeStatementOfWork findByUsersAndSOW(Users u, StatementOfWork sow) throws Exception {
		return dao.findByUsersAndSOW(u.getUid(), sow.getId());
	}

	public List<EmployeeStatementOfWork> findBySOW(StatementOfWork sow) throws Exception {
		return dao.findBySOW(sow);
	}

	public List<EmployeeStatementOfWork> findSowMilestone(StatementOfWork sow) throws Exception {
		return dao.findSowMilestone(sow);
	}

	public List<EmployeeStatementOfWork> findBySOWandNull(StatementOfWork sow) throws Exception {
		return dao.findBySOWandNull(sow.getId());
	}

	public List<EmployeeStatementOfWork> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	public List<EmployeeStatementOfWork> findBySOWForInvoice(StatementOfWork sow) throws Exception {
		return dao.findBySOWForInvoice(sow.getId());
	}
	/*
	 * public List<EmployeeStatementOfWork> findByCompany(haj.com.entity.Company
	 * company) throws Exception { return dao.findByCompany(company.getId()); }
	 */

	public List<EmployeeStatementOfWork> findBySowAndUser(StatementOfWork sow, Users user) throws Exception {
		return dao.findBySowAndUser(sow.getId(), user.getUid());
	}
}
