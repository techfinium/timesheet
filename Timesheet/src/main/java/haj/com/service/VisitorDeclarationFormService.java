package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.VisitorDeclarationFormDAO;
import haj.com.entity.Address;
import haj.com.entity.AddressTypeEnum;
import haj.com.entity.Company;
import haj.com.entity.MonitoringRegister;
import haj.com.entity.Users;
import haj.com.entity.Visitor;
import haj.com.entity.VisitorDeclarationForm;
import haj.com.entity.enums.VisitorTypeEnum;
import haj.com.framework.AbstractService;

public class VisitorDeclarationFormService extends AbstractService {

	VisitorService visitorService = new VisitorService();
	AddressService addressService = new AddressService();
	CompanyService companyService = new CompanyService();
	UsersService userService = new UsersService();

	private VisitorDeclarationFormDAO dao = new VisitorDeclarationFormDAO();

	public List<VisitorDeclarationForm> allVisitorDeclarationForm() throws Exception {
		return dao.allVisitorDeclarationForm();
	}

	public void create(VisitorDeclarationForm entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

	public void createVisitorDeclarationForm(VisitorDeclarationForm visitorDeclarationForm) throws Exception {
		if (visitorDeclarationForm.getVisitorType().equals(VisitorTypeEnum.New)) {
			visitorService.create(visitorDeclarationForm.getVisitor());
			create(visitorDeclarationForm);
			addressService.createResidentialAddress(visitorDeclarationForm.getVisitor().getPhysicalAddress(), visitorDeclarationForm.getVisitor().getClass().getName(), visitorDeclarationForm.getVisitor().getId());
			visitorDeclarationForm.getVisitor().getCompanyAddress().setAddrType(AddressTypeEnum.Work);
			addressService.createWorkAddress(visitorDeclarationForm.getVisitor().getCompanyAddress(), visitorDeclarationForm.getVisitor().getClass().getName(), visitorDeclarationForm.getVisitor().getId());
		} else if(visitorDeclarationForm.getVisitorType().equals(VisitorTypeEnum.Existing)) {
			create(visitorDeclarationForm);
		}
			
		
	}

	public void update(VisitorDeclarationForm entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(VisitorDeclarationForm entity) throws Exception {
		this.dao.delete(entity);
	}
	
	public void deletevisitorDeclarationForm(VisitorDeclarationForm visitorDeclarationForm) throws Exception {
		populateVisitorAddresses(visitorDeclarationForm);
		addressService.delete(visitorDeclarationForm.getVisitor().getPhysicalAddress());
		addressService.delete(visitorDeclarationForm.getVisitor().getCompanyAddress());
		visitorService.delete(visitorDeclarationForm.getVisitor());
		delete(visitorDeclarationForm);
	}

	public VisitorDeclarationForm findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<VisitorDeclarationForm> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	/**
	 * Lazy load VisitorDeclarationForm with pagination, filter, sorting
	 * 
	 * @author TechFinium
	 * @param class1    VisitorDeclarationForm class
	 * @param first     the first
	 * @param pageSize  the page size
	 * @param sortField the sort field
	 * @param sortOrder the sort order
	 * @param filters   the filters
	 * @return a list of {@link haj.com.entity.VisitorDeclarationForm} containing
	 *         data
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public List<VisitorDeclarationForm> allVisitorDeclarationForm(Class<VisitorDeclarationForm> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return resolveTransientValues((List<VisitorDeclarationForm>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters));

	}

	/**
	 * Get count of VisitorDeclarationForm for lazy load with filters
	 * 
	 * @author TechFinium
	 * @param entity  VisitorDeclarationForm class
	 * @param filters the filters
	 * @return Number of rows in the VisitorDeclarationForm entity
	 * @throws Exception the exception
	 */
	public int count(Class<VisitorDeclarationForm> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	/*
	 * public List<VisitorDeclarationForm> findByCompany(haj.com.entity.Company
	 * company) throws Exception { return dao.findByCompany(company.getId()); }
	 */
	public List<VisitorDeclarationForm> resolveTransientValues(List<VisitorDeclarationForm> l) throws Exception{
		for (VisitorDeclarationForm visitorDeclarationForm : l) {
				visitorDeclarationForm.getVisitor().setPhysicalAddress(addressService.findByTagetClassTagerKeyAndAddressType(visitorDeclarationForm.getVisitor().getClass().getName(), visitorDeclarationForm.getVisitor().getId(), AddressTypeEnum.Residential));
				visitorDeclarationForm.getVisitor().setCompanyAddress(addressService.findByTagetClassTagerKeyAndAddressType(visitorDeclarationForm.getVisitor().getClass().getName(), visitorDeclarationForm.getVisitor().getId(), AddressTypeEnum.Work));
		}return l;
	}
	
	public void populateVisitorAddresses(VisitorDeclarationForm visitorDeclarationForm) throws Exception {
		visitorDeclarationForm.setVisitorType(VisitorTypeEnum.Existing);
		visitorDeclarationForm.getVisitor().setPhysicalAddress(addressService.findByTagetClassTagerKeyAndAddressType(visitorDeclarationForm.getVisitor().getClass().getName(), visitorDeclarationForm.getVisitor().getId(), AddressTypeEnum.Residential));
		visitorDeclarationForm.getVisitor().setCompanyAddress(addressService.findByTagetClassTagerKeyAndAddressType(visitorDeclarationForm.getVisitor().getClass().getName(), visitorDeclarationForm.getVisitor().getId(), AddressTypeEnum.Work));
	}

}
