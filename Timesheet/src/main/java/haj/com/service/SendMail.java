package haj.com.service;



import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import haj.com.bean.AttachmentBean;
import haj.com.constants.HAJConstants;
import haj.com.constants.custom.CustomPath;
import haj.com.entity.MailLog;
import haj.com.framework.AbstractService;
import haj.com.utils.GenericUtility;





public class SendMail  extends AbstractService {

	/**
	 * 
	 */
	
	
	String mailServer = "smtp.office365.com";// ((java.util.Properties)
	String uid = "testing@dajotechnologies.com";// ((java.util.Properties) 
	String pwd = "PaSSword@132";
	private int port = 587;
	
	
	private static final long serialVersionUID = 1L;
	private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();

	private Properties props = new Properties();
	private String SMTP_HOST_NAME;
	private int SMTP_HOST_PORT;
	private String SMTP_AUTH_USER;
	private String SMTP_AUTH_PWD;
	protected final Log logger = LogFactory.getLog(this.getClass());

	private static String EMAIL_HEADER = "headerLogo";
	private static String EMAIL_RIGHTHEADERLOGO = "rightHeaderLogo";
	private static String EMAIL_FBLOGO = "fbLogo";
	private static String EMAIL_TWTLOGO = "twtLogo";
	private static String EMAIL_APPSTR = "appstr";
	private static String EMAIL_FBACLOGO = "fbacLogo";
	private static String EMAIL_GWSLOGO = "gwsLogo";
	
	
	//TODO: if user wants to use a custom logo instead of JMR logo this field should have the path to the logo image
	private static String customLogo = "";
	private static String customWeblink = "";







	private String zipFiles(List<String> files) throws Exception{
		byte[] buf = new byte[20480];
		String filename ="pn_"+new java.util.Date().getTime()+".zip";
		String outFilename =  HAJConstants.DOC_PATH+filename;
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFilename));
		for (String f : files) {
			FileInputStream in = new FileInputStream(f);
			// Add ZIP entry to output stream.

			out.putNextEntry(new ZipEntry( GenericUtility.convertFileName(f)));

			// Transfer bytes from the file to the ZIP file
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}

			// Complete the entry
			out.closeEntry();
			in.close();
		}

		// Complete the ZIP file
		out.close();

		return filename;
	}
	
	public void mailWithAttachement(String to_address, String subject, String text, byte[] attachment, String attachmentDescription, String attachmentName, String mime, MailLog mailLog) {
		try {

			HtmlEmail email = new HtmlEmail();
			setupEmail(to_address, email);
			email.setSubject(subject);
			email.setSubject(subject);
			email.setHtmlMsg(createHtml(email, text, null));

			// add the attachment

			if (mime.toLowerCase().contains("image/")) email.attach(new ByteArrayDataSource(attachment, mime.toLowerCase()), attachmentName, attachmentDescription, EmailAttachment.ATTACHMENT);
			else email.attach(new ByteArrayDataSource(attachment, "application/" + mime.toLowerCase()), attachmentName, attachmentDescription, EmailAttachment.ATTACHMENT);

			// send the email
			email.send();
			if (mailLog != null) {
				MailLogService.update(mailLog, attachmentName, mime, attachment);
			}
		} catch (Exception e) {
			if (mailLog != null) MailLogService.update(mailLog, e);
			e.printStackTrace();
			logger.fatal(e);

		}
	}
	
	private static String createHtml(HtmlEmail email, String body, String logo) {
		String html = "";
		html = HAJConstants.EMAIL_TEMPLATE;
		if (logo == null) logo = "logo1.png";
		html = html.replace("#LOGO#", logo);
		html = html.replace("#BODY#", body);
		return html;
	}
	
	private void setupEmail(String to_address, HtmlEmail email) throws EmailException {
	
			mailServer = "smtp.office365.com";// ((java.util.Properties)
			uid = "testing@dajotechnologies.com";// ((java.util.Properties)
			pwd = "PaSSword@132";
			email.setHostName(mailServer);
			email.setSmtpPort(port);
			email.setAuthenticator(new DefaultAuthenticator(uid, pwd));
			email.setStartTLSRequired(true);
			email.setDebug(HAJConstants.MAIL_DEBUG);
			email.addTo(to_address);
			email.setFrom(uid);
		}
	
	public synchronized void sendMailCommons(String to_address, String subject, String text, MailLog mailLog, String logo) {
		try {
			HtmlEmail email = new HtmlEmail();
			setupEmail(to_address, email);
			email.setSubject(subject);
			email.setHtmlMsg(createHtml(email, text, logo));
			email.send();

			if (mailLog != null) {
				MailLogService.update(mailLog);
			}
		} catch (Exception e) {
			MailLogService.update(mailLog, e);
			logger.fatal(e);
		}

	}



	public void hostProperties() throws Exception {
		String mailServer = HAJConstants.MAIL_SERVER;

		String smtpStr = "";
		String postParm = "";

		if (mailServer.equals("gmail")) {
			smtpStr = "smtps";
			postParm = "gmail";
		}
		else 
			if (mailServer.equals("pn")) {
				smtpStr = "smtp";
				postParm = "pn";								
			}		

		SMTP_HOST_NAME = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_HOST_NAME_" + postParm);
		SMTP_HOST_PORT = Integer.parseInt(((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_HOST_PORT_" + postParm));
		SMTP_AUTH_USER = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_AUTH_USER_" + postParm);
		SMTP_AUTH_PWD = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_AUTH_PWD_" + postParm);
		String SMTP_FROM = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_FROM");
		String SMTP_AUTH_REQUIRED = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_AUTH_REQUIRED_" + postParm);


		props.put("mail.transport.protocol", smtpStr);
		props.put("mail." + smtpStr + ".host", SMTP_HOST_NAME);
		props.put("mail." + smtpStr + ".auth", SMTP_AUTH_REQUIRED);
		props.put("mail." + smtpStr + ".quitwait", "false");
		props.put("mail." + smtpStr + ".from", SMTP_FROM);
	}



	/**
	 * Method for sending out emails
	 * 
	 * @param from_address The address which will be used as from address, if left blank will use default from address
	 * @param to_address The address/s where the email is to be sent to
	 * @param subject The subject of the email
	 * @param messageBody Body of the message
	 * @param attachments List of attachments to add to the email, MAKE SURE THIS IS LIST OF DIRECTORY + FILENAME
	 * @param zipAttachments True will zip all the attachments, false will add all attchment individually
	 * @param useDefaultTemplate Apply the default email template to the email
	 * @throws Exception
	 */
	public void sendMail(String from_address, List<String> to_addresses, String subject, String messageBody, List<String> attachments, boolean zipAttachments, boolean useDefaultTemplate) throws Exception{

		Properties props = new Properties();

		String mailServer = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("mailServer");

		String smtpStr = "";
		String postParm = "";

		if (mailServer.equals("gmail")) {
			smtpStr = "smtps";
			postParm = "gmail";
		}
		else 
			if (mailServer.equals("PN")) {
				smtpStr = "smtp";
				postParm = "PN";								
			}		


		//Grab default mail server settings from properties table
		String SMTP_HOST_NAME = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_HOST_NAME_" + postParm);
		int SMTP_HOST_PORT = Integer.parseInt(((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_HOST_PORT_" + postParm));
		String SMTP_AUTH_USER = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_AUTH_USER_" + postParm);
		String SMTP_AUTH_PWD = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_AUTH_PWD_" + postParm);
		String SMTP_FROM = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_FROM");
		String SMTP_AUTH_REQUIRED = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("SMTP_AUTH_REQUIRED_" + postParm);

		//Check if a from address was supplied and override default address
		if(from_address != null)
		{
			if(!from_address.equalsIgnoreCase("")){
				SMTP_FROM = from_address;
			}
		}

		props.put("mail.transport.protocol", smtpStr);
		props.put("mail." + smtpStr + ".host", SMTP_HOST_NAME);
		props.put("mail." + smtpStr + ".auth", SMTP_AUTH_REQUIRED);
		props.put("mail." + smtpStr + ".quitwait", "false");
		props.put("mail." + smtpStr + ".from", SMTP_FROM);
		//props.put("mail.smtp.dsn.ret","HDRS");	
		//props.put("mail.smtp.dsn.notify","SUCCESS, FAILURE");	
		
	    logger.info("=======================================================>>>>> "+props.toString());
		Session mailSession = Session.getDefaultInstance(props);
		mailSession.setDebug(HAJConstants.MAIL_DEBUG);
		Transport transport = mailSession.getTransport();

		MimeMessage message = new MimeMessage(mailSession);
		message.setSubject(subject);        
		message.setFrom(new InternetAddress(props.getProperty("mail." + smtpStr + ".from")));
		//message.addRecipient(Message.RecipientType.TO,  new InternetAddress(to_address));

		for (String to_address : to_addresses) {
			message.addRecipient(Message.RecipientType.TO,  new InternetAddress(to_address));
		}


		MimeMultipart multipart = new MimeMultipart("mixed");


		//Add email body text

		//Check if default template should be applied
		try{
			if(useDefaultTemplate){
				//String newText = readEmailTemplate();
				
				String rightHeaderWeblink = "http://www.judge-finder.com";
				
				if(!customWeblink.isEmpty()){
					rightHeaderWeblink = customWeblink;
				}

				//Get the template
				String appPath = HAJConstants.APP_PATH;
				String template =   GenericUtility.readFile(appPath+"/emailTemplate/template.html");

				
				template =  template.replaceAll("#PATH#", HAJConstants.PL_LINK+"/emailTemplate");
				template =  template.replaceAll("#WEBLINK#", "http://www.hslpro.com");//HAJConstants.HAJ_LINK);
				template =  template.replaceAll("#RIGHTHEADERWEBLINK#", rightHeaderWeblink);
				template =  template.replaceAll("#CURRENT_YEAR#", HAJConstants.EMAIL_TEMPLATE_COPYWRITE_YEAR);
				template =  template.replaceAll("#LIST:COMPANY#", HAJConstants.EMAIL_TEMPLATE_COPYWRITE_COMPANY);

				template =  template.replaceAll("#TWITTER:PROFILEURL#", HAJConstants.TWITTER_URL);
				template =  template.replaceAll("#FACEBOOK:PROFILEURL#", HAJConstants.FACEBOOK_URL);
				template =  template.replaceAll("#FACEBOOK:APPCENTERURL#", HAJConstants.FACEBOOK_APP_CENTER_URL);
				template =  template.replaceAll("#FACEBOOK:PROFILEURL#", HAJConstants.FACEBOOK_URL);
				template =  template.replaceAll("#GOOGLE:WEBSTOREURL#", HAJConstants.CHROME_WEBSTORE_URL);

				template =  template.replaceAll("#FORWARD#", HAJConstants.FOWARD_TO_FRIEND);	

				//Setting embedded logo links		
				template =  template.replaceAll("#HEADER#", EMAIL_HEADER);	
				template =  template.replaceAll("#RIGHTHEADERLOGO#", EMAIL_RIGHTHEADERLOGO);	
				template =  template.replaceAll("#FBLOGO#", EMAIL_FBLOGO);	
				template =  template.replaceAll("#FBACLOGO#", EMAIL_FBACLOGO);	
				template =  template.replaceAll("#TWTLOGO#", EMAIL_TWTLOGO);	
				template =  template.replaceAll("#APPSTRLOGO#", EMAIL_APPSTR);   
				template =  template.replaceAll("#GWSLOGO#", EMAIL_GWSLOGO); 

	
				//add in the content
				template = template.replaceAll("#CONTENT#", messageBody);

				//Apply template to message body
				messageBody = template;
			}
		} catch (Exception e) {

			//If something went wrong then don't use default template further
			useDefaultTemplate = false;
			e.printStackTrace();
		}


		MimeBodyPart messageBodyPart  = new MimeBodyPart();
		messageBodyPart.setContent(messageBody, "text/html");
		multipart.addBodyPart(messageBodyPart);      
        String logo = "logo.png";
        
        
		// Add embedded images 
	
		if(useDefaultTemplate){
			messageBodyPart = new MimeBodyPart();		
			String fnHeader =  HAJConstants.APP_PATH +"/images/"+logo;    	
			DataSource fdsHeader = new FileDataSource(fnHeader);
			messageBodyPart.setDataHandler(new DataHandler(fdsHeader));
			messageBodyPart.setHeader("Content-ID","<" + EMAIL_HEADER + ">");       
			multipart.addBodyPart(messageBodyPart);    	
			
			//String fnDJLogo =  HAJConstants.APP_PATH +"images/djMail.png"; 

		}    	

		// Adding attachments
		String fn = "";
		boolean hasZip = false;

		//Check for any attachments
		if(attachments != null){
			//Make sure there is at least 1 attachment
			if(attachments.size() > 0){    		
				//Zip attachments
				if(zipAttachments){
					messageBodyPart = new MimeBodyPart();        
					fn =  HAJConstants.DOC_PATH +"pdf/" + zipFiles(attachments);
					hasZip = true;
					FileDataSource fds = new FileDataSource(fn);
					messageBodyPart.setDataHandler(new DataHandler(fds));
					messageBodyPart.setFileName(fds.getName());
					multipart.addBodyPart(messageBodyPart);

					//Remove zip file, do not stop sending this mail if this fails
					hasZip = true;
				}
				//Attach each file to message without zipping
				else{    			
					for (String attachment : attachments) {
						messageBodyPart = new MimeBodyPart();         			
						FileDataSource fds = new FileDataSource(attachment);
						messageBodyPart.setDataHandler(new DataHandler(fds));
						messageBodyPart.setFileName(fds.getName());
						multipart.addBodyPart(messageBodyPart);
					}    			
				}
			} 
		}



		// add the Multipart to the message
		message.setContent(multipart);

		// set the Date: header
		message.setSentDate(new java.util.Date());        

		//transport.setReturnOption(SMTPMessage.RETURN_HDRS);
		//smtpMsg.setNotifyOptions(
		//  SMTPMessage.NOTIFY_DELAY|SMTPMessage.NOTIFY_FAILURE|SMTPMessage.NOTIFY_SUCCESS);
		
		transport.connect
		(SMTP_HOST_NAME, SMTP_HOST_PORT, SMTP_AUTH_USER, SMTP_AUTH_PWD);

		transport.sendMessage(message,
				message.getRecipients(Message.RecipientType.TO));
		transport.close();       

		//	HAJFileUtils.deleteFile(fn);


		if(hasZip){
			GenericUtility.deleteFile(fn);
		}


	}



	public void sendMailWithTemplate(String to_address, String subject, String text) {
		try {
	        logger.info("About to send mail to :" + to_address);
			List<String> to_addresses = new ArrayList<String>();
			to_addresses.add(to_address);

			sendMail(null, to_addresses, subject, text, null, false, true);
		} catch (Exception e) {
			logger.fatal("FATAL: SENDMAIL: sendMailWithTemplate",e);
		}
	}

	public void sendMailWithTemplate(String to_address, String subject, String text,List<String> fileNames) {
		try {


			List<String> to_addresses = new ArrayList<String>();
			to_addresses.add(to_address);

			sendMail(null, to_addresses, subject, text, fileNames, false, true);


		} catch (Exception e) {
			logger.fatal("FATAL: SENDMAIL: sendMailWithTemplate:",e);
		}
	}

	

	public void sendMailWithTemplateWithAttachements(String to_address, String subject, String text,String fileName) {
		try {


			List<String> to_addresses = new ArrayList<String>();
			to_addresses.add(to_address);

			List<String> fileNames = new ArrayList<String>();
			fileNames.add(fileName);

			sendMail(null, to_addresses, subject, text, fileNames, false, true);
		} catch (Exception e) {
			logger.fatal("FATAL: SENDMAIL: sendMailWithTemplate:",e);
		}
	}	

	public void sendMailFromWithTemplate(String from, List<String> to_address, String subject, String text) {
		try {


			sendMail(from, to_address, subject, text, null, false, true);

		} catch (Exception e) {
			logger.fatal("FATAL: SENDMAIL: sendMailFromWithTemplate",e);
		}
	}


	public void mailWithAttachement(String from, String to, String subject, String body,List<AttachmentBean> files, boolean custom, String logo) {
		  try {
			  
				String mailServer = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver");
				final String uid = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_uid");
				final String pwd = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_pwd");
				String portS = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_port");
				
				
				HtmlEmail email = new HtmlEmail();
				
				  email.setHostName(mailServer);
				  email.setSmtpPort(Integer.valueOf(portS.trim()));
				  email.setAuthenticator(new DefaultAuthenticator(uid, pwd));
				  //email.setStartTLSRequired(true);
				  email.setDebug(HAJConstants.MAIL_DEBUG);
				  email.addTo(to);
				  email.setFrom(HAJConstants.MAIL_FROM);
				  email.setSubject(subject);
				  email.setHtmlMsg(createHtml(email,  body,custom,logo));

			  // add the attachments
			for (AttachmentBean ab : files) {
				  email.attach(new ByteArrayDataSource(ab.getFile(), "application/"+ab.getExt()),
						  ab.getFilename(), ab.getFilename(),
					       EmailAttachment.ATTACHMENT);
			}
			  email.send();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void sendMailCommons(String to_address, String subject, String text, boolean custom, String logo) {
		try {
			String mailServer = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver");
			final String uid = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_uid");
			final String pwd = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_pwd");
			String portS = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_port");
	
			logger.info("about to send mail! uid:"+uid + " pwd:"+pwd );
			
			HtmlEmail email = new HtmlEmail();
		/*	
			  email.setHostName(mailServer);
			  email.setSmtpPort(Integer.valueOf(portS.trim()));
			  email.setAuthenticator(new DefaultAuthenticator(uid, pwd));
			  email.addTo(to_address);
			  email.setFrom(HAJConstants.MAIL_FROM);
			  email.setSubject(subject);
			  email.setHtmlMsg(createHtml(email,  text,custom,logo));
			  email.send();
	*/
			 
			   			
			  email.setHostName("smtp.office365.com");
			  email.setSmtpPort(587);
			  email.setAuthenticator(new DefaultAuthenticator("info@techfinium.com", "Oaklodge123*"));
			  email.setStartTLSRequired(true);
			  email.setDebug(false);
			  email.addTo(to_address);
			  email.setFrom("info@techfinium.com ");
			  email.setSubject(subject);

				email.setHtmlMsg(createHtml(email,  text,custom,logo));
			  email.send();
			   
			  
			  
	/*		email.setStartTLSRequired(true);
			email.setDebug(HAJConstants.MAIL_DEBUG);
			email.setHostName(mailServer.trim());
			email.setSmtpPort(Integer.valueOf(portS.trim()));
			javax.mail.Authenticator newAuthenticator =   new javax.mail.Authenticator() {
      	        protected PasswordAuthentication getPasswordAuthentication() {
      	            return new PasswordAuthentication(uid, pwd);
      	      }};
      	      
			email.setAuthenticator(newAuthenticator);
				email.setFrom(mailFrom.trim());
			email.setSubject(subject.trim());
			email.setHtmlMsg(createHtml(email, text));
			email.addTo(to_address);
		
			email.send();
		*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.fatal(e);
		}
		
	}

	private static String createHtml(HtmlEmail email, String body, boolean custom,String logo) {
		String html =  "";
		    if (custom) {
		
			       html = HAJConstants.EMAIL_TEMPLATE_CUSTOM;
				   html = html.replace("#BODY#", body);
				 try {
					 html = html.replace("#LOGO#", logo);
/*					   html  =  html.replace("#PAGEBG#",   email.embed(new ByteArrayDataSource(CustomPath.class.getResourceAsStream("page-bg.jpg"), "image/jpg"),"image/jpg"));
					   html  =  html.replace("#BACKGROUND#",  email.embed(new ByteArrayDataSource(CustomPath.class.getResourceAsStream("background.jpg"), "image/jpg"),"image1/jpg"));
					   html  =  html.replace("#BOXBG2#",  email.embed(new ByteArrayDataSource(CustomPath.class.getResourceAsStream("box-bg-2.jpg"), "image/jpg"),"image2/jpg"));
					   html  =  html.replace("#BOXBG3#",  email.embed(new ByteArrayDataSource(CustomPath.class.getResourceAsStream("box-bg-3.jpg"), "image/jpg"),"image3/jpg"));
					   html  =  html.replace("#BOXBG4#",  email.embed(new ByteArrayDataSource(CustomPath.class.getResourceAsStream("box-bg-4.jpg"), "image/jpg"),"image4/jpg"));
					   
					   String ext = FilenameUtils.getExtension(logo).toLowerCase();
					   byte[] b = FileUtils.readFileToByteArray(new File(HAJConstants.DOC_PATH+logo));
					   if ("png".equals(ext)) {
						   html  =  html.replace("#IMAGE1#",  email.embed(new ByteArrayDataSource(b,"image/png"),"image5/png"));  
					   }
					   else {
					       html  =  html.replace("#IMAGE1#",  email.embed(new ByteArrayDataSource(b,"image/jpg"),"image5/jpg"));
					   }
*/					   
				 } catch (Exception e) {
					e.printStackTrace();
				 } 
		    	
		    	
		    	
		    }   
		    else {
		       html = HAJConstants.EMAIL_TEMPLATE;
			   html = html.replace("#BODY#", body);
/*			 try {
				  
				   html  =  html.replace("#BACKHS1#",   email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("background-1.jpg"), "image/jpg"),"image/jpg"));
				   html  =  html.replace("#BACKHS2#",  email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("background.jpg"), "image/jpg"),"image1/jpg"));
				   html  =  html.replace("#IMAGEHS1#",  email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("image-1.png"), "image/png"),"image2/png"));
				   html  =  html.replace("#IMAGEHS8#",  email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("image-8.jpg"), "image/jpg"),"image3/jpg"));
				   html  =  html.replace("#PAGEBGHS#",  email.embed(new ByteArrayDataSource(HAJConstants.class.getResourceAsStream("page-bg.jpg"), "image/jpg"),"image4/jpg"));

			 } catch (Exception e) {
				e.printStackTrace();
			 }  
*/		    }
		return html;
	}
	
	public void sendMailJava(String to_address, String subject, String text) {
		try {
			String mailServer = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver");
			final String uid = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_uid");
			final String pwd = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_pwd");
			String portS = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_port");
			String mailFrom = ((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("ds_mailserver_mailFrom");
			
			logger.info("about to send mail! uid:"+uid + " pwd:"+pwd );
			
			
	        Properties prop=new Properties();
	        prop.put("mail.smtp.auth", "true");
	        prop.put("mail.smtp.host", mailServer);
	        prop.put("mail.smtp.port", portS);
	      //  prop.put("mail.smtp.starttls.enable", "true");
	        prop.put("mail.debug", HAJConstants.MAIL_DEBUG?"true":"false");
			
			
	        Session session = Session.getDefaultInstance(prop,
	        	      new javax.mail.Authenticator() {
	        	        protected PasswordAuthentication getPasswordAuthentication() {
	        	            return new PasswordAuthentication(uid, pwd);
	        	      }
	        	    });
			

			
			String appPath = HAJConstants.APP_PATH;//"/Users/hendrik/Documents/workspaceLuna/Drugsign/WebContent";
			String template =   GenericUtility.readFile(appPath+"/emailTemplate/template.html");
			template = template.replaceAll("#HEADING#", subject.trim());
			template = template.replaceAll("#BODY#", text.trim());
			
			
		     Message message = new MimeMessage(session);
		     message.setFrom(new InternetAddress(mailFrom));
		             message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(to_address));
		    message.setSubject(subject);
		    MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
		    mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
		    mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
		    mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
		    mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
		    mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
		    CommandMap.setDefaultCommandMap(mc);
		        message.setText(template);
		                    message.setContent(template, "text/html");
		        Transport.send(message);
			
			
//			email.setHtmlMsg(template);
//			email.addTo(to_address);
//		
//			email.send();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.fatal(e);
		}
		
	}	


	
	public void sendMailOffice365(String from, String to_address, String subject, String password) throws Exception {
	
	
				
			HtmlEmail email = new HtmlEmail();
			
			  email.setHostName("smtp.office365.com");
			  email.setSmtpPort(587);
			  email.setAuthenticator(new DefaultAuthenticator(from, password));
			  email.setStartTLSRequired(true);
			  email.setDebug(false);
			  email.addTo(to_address);
			  email.setFrom(from);
			  email.setSubject(subject);
				InputStream inputStream = CustomPath.class.getResourceAsStream("index.html");
				String html = IOUtils.toString(inputStream, "UTF8");
			  email.setHtmlMsg(html);
			  email.send();
	
			  

	
		
	}
	
	public static void main(String[] args) {
		System.out.println("Start");
		try {
			SendMail sm = new SendMail();
			sm.sendMailOffice365("hendrik.strydom@jmruk.com", "rajinder.gill@jmruk.com","","sapiens02$");
			System.out.println("Done");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(0);
	}
}
