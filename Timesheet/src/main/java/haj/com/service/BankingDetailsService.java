package haj.com.service;

import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.BankingDetailsDAO;
import haj.com.entity.BankingDetails;
import haj.com.framework.AbstractService;

public class BankingDetailsService extends AbstractService {
	/** The dao. */
	private BankingDetailsDAO dao = new BankingDetailsDAO();

	/**
	 * Get all BankingDetails
 	 * @author TechFinium 
 	 * @see   BankingDetails
 	 * @return a list of {@link haj.com.entity.BankingDetails}
	 * @throws Exception the exception
 	 */
	public List<BankingDetails> allBankingDetails() throws Exception {
	  	return dao.allBankingDetails();
	}


	/**
	 * Create or update BankingDetails.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see     BankingDetails
	 */
	public void create(BankingDetails entity) throws Exception  {
	   if (entity.getId() ==null) {
		 this.dao.create(entity);
	   }
		else
		 this.dao.update(entity);
	}


	/**
	 * Update  BankingDetails.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see    BankingDetails
	 */
	public void update(BankingDetails entity) throws Exception  {
		this.dao.update(entity);
	}

	/**
	 * Delete  BankingDetails.
	 *
	 * @author TechFinium
	 * @param entity the entity
	 * @throws Exception the exception
	 * @see    BankingDetails
	 */
	public void delete(BankingDetails entity) throws Exception  {
		this.dao.delete(entity);
	}

	/**
	 * Find object by primary key.
	 *
	 * @author TechFinium
	 * @param id the id
	 * @return a {@link haj.com.entity.BankingDetails}
	 * @throws Exception the exception
	 * @see    BankingDetails
	 */
	public BankingDetails findByKey(long id) throws Exception {
       return dao.findByKey(id);
	}

	/**
	 * Find BankingDetails by description.
	 *
	 * @author TechFinium
	 * @param desc the desc
	 * @return a list of {@link haj.com.entity.BankingDetails}
	 * @throws Exception the exception
	 * @see    BankingDetails
	 */
	public List<BankingDetails> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}
	
	/**
	 * Lazy load BankingDetails
	 * @param first from key
	 * @param pageSize no of rows to fetch
	 * @return a list of {@link haj.com.entity.BankingDetails}
	 * @throws Exception the exception
	 */
	public List<BankingDetails> allBankingDetails(int first, int pageSize) throws Exception {
		return dao.allBankingDetails(Long.valueOf(first), pageSize);
	}
		
	
	/**
	 * Get count of BankingDetails for lazy load
	 * @author TechFinium 
	 * @return Number of rows in the BankingDetails
	 * @throws Exception the exception
	 */
	public Long count() throws Exception {
	       return dao.count(BankingDetails.class);
	}
	
    /**
     * Lazy load BankingDetails with pagination, filter, sorting
     * @author TechFinium 
     * @param class1 BankingDetails class
     * @param first the first
     * @param pageSize the page size
     * @param sortField the sort field
     * @param sortOrder the sort order
     * @param filters the filters
     * @return  a list of {@link haj.com.entity.BankingDetails} containing data
     * @throws Exception the exception
     */	
	@SuppressWarnings("unchecked")
	public List<BankingDetails> allBankingDetails(Class<BankingDetails> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return ( List<BankingDetails>)dao.sortAndFilter(class1,first,pageSize,sortField,sortOrder,filters);
	
	}
	
    /**
     * Get count of BankingDetails for lazy load with filters
     * @author TechFinium 
     * @param entity BankingDetails class
     * @param filters the filters
     * @return Number of rows in the BankingDetails entity
     * @throws Exception the exception     
     */	
	public int count(Class<BankingDetails> entity, Map<String, Object> filters) throws Exception {
		return  dao.count(entity, filters);
	}
}
