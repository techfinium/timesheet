package haj.com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.SortOrder;

import haj.com.dao.MedicalScreeningFormDAO;
import haj.com.entity.MedicalScreeningForm;
import haj.com.entity.MedicalScreeningFormCronicDiseases;
import haj.com.entity.Users;
import haj.com.entity.lookup.CronicDiseases;
import haj.com.framework.AbstractService;

public class MedicalScreeningFormService extends AbstractService {

	private MedicalScreeningFormCronicDiseasesService medicalScreeningFormCronicDiseasesService = new MedicalScreeningFormCronicDiseasesService();

	private MedicalScreeningFormDAO dao = new MedicalScreeningFormDAO();
	

	public List<MedicalScreeningForm> allMedicalScreeningForm() throws Exception {
		return dao.allMedicalScreeningForm();
	}

	public void create(MedicalScreeningForm entity) throws Exception {
		if (entity.getId() == null)
			this.dao.create(entity);
		else
			this.dao.update(entity);
	}

	public void createMedicalScreeningForm(MedicalScreeningForm medicalScreeningForm) throws Exception {
		dao.update(medicalScreeningForm.getUser());
		
		create(medicalScreeningForm);
		if (medicalScreeningForm.getCronicDiseasesList().isEmpty()) {
			medicalScreeningForm.setSufferFromCronicDisease(false);
			update(medicalScreeningForm);
		} else {
			medicalScreeningForm.setSufferFromCronicDisease(true);
			for (CronicDiseases element : medicalScreeningForm.getCronicDiseasesList()) {
				MedicalScreeningFormCronicDiseases medicalScreeningFormCronicDiseases = new MedicalScreeningFormCronicDiseases();
				medicalScreeningFormCronicDiseases.setMedicalScreeningForm(medicalScreeningForm);
				medicalScreeningFormCronicDiseases.setCronicDiseases(element);
				dao.create(medicalScreeningFormCronicDiseases);
			}
			update(medicalScreeningForm);
		}
	}

	public void update(MedicalScreeningForm entity) throws Exception {
		this.dao.update(entity);
	}

	public void delete(MedicalScreeningForm entity) throws Exception {
		this.dao.delete(entity);
	}

	public void deleteMedicalSecreeningForm(MedicalScreeningForm medicalScreeningForm) throws Exception {
		if (populateMedicalScreeningFormCronicDiseases(medicalScreeningForm).isEmpty()) {
			delete(medicalScreeningForm);
		} else {
			for (MedicalScreeningFormCronicDiseases element : populateMedicalScreeningFormCronicDiseases(medicalScreeningForm)) {
				medicalScreeningFormCronicDiseasesService.delete(element);
			}
			delete(medicalScreeningForm);
		}
	}

	public MedicalScreeningForm findByKey(long id) throws Exception {
		return dao.findByKey(id);
	}

	public List<MedicalScreeningForm> findByName(String desc) throws Exception {
		return dao.findByName(desc);
	}

	/**
	 * Lazy load MedicalScreeningForm with pagination, filter, sorting
	 * 
	 * @author TechFinium
	 * @param class1    MedicalScreeningForm class
	 * @param first     the first
	 * @param pageSize  the page size
	 * @param sortField the sort field
	 * @param sortOrder the sort order
	 * @param filters   the filters
	 * @return a list of {@link haj.com.entity.MedicalScreeningForm} containing data
	 * @throws Exception the exception
	 */
	@SuppressWarnings("unchecked")
	public List<MedicalScreeningForm> allMedicalScreeningForm(Class<MedicalScreeningForm> class1, int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) throws Exception {
		return (List<MedicalScreeningForm>) dao.sortAndFilter(class1, first, pageSize, sortField, sortOrder, filters);

	}

	/**
	 * Get count of MedicalScreeningForm for lazy load with filters
	 * 
	 * @author TechFinium
	 * @param entity  MedicalScreeningForm class
	 * @param filters the filters
	 * @return Number of rows in the MedicalScreeningForm entity
	 * @throws Exception the exception
	 */
	public int count(Class<MedicalScreeningForm> entity, Map<String, Object> filters) throws Exception {
		return dao.count(entity, filters);
	}

	/*
	 * public List<MedicalScreeningForm> findByCompany(haj.com.entity.Company
	 * company) throws Exception { return dao.findByCompany(company.getId()); }
	 */

	public List<CronicDiseases> populateCronicDiseases(MedicalScreeningForm medicalScreeningForm) throws Exception {
		List<CronicDiseases> a = new ArrayList<>();
		List<MedicalScreeningFormCronicDiseases> l = medicalScreeningFormCronicDiseasesService.findByMedicalScreeningFormId(medicalScreeningForm.getId());
		for (MedicalScreeningFormCronicDiseases element : l) {
			a.add(element.getCronicDiseases());
		}
		medicalScreeningForm.setCronicDiseasesList(a);
		return a;
	}

	public List<MedicalScreeningFormCronicDiseases> populateMedicalScreeningFormCronicDiseases(MedicalScreeningForm medicalScreeningForm) throws Exception {
		List<MedicalScreeningFormCronicDiseases> l = medicalScreeningFormCronicDiseasesService.findByMedicalScreeningFormId(medicalScreeningForm.getId());
		return l;
	}
	
	public MedicalScreeningForm findLatestFormByUser(Users users) throws Exception {
		return dao.findLatestFormByUser(users);
	}
}
