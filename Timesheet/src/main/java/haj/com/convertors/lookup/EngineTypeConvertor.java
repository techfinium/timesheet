package haj.com.convertors.lookup;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.lookup.EngineType;
import haj.com.service.lookup.EngineTypeService;

@FacesConverter(value = "EngineTypeConvertor")

public class EngineTypeConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());

	 private EngineTypeService service = new EngineTypeService();

	/**
	 * Used by JSF to get a EngineType
 	 * @author TechFinium
 	 * @see    EngineType
 	 * @return EngineType
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return service.findByKey(Long.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert EngineType key to String object
 	 * @author TechFinium
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 == null) return "";
		if (arg2 instanceof String) return arg2.toString();
		return ""+((EngineType)arg2).getId();
	}

/*
       <p:selectOneMenu id="EngineTypeId" value="#{xxxUI.EngineType.xxxType}" converter="EngineTypeConvertor" style="width:95%">
         <f:selectItems value="#{EngineTypeUI.EngineTypeList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>

        <h:outputLabel value="EngineType" for="EngineTypeID"/>
        <p:autoComplete id="EngineTypeID" value="#{EngineTypeUI.EngineType.municipality}" completeMethod="#{EngineTypeUI.completeEngineType}"
                            var="rv" itemLabel="#{rv.EngineTypeDescription}" itemValue="#{rv}"
                            forceSelection="true" converter="EngineTypeConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="EngineType" style="white-space: nowrap">#{rv.EngineTypeDescription}</p:column>
       </p:autoComplete>

*/

}
