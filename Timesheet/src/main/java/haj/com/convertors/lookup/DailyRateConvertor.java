package haj.com.convertors.lookup;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.lookup.DailyRate;
import haj.com.service.lookup.DailyRateService;

@FacesConverter(value = "DailyRateConvertor")

public class DailyRateConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());

	 private DailyRateService service = new DailyRateService();

	/**
	 * Used by JSF to get a DailyRate
 	 * @author TechFinium
 	 * @see    DailyRate
 	 * @return DailyRate
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return service.findByKey(Long.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert DailyRate key to String object
 	 * @author TechFinium
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 == null) return "";
		if (arg2 instanceof String) return arg2.toString();
		return ""+((DailyRate)arg2).getId();
	}

/*
       <p:selectOneMenu id="DailyRateId" value="#{xxxUI.DailyRate.xxxType}" converter="DailyRateConvertor" style="width:95%">
         <f:selectItems value="#{DailyRateUI.DailyRateList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>

        <h:outputLabel value="DailyRate" for="DailyRateID"/>
        <p:autoComplete id="DailyRateID" value="#{DailyRateUI.DailyRate.municipality}" completeMethod="#{DailyRateUI.completeDailyRate}"
                            var="rv" itemLabel="#{rv.DailyRateDescription}" itemValue="#{rv}"
                            forceSelection="true" converter="DailyRateConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="DailyRate" style="white-space: nowrap">#{rv.DailyRateDescription}</p:column>
       </p:autoComplete>

*/

}
