package haj.com.convertors.lookup;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.lookup.AllowanceRate;
import haj.com.service.lookup.AllowanceRateService;

@FacesConverter(value = "AllowanceRateConvertor")

public class AllowanceRateConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());

	 private AllowanceRateService service = new AllowanceRateService();

	/**
	 * Used by JSF to get a AllowanceRate
 	 * @author TechFinium
 	 * @see    AllowanceRate
 	 * @return AllowanceRate
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return service.findByKey(Long.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert AllowanceRate key to String object
 	 * @author TechFinium
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 == null) return "";
		if (arg2 instanceof String) return arg2.toString();
		return ""+((AllowanceRate)arg2).getId();
	}

/*
       <p:selectOneMenu id="AllowanceRateId" value="#{xxxUI.AllowanceRate.xxxType}" converter="AllowanceRateConvertor" style="width:95%">
         <f:selectItems value="#{AllowanceRateUI.AllowanceRateList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>

        <h:outputLabel value="AllowanceRate" for="AllowanceRateID"/>
        <p:autoComplete id="AllowanceRateID" value="#{AllowanceRateUI.AllowanceRate.municipality}" completeMethod="#{AllowanceRateUI.completeAllowanceRate}"
                            var="rv" itemLabel="#{rv.AllowanceRateDescription}" itemValue="#{rv}"
                            forceSelection="true" converter="AllowanceRateConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="AllowanceRate" style="white-space: nowrap">#{rv.AllowanceRateDescription}</p:column>
       </p:autoComplete>

*/

}
