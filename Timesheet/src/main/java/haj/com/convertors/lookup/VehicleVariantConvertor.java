package haj.com.convertors.lookup;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.lookup.VehicleVariant;
import haj.com.service.lookup.VehicleVariantService;

@FacesConverter(value = "VehicleVariantConvertor")
@Named
public class VehicleVariantConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	 @Inject
	 private VehicleVariantService service;

	/**
	 * Used by JSF to get a VehicleVariant
 	 * @author TechFinium
 	 * @see    VehicleVariant
 	 * @return VehicleVariant
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return service.findByKey(Long.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert VehicleVariant key to String object
 	 * @author TechFinium
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 == null) return "";
		if (arg2 instanceof String) return arg2.toString();
		return ""+((VehicleVariant)arg2).getId();
	}

/*
       <p:selectOneMenu id="VehicleVariantId" value="#{xxxUI.VehicleVariant.xxxType}" converter="VehicleVariantConvertor" style="width:95%">
         <f:selectItems value="#{VehicleVariantUI.VehicleVariantList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>

        <h:outputLabel value="VehicleVariant" for="VehicleVariantID"/>
        <p:autoComplete id="VehicleVariantID" value="#{VehicleVariantUI.VehicleVariant.municipality}" completeMethod="#{VehicleVariantUI.completeVehicleVariant}"
                            var="rv" itemLabel="#{rv.VehicleVariantDescription}" itemValue="#{rv}"
                            forceSelection="true" converter="VehicleVariantConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="VehicleVariant" style="white-space: nowrap">#{rv.VehicleVariantDescription}</p:column>
       </p:autoComplete>

*/

}
