package haj.com.convertors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.UserProjects;
import haj.com.service.UserProjectsService;

@FacesConverter(value = "UserProjectsConvertor")
public class UserProjectsConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	
	/**
	 * Used by JSF to get a UserProjects
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @return UserProjects
 	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return new UserProjectsService()
						.findByKey(Integer.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}


	/**
	 * Convert UserProjects key to String object
 	 * @author TechFinium 
 	 * @see    String
 	 * @return String
 	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ""+((UserProjects)arg2).getId();
	}

/*
       <p:selectOneMenu id="UserProjectsId" value="#{xxxUI.UserProjects.xxxType}" converter="UserProjectsConvertor" style="width:95%">
         <f:selectItems value="#{UserProjectsUI.UserProjectsList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>
       
        <h:outputLabel value="UserProjects" for="UserProjectsID"/>
        <p:autoComplete id="UserProjectsID" value="#{UserProjectsUI.UserProjects.municipality}" completeMethod="#{UserProjectsUI.completeUserProjects}"
                            var="rv" itemLabel="#{rv.UserProjectsDescription}" itemValue="#{rv}" 
                            forceSelection="true" converter="UserProjectsConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="UserProjects" style="white-space: nowrap">#{rv.UserProjectsDescription}</p:column>
       </p:autoComplete>         
       
*/

}
