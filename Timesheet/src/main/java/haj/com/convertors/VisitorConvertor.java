package haj.com.convertors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.Visitor;
import haj.com.entity.lookup.CronicDiseases;
import haj.com.service.VisitorService;

// TODO: Auto-generated Javadoc
/**
 * The Class VisitorConvertor.
 */
@FacesConverter(value = "VisitorConvertor")
public class VisitorConvertor implements Converter {

	/** The logger. */
	protected final Log logger = LogFactory.getLog(this.getClass());

	/**
	 * Used by JSF to get a Visitor.
	 *
	 * @author TechFinium
	 * @param arg0  the arg 0
	 * @param arg1  the arg 1
	 * @param value the value
	 * @return Visitor
	 * @see Visitor
	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return new VisitorService().findByKey(Long.valueOf(value).longValue());
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
		return null;
	}

	/**
	 * Convert Visitor key to String object.
	 *
	 * @author TechFinium
	 * @param arg0 the arg 0
	 * @param arg1 the arg 1
	 * @param arg2 the arg 2
	 * @return String
	 * @see String
	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 == null) return "";
		if (arg2 instanceof String) return arg2.toString();
		return ""+((Visitor)arg2).getId();
	}

	/*
	 * <p:selectOneMenu id="VisitorId" value="#{xxxUI.Visitor.xxxType}"
	 * converter="VisitorConvertor" style="width:95%"> <f:selectItems
	 * value="#{VisitorUI.VisitorList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
	 * </p:selectOneMenu>
	 * 
	 * <h:outputLabel value="Visitor" for="VisitorID"/> <p:autoComplete id="VisitorID"
	 * value="#{VisitorUI.Visitor.municipality}"
	 * completeMethod="#{VisitorUI.completeVisitor}" var="rv"
	 * itemLabel="#{rv.VisitorDescription}" itemValue="#{rv}" forceSelection="true"
	 * converter="VisitorConvertor" dropdown="true" minQueryLength="3" maxResults="10"
	 * > <p:column headerText="Visitor"
	 * style="white-space: nowrap">#{rv.VisitorDescription}</p:column>
	 * </p:autoComplete>
	 * 
	 */

}
