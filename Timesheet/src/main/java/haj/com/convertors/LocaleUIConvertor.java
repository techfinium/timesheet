package haj.com.convertors;

import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@FacesConverter(value = "localeUIConvertor")
public class LocaleUIConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				Locale l = Locale.forLanguageTag(value);
				return l;
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return ""+arg2;
	}

}
