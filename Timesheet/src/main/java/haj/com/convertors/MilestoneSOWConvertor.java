package haj.com.convertors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.MilestoneStatementOfWork;
import haj.com.entity.StatementOfWork;
import haj.com.service.MilestoneStatementOfWorkService;

@FacesConverter(value = "MilestoneSOWConvertor")
public class MilestoneSOWConvertor implements Converter {
	protected final Log logger = LogFactory.getLog(this.getClass());
	private MilestoneStatementOfWorkService service = new MilestoneStatementOfWorkService();

	/**
	 * Used by JSF to get a Invoice
	 * 
	 * @author TechFinium
	 * @see StatementOfWork
	 * @return StatementOfWork
	 */
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return service.findByKey(Long.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
		return null;
	}

	/**
	 * Convert Invoice key to String object
	 * 
	 * @author TechFinium
	 * @see String
	 * @return String
	 */
	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		return "" + ((MilestoneStatementOfWork) arg2).getId();
	}

	/*
	 * <p:selectOneMenu id="InvoiceId" value="#{xxxUI.Invoice.xxxType}"
	 * converter="InvoiceConvertor" style="width:95%"> <f:selectItems
	 * value="#{InvoiceUI.InvoiceList}" var="rv" itemLabel="#{rv.a}"
	 * itemValue="#{rv}"/> </p:selectOneMenu>
	 * 
	 * <h:outputLabel value="Invoice" for="InvoiceID"/> <p:autoComplete
	 * id="InvoiceID" value="#{InvoiceUI.Invoice.municipality}"
	 * completeMethod="#{InvoiceUI.completeInvoice}" var="rv"
	 * itemLabel="#{rv.InvoiceDescription}" itemValue="#{rv}"
	 * forceSelection="true" converter="InvoiceConvertor" dropdown="true"
	 * minQueryLength="3" maxResults="10" > <p:column headerText="Invoice"
	 * style="white-space: nowrap">#{rv.InvoiceDescription}</p:column>
	 * </p:autoComplete>
	 * 
	 */

}
