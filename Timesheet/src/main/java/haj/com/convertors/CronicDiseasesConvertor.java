package haj.com.convertors;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.entity.lookup.CronicDiseases;
import haj.com.entity.lookup.Roles;
import haj.com.service.lookup.CronicDiseasesService;

@FacesConverter(value = "CronicDiseasesConvertor")
public class CronicDiseasesConvertor implements Converter {
	 protected final Log logger = LogFactory.getLog(this.getClass());
	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String value) {
		if (StringUtils.isBlank(value)) {
			return null;
		} else {

			try {
				return new CronicDiseasesService()
						.findByKey(Integer.valueOf(value));
			} catch (NumberFormatException e) {
				logger.fatal(e);
			} catch (Exception e) {
				logger.fatal(e);
			}

		}
	    return null;
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {
		if (arg2 == null) return "";
		if (arg2 instanceof String) return arg2.toString();
		return ""+((CronicDiseases)arg2).getId();
	}

/*
       <p:selectOneMenu id="CronicDiseasesId" value="#{xxxUI.CronicDiseases.xxxType}" converter="CronicDiseasesConvertor" style="width:95%">
         <f:selectItems value="#{CronicDiseasesUI.CronicDiseasesList}" var="rv" itemLabel="#{rv.a}" itemValue="#{rv}"/>
       </p:selectOneMenu>
       
        <h:outputLabel value="CronicDiseases" for="CronicDiseasesID"/>
        <p:autoComplete id="CronicDiseasesID" value="#{CronicDiseasesUI.CronicDiseases.municipality}" completeMethod="#{CronicDiseasesUI.completeCronicDiseases}"
                            var="rv" itemLabel="#{rv.CronicDiseasesDescription}" itemValue="#{rv}" 
                            forceSelection="true" converter="CronicDiseasesConvertor" dropdown="true" minQueryLength="3" maxResults="10" >
                 <p:column headerText="CronicDiseases" style="white-space: nowrap">#{rv.CronicDiseasesDescription}</p:column>
       </p:autoComplete>         
       
*/

}
