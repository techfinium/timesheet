package haj.com.billing;



import java.io.Serializable;

import haj.com.billing.entity.AccountReceipts;
import haj.com.billing.entity.Accounts;
import haj.com.entity.Address;
import haj.com.entity.Company;
import haj.com.entity.Users;


/**
 * The request holds information regarding the request to deduct unit(s), or
 * none, from an account, based on the event code and account linked to the
 * document.
 * 
 */
public class TransactOnAccountRequest implements Serializable {

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Indicates the event code, this typically denotes the current event taking
   * place, and will be used to determine the cost, as cost is based on a given
   * event, and deducted from an account, linked to the document.
   */
  private Integer eventCode = null;

  /** The document will have an account linked. */

  /**
   * Either account, or document may be set, to indicate on which account the
   * transaction must be effected on.
   */
  private Accounts account = null;


  /**
   * Update billing address with latest info
   */
  private Address billingAddress = null;
  
  /**
   * Used for receipt reports, want to save this in the same transaction as the rest of the stuffz
   */
  private AccountReceipts accountInv = null;
  
  
  /**
   * Set this to true if the external payment was successful
   */
  private char transactionStatus = BillingConstants.ACC_TRANSACTION_FAILED;
  
  private String externalRef = null;
  
  private byte[] externalResp = null;
  
  
  /**
   * Need user to generate receipt
   */
  private Users user = null;
  private Users emailUsers = null;
  private Company company;
  
  
  /**
   * Monster Pay response
   */
  //private MonsterPayResponseBean monsterResp = null;
  

  
  private String description = null;

  /**
   * Default.
   */
  public TransactOnAccountRequest() {

  }

  /**
   * @return the eventCode
   */
  public Integer getEventCode() {
    return eventCode;
  }

  /**
   * @param eventCode
   *          the eventCode to set
   */
  public void setEventCode(Integer eventCode) {
    this.eventCode = eventCode;
  }





  /**
   * @return the account
   */
  public Accounts getAccount() {
    return account;
  }

  /**
   * @param account the account to set
   */
  public void setAccount(Accounts account) {
    this.account = account;
  }

public AccountReceipts getAccountInv() {
	return accountInv;
}

public void setAccountInv(AccountReceipts accountInv) {
	this.accountInv = accountInv;
}
/*
public MonsterPayResponseBean getMonsterResp() {
	return monsterResp;
}

public void setMonsterResp(MonsterPayResponseBean monsterResp) {
	this.monsterResp = monsterResp;
}
*/
public char getTransactionStatus() {
	return transactionStatus;
}

public void setTransactionStatus(char transactionStatus) {
	this.transactionStatus = transactionStatus;
}

public String getExternalRef() {
	return externalRef;
}

public void setExternalRef(String externalRef) {
	this.externalRef = externalRef;
}

public byte[] getExternalResp() {
	return externalResp;
}

public void setExternalResp(byte[] externalResp) {
	this.externalResp = externalResp;
}

public Users getUser() {
	return user;
}

public void setUser(Users user) {
	this.user = user;
}



public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public Users getEmailUsers() {
	return emailUsers;
}

public void setEmailUsers(Users emailUsers) {
	this.emailUsers = emailUsers;
}

public Address getBillingAddress() {
	return billingAddress;
}

public void setBillingAddress(Address billingAddress) {
	this.billingAddress = billingAddress;
}

public Company getCompany() {
	return company;
}

public void setCompany(Company company) {
	this.company = company;
}



}
