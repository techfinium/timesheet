package haj.com.billing;

import java.util.List;

import haj.com.billing.entity.BillingEvents;
import haj.com.billing.entity.EventCost;
import haj.com.framework.AbstractService;
import haj.com.framework.IDataEntity;

public class BillingAdminService extends AbstractService {

	private  BillingDAO dao = new BillingDAO();
	
	public List<BillingEvents> allBillingEvents() throws Exception { 
		return dao.allBillingEvents();
	}
	
	public List<EventCost> allEventCost() throws Exception { 
		return dao.allEventCost();
	}
	
	public void create(IDataEntity entity) throws Exception {
		dao.create(entity);
	}
	
	public void update(IDataEntity entity) throws Exception {
		dao.update(entity);
	}
}
