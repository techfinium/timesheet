package haj.com.billing;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.billing.entity.EventCost;
import haj.com.entity.Company;
import haj.com.entity.Users;





public class BillingHelper implements Serializable {
	
	private static BillingDAO dao = new BillingDAO();
	protected static final Log logger = LogFactory.getLog(BillingHelper.class);
	private static final BillingEngine engine = new BillingEngineImpl();
	
	
	public static void checkUnits(Users user) throws InsufficientUnitsException, Exception {
		if (BillingConstants.BILLING_ENABLED) {
		 if (user.getAccounts()==null) throw new Exception("You do not have an account! Please create an account!");
		 boolean enough = enoughUnits(BillingConstants.EVENT_CODE_DEDUCTION, user);
		 if (!enough) throw new InsufficientUnitsException("You do not have sufficient units in your account to do this transaction");
		}
	}
	
	public static void checkUnits(Company company) throws InsufficientUnitsException, Exception {
		if (BillingConstants.BILLING_ENABLED) {
		 if (company.getAccounts()==null) throw new Exception("You do not have an account! Please create an account!");
		 boolean enough = enoughUnits(BillingConstants.EVENT_CODE_DEDUCTION, company);
		 if (!enough) throw new InsufficientUnitsException("You do not have sufficient units in your account to do this transaction");
		}
	}
	

	
	public static void processTransaction(Company company,Integer addDedcuct,String additionalText) throws Exception { 
		if (BillingConstants.BILLING_ENABLED) {		
			if (company!=null) {
				if (company.getAccounts()==null) throw new Exception(company.getCompanyName()+  " does not have an account! Please create an account");
				processTransactionDetail(company, addDedcuct, additionalText);
			}
		}
	}
	
	private static void processTransactionDetail(Company company,Integer addDedcuct,String additionalText) throws Exception { 
		    TransactOnAccountRequest request = new TransactOnAccountRequest();
			request.setAccount(company.getAccounts());
			request.setEventCode(addDedcuct);	
			request.setExternalResp(null);
			request.setTransactionStatus(BillingConstants.ACC_TRANSACTION_SUCCESS);
			//request.setUser(user);
			request.setCompany(company);
			request.setDescription(additionalText);
			engine.transactOnAccount(request);	
		
	}

	
	public static void processTransaction(Users user,Integer addDedcuct,String additionalText) throws Exception { 
		if (BillingConstants.BILLING_ENABLED) {		
			if (user!=null) {
				if (user.getAccounts()==null) throw new Exception(user.getFirstName() + " "+user.getLastName()+  " does not have an account! Please create an account");
				processTransactionDetail(user, addDedcuct, additionalText);
			}
		}
	}
	
	private static void processTransactionDetail(Users user,Integer addDedcuct,String additionalText) throws Exception { 
		    TransactOnAccountRequest request = new TransactOnAccountRequest();
			request.setAccount(user.getAccounts());
			request.setEventCode(addDedcuct);	
			request.setExternalResp(null);
			request.setTransactionStatus(BillingConstants.ACC_TRANSACTION_SUCCESS);
			request.setUser(user);
			request.setDescription(additionalText);
			engine.transactOnAccount(request);	
		
	}
	
/*
	protected static final Log logger = LogFactory.getLog(BillingHelper.class);
	private static final BillingEngine engine = new BillingEngineImpl();
	private static final AuthenticationBL authenticationBL = new AuthenticationBL();
	private static final DAO dao = new DAO();


	

	public static void processTransaction(Users users,  Integer product, Providers providers,String additionalText) throws Exception {
		 Transactions tran = new TransactionsDAO().transactionsbyExternaleProduct(providers, product);
		 if (tran==null) throw new Exception("Could not find a transaction!");
		 Companies comp = authenticationBL.getAccountingCompany(users.getCompanies());
		 if (comp==null || comp.getAccounts()==null) throw new Exception("Could not find a account for any of the companies in question!");
		    TransactOnAccountRequest request = new TransactOnAccountRequest();
				request.setAccount(comp.getAccounts());
				request.setEventCode(F9Constants.EVENT_CODE_DEDUCTION);	
				request.setDescription("User: " +users.getFirstName() + " "+ users.getLastName() + " did transanction: "+tran.getTransactionDesc() +" "+additionalText);
				request.setExternalResp(null);
				request.setTransactions(tran);
				request.setTransactionStatus(F9Constants.ACC_TRANSACTION_SUCCESS);
				request.setUser(users);
				engine.transactOnAccount(request);	
	}
	

		public static void processTransaction(Users users,  Transactions tran,Integer addDedcuct,String additionalText) throws Exception {
			 if (tran==null) throw new Exception("Could not find a transaction!");
			 Companies comp = authenticationBL.getAccountingCompany(users.getCompanies());
			 if (comp==null || comp.getAccounts()==null) throw new Exception("Could not find a account for any of the companies in question!");
			    TransactOnAccountRequest request = new TransactOnAccountRequest();
					request.setAccount(comp.getAccounts());
					request.setEventCode(addDedcuct);	
					request.setDescription("User: " +users.getFirstName() + " "+ users.getLastName() + " did transanction: "+tran.getTransactionDesc()+" "+additionalText);
					request.setExternalResp(null);
					request.setTransactions(tran);
					request.setTransactionStatus(F9Constants.ACC_TRANSACTION_SUCCESS);
					request.setUser(users);
					engine.transactOnAccount(request);	
		}
	
*/
	private static boolean enoughUnits(Integer eventCode,  Company company) throws EventCodeNotFoundException, Exception  {
		boolean enough = false;
		try {
			    
			 EventCost ec = dao.retrieveActiveEventCostByEventCode(eventCode);
			 if (ec==null) throw new EventCodeNotFoundException("No Event Cost for billing event " + eventCode);
			 long balance = 0;
			 if (company.getAccounts().getUnitBalance() != null) balance  = company.getAccounts().getUnitBalance().longValue();
			 
			 if (ec.getUnits()==null) enough = true;
			 else {
				 balance += ec.getUnits().longValue();
				 if (balance >= 0) enough = true;
			 }
			 
		} catch (EventCodeNotFoundException ec) {
			logger.fatal(ec);
			throw ec;
		}		 
		  catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
		return enough;
	}

	
	private static boolean enoughUnits(Integer eventCode,  Users user) throws EventCodeNotFoundException, Exception  {
		boolean enough = false;
		try {
			    
			 EventCost ec = dao.retrieveActiveEventCostByEventCode(eventCode);
			 if (ec==null) throw new EventCodeNotFoundException("No Event Cost for billing event " + eventCode);
			 long balance = 0;
			 if (user.getAccounts().getUnitBalance() != null) balance  = user.getAccounts().getUnitBalance().longValue();
			 
			 if (ec.getUnits()==null) enough = true;
			 else {
				 balance += ec.getUnits().longValue();
				 if (balance >= 0) enough = true;
			 }
			 
		} catch (EventCodeNotFoundException ec) {
			logger.fatal(ec);
			throw ec;
		}		 
		  catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
		return enough;
	}
	
	/*
	  EFT
	  ---
	 
	 Dear(client)
I would like to thank you for selecting Protecting Lives. It is our commitment to provide your child with the best and reliable 24-hour emergency assistance.
Please find listed below our banking details for your Electronic Funds Transfer (EFT). Please use your ID number as reference.
Proof of payment can be sent directly to our accounts department in order to activate your account.  
 
First National Bank
Karaglen Branch (252442)
Protecting Lives Cheque account
1234
Ref: Your ID Number
 
If you have any further questions or queries, please do not hesitate to contact me directly or our accounts department via accounts@protectinglives.co.za.
Sincerely,
 
Carol Pirie 


DEBIT ORDER
-----------

Dear(client)

I would like to thank you for selecting Protecting Lives. It is our commitment to provide your child with the best and reliable 24-hour emergency assistance.

Please find enclosed our debit order authorisation letter for your agreement. Please sign and return it marked for our accounts department.

If you have any further questions or queries, please do not hesitate to contact me directly or our accounts department via accounts@protectinglives.co.za.

Sincerely,

 
Carol Pirie
	 */

	
	/*
INSERT INTO `haj_billing_billing_events` (`id`, `event_desc`, `event_display_name`, `event_type`)
VALUES
	(4, 'ACCOUNT_TOP_UP_6_SILVER', 'Top Up with 6 Months - Silver', 'T'),
	(5, 'ACCOUNT_TOP_UP_12_SILVER', 'Top Up with 12 Months - Silver', 'T'),
	(6, 'ACCOUNT_TOP_UP_6_PLAT', 'Top Up with 6 Months - Platinum', 'T'),
	(7, 'ACCOUNT_TOP_UP_12_PLAT', 'Top Up with 12 Months - Platinum', 'T');

	 */


	
}
