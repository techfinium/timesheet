package haj.com.billing;

/**
 * Indicates an event could not be found by code.
 * 
 * 
 */
public class EventNotFoundException extends Exception {

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;

  private Integer eventCode = Integer.valueOf(0);

  /**
   * Default.
   */
  public EventNotFoundException(Integer eventCode) {
    super("The event with code: " + eventCode + " could not be found.");

    this.eventCode = eventCode;
  }

  /**
   * @return the eventCode
   */
  public Integer getEventCode() {
    return eventCode;
  }

  /**
   * @param eventCode the eventCode to set
   */
  public void setEventCode(Integer eventCode) {
    this.eventCode = eventCode;
  }

}
