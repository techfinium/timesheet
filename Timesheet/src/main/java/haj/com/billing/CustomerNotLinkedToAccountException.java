package haj.com.billing;


import haj.com.entity.Users;




/**
 * This signal indicates the company is not liniked to an account
 */
public class CustomerNotLinkedToAccountException extends Exception {

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;


  private Users user;

  
  public CustomerNotLinkedToAccountException(String message) {
	super(message);	// TODO Auto-generated constructor stub
}

/**
   * Default.
   */


  public CustomerNotLinkedToAccountException(Users user) {
	    super(user.getFirstName() + " " + user.getLastName() + " (id=" + user.getUid() + ") is not linked to any account.");
	    this.user = user;
	  }  
  

public Users getUser() {
	return user;
}

public void setUser(Users user) {
	this.user = user;
}




}
