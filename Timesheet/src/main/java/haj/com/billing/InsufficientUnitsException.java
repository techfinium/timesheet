package haj.com.billing;

import haj.com.billing.entity.Accounts;
import haj.com.billing.entity.EventCost;



/**
 * This signal indicates the account has insufficient funds to process the
 * deduct unit request.
 * 
 */
public class InsufficientUnitsException extends Exception {

  private Accounts account;
  private EventCost eventCost;

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;

 
  
  public InsufficientUnitsException(String message) {
	super(message);
}

/**
   * Default.
   */
  public InsufficientUnitsException(Accounts account, EventCost eventCost) {
    super("Account: " + account.getId() + " Balance: " + account.getUnitBalance() + " has insufficient funds to process event code: " + eventCost.getBillingEvent().getId() + ", Cost: " + eventCost.getUnits());
    this.account = account;
    this.eventCost = eventCost;
  }

  /**
   * @return the account
   */
  public Accounts getAccount() {
    return account;
  }

  /**
   * @param account
   *          the account to set
   */
  public void setAccount(Accounts account) {
    this.account = account;
  }

  /**
   * @return the eventCost
   */
  public EventCost getEventCost() {
    return eventCost;
  }

  /**
   * @param eventCost
   *          the eventCost to set
   */
  public void setEventCost(EventCost eventCost) {
    this.eventCost = eventCost;
  }

}
