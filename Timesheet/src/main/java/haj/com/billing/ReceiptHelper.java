package haj.com.billing;


import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import haj.com.constants.HAJConstants;
import haj.com.entity.Users;
import haj.com.service.JasperService;
import haj.com.service.SendMail;


public class ReceiptHelper implements Serializable {

	protected static final Log logger = LogFactory.getLog(ReceiptHelper.class);

	public static byte[] genReceipt(Users u, Long aacountId, Long billingId, Long receiptNo) throws Exception {
		byte[] b = null;
		try {

		
			String sigFname = genReceipt(aacountId, billingId, receiptNo);
			
			emailReceipt(u, receiptNo, sigFname);

			b = FileUtils.readFileToByteArray(new File(sigFname));
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}
		return b;
	}

	/**
	 * Generate receipt without mailing user
	 * @param aacountId
	 * @param billingId
	 * @param receiptNo
	 * @return
	 * @throws Exception
	 */
	public static String genReceipt(Long aacountId, Long billingId, Long receiptNo) throws Exception{
		String sigFname = "";

		try {
			JasperService jasperHelper = new JasperService();
			Map<String,Object> m = new HashMap<String,Object>();
			m.put("f9logo", HAJConstants.APP_PATH +"templates/f9/images/logo.png");  
			m.put("accountId", aacountId);
			m.put("billingId", billingId);
			m.put("receiptNo", receiptNo);
			logger.info("About to generate receipt");
			sigFname = "receipt_"+receiptNo+".pdf";
			jasperHelper.createReportFromDBtoFile("receipt.jasper", m,"receipt",sigFname);
	
		} catch (Exception e) {
			logger.fatal(e);
			throw e;
		}


		return sigFname;
	}

	public static void emailReceipt(Users u, Long receiptNo, String sigFname) throws Exception{		 

		SendMail sm = new SendMail();
		String text = 
				"<p>Dear "+u.getFirstName()+",</p>" +
						"<p>Thank you for your payment</p> "+
						"<p>Please find attached a receipt for your records."+			
						"<p>"+
						"Regards"+
						"<br/>"+
						"The infoFINIUM team"+
						"</p>"+
						"<br/>";
		sm.sendMailWithTemplateWithAttachements(u.getEmail(), "infoFINIUM receipt no: "+receiptNo, text,sigFname);
	
	}


}
