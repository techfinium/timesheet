package haj.com.billing;

import java.io.Serializable;

public class BillingConstants implements Serializable {

	  //Account Transaction Status
	  public static final char ACC_TRANSACTION_SUCCESS = 'S';
	  public static final char ACC_TRANSACTION_PENDING = 'P';
	  public static final char ACC_TRANSACTION_FAILED = 'F';
	  
	  //Event Types
	  public static final char EVENT_TYPE_TOPUP = 'T';
	  public static final char EVENT_TYPE_DEDUCTION = 'D';  
	  
	  //Enable Billing Module
	  public static final boolean BILLING_ENABLED = Boolean.parseBoolean(((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("BILLING_ENABLED").trim());

	  public static final Integer NEGATIVE_THREASHOLD = Integer.parseInt(((java.util.Properties) System.getProperties().get("TK-PROPERTIES")).getProperty("NEGATIVE_THREASHOLD").trim());;

	  
	  public static final Integer EVENT_CODE_TOPUP = 1;
	  public static final Integer EVENT_CODE_DEDUCTION = 2; 
	  
	  public static final Long FREE_UNIT = 8L;
	  public static final Integer DEDUCT_UNIT = 2;
}
