package haj.com.billing;



import java.io.Serializable;

import haj.com.billing.entity.Accounts;

/**
 * This response object will hold information regarding the result of adding the
 * account balance.
 * 
 * 
 */
public class AddUnitsToAccountResponse implements Serializable {

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;

  /** The account will contain the updated balance. */
  private Accounts account = null;

  /**
   * Default.
   */
  public AddUnitsToAccountResponse() {

  }

  /**
   * @return the account
   */
  public Accounts getAccount() {
    return account;
  }

  /**
   * @param account
   *          the account to set
   */
  public void setAccount(Accounts account) {
    this.account = account;
  }


}
