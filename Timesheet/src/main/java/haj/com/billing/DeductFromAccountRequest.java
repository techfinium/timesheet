package haj.com.billing;



import java.io.Serializable;

import haj.com.entity.Users;



/**
 * The request holds information regarding the request to deduct unit(s), or
 * none, from an account, based on the event code and account linked to the
 * document.
 * 
 * @author Hendrik
 * 
 */
public class DeductFromAccountRequest implements Serializable {

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;

  /**
   * Indicates the event code, this typically denotes the current event taking
   * place, and will be used to determine the cost, as cost is based on a given
   * event, and deducted from an account, linked to the document.
   */
  private Integer eventCode = null;
  

  private Users users;

  /** The document will have an account linked. */
 
  /**
   * Default.
   */
  public DeductFromAccountRequest() {

  }

  /**
   * @return the eventCode
   */
  public Integer getEventCode() {
    return eventCode;
  }

  /**
   * @param eventCode
   *          the eventCode to set
   */
  public void setEventCode(Integer eventCode) {
    this.eventCode = eventCode;
  }



public Users getUsers() {
	return users;
}

public void setUsers(Users users) {
	this.users = users;
}




}
