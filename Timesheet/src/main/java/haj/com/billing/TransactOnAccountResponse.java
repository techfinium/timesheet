package haj.com.billing;


import java.io.Serializable;

import haj.com.billing.entity.Accounts;

/**
 * The response object will typicaly indicate success, but will also hold the
 * current account, including the updated balance.
 * 
 */
public class TransactOnAccountResponse implements Serializable {

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;

  /** The account that was updated by the deduct process. */
  private Accounts account = null;

  /**
   * Default.
   */
  public TransactOnAccountResponse() {

  }

  /**
   * @return the account
   */
  public Accounts getAccount() {
    return account;
  }

  /**
   * @param account
   *          the account to set
   */
  public void setAccount(Accounts account) {
    this.account = account;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AccountId: " + account.getId() + " Balance: " + account.getUnitBalance();
  }

}
