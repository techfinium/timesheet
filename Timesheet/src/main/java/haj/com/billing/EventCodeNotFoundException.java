package haj.com.billing;

/**
 * Indicates the eezi-event could not be found via the event code. Also could
 * indicate that there is no event code effective as of today's date.
 * 
 * 
 * 
 */
public class EventCodeNotFoundException extends Exception {

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;

  private Integer eventCode;

  public EventCodeNotFoundException(String msg) { 
	  super(msg);
  }
  /**
   * Default.
   */
  public EventCodeNotFoundException(Integer eventCode) {
    super("The Billing Event code: " + eventCode + " could not be found.");
    this.eventCode = eventCode;
  }

  /**
   * @return the eventCode
   */
  public Integer getEventCode() {
    return eventCode;
  }

  /**
   * @param eventCode
   *          the eventCode to set
   */
  public void setEventCode(Integer eventCode) {
    this.eventCode = eventCode;
  }

}
