package haj.com.billing;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import haj.com.billing.entity.Accounts;
import haj.com.billing.entity.BillingEvents;
import haj.com.billing.entity.EventCost;
import haj.com.framework.AbstractDAO;
import haj.com.framework.AbstractDataProvider;
import haj.com.provider.MySQLProvider;

public class BillingDAO extends AbstractDAO  {

	@Override
	public AbstractDataProvider getDataProvider() {
		return new MySQLProvider();
	}

	public Accounts findAccountById(Long id) throws Exception {
	 	String hql = "select o from Accounts o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (Accounts)super.getUniqueResult(hql, parameters);
	}	
	
	
	public BillingEvents retrieveEventByEventCode(Integer id) throws Exception {
	 	String hql = "select o from BillingEvents o where o.id = :id " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
		return (BillingEvents)super.getUniqueResult(hql, parameters);
	}	
	
	@SuppressWarnings("unchecked")
	public EventCost retrieveActiveEventCostByEventCode(Integer id) throws Exception { 
		// select L from EventCost L left join fetch L.eeziEvents where L.eeziEvents.eventCode = :x 
		// and L.effectiveDate <= :y order by L.effectiveDate DESC
		EventCost ev = null;
		String hql = "select o from EventCost o left join fetch o.billingEvent where o.billingEvent.id = :id "
	 			+ " and o.effectiveDate <= :effectiveDate order by o.effectiveDate DESC " ;
	    Map<String, Object> parameters = new Hashtable<String, Object>();
	    parameters.put("id", id);
	    parameters.put("effectiveDate", new java.util.Date());
	    List<EventCost> list = (List<EventCost>)super.getList(hql, parameters, 1);
	    for (EventCost eventCost : list) {
	    	ev = eventCost;
		}
		return ev;
	}
	

	@SuppressWarnings("unchecked")
	public List<BillingEvents> allBillingEvents() throws Exception {
	 	String hql = "select o from BillingEvents o " ;
		return (List<BillingEvents>)super.getList(hql);
	}
	
	@SuppressWarnings("unchecked")
	public List<EventCost> allEventCost() throws Exception { 
			String hql = "select o from EventCost o " ;
 	    return (List<EventCost>)super.getList(hql);
	}
}
