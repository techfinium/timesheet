package haj.com.billing;


import java.io.Serializable;

import haj.com.billing.entity.Accounts;

/**
 * This request will hold enough information to enable the billing engine to add
 * units to an account, and update it's balance.
 * 
 * 
 */
public class AddUnitsToAccountRequest implements Serializable {

  /**
   * The serial ID.
   */
  private static final long serialVersionUID = 1L;

  /** The account to update. */
  private Accounts account = null;

  /** This is the units to add to the account, this may be negative. */
  private Long unitsToAdd = 0L;

  /**
   * Default.
   */
  public AddUnitsToAccountRequest() {

  }

  /**
   * @return the account
   */
  public Accounts getAccount() {
    return account;
  }

  /**
   * @param account
   *          the account to set
   */
  public void setAccount(Accounts account) {
    this.account = account;
  }

  /**
   * @return the unitsToAdd
   */
  public Long getUnitsToAdd() {
    return unitsToAdd;
  }

  /**
   * @param unitsToAdd
   *          the unitsToAdd to set
   */
  public void setUnitsToAdd(Long unitsToAdd) {
    this.unitsToAdd = unitsToAdd;
  }


 
}
