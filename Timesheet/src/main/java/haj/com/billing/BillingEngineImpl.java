package haj.com.billing;


import java.io.File;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import haj.com.billing.entity.Accounts;
import haj.com.billing.entity.AcctTransactions;
import haj.com.billing.entity.EventCost;
import haj.com.entity.Address;
import haj.com.entity.Users;
import haj.com.utils.GenericUtility;

/**
 * This is the billing engine implementation.
 * 
 * 
 */
public class BillingEngineImpl implements BillingEngine {

	/** A log instance. */
//	private final static Logger log = Logger.getLogger(BillingEngineImpl.class.getName());
	protected final Log log = LogFactory.getLog(BillingEngineImpl.class);
	private static final BillingDAO dao = new BillingDAO();

	private Session session = null;

	/**
	 * @see {@link BillingEngine#transactOnAccount(TransactOnAccountRequest)}
	 */
	@Override
	public TransactOnAccountResponse transactOnAccount(TransactOnAccountRequest request) throws InsufficientUnitsException, EventCodeNotFoundException {
		log.info("transactOnAccount -- START: request:= " + request);

		//Transaction tx = null;
		String recp = "";

		try {

			// This method will always return a response, if a signal was not raised,
			// never return null.
			TransactOnAccountResponse response = new TransactOnAccountResponse();

			// If the document is not linked to an account,
			// this transaction can not take place.
			if (request.getAccount() == null )
				throw new Exception ("Account is not supplied");

			// Because all of this must happen within a managed transaction,
			// else it will create orphan records, transaction management is handled
			// by
			// this method.
			// All db code must roll back if anything from here on fails.
			// Else it will render the biling engine useless on case of error.
			log.info("\ttransactOnAccount: -- Starting new transaction..");
			EventCost eventCostToTransact = null;
            eventCostToTransact = dao.retrieveActiveEventCostByEventCode(request.getEventCode());

			// Refresh the account from DB to transact on.
			Accounts accountToTransactOn = null;

			// Now check the account, first refresh from DB.
			try {
//				if (request.getAccount() == null)
//					accountToTransactOn = (Accounts) session.get(Accounts.class, request.getAccount().getId());
//				else
					accountToTransactOn =  request.getAccount();

				if (accountToTransactOn == null)
					throw new Exception("accountToTransactOn is null");	
			} catch (HibernateException ex) {
				throw new Exception(ex.getMessage());
			}

			long currBalance = accountToTransactOn.getUnitBalance() == null ? 0L : accountToTransactOn.getUnitBalance().longValue();
			if (eventCostToTransact.getUnits() < 0L) {
				accountToTransactOn.setDeductDate(new java.util.Date());
				// Now check if the account can be deducted from.
				// If eventCost unit is a positive value, it will add to the balance.
				if (currBalance < -eventCostToTransact.getUnits().longValue()) {
				  
				  //Store what the balance will be after the cost has been deducted.
				  long temp = currBalance - eventCostToTransact.getUnits().longValue();
				  
				  //First check the Negative Threashold.
				  long nthreas = BillingConstants.NEGATIVE_THREASHOLD;
				  
				  //Threashold must be negative to compare against negative balance
				  if (nthreas > 0) nthreas *= -1;
				  
				  //If the negative threashold is set
				  if (nthreas < 0) {
				    //If the current balance is less than the negative threashold.
				    if (temp < nthreas) throw new InsufficientUnitsException(accountToTransactOn, eventCostToTransact);
				  } else 
				    //There is no negative threashold.
				    throw new InsufficientUnitsException(accountToTransactOn, eventCostToTransact);
					
				}
			}
			// Top Up Transaction
			
			
	
			// Deduct from the account, and create a transaction to indicate such.
			// If eventCost unit is a negative value, it will deduct from the balance,
			// and vice versa.
			       	  
			currBalance += eventCostToTransact.getUnits().longValue(); 
 
			if(eventCostToTransact.getBillingEvent().getEventType().equalsIgnoreCase(""+BillingConstants.EVENT_TYPE_TOPUP) && request.getTransactionStatus() == BillingConstants.ACC_TRANSACTION_SUCCESS){
				if (request.getAccountInv()!=null) currBalance += request.getAccountInv().getNoUnits();
				accountToTransactOn.setUnitBalance(currBalance);
				calcTopUpAndExpireDates(accountToTransactOn,eventCostToTransact);
			}
			else if(!eventCostToTransact.getBillingEvent().getEventType().equalsIgnoreCase(""+BillingConstants.EVENT_TYPE_TOPUP)){
				accountToTransactOn.setUnitBalance(currBalance);
				request.setTransactionStatus(BillingConstants.ACC_TRANSACTION_SUCCESS);
			}
			
			if(request.getBillingAddress() != null){
				
				Address billing = new Address();
				
//				//update billing address
//				if(accountToTransactOn.getBillingAddresses().size() > 0){					
//					billing = accountToTransactOn.getBillingAddresses().iterator().next();
//					billing.setCity(request.getBillingAddress().getCity());
//					billing.setCountry(request.getBillingAddress().getCountry());
//					billing.setPhoneNumber(request.getBillingAddress().getPhoneNumber());
//					billing.setPostalCode(request.getBillingAddress().getPostalCode());
//					billing.setRegionProvince(request.getBillingAddress().getRegionProvince());
//					billing.setState(request.getBillingAddress().getState());
//					billing.setStreet1(request.getBillingAddress().getStreet1());
//					billing.setStreet2(request.getBillingAddress().getStreet2());
//				}
//				else{//save new billing address
					billing = request.getBillingAddress();
				//}
				
				
				
					
				
				//accountToTransactOn.setAddress(address);
			}

			// UPDATE THE ACCOUNT
			dao.update(accountToTransactOn);
			
			checkifMustNotifyUserOfZeroUnits(accountToTransactOn,request.getUser());

			// Create an accounting transaction.
//			AcctTransactions accTrans = createAccountTransaction(session, accountToTransactOn, eventCostToTransact, 
//					  											request.getExternalRef(), request.getExternalResp(), 
//					  											request.getTransactionStatus(),request.getDescription()
//					  											);
			AcctTransactions accTrans = createAccountTransaction( accountToTransactOn, eventCostToTransact, request.getExternalRef(), request.getExternalResp(), request.getTransactionStatus(),request.getDescription());

			//Create Accounting Receipt Info - Used later when generating Receipt for an account
			if(request.getAccountInv() != null && request.getTransactionStatus() == BillingConstants.ACC_TRANSACTION_SUCCESS){
				request.getAccountInv().setAcctTransactions(accTrans);  
				
				dao.update(request.getAccountInv());			
			}  

			// Reload the account from DB, should refresh the account transactions as
			// well.
			// Now check the account, first refresh from DB.
			try {
				accountToTransactOn = dao.findAccountById(accountToTransactOn.getId());
			} catch (HibernateException ex) {
				throw new Exception(ex.getMessage());
			}

			// Commit all work.
			//tx.commit();	
			
			
			//create receipt for user and save it
			if(request.getAccountInv() != null && request.getTransactionStatus() == BillingConstants.ACC_TRANSACTION_SUCCESS){
				try {
					recp = ReceiptHelper.genReceipt(request.getAccount().getId(), request.getAccount().getBillingAddress().getIdaddress(), request.getAccountInv().getReceiptId());
					
					if (request.getEmailUsers()==null) request.setEmailUsers(request.getUser());
					
					request.getAccountInv().setInvoiceBlob(FileUtils.readFileToByteArray(new File(recp)));
					ReceiptHelper.emailReceipt(request.getEmailUsers(), request.getAccountInv().getReceiptId(), recp);
					dao.update(request.getAccountInv()); 
					
				
				} catch (Exception e) {				
					e.printStackTrace();
					log.fatal( "Problem generating/persisting receipt : " + e.getMessage(), e);
				}
			}
			
			//If a receipt was generated email it
			try{
				if(!recp.isEmpty()){
					//ReceiptHelper.emailReceipt(request.getUser(), request.getAccountInv().getInvoiceId(), recp);
				}
				
			}catch(Exception e){
				e.printStackTrace();
				log.fatal( "Problem emailing receipt : " + e.getMessage(), e);
			}

			// Return response with updated account + transactions created.
			response.setAccount(accountToTransactOn);
			log.info("transactOnAccount -- OK: response:= " + response);

			return response;



		} catch (EventCodeNotFoundException iue) {
			log.fatal( "Billing EventCodeNotFoundException: Request: " + request + " " + iue.getMessage(), iue);
			
			throw iue;

		} catch (InsufficientUnitsException iue) {
			log.fatal( "Billing InsufficientUnitsException: Request: " + request + " " + iue.getMessage(), iue);
			
			throw iue;

		} catch (Exception ex) {
			log.fatal( "Billing Exception: Request: " + request + " " + ex.getMessage(), ex);
			
			throw new RuntimeException(ex);
		}
	}

	private void checkifMustNotifyUserOfZeroUnits(Accounts accountToTransactOn,Users user) {
		try {
			if (accountToTransactOn.getUnitBalance().longValue()==0) {
				String msg = "Dear #NAME#" + 
						"<br/>" + "<br/>" + 
						"You have no more units available on your account<br/>" + 
						" <br/>" + 
						"Please logon to the system and update your top-up your account<br/>" + 
						" <br/>" + 
						"If you have any further questions or queries, please do not hesitate to contact me directly or our accounts department via accounts@protectinglives.co.za.<br/>" + 
						" <br/>" + 
						"Regards,"+
						"<br/>"+
						"<p>The Protecting Lives team</p>";
						msg = msg.replaceAll("#NAME#",user.getFirstName().trim());
						GenericUtility.sendMail(user.getEmail(), "Protecting Lives Account update",msg);
			}
		} catch (Exception e) {
			log.fatal(e);
		}
		
	}

	private void calcTopUpAndExpireDates(Accounts accountToTransactOn, EventCost eventCostToTransact) {
		accountToTransactOn.setTopUpDate(new java.util.Date());
		long units = eventCostToTransact.getUnits();
		if (accountToTransactOn.getExpireDate()==null) {
			accountToTransactOn.setExpireDate(GenericUtility.addMonthsToDate(accountToTransactOn.getTopUpDate(), (int)units));
		}
		else {
			accountToTransactOn.setExpireDate(GenericUtility.addMonthsToDate(accountToTransactOn.getExpireDate(), (int)units));
		}
	}

	/**
	 * Attempts to create an account transaction.
	 */
	public AcctTransactions createAccountTransaction(Accounts account, EventCost eventCost, String externalRef, byte[] externalResp, char transactionStatus, String tranDescription) throws Exception {
		log.info("createAccountTransaction -- START: account:= " + account.getId() + ", eventCost: " + eventCost.getId());

		AcctTransactions tx = new AcctTransactions();
		tx.setAccounts(account);
		tx.setEventCost(eventCost);
		tx.setTranDate(new Date());
		if(externalRef != null){ tx.setExternalRef(externalRef); }
		if(externalResp != null){ tx.setExternalResp(externalResp); };     
		tx.setTransactionStatus(""+transactionStatus);
		tx.setTranDescription(tranDescription);
		

		// tx = (AcctTransactions) session.save(tx);
		dao.create(tx);

		log.info("\tcreateAccountTransaction -- acctTrans created.. id: " + tx.getId());
		account.setWsLastUsed(new java.util.Date());
		dao.update(account);
		return tx;
	}

/*
 event_code	event_desc	event_display_name	event_type
	1	ACCOUNT_TOP_UP_10	Top Up 10 Units	T
	2	ACCOUNT_TOP_UP_100	Top Up 100 Units	T
	3	SIGN_DOCUMENT	Sign Document	D
	4	DOCUMENT_COMPETED	Document Completed	D
	5	SMS_SEND	SMS Sent	D
	6	TEMPLATE_CREATE	Template Create	D
 */
}
