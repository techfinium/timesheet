package haj.com.billing;



/**
 * This service is responsible for deducting /adding the correct unit from / to
 * an account, creating accounting transaction(s), and determining the correct
 * unit/cost to deduct.
 * 
 * 
 */
public interface BillingEngine {

  /**
   * This method is responsible for effecting a transaction on an account, and
   * updating the balance, and creating a transaction for it, cost to
   * deduct/add, based on the event code, and will raise signals if unable to
   * deduct due to insufficient units.
   * 
   * @throws InsufficientUnitsException
   *           If there were not enough units to deduct from account.
   * @throws EventCodeNotFoundException
   *           If the requested event code was not found.
   */
  public TransactOnAccountResponse transactOnAccount(TransactOnAccountRequest request) throws InsufficientUnitsException, EventCodeNotFoundException;
  

}
