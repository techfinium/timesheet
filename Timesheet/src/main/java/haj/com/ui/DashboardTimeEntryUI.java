package haj.com.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.PieChartModel;

import haj.com.bean.ReportBean;
import haj.com.entity.BankHolidayChild;
import haj.com.entity.BankHolidayParent;
import haj.com.entity.Projects;
import haj.com.entity.StatementOfWork;
import haj.com.entity.Tasks;
import haj.com.entity.Timesheet;
import haj.com.entity.TimesheetDetails;
import haj.com.framework.AbstractUI;
import haj.com.service.BankHolidayParentService;
import haj.com.service.DashboardService;
import haj.com.service.ProjectsService;
import haj.com.service.ReportService;
import haj.com.service.StatementOfWorkService;
import haj.com.service.TasksService;
import haj.com.service.TimesheetDetailsService;
import haj.com.service.TimesheetService;
import haj.com.service.UserProjectsService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "dashboardTimeEntryUI")
@ViewScoped
public class DashboardTimeEntryUI extends AbstractUI {


	private DashboardService service = new DashboardService();

	private String noClients;
	private String noQuestionaire;
	private String noCompletedQuestionaire;
	private String noInPorgressQuestionaire;
	private TimesheetDetails timesheet;
	private Timesheet selectedTimesheet;
	private List<TimesheetDetails> timesheetList;
	private BarChartModel barModel;
	private PieChartModel pieModel1;
	private List<Projects> projects;
	private List<BarChartModel> barChartModels;
	private TimesheetDetails selectedDate;
	private ProjectsService projectsService;
	private List<Tasks> tasks;
	private Date initialDate;
	private TasksService tasksService;
	private TimesheetService timesheetService = new TimesheetService();
	private TimesheetDetailsService timesheetDetailsService = new TimesheetDetailsService();
	private ScheduleModel eventModel;
	private ScheduleEvent event = new DefaultScheduleEvent();
	private BankHolidayParentService bankHolidayParentService = new BankHolidayParentService();
	private List<Integer> minsList = new ArrayList<>();
	private boolean excludeWeekends;
	private Date fromDate, toDate;
	private Date searchFromDate, searchToDate;
	private ReportService reportsService = new ReportService();
	private ReportBean reportBean;
	private List<ReportBean> reportBeanList;
	private Double totalHours;
	private Long totalMins;
	private Integer totalHoursConverted;
	private String displayMessage;
	private Locale locale;
	private List<Projects> userProjectsList;
	private UserProjectsService userProjectsService = new UserProjectsService();
	
	private StatementOfWorkService statementOfWorkService = new StatementOfWorkService();
	private List<StatementOfWork> statementOfWorkList = new ArrayList<StatementOfWork>();
	private List<StatementOfWork> displayStatementOfWorkList = new ArrayList<StatementOfWork>();
	private Boolean displaySow;
	
	public static final SimpleDateFormat sdfYYYY = new SimpleDateFormat("yyyy");
	
	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		displayMessage = locale.getCountry();
		this.timesheet = new TimesheetDetails();
		populateMinsList();
		initialDate = new Date();
		tasksService = new TasksService();
		projectsService = new ProjectsService();
		searchToDate = GenericUtility.deductDaysFromDate(new Date(), 1);
		searchFromDate = GenericUtility.getFirstDayOfMonth(searchToDate);
		displaySow = false;
		if (!getSessionUI().isAdmin()) {
			fineUserProjects();
			if (getSessionUI().isMember()) {
				eventModel = new DefaultScheduleModel();
				projects = projectsService.allProjectsActive();
				tasks = tasksService.allTasks();
//				getTimesheetDetails();
				getTimesheetDetailsV2();
				
			} else if (getSessionUI().isCustomerAdmin()) {
//				createModelsManager();
			} else if (getSessionUI().isDirector()) {
//				createModelsDirector();
			}
		}
	}
	
	private void populateMinsList() throws Exception {
		minsList = new ArrayList<>();
		minsList.add(00);
		minsList.add(15);
		minsList.add(30);
		minsList.add(45);
	}

	private void fineUserProjects() {
		try {
			userProjectsList = new ArrayList<>();
			Projects select = new Projects();
			select.setCode("Select Project");
			userProjectsList.add(select);
			userProjectsList.addAll(userProjectsService.findProjectsForUser(getSessionUI().getUser()));
		} catch (Exception e) {
			logger.fatal(e);
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void resetDates() {
		excludeWeekends = true;
		toDate = new Date();
		fromDate = GenericUtility.getFirstDayOfWeek(toDate);
	}

	public void getTimesheetDetails() {
		try {
			eventModel = new DefaultScheduleModel();
			int year = Integer.valueOf(DashboardTimeEntryUI.sdfYYYY.format(new Date()));
			BankHolidayParent bh = bankHolidayParentService.findByKey(year);
			if (bh != null) {
				for (BankHolidayChild bhc : bh.getBankHolidayChildrens()) {
					DefaultScheduleEvent defaultScheduleEvent = new DefaultScheduleEvent(bhc.getHolidayNames(), bhc.getBankHoliday(), bhc.getBankHoliday(), true);
					defaultScheduleEvent.setStyleClass("hsPublicHoliday");
					eventModel.addEvent(defaultScheduleEvent);
				}
			}
			List<Timesheet> timesheets = timesheetService.findByCompanyUser(getSessionUI().getCompanyUsers());
			for (Timesheet timesheet : timesheets) {
				int amountDays = GenericUtility.getDaysBetweenDates(timesheet.getFromDate(), timesheet.getToDate());
				for (int i = 0; i <= amountDays; i++) {
					timesheetList = timesheetDetailsService.findByForCompUserTimesheetBetweenDates(timesheet, GenericUtility.addDaysToDate(GenericUtility.getStartOfDay(timesheet.getFromDate()), i), GenericUtility.addDaysToDate(GenericUtility.getEndOfDay(timesheet.getFromDate()), i));
					for (TimesheetDetails timesheetDetail : timesheetList) {
						this.timesheet = timesheetDetail;
						for (TimesheetDetails timesheetDetails : timesheetDetail.getTimesheetDetailList()) {
							DefaultScheduleEvent defaultScheduleEvent = new DefaultScheduleEvent(timesheetDetails.getProjects().getCode() + " " + timesheetDetails.getHours().intValue() + ":" + timesheetDetails.getMinutes(), timesheetDetails.getTimesheetDetails().getFromDateTime(), timesheetDetails.getTimesheetDetails().getToDateTime(), timesheetDetails);
							defaultScheduleEvent.setStyleClass("hsMedium");
							if (timesheetDetails.getAllDayEvent() != null && timesheetDetails.getAllDayEvent()) {
								defaultScheduleEvent.setAllDay(true);
								defaultScheduleEvent.setStyleClass("hsLow");
							}
							eventModel.addEvent(defaultScheduleEvent);
						}
					}
				}
			}
			runClientSideExecute("PF('dlgWait').hide()");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	private ScheduleModel lazyEventModel;
	
	public void getTimesheetDetailsV2(){
		 lazyEventModel = new LazyScheduleModel() {   
            @Override
            public void loadEvents(Date start, Date end) {
            	try {
            		List<TimesheetDetails> usersDetails = timesheetDetailsService.findByTimesheetDetailBetweenDatesAndUser(getSessionUI().getUser().getUid(), GenericUtility.getStartOfDay(start), GenericUtility.getEndOfDay(end));
	        		for (TimesheetDetails timesheetDetails : usersDetails) {
	        			DefaultScheduleEvent defaultScheduleEvent = new DefaultScheduleEvent(timesheetDetails.getProjects().getCode() + " " + timesheetDetails.getHours().intValue() + ":" + timesheetDetails.getMinutes(), timesheetDetails.getTimesheetDetails().getFromDateTime(), timesheetDetails.getTimesheetDetails().getToDateTime(), timesheetDetails);
	        			defaultScheduleEvent.setStyleClass("hsMedium");
	        			if (timesheetDetails.getAllDayEvent() != null && timesheetDetails.getAllDayEvent()) {
	        				defaultScheduleEvent.setAllDay(true);
	        				defaultScheduleEvent.setStyleClass("hsLow");
	        			}
	        			addEvent(defaultScheduleEvent);
	        		}
				} catch (Exception e) {
					logger.fatal(e);
				}
            }
	      };
//		List<TimesheetDetails> usersDetails = timesheetDetailsService.findByTimesheetDetailBetweenDatesAndUser(getSessionUI().getUser().getUid(), GenericUtility.getStartOfDay(GenericUtility.getFirstDayOfMonth(new Date())), GenericUtility.getEndOfDay(GenericUtility.getLasttDayOfMonth(new Date())));
//		for (TimesheetDetails timesheetDetails : usersDetails) {
//			DefaultScheduleEvent defaultScheduleEvent = new DefaultScheduleEvent(timesheetDetails.getProjects().getCode() + " " + timesheetDetails.getHours().intValue() + ":" + timesheetDetails.getMinutes(), timesheetDetails.getTimesheetDetails().getFromDateTime(), timesheetDetails.getTimesheetDetails().getToDateTime(), timesheetDetails);
//			defaultScheduleEvent.setStyleClass("hsMedium");
//			if (timesheetDetails.getAllDayEvent() != null && timesheetDetails.getAllDayEvent()) {
//				defaultScheduleEvent.setAllDay(true);
//				defaultScheduleEvent.setStyleClass("hsLow");
//			}
//			eventModel.addEvent(defaultScheduleEvent);
//		}
		runClientSideExecute("PF('dlgWait').hide()");
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (ScheduleEvent) selectEvent.getObject();
		if (event.getData() instanceof TimesheetDetails) {
			this.timesheet = (TimesheetDetails) event.getData();
			initialDate = timesheet.getTimesheetDetails().getFromDateTime();
			populateSOW();
		}
	}

	public void onDateSelect(SelectEvent selectEvent) {
		try {
			displaySow = false;
			Date selectedDate = (Date) selectEvent.getObject();
			initialDate = selectedDate;
			prepTimesheetDetail(selectedDate);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void onEventMove(ScheduleEntryMoveEvent scheduleEntryMoveEvent) {
		try {
			event = (ScheduleEvent) scheduleEntryMoveEvent.getScheduleEvent();
			int days = scheduleEntryMoveEvent.getDayDelta();
			int min = scheduleEntryMoveEvent.getMinuteDelta();
			if (event.getData() instanceof TimesheetDetails) {
				selectedDate = (TimesheetDetails) event.getData();
				Date newDate = GenericUtility.adjustDate(selectedDate.getTimesheetDetails().getFromDateTime(), days,
						min);
				this.initialDate = newDate;
				prepTimesheetDetail(newDate);
			}
			addInfoMessage("Event has been moved");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void copyEvent() {
		timesheet.setProjects(selectedDate.getProjects());
		timesheet.setComments(selectedDate.getComments());
		timesheet.setTasks(selectedDate.getTasks());
		timesheet.setHours(selectedDate.getHours());
		timesheet.setAllDayEvent(selectedDate.getAllDayEvent());
		timesheet.setMinutes(selectedDate.getMinutes());
		timesheet.setUser(getSessionUI().getUser());
		timesheetInsert();
		
			getTimesheetDetailsV2();
		
		runClientSideExecute("PF('dlgWait').hide()");
	}

	public void moveEvent() {
		try {
			timesheet.setProjects(selectedDate.getProjects());
			timesheet.setComments(selectedDate.getComments());
			timesheet.setTasks(selectedDate.getTasks());
			timesheet.setHours(selectedDate.getHours());
			timesheet.setMinutes(selectedDate.getMinutes());
			timesheet.setAllDayEvent(selectedDate.getAllDayEvent());
			timesheet.setUser(selectedDate.getUser());
			timesheetInsert();
			if (timesheet.getId() != null) {
				timesheetDetailsService.delete(selectedDate);
			}
			runClientSideExecute("PF('dlgWait').hide()");
			getTimesheetDetailsV2();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void bulktimesheetInsert() {
		try {
			informationCheck();
			if (timesheet.getHours() == null) {
				timesheet.setHours(8.0);
			}
			if (timesheet.getHours() > 24) {
				timesheet.setHours(24.0);
			}
			if (timesheet.getMinutes() == null) {
				timesheet.setMinutes(0);
			}

			int days = GenericUtility.getDaysBetweenDates(fromDate, toDate);
			for (int i = 0; i <= days; i++) {
				Date date = GenericUtility.deductDaysFromDate(toDate, i);
				this.timesheet.setTimesheetDetails(prepNew(date));
				this.timesheet.setUser(getSessionUI().getUser());
				if (timesheetDetailsService.calcTot(timesheet.getTimesheetDetails(), timesheet)) {
					if (!timesheetDetailsService.checkAllDay(timesheet.getTimesheetDetails(), timesheet)) {
						if (excludeWeekends) {
							if (!GenericUtility.isSaturdaySunday(date)) {								
								timesheetDetailsService.create(this.timesheet);
							}
						} else {
							timesheetDetailsService.create(this.timesheet);
						}
					}
				}

			}
			// timesheetDetailsService.create(this.timesheet);
			addInfoMessage("Bulk Insert Successful");
			super.runClientSideExecute("PF('dlgBulk').hide()");
			getTimesheetDetailsV2();
			super.runClientSideUpdate("sheduleForm");
			
		} catch (Exception e) {
			super.runClientSideExecute("PF('dlgWait').hide()");
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	private void informationCheck() throws Exception{
		// check to ensure mandatory information provided
		if (timesheet != null) {
			// project assigned
			if (timesheet.getProjects() == null || timesheet.getProjects().getId() == null) {
				throw new Exception("Please Select A Project");
			}
			// task assigned
			if (timesheet.getTasks() == null || timesheet.getTasks().getId() == null) {
				throw new Exception("Please Select A Task");
			}
			// check all day event
			if (!timesheet.getAllDayEvent()) {
				// hours
				if (timesheet.getHours() == null) {
					throw new Exception("Please Input Hours");
				}
			}
			
			
		}
	}

	public void timesheetInsert() {
		try {
			informationCheck();
			if (timesheet.getHours() == null) {
				timesheet.setHours(8.0);
			}
			if (timesheet.getHours() > 24) {
				timesheet.setHours(24.0);
			}
			if (timesheet.getMinutes() == null) {
				timesheet.setMinutes(0);
			}
			if (timesheetDetailsService.calcTot(timesheet.getTimesheetDetails(), timesheet)) {
				if (!timesheetDetailsService.checkAllDay(timesheet.getTimesheetDetails(), timesheet)) {
					this.timesheet.setUser(getSessionUI().getUser());
					timesheetDetailsService.create(this.timesheet);
					super.runClientSideExecute("PF('dlg').hide()");	
					super.addInfoMessage("Insert successful");
					//org.primefaces.context.RequestContext.getCurrentInstance().update("growl");
					PrimeFaces.current().ajax().update("growl");
					getTimesheetDetailsV2();
					super.runClientSideUpdate("sheduleForm");
					super.runClientSideUpdate(":sheduleForm");
				} else {
					super.addErrorMessage("Cant assign time to date with an all day event.");
					super.runClientSideUpdate("sheduleForm");
				}
			} else {
				super.runClientSideUpdate("sheduleForm");
				super.addErrorMessage("Total hours per day cannot exceed 24");
			}
			
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void timesheetUpdate() {
		try {
			informationCheck();
			if (timesheet.getMinutes() == null) {
				timesheet.setMinutes(0);
			}
			if (timesheetDetailsService.calcTot(timesheet.getTimesheetDetails(), timesheet)) {
				if (!timesheetDetailsService.checkAllDay(timesheet.getTimesheetDetails(), timesheet)) {
					if (displayStatementOfWorkList.size() == 0) {
						this.timesheet.setStatementOfWork(null);
					}
					this.timesheet.setUser(getSessionUI().getUser());
					timesheetDetailsService.update(this.timesheet);
					getTimesheetDetailsV2();
					super.runClientSideExecute("PF('dlgUpd').hide()");
					super.addInfoMessage("Update successful");
				} else {
					super.addErrorMessage("Cant assign time to date with an all day event.");
				}
			} else {
				super.addErrorMessage("Total hours per day cannot exceed 24");
			}

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void timesheetdetailsDelete() {
		try {
			timesheetDetailsService.delete(this.timesheet);
			super.addWarningMessage("Delete Successful.");
			getTimesheetDetailsV2();
			super.runClientSideExecute("PF('dlgUpd').hide()");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepTimesheetDetail(Date selectedDate) throws Exception {
		TimesheetDetails timesheetDetail;
		Timesheet timesheet = new Timesheet();
		List<Timesheet> timesheets = timesheetService.findByCompanyUserByDate(getSessionUI().getCompanyUsers(), GenericUtility.getFirstDayOfMonth(selectedDate), GenericUtility.getLasttDayOfMonth(selectedDate));
		if (timesheets.size() == 0) {
			timesheet = new Timesheet(GenericUtility.getFirstDayOfMonth(selectedDate), GenericUtility.getLasttDayOfMonth(selectedDate), getSessionUI().getCompanyUsers());
			timesheetService.create(timesheet);
		} else {
			timesheet = timesheets.get(0);
		}
		List<TimesheetDetails> timesheetDetails = timesheetDetailsService.findByForCompUserTimesheetBetweenDates(timesheet, GenericUtility.getStartOfDay(selectedDate), GenericUtility.getStartOfDay(selectedDate));
		if (timesheetDetails.size() == 0) {
			timesheetDetail = new TimesheetDetails(timesheet, GenericUtility.getStartOfDay(selectedDate), GenericUtility.getEndOfDay(selectedDate));
			timesheetDetail.setCreateDate(new Date());
			timesheetDetail.setWeekNumber(GenericUtility.weekOfMonth(selectedDate));
			timesheetDetailsService.create(timesheetDetail);
		} else {
			timesheetDetail = timesheetDetails.get(0);
		}
		this.timesheet = new TimesheetDetails();
		this.timesheet.setTimesheetDetails(timesheetDetail);
		this.timesheet.setCreateDate(new Date());
		this.timesheet.setAllDayEvent(false);
	}

	private TimesheetDetails prepNew(Date selectedDate) throws Exception {
		TimesheetDetails timesheetDetail;
		Timesheet timesheet = new Timesheet();
		List<Timesheet> timesheets = timesheetService.findByCompanyUserByDate(getSessionUI().getCompanyUsers(),
				GenericUtility.getFirstDayOfMonth(selectedDate), GenericUtility.getLasttDayOfMonth(selectedDate));
		if (timesheets.size() == 0) {
			timesheet = new Timesheet(GenericUtility.getFirstDayOfMonth(selectedDate),
					GenericUtility.getLasttDayOfMonth(selectedDate), getSessionUI().getCompanyUsers());
			timesheetService.create(timesheet);
		} else {
			timesheet = timesheets.get(0);
		}
		List<TimesheetDetails> timesheetDetails = timesheetDetailsService.findByForCompUserTimesheetBetweenDates(
				timesheet, GenericUtility.getStartOfDay(selectedDate), GenericUtility.getStartOfDay(selectedDate));
		if (timesheetDetails.size() == 0) {
			timesheetDetail = new TimesheetDetails(timesheet, GenericUtility.getStartOfDay(selectedDate),
					GenericUtility.getEndOfDay(selectedDate));
			timesheetDetail.setCreateDate(new Date());
			timesheetDetail.setWeekNumber(GenericUtility.weekOfMonth(selectedDate));
			timesheetDetailsService.create(timesheetDetail);
		} else {
			timesheetDetail = timesheetDetails.get(0);
		}
		return timesheetDetail;
	}

	public void prepBulkTimesheetDetail() throws Exception {
		displaySow = false;
		statementOfWorkList = new ArrayList<>();
		this.timesheet = new TimesheetDetails();
		// this.timesheet.setTimesheetDetails();
		this.timesheet.setCreateDate(new Date());
		this.timesheet.setAllDayEvent(false);
		resetDates();
	}
	
	public void generateTotalHoursReport(){
		try {
			reportBean = new ReportBean();
			reportBeanList = new ArrayList<ReportBean>();
			reportBean = reportsService.totalMinutesForProjectForUser(searchFromDate, searchToDate, getSessionUI().getUser());
			reportBeanList = reportsService.projectsFromToDateUserReport(searchFromDate, searchToDate, getSessionUI().getUser());
			totalHours = 0.0;
			totalMins = (long) 0;
			for (ReportBean reportBean : reportBeanList) {
				if (reportBean.getHours() != null) {
					totalHours += reportBean.getHours();
				} else {
					totalHours += 0;
				}
				if (reportBean.getLongOne() != null) {
					totalMins += reportBean.getLongOne();
				} else {
					totalMins += 0;
				}
				
			}
			if (totalMins == null) {
				totalMins = (long) 00;
			}
			if (totalMins >= 60) {
				int m = (int) (totalMins / 60);
				totalHours += m;
				totalMins -= m * 60;
			}

			totalHoursConverted = totalHours.intValue();
			
			if (reportBeanList.size() == 0) {
				addInfoMessage(super.getEntryLanguage("no.results.found"));
			}else {
				addInfoMessage(super.getEntryLanguage("search.completed"));
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		
	}
	
	public void populateSOW(){
		try {
			displayStatementOfWorkList = new ArrayList<>();
			statementOfWorkList = new ArrayList<>();
			Boolean allProjects = false;
			if (getSessionUI().isDirector() || getSessionUI().isAdmin()) {
				allProjects = true;
			}
			statementOfWorkList = statementOfWorkService.findByProjectActive(this.timesheet.getProjects(), getSessionUI().getUser(), allProjects);
			if (statementOfWorkList.size() == 0) {
				displaySow = false;
			} else {
				displaySow = true;
			}
		} catch (Exception e) {
			logger.fatal(e);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public String getTimeZoneString() {
		return java.util.TimeZone.getDefault().getID();
	}

	public String getNoClients() {
		return noClients;
	}

	public void setNoClients(String noClients) {
		this.noClients = noClients;
	}

	public String getNoQuestionaire() {
		return noQuestionaire;
	}

	public void setNoQuestionaire(String noQuestionaire) {
		this.noQuestionaire = noQuestionaire;
	}

	public String getNoCompletedQuestionaire() {
		return noCompletedQuestionaire;
	}

	public void setNoCompletedQuestionaire(String noCompletedQuestionaire) {
		this.noCompletedQuestionaire = noCompletedQuestionaire;
	}

	public String getNoInPorgressQuestionaire() {
		return noInPorgressQuestionaire;
	}

	public void setNoInPorgressQuestionaire(String noInPorgressQuestionaire) {
		this.noInPorgressQuestionaire = noInPorgressQuestionaire;
	}

	public TimesheetDetails getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(TimesheetDetails timesheet) {
		this.timesheet = timesheet;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	public List<Tasks> getTasks() {
		return tasks;
	}

	public void setTasks(List<Tasks> tasks) {
		this.tasks = tasks;
	}

	public List<Projects> getProjects() {
		return projects;
	}

	public void setProjects(List<Projects> projects) {
		this.projects = projects;
	}

	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.pieModel1 = pieModel1;
	}

	public List<BarChartModel> getBarChartModels() {
		return barChartModels;
	}

	public void setBarChartModels(List<BarChartModel> barChartModels) {
		this.barChartModels = barChartModels;
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public Timesheet getSelectedTimesheet() {
		return selectedTimesheet;
	}

	public void setSelectedTimesheet(Timesheet selectedTimesheet) {
		this.selectedTimesheet = selectedTimesheet;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public List<Integer> getMinsList() {
		return minsList;
	}

	public void setMinsList(List<Integer> minsList) {
		this.minsList = minsList;
	}

	public boolean isExcludeWeekends() {
		return excludeWeekends;
	}

	public void setExcludeWeekends(boolean excludeWeekends) {
		this.excludeWeekends = excludeWeekends;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getSearchFromDate() {
		return searchFromDate;
	}

	public void setSearchFromDate(Date searchFromDate) {
		this.searchFromDate = searchFromDate;
	}

	public Date getSearchToDate() {
		return searchToDate;
	}

	public void setSearchToDate(Date searchToDate) {
		this.searchToDate = searchToDate;
	}

	public List<ReportBean> getReportBeanList() {
		return reportBeanList;
	}

	public void setReportBeanList(List<ReportBean> reportBeanList) {
		this.reportBeanList = reportBeanList;
	}

	public Double getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Double totalHours) {
		this.totalHours = totalHours;
	}

	public Long getTotalMins() {
		return totalMins;
	}

	public void setTotalMins(Long totalMins) {
		this.totalMins = totalMins;
	}

	public Integer getTotalHoursConverted() {
		return totalHoursConverted;
	}

	public void setTotalHoursConverted(Integer totalHoursConverted) {
		this.totalHoursConverted = totalHoursConverted;
	}

	public List<Projects> getUserProjectsList() {
		return userProjectsList;
	}

	public void setUserProjectsList(List<Projects> userProjectsList) {
		this.userProjectsList = userProjectsList;
	}


	public List<StatementOfWork> getDisplayStatementOfWorkList() {
		return displayStatementOfWorkList;
	}


	public void setDisplayStatementOfWorkList(List<StatementOfWork> displayStatementOfWorkList) {
		this.displayStatementOfWorkList = displayStatementOfWorkList;
	}


	public Boolean getDisplaySow() {
		return displaySow;
	}


	public void setDisplaySow(Boolean displaySow) {
		this.displaySow = displaySow;
	}

	public List<StatementOfWork> getStatementOfWorkList() {
		return statementOfWorkList;
	}

	public void setStatementOfWorkList(List<StatementOfWork> statementOfWorkList) {
		this.statementOfWorkList = statementOfWorkList;
	}

	public ScheduleModel getLazyEventModel() {
		return lazyEventModel;
	}

	public void setLazyEventModel(ScheduleModel lazyEventModel) {
		this.lazyEventModel = lazyEventModel;
	}

}
