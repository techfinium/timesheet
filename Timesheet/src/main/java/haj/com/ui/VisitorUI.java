package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;

import haj.com.entity.Visitor;
import haj.com.entity.datamodel.VisitorDatamodel;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.VisitorService;

@ManagedBean(name = "visitorUI")
@ViewScoped
public class VisitorUI extends AbstractUI {

	private VisitorService service = new VisitorService();;
	private List<Visitor> visitorList = null;
	private List<Visitor> visitorfilteredList = null;
	private Visitor visitor;
	private LazyDataModel<Visitor> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all Visitor and prepare a for a create
	 * of a new Visitor
	 * 
	 * @author TechFinium
	 * @see Visitor
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		visitorInfo();
	}

	/**
	 * Get all Visitor for data table
	 * 
	 * @author TechFinium
	 * @see Visitor
	 */
	public void visitorInfo() {
		dataModel = new VisitorDatamodel();
	}

	/**
	 * Insert Visitor into database
	 * 
	 * @author TechFinium
	 * @see Visitor
	 */
	public void visitorInsert() {
		try {
			service.create(this.visitor);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			visitorInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update Visitor in database
	 * 
	 * @author TechFinium
	 * @see Visitor
	 */
	public void visitorUpdate() {
		try {
			service.update(this.visitor);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			visitorInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete Visitor from database
	 * 
	 * @author TechFinium
	 * @see Visitor
	 */
	public void visitorDelete() {
		try {
			service.delete(this.visitor);
			prepareNew();
			visitorInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of Visitor
	 * 
	 * @author TechFinium
	 * @see Visitor
	 */
	public void prepareNew() {
		visitor = new Visitor();
	}

	/*
	 * public List<SelectItem> getVisitorDD() { List<SelectItem> l =new
	 * ArrayList<SelectItem>(); l.add(new
	 * SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
	 * visitorInfo(); for (Visitor ug :
	 * visitorList) { // l.add(new SelectItem(ug.getUserGroupId(),
	 * ug.getUserGroupDesc())); } return l; }
	 */

	/**
	 * Complete.
	 *
	 * @param desc the desc
	 * @return the list
	 */
	public List<Visitor> complete(String desc) {
		List<Visitor> l = null;
		try {
			l = service.findByName(desc);
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}


	public List<Visitor> getVisitorList() {
		return visitorList;
	}

	public void setVisitorList(List<Visitor> visitorList) {
		this.visitorList = visitorList;
	}

	public Visitor getVisitor() {
		return visitor;
	}

	public void setVisitor(Visitor visitor) {
		this.visitor = visitor;
	}

	public List<Visitor> getVisitorfilteredList() {
		return visitorfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @param visitorfilteredList the new
	 *                                         visitorfilteredList list
	 * @see Visitor
	 */
	public void setVisitorfilteredList(List<Visitor> visitorfilteredList) {
		this.visitorfilteredList = visitorfilteredList;
	}

	public LazyDataModel<Visitor> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Visitor> dataModel) {
		this.dataModel = dataModel;
	}

}
