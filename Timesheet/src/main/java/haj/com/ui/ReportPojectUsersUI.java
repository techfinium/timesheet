package haj.com.ui;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.swing.ImageIcon;

import org.apache.commons.io.FileUtils;

import haj.com.bean.ReportBean;
import haj.com.constants.HAJConstants;
import haj.com.entity.Projects;
import haj.com.framework.AbstractUI;
import haj.com.service.JasperService;
import haj.com.service.ReportService;

@ManagedBean(name = "reportPojectUsersUI")
@ViewScoped
public class ReportPojectUsersUI extends AbstractUI {

	private ReportService service = new ReportService();
	private ReportBean reportBean;
	private List<ReportBean> reportBeanList;
	private Double totalHours;
	private Long totalMins;
	private Integer totalHoursConverted;
	private JasperService jasperService = new JasperService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void projectsFromToDateDownloadPDF() {
		jasperReportDownloadPDF(getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate(), getSessionUI().getReportBean().getProject());

	}

	private void jasperReportDownloadPDF(Date fromDate, Date toDate, Projects project) {
		Map<String, Object> params = new HashMap<String, Object>();
		String path = HAJConstants.APP_PATH;
		String sub_path = "/resources/hls/images/techLogo.png";
		byte[] buff;
		try {
			buff = FileUtils.readFileToByteArray(new File(path + sub_path));
			params.put("techLogo", (new ImageIcon(buff).getImage()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		java.sql.Date sqlToDate = new java.sql.Date(toDate.getTime());
		java.sql.Date sqlFromDate = new java.sql.Date(fromDate.getTime());
		params.put("fromDateTime", (java.sql.Date) sqlFromDate);
		params.put("toDateTime", (java.sql.Date)  sqlToDate);
		params.put("projectsId", project.getId());
		String fname = project.getCode().replaceAll(" ", "");
		fname += "-users-hours.pdf";
		try {
			jasperService.createReportFromDBtoServletOutputStream("projectUsers.jasper", params, fname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void projectsFromToDateDownloadExcel() {
		jasperReportDownloadExcel(getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate(), getSessionUI().getReportBean().getProject());

	}

	private void jasperReportDownloadExcel(Date fromDate, Date toDate, Projects project) {
		Map<String, Object> params = new HashMap<String, Object>();
		String path = HAJConstants.APP_PATH;
		String sub_path = "/resources/hls/images/techLogo.png";
		byte[] buff;
		try {
			buff = FileUtils.readFileToByteArray(new File(path + sub_path));
			params.put("techLogo", (new ImageIcon(buff).getImage()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		java.sql.Date sqlToDate = new java.sql.Date(toDate.getTime());
		java.sql.Date sqlFromDate = new java.sql.Date(fromDate.getTime());
		params.put("fromDateTime", (java.sql.Date) sqlFromDate);
		params.put("toDateTime", (java.sql.Date)  sqlToDate);
		params.put("projectsId", project.getId());

		try {
			String fname = project.getCode().replaceAll(" ", "");
			fname += "-users-hours.xlsx";
			jasperService.exportToExcel("projectUsers.jasper", params, fname);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void runInit() throws Exception {
		totalHours = 0.0;
		totalMins = (long) 0;
		if (!getSessionUI().isDirector()) {
			reportBean = service.totalMinutesForProject(getSessionUI().getReportBean().getProject(), getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate(), getSessionUI().getCompany());
			reportBeanList = service.projectDetailsReport(getSessionUI().getReportBean().getProject(), getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate(), getSessionUI().getCompany());

			for (ReportBean reportBean : reportBeanList) {
				totalHours += reportBean.getHours();
				totalMins += reportBean.getLongOne();
			}
			if (totalMins == null) {
				totalMins = (long) 00;
			}
			if (totalMins >= 60) {
				int m = (int) (totalMins / 60);
				totalHours += m;
				totalMins -= m * 60;
			}

			totalHoursConverted = totalHours.intValue();

		} else {
			reportBean = service.totalMinutesForProjectDirector(getSessionUI().getReportBean().getProject(), getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate());
			reportBeanList = service.projectDetailsDirector(getSessionUI().getReportBean().getProject(), getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate());

			for (ReportBean reportBean : reportBeanList) {
				totalHours += reportBean.getHours();
				totalMins += reportBean.getLongOne();
			}
			if (totalMins == null) {
				totalMins = (long) 00;
			}
			if (totalMins >= 60) {
				int m = (int) (totalMins / 60);
				totalHours += m;
				totalMins -= m * 60;
			}

			totalHoursConverted = totalHours.intValue();

		}
	}

	public void setUpReportBean() {
		reportBean.setFromDate(getSessionUI().getReportBean().getFromDate());
		reportBean.setToDate(getSessionUI().getReportBean().getToDate());
		reportBean.setProject(getSessionUI().getReportBean().getProject());
		getSessionUI().setReportBean(reportBean);
	}

	public void goToUserReport() {
		setUpReportBean();
		String outcome = "/pages/reports/projectUsersWeek.jsf";
		super.redirect(outcome);

	}

	public void goToUserWeekReport() {
		setUpReportBean();
		String outcome = "/pages/reports/userTimeSheetDetails.jsf";
		super.redirect(outcome);

	}

	public ReportBean getReportBean() {
		return reportBean;
	}

	public void setReportBean(ReportBean reportBean) {
		this.reportBean = reportBean;
	}

	public List<ReportBean> getReportBeanList() {
		return reportBeanList;
	}

	public void setReportBeanList(List<ReportBean> reportBeanList) {
		this.reportBeanList = reportBeanList;
	}

	public Double getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Double totalHours) {
		this.totalHours = totalHours;
	}

	public Long getTotalMins() {
		return totalMins;
	}

	public void setTotalMins(Long totalMins) {
		this.totalMins = totalMins;
	}

	public Integer getTotalHoursConverted() {
		return totalHoursConverted;
	}

	public void setTotalHoursConverted(Integer totalHoursConverted) {
		this.totalHoursConverted = totalHoursConverted;
	}

}
