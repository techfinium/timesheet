package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.ExpenseForm;
import haj.com.framework.AbstractUI;
import haj.com.service.DocService;
import haj.com.service.ExpenseFormService;

@ManagedBean(name = "myExpensesUI")
@ViewScoped
public class MyExpensesUI extends AbstractUI {
	private ExpenseFormService expenseFormService = new ExpenseFormService();
	private List<ExpenseForm> expenseFormList = new ArrayList();
	private ExpenseForm expenseForm = new ExpenseForm();
	private DocService docService = new DocService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		expenseFormList = expenseFormService.findByUser(getSessionUI().getUser());
	}

	public void populateExpenseDocs() {
		try {
			expenseForm.setDocList(docService.searchByTargetKeyAndClass(expenseForm.getClass().getName(), expenseForm.getId()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<ExpenseForm> getExpenseFormList() {
		return expenseFormList;
	}

	public void setExpenseFormList(List<ExpenseForm> expenseFormList) {
		this.expenseFormList = expenseFormList;
	}

	public ExpenseForm getExpenseForm() {
		return expenseForm;
	}

	public void setExpenseForm(ExpenseForm expenseForm) {
		this.expenseForm = expenseForm;
	}
}
