package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.PieChartModel;

import haj.com.constants.HAJConstants;
import haj.com.entity.BankHolidayChild;
import haj.com.entity.BankHolidayParent;
import haj.com.entity.Projects;
import haj.com.entity.StatementOfWork;
import haj.com.entity.Tasks;
import haj.com.entity.Timesheet;
import haj.com.entity.TimesheetDetails;
import haj.com.framework.AbstractUI;
import haj.com.service.BankHolidayParentService;
import haj.com.service.DashboardService;
import haj.com.service.ProjectsService;
import haj.com.service.StatementOfWorkService;
import haj.com.service.TasksService;
import haj.com.service.TimesheetDetailsService;
import haj.com.service.TimesheetService;
import haj.com.service.UserProjectsService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "calendarUI")
@ViewScoped
public class CalendarUI extends AbstractUI {

	private DashboardService service = new DashboardService();

	private String noClients;
	private String noQuestionaire;
	private String noCompletedQuestionaire;
	private String noInPorgressQuestionaire;
	private TimesheetDetails timesheet;
	private Timesheet selectedTimesheet;
	private List<TimesheetDetails> timesheetList;
	private BarChartModel barModel;
	private PieChartModel pieModel1;
	private List<Projects> projects;
	private List<BarChartModel> barChartModels;
	private TimesheetDetails selectedDate;
	private ProjectsService projectsService;
	private List<Tasks> tasks;
	private Date initialDate;
	private TasksService tasksService;
	private TimesheetService timesheetService = new TimesheetService();
	private TimesheetDetailsService timesheetDetailsService = new TimesheetDetailsService();
	private ScheduleModel eventModel;
	private ScheduleEvent event = new DefaultScheduleEvent();
	private BankHolidayParentService bankHolidayParentService = new BankHolidayParentService();
	private List<Integer> minsList = new ArrayList<>();
	private boolean excludeWeekends;
	private Date fromDate, toDate;
	private StatementOfWorkService statementOfWorkService = new StatementOfWorkService();
	private List<StatementOfWork> statementOfWorkList = new ArrayList<StatementOfWork>();
	private List<StatementOfWork> displayStatementOfWorkList = new ArrayList<StatementOfWork>();
	private Boolean displaySow;
	private List<Projects> userProjectsList;
	private UserProjectsService userProjectsService = new UserProjectsService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.timesheet = new TimesheetDetails();
		minsList.add(00);
		minsList.add(15);
		minsList.add(30);
		minsList.add(45);
		initialDate = new Date();
		tasksService = new TasksService();
		projectsService = new ProjectsService();
		eventModel = new DefaultScheduleModel();
		getTimesheetDetails();
		projects = projectsService.allProjects();
		tasks = tasksService.allTasks();
		fineUserProjects();
	}

	public void getTimesheetDetails() {
		try {
			eventModel = new DefaultScheduleModel();
			int year = Integer.valueOf(HAJConstants.sdfYYYY.format(new Date()));

			BankHolidayParent bh = bankHolidayParentService.findByKey(year);
			if (bh != null) {
				for (BankHolidayChild bhc : bh.getBankHolidayChildrens()) {
					DefaultScheduleEvent defaultScheduleEvent = new DefaultScheduleEvent(bhc.getHolidayNames(),
							bhc.getBankHoliday(), bhc.getBankHoliday(), true);
					defaultScheduleEvent.setStyleClass("hsPublicHoliday");
					eventModel.addEvent(defaultScheduleEvent);
				}
			}
			List<Timesheet> timesheets = timesheetService.findByCompanyUserCal(getSessionUI().getCompanyUsers());
			for (Timesheet timesheet : timesheets) {
				int amountDays = GenericUtility.getDaysBetweenDates(timesheet.getFromDate(), timesheet.getToDate());
				for (int i = 0; i <= amountDays; i++) {
					timesheetList = timesheetDetailsService.findByForCompUserTimesheetBetweenDates(timesheet,
							GenericUtility.addDaysToDate(GenericUtility.getStartOfDay(timesheet.getFromDate()), i),
							GenericUtility.addDaysToDate(GenericUtility.getEndOfDay(timesheet.getFromDate()), i));
					for (TimesheetDetails timesheetDetail : timesheetList) {
						this.timesheet = timesheetDetail;
						for (TimesheetDetails timesheetDetails : timesheetDetail.getTimesheetDetailList()) {
							DefaultScheduleEvent defaultScheduleEvent = new DefaultScheduleEvent(
									timesheetDetails.getProjects().getCode() + " "
											+ timesheetDetails.getHours().intValue() + ":"
											+ timesheetDetails.getMinutes(),
									timesheetDetails.getTimesheetDetails().getFromDateTime(),
									timesheetDetails.getTimesheetDetails().getToDateTime(), timesheetDetails);
							defaultScheduleEvent.setStyleClass("hsMedium");
							if (timesheetDetails.getAllDayEvent() != null && timesheetDetails.getAllDayEvent()) {
								defaultScheduleEvent.setAllDay(true);
								defaultScheduleEvent.setStyleClass("hsLow");
							}
							eventModel.addEvent(defaultScheduleEvent);
						}
					}
				}
			}
			runClientSideExecute("PF('dlgWait').hide()");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void onEventSelect(SelectEvent selectEvent) {
		event = (ScheduleEvent) selectEvent.getObject();
		if (event.getData() instanceof TimesheetDetails) {
			this.timesheet = (TimesheetDetails) event.getData();
			initialDate = timesheet.getTimesheetDetails().getFromDateTime();
			populateSOW();
		}
	}

	public void onDateSelect(SelectEvent selectEvent) {
		try {
			displaySow = false;
			Date selectedDate = (Date) selectEvent.getObject();
			initialDate = selectedDate;
			prepTimesheetDetail(selectedDate);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void onEventMove(ScheduleEntryMoveEvent scheduleEntryMoveEvent) {
		try {
			event = (ScheduleEvent) scheduleEntryMoveEvent.getScheduleEvent();
			int days = scheduleEntryMoveEvent.getDayDelta();
			int min = scheduleEntryMoveEvent.getMinuteDelta();
			if (event.getData() instanceof TimesheetDetails) {
				selectedDate = (TimesheetDetails) event.getData();
				Date newDate = GenericUtility.adjustDate(selectedDate.getTimesheetDetails().getFromDateTime(), days,
						min);
				this.initialDate = newDate;
				prepTimesheetDetail(newDate);
			}
			addInfoMessage("Event has been moved");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void copyEvent() {
		timesheet.setProjects(selectedDate.getProjects());
		timesheet.setComments(selectedDate.getComments());
		timesheet.setTasks(selectedDate.getTasks());
		timesheet.setHours(selectedDate.getHours());
		timesheet.setAllDayEvent(selectedDate.getAllDayEvent());
		timesheet.setMinutes(selectedDate.getMinutes());
		if (selectedDate != null && selectedDate.getUser() != null) {
			timesheet.setUser(selectedDate.getUser());
		}		
		
		timesheetInsert();
		if (timesheet.getId() == null) {
			getTimesheetDetails();
		}
		runClientSideExecute("PF('dlgWait').hide()");
	}

	public void bulktimesheetInsert() {
		try {
			if (timesheet.getHours() == null) {
				timesheet.setHours(8.0);
			}
			if (timesheet.getHours() > 24) {
				timesheet.setHours(24.0);
			}
			if (timesheet.getMinutes() == null) {
				timesheet.setMinutes(0);
			}

			int days = GenericUtility.getDaysBetweenDates(fromDate, toDate);
			for (int i = 0; i <= days; i++) {
				Date date = GenericUtility.deductDaysFromDate(toDate, i);
				this.timesheet.setTimesheetDetails(prepNew(date));

				if (timesheetDetailsService.calcTot(timesheet.getTimesheetDetails(), timesheet)) {
					if (!timesheetDetailsService.checkAllDay(timesheet.getTimesheetDetails(), timesheet)) {
						if (excludeWeekends) {
							if (!GenericUtility.isSaturdaySunday(date)) {
								timesheetDetailsService.create(this.timesheet);
							}
						} else {
							timesheetDetailsService.create(this.timesheet);
						}
					}
				}

			}
			// timesheetDetailsService.create(this.timesheet);
			addInfoMessage("Bulk Insert successful");

			super.runClientSideExecute("PF('dlgBulk').hide()");
			super.runClientSideUpdate("sheduleForm");
			getTimesheetDetails();

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	private TimesheetDetails prepNew(Date selectedDate) throws Exception {
		TimesheetDetails timesheetDetail;
		Timesheet timesheet = new Timesheet();
		List<Timesheet> timesheets = timesheetService.findByCompanyUserByDate(getSessionUI().getCompanyUsers(),
				GenericUtility.getFirstDayOfMonth(selectedDate), GenericUtility.getLasttDayOfMonth(selectedDate));
		if (timesheets.size() == 0) {
			timesheet = new Timesheet(GenericUtility.getFirstDayOfMonth(selectedDate),
					GenericUtility.getLasttDayOfMonth(selectedDate), getSessionUI().getCompanyUsers());
			timesheetService.create(timesheet);
		} else {
			timesheet = timesheets.get(0);
		}
		List<TimesheetDetails> timesheetDetails = timesheetDetailsService.findByForCompUserTimesheetBetweenDates(
				timesheet, GenericUtility.getStartOfDay(selectedDate), GenericUtility.getStartOfDay(selectedDate));
		if (timesheetDetails.size() == 0) {
			timesheetDetail = new TimesheetDetails(timesheet, GenericUtility.getStartOfDay(selectedDate),
					GenericUtility.getEndOfDay(selectedDate));
			timesheetDetail.setCreateDate(new Date());
			timesheetDetail.setWeekNumber(GenericUtility.weekOfMonth(selectedDate));
			timesheetDetailsService.create(timesheetDetail);
		} else {
			timesheetDetail = timesheetDetails.get(0);
		}
		return timesheetDetail;
	}

	public void prepBulkTimesheetDetail() throws Exception {
		displaySow = false;
		statementOfWorkList = new ArrayList<>();
		this.timesheet = new TimesheetDetails();
		// this.timesheet.setTimesheetDetails();
		this.timesheet.setCreateDate(new Date());
		this.timesheet.setAllDayEvent(false);
		resetDates();
	}

	private void resetDates() {
		excludeWeekends = true;
		toDate = new Date();
		fromDate = GenericUtility.getFirstDayOfWeek(toDate);
	}

	public void moveEvent() {
		try {
			timesheet.setProjects(selectedDate.getProjects());
			timesheet.setComments(selectedDate.getComments());
			timesheet.setTasks(selectedDate.getTasks());
			timesheet.setHours(selectedDate.getHours());
			timesheet.setMinutes(selectedDate.getMinutes());
			timesheet.setAllDayEvent(selectedDate.getAllDayEvent());
			timesheetInsert();
			if (timesheet.getId() != null) {
				timesheetDetailsService.delete(selectedDate);
			}
			getTimesheetDetails();
			runClientSideExecute("PF('dlgWait').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}


	private void informationCheck() throws Exception{
		// check to ensure mandatory information provided
		if (timesheet != null) {
			// project assigned
			if (timesheet.getProjects() == null || timesheet.getProjects().getId() == null) {
				throw new Exception("Please Select A Project");
			}
			// task assigned
			if (timesheet.getTasks() == null || timesheet.getTasks().getId() == null) {
				throw new Exception("Please Select A Task");
			}
			// check all day event
			if (!timesheet.getAllDayEvent()) {
				// hours
				if (timesheet.getHours() == null) {
					throw new Exception("Please Input Hours");
				}
			}
			
			
		}
	}
	
	public void timesheetInsert() {
		try {
			informationCheck();
			if (timesheet.getHours() == null) {
				timesheet.setHours(8.0);
			}
			if (timesheet.getHours() > 24) {
				timesheet.setHours(24.0);
			}
			if (timesheet.getMinutes() == null) {
				timesheet.setMinutes(0);
			}
			if (timesheetDetailsService.calcTot(timesheet.getTimesheetDetails(), timesheet)) {
				if (!timesheetDetailsService.checkAllDay(timesheet.getTimesheetDetails(), timesheet)) {
					this.timesheet.setUser(getSessionUI().getUser());
					timesheetDetailsService.create(this.timesheet);
					super.runClientSideUpdate("sheduleForm");
					getTimesheetDetails();
					super.runClientSideExecute("PF('dlg').hide()");
					addInfoMessage("Insert successful");

				} else {
					super.addErrorMessage("Cant assign time to date with an all day event.");
					super.runClientSideUpdate("sheduleForm");
				}
			} else {
				addErrorMessage("Total hours per day cannot exceed 24");
				super.runClientSideUpdate("sheduleForm");
				// update="sheduleForm"
			}

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void timesheetUpdate() {
		try {
			informationCheck();
			if (timesheet.getMinutes() == null) {
				timesheet.setMinutes(0);
			}

			if (timesheetDetailsService.calcTot(timesheet.getTimesheetDetails(), timesheet)) {
				if (!timesheetDetailsService.checkAllDay(timesheet.getTimesheetDetails(), timesheet)) {
					if (displayStatementOfWorkList.size() == 0) {
						this.timesheet.setStatementOfWork(null);
					}
					timesheet.setUser(getSessionUI().getUser());
					timesheetDetailsService.update(this.timesheet);
					getTimesheetDetails();
					super.runClientSideExecute("PF('dlgUpd').hide()");
					super.addInfoMessage("Update successful");
				} else {
					addErrorMessage("Cant assign time to date with an all day event.");
					super.runClientSideUpdate("sheduleForm");
				}
			} else {
				addErrorMessage("Total hours per day cannot exceed 24");
				super.runClientSideUpdate("sheduleForm");
			}

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void timesheetdetailsDelete() {
		try {
			timesheetDetailsService.delete(this.timesheet);
			super.addWarningMessage("Row deleted.");
			getTimesheetDetails();
			super.runClientSideExecute("PF('dlgUpd').hide()");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepTimesheetDetail(Date selectedDate) throws Exception {
		TimesheetDetails timesheetDetail;
		Timesheet timesheet = new Timesheet();
		List<Timesheet> timesheets = timesheetService.findByCompanyUserByDate(getSessionUI().getCompanyUsers(),
				GenericUtility.getFirstDayOfMonth(selectedDate), GenericUtility.getLasttDayOfMonth(selectedDate));
		if (timesheets.size() == 0) {
			timesheet = new Timesheet(GenericUtility.getFirstDayOfMonth(selectedDate),
					GenericUtility.getLasttDayOfMonth(selectedDate), getSessionUI().getCompanyUsers());
			timesheetService.create(timesheet);
		} else {
			timesheet = timesheets.get(0);
		}
		List<TimesheetDetails> timesheetDetails = timesheetDetailsService.findByForCompUserTimesheetBetweenDates(
				timesheet, GenericUtility.getStartOfDay(selectedDate), GenericUtility.getStartOfDay(selectedDate));
		if (timesheetDetails.size() == 0) {
			timesheetDetail = new TimesheetDetails(timesheet, GenericUtility.getStartOfDay(selectedDate),
					GenericUtility.getEndOfDay(selectedDate));
			timesheetDetail.setCreateDate(new Date());
			timesheetDetail.setWeekNumber(GenericUtility.weekOfMonth(selectedDate));
			timesheetDetailsService.create(timesheetDetail);
		} else {
			timesheetDetail = timesheetDetails.get(0);
		}
		this.timesheet = new TimesheetDetails();
		this.timesheet.setTimesheetDetails(timesheetDetail);
		this.timesheet.setCreateDate(new Date());
		this.timesheet.setAllDayEvent(false);
	}

	private void fineUserProjects() {
		try {
			userProjectsList = new ArrayList<>();
			Projects select = new Projects();
			select.setCode("Select Project");
			userProjectsList.add(select);
			if (getSessionUI().isDirector() || getSessionUI().isAdmin()) {
				userProjectsList.addAll(projectsService.allProjectsActive());
			} else {
				userProjectsList.addAll(userProjectsService.findProjectsForUser(getSessionUI().getUser()));
			}

		} catch (Exception e) {
			logger.fatal(e);
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void populateSOW() {
		try {
			displayStatementOfWorkList = new ArrayList<>();
			statementOfWorkList = new ArrayList<>();
			Boolean allProjects = false;
			if (getSessionUI().isDirector() || getSessionUI().isAdmin()) {
				allProjects = true;
			}
			statementOfWorkList = statementOfWorkService.findByProjectActive(this.timesheet.getProjects(),getSessionUI().getUser(), allProjects);
			if (statementOfWorkList.size() > 0) {
				displaySow = true;
			} else {
				displaySow = false;
			}
		} catch (Exception e) {
			logger.fatal(e);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public String getTimeZoneString() {
		return java.util.TimeZone.getDefault().getID();
	}

	public String getNoClients() {
		return noClients;
	}

	public void setNoClients(String noClients) {
		this.noClients = noClients;
	}

	public String getNoQuestionaire() {
		return noQuestionaire;
	}

	public void setNoQuestionaire(String noQuestionaire) {
		this.noQuestionaire = noQuestionaire;
	}

	public String getNoCompletedQuestionaire() {
		return noCompletedQuestionaire;
	}

	public void setNoCompletedQuestionaire(String noCompletedQuestionaire) {
		this.noCompletedQuestionaire = noCompletedQuestionaire;
	}

	public String getNoInPorgressQuestionaire() {
		return noInPorgressQuestionaire;
	}

	public void setNoInPorgressQuestionaire(String noInPorgressQuestionaire) {
		this.noInPorgressQuestionaire = noInPorgressQuestionaire;
	}

	public TimesheetDetails getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(TimesheetDetails timesheet) {
		this.timesheet = timesheet;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}

	public List<Tasks> getTasks() {
		return tasks;
	}

	public void setTasks(List<Tasks> tasks) {
		this.tasks = tasks;
	}

	public List<Projects> getProjects() {
		return projects;
	}

	public void setProjects(List<Projects> projects) {
		this.projects = projects;
	}

	public PieChartModel getPieModel1() {
		return pieModel1;
	}

	public void setPieModel1(PieChartModel pieModel1) {
		this.pieModel1 = pieModel1;
	}

	public List<BarChartModel> getBarChartModels() {
		return barChartModels;
	}

	public void setBarChartModels(List<BarChartModel> barChartModels) {
		this.barChartModels = barChartModels;
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public Timesheet getSelectedTimesheet() {
		return selectedTimesheet;
	}

	public void setSelectedTimesheet(Timesheet selectedTimesheet) {
		this.selectedTimesheet = selectedTimesheet;
	}

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public List<Integer> getMinsList() {
		return minsList;
	}

	public void setMinsList(List<Integer> minsList) {
		this.minsList = minsList;
	}

	public boolean isExcludeWeekends() {
		return excludeWeekends;
	}

	public void setExcludeWeekends(boolean excludeWeekends) {
		this.excludeWeekends = excludeWeekends;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<StatementOfWork> getDisplayStatementOfWorkList() {
		return displayStatementOfWorkList;
	}

	public void setDisplayStatementOfWorkList(List<StatementOfWork> displayStatementOfWorkList) {
		this.displayStatementOfWorkList = displayStatementOfWorkList;
	}

	public Boolean getDisplaySow() {
		return displaySow;
	}

	public void setDisplaySow(Boolean displaySow) {
		this.displaySow = displaySow;
	}

	public List<Projects> getUserProjectsList() {
		return userProjectsList;
	}

	public void setUserProjectsList(List<Projects> userProjectsList) {
		this.userProjectsList = userProjectsList;
	}

	public List<StatementOfWork> getStatementOfWorkList() {
		return statementOfWorkList;
	}

	public void setStatementOfWorkList(List<StatementOfWork> statementOfWorkList) {
		this.statementOfWorkList = statementOfWorkList;
	}
}
