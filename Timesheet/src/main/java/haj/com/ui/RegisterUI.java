package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.constants.HAJConstants;
import haj.com.entity.Address;
import haj.com.entity.AddressTypeEnum;
import haj.com.entity.Company;
import haj.com.entity.Users;
import haj.com.entity.UsersStatusEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.RegisterService;

@ManagedBean(name = "registerUI")
@ViewScoped
public class RegisterUI extends AbstractUI {

	private Company company;
	private Users user;
	private String password;
	private Address address;
	private Address postalAddress;
	private boolean copyAddr;
	private Long barcode;
	private RegisterService service = new RegisterService();
	
    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception { 
		this.user = new Users();
		this.company = new Company();
		//this.user.setCompany(new Company());
	
		this.user.setStatus(UsersStatusEnum.EmailNotConfrimed);
		this.user.setAcceptedTAndC(Boolean.TRUE);
		

		this.address = new Address();
		this.address.setPrimaryAddr(Boolean.TRUE);
		this.address.setAddrType(AddressTypeEnum.Billing);
		/*	
		this.postalAddress = new Address();
		this.postalAddress.setPrimaryAddr(Boolean.TRUE);
		this.postalAddress.setAddrType(AddressTypeEnum.Postal);
		
		this.copyAddr = false;
		

		this.user.setEmail("hs241267@gmail.com");
		this.user.setFirstName("Hendrik");
		this.user.setLastName("Strydom");
		this.user.setIdNumber("6712245010081");
		this.user.setCellNumber("0835650429");
		this.user.setAlternativeContactName("Liesl Bowett");
		this.user.setAlternativeContactNumber("0828046090");
		this.address.setAddressLine1("2 Monte Pilciano");
		this.address.setAddressLine2("Jolin Avenue");
		this.address.setAddressLine3("Morehill");
		this.address.setAddressLine4("Benoni");
		this.address.setPostcode("1501");
	*/
		
		
		
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}
	
	
	public void logonWithBarcode() {
		try {
			if (barcode==null) throw new Exception("Enter barcode number!");
			this.user = service.logonBarcode(barcode);
			super.runClientSideExecute("PF('dlg').show()");
		
		}
		 catch (Exception e) {
			 super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void register() {
		try {
			preRegister();
			super.ajaxRedirect("/afterconfirmemail.jsf");
		} catch	(javax.validation.ConstraintViolationException i_id) {
			addErrorMessage("RSA ID number not valid", i_id);
		}
		 catch (Exception e) {
			 super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void registerNewCompany() {
		try {
			getSessionUI().setCompanyUsersList(service.registerCompany(this.company,getSessionUI().getUser(),this.address));
			addInfoMessage("Company registered succesfully");
			
			this.company = new Company();
			this.address = new Address();
			this.address.setPrimaryAddr(Boolean.TRUE);
			this.address.setAddrType(AddressTypeEnum.Billing);
			
		}
		 catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	private void preRegister() throws Exception {
	//	if (this.copyAddr) copyResiToPostal();
		this.user.setPassword(password);
		service.register(this.company,this.user,this.address);
		addInfoMessage("Company and user succesfully registered.");
	}
	
	public void registerMobile() {
		try {
			
			preRegister();
			super.redirect("/mobile/afterconfirmemail.jsf");
		} catch	(javax.validation.ConstraintViolationException i_id) {
			addErrorMessage("RSA ID number not valid", i_id);
		}
		 catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void copyResidentialToPostal() {
		try {
			if (this.copyAddr) copyResiToPostal();
			else {
				this.postalAddress = new Address();
				this.postalAddress.setPrimaryAddr(Boolean.TRUE);
				this.postalAddress.setAddrType(AddressTypeEnum.Postal);
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	private void copyResiToPostal() {
		this.postalAddress.setAddressLine1(this.address.getAddressLine1());
		this.postalAddress.setAddressLine2(this.address.getAddressLine2());
		this.postalAddress.setAddressLine3(this.address.getAddressLine3());
		this.postalAddress.setAddressLine4(this.address.getAddressLine4());
		this.postalAddress.setPostcode(this.address.getPostcode());
	}
	
	public  List<SelectItem> getUserLevelDD() {
		return HAJConstants.userLevelDD;
	}
	public  List<SelectItem> getUserStatusDD() {
		return HAJConstants.userStatusDD;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(Address postalAddress) {
		this.postalAddress = postalAddress;
	}

	public boolean isCopyAddr() {
		return copyAddr;
	}

	public void setCopyAddr(boolean copyAddr) {
		this.copyAddr = copyAddr;
	}

	public Long getBarcode() {
		return barcode;
	}

	public void setBarcode(Long barcode) {
		this.barcode = barcode;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}

