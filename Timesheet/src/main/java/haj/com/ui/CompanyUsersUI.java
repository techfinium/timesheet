package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.constants.HAJConstants;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Users;
import haj.com.framework.AbstractUI;
import haj.com.service.CompanyUsersService;

@ManagedBean(name = "companyusersUI")
@ViewScoped
public class CompanyUsersUI extends AbstractUI {

	private CompanyUsersService service = new CompanyUsersService();
	private List<CompanyUsers> companyusersList = null;
	private List<CompanyUsers> companyusersfilteredList = null;
	private CompanyUsers companyusers = null;
	private Users user;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.service = new CompanyUsersService();
		companyusers = new CompanyUsers();
		companyusers.setUsers(new Users());

		companyusersInfo();
	}

	public void companyusersInfo() {
		try {
			if (getSessionUI().isCustomerAdmin()) {
				companyusersList = service.allCompanyUsersNotAdmin();
				companyusersList.addAll(service.byCompanyExDirector(getSessionUI().getCompany()));
			}else {
				companyusersList = service.allCompanyUsersExAdmin();
			}
	
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void companyusersInfoReport() {
		try {
			companyusersList = service.allCompanyUsersNotAdmin();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void companyusersInsert() {
		try {
			service.create(this.companyusers);
			addInfoMessage("Insert successful");
			companyusersInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void companyusersDelete() {
		try {
			service.delete(this.companyusers);
			companyusersInfo();
			super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<CompanyUsers> getCompanyUsersList() {
		return companyusersList;
	}

	public void setCompanyUsersList(List<CompanyUsers> companyusersList) {
		this.companyusersList = companyusersList;
	}

	public CompanyUsers getCompanyusers() {
		return companyusers;
	}

	public void setCompanyusers(CompanyUsers companyusers) {
		this.companyusers = companyusers;
	}

	public void prepareNewCompanyUsers() {
		companyusers = new CompanyUsers();
	}

	public void companyusersUpdate() {
		try {
			service.update(this.companyusers);
			addInfoMessage("Update successful");
			companyusersInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<SelectItem> getCompanyUsersDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), "-------------Select-------------"));
		companyusersInfo();
		for (CompanyUsers ug : companyusersList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<CompanyUsers> getCompanyUsersfilteredList() {
		return companyusersfilteredList;
	}

	public void setCompanyUsersfilteredList(List<CompanyUsers> companyusersfilteredList) {
		this.companyusersfilteredList = companyusersfilteredList;
	}

	public List<CompanyUsers> complete(String desc) {
		List<CompanyUsers> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<SelectItem> getUserLevelDD() {
		return HAJConstants.userLevelDD;
	}

	public List<SelectItem> getUserStatusDD() {
		return HAJConstants.userStatusDD;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}
}
