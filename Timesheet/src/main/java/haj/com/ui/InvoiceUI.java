package haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.ibm.icu.util.Calendar;

import haj.com.entity.Doc;
import haj.com.entity.EmployeeSOW;
import haj.com.entity.Invoice;
import haj.com.entity.InvoiceHist;
import haj.com.entity.MilestoneStatementOfWork;
import haj.com.entity.Notes;
import haj.com.entity.StatementOfWork;
import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.CurrencyService;
import haj.com.service.EmployeeSOWService;
import haj.com.service.InvoiceService;
import haj.com.service.MilestoneStatementOfWorkService;
import haj.com.service.NotesService;
import haj.com.service.StatementOfWorkService;

@ManagedBean(name = "invoiceUI")
@ViewScoped
public class InvoiceUI extends AbstractUI {

	private List<MilestoneStatementOfWork> milestoneList;
	private List<EmployeeSOW> employeeSowList;
	private List<Invoice> invoicefilteredList;
	private List<Invoice> invoiceList;
	private LazyDataModel<Invoice> invoiceDataModel;
	private List<InvoiceHist> invoiceHistList;

	private Invoice invoice;
	private Doc doc;
	private Notes note;
	private double sowValue;
	private double sowRemainValue;

	private InvoiceService invoiceService = new InvoiceService();
	private EmployeeSOWService employeesowService = new EmployeeSOWService();
	private MilestoneStatementOfWorkService milestonesowService = new MilestoneStatementOfWorkService();
	private StatementOfWorkService statementOfWorkService = new StatementOfWorkService();
	private NotesService notesService = new NotesService();

	@ManagedProperty(value = "#{uploadDocUI}")
	private UploadDocUI uploadDocUI;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepNew();
		prepInvoiceList();
		prepDoc();

	}

	public void findSOWvalue() {
		try {
			sowRemainValue = invoiceService.findBySOWInvoiceTotal(invoice.getStatementOfWork().getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepNew() {
		try {
			invoice = new Invoice();
			invoice.setLocale(CurrencyService.baseLocale);
			invoice.setExhangeRate(1.0);
			invoice.setStatementOfWork(null);
			invoice.setPaid(Boolean.FALSE);
			invoice.setInvoiceDate(new java.util.Date());
			invoice.setCreatedBy(getSessionUI().getUser());
			employeeSowList = new ArrayList<EmployeeSOW>();
			milestoneList = new ArrayList<MilestoneStatementOfWork>();
			invoiceHistList = new ArrayList<InvoiceHist>();
			sowValue = 0.0;
			sowRemainValue = 0.0;
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepSOWSelection() {
		try {
			sowValue = 0.0;
			prepSOWValue();
			if (invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.Fixed) {
				MilestoneStatementOfWork miles = new MilestoneStatementOfWork();
				miles.setMilestoneDescription("---Select Milestone---");
				milestoneList.add(miles);
				milestoneList.addAll(milestonesowService.findSowMilestone(invoice.getStatementOfWork()));
			}
			if (invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.TimeMaterials)
				prepEmployeeSow();
			if ((invoice.getMilestoneStatementOfWork() != null) && (invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.Fixed))
				prepMilestoneSow();
			addInfoMessage("S.O.W " + invoice.getStatementOfWork().getSowNumber() + " has been Selected");
			prepAuditInvoice();
			if (invoiceService.findBySOWB(invoice.getStatementOfWork().getId()))
				findSOWvalue();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepDataModel() {
		invoiceDataModel = new LazyDataModel<Invoice>() {
			private static final long serialVersionUID = 1L;
			private List<Invoice> retorno = new ArrayList<Invoice>();

			@Override
			public List<Invoice> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
				try {
					sortOrder =  SortOrder.DESCENDING;
					sortField = "createDate";
					retorno = invoiceService.allInvoice(Invoice.class, first, pageSize, sortField, sortOrder, filters);
					invoiceDataModel.setRowCount(invoiceService.count(Invoice.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Invoice obj) {
				return obj.getId();
			}

			@Override
			public Invoice getRowData(String rowKey) {
				for (Invoice obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}
		};
	}
	
	
	public void prepInvoiceList(){
		try {
			invoiceList = new ArrayList<Invoice>();
			invoiceList = invoiceService.allInvoice();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void onCountryChange() {
		try {
			invoice.setExhangeRate(CurrencyService.exhangeRateFromToBase(invoice.getLocale()));
			if (this.invoice.getStatementOfWork() != null)
				prepSOWSelection();
			addInfoMessage("Some Values Have Changed!!");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void safeFile(FileUploadEvent event) {
		try {
			uploadDocUI.setDocs(null);
			uploadDocUI.safeFile(event, this.doc, getSessionUI().getUser());
			runInit();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void prepAuditInvoice() {
		try {
			if (this.invoice.getId() != null)
				invoiceHistList = invoiceService.findByKeyHist(this.invoice.getId());
			else
				invoiceHistList = new ArrayList<InvoiceHist>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void notesInsert() {
		try {
			notesService.create(this.note);
			prepNote();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			super.runClientSideExecute("PF('noteDialog').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepDoc() throws Exception {
		this.doc = new Doc();
		this.doc.setUser(getSessionUI().getUser());
		this.doc.setInvoice(invoice);
	}

	public void prepNote() throws Exception {
		this.note = new Notes();
		this.note.setUsers(getSessionUI().getUser());
		this.note.setInvoice(invoice);
	}

	public void prepEmployeeSow() {
		try {
			employeeSowList = new ArrayList<EmployeeSOW>();
			employeeSowList = employeesowService.findBySOW(invoice.getStatementOfWork());
			Double temp;
			Double empChargeRate = 0.0;
			Double employeevalue = 0.0;
			for (EmployeeSOW empsow : employeeSowList) {
				temp = 0.0;
				empChargeRate = empsow.getChargeRate();
				if ((empsow.getStartDate() != null) && (empsow.getEndDate() != null)) {
					if (empsow.getStartDate().equals(empsow.getEndDate())) {
						temp += empChargeRate;
					} else if (empsow.getStartDate().before(empsow.getEndDate())) {
						Calendar start = Calendar.getInstance();
						Calendar end = Calendar.getInstance();
						start.setTime(empsow.getStartDate());
						end.setTime(empsow.getEndDate());
						int workDays = 0;
						while (!start.after(end)) {
							int day = start.get(Calendar.DAY_OF_WEEK);
							if ((day != Calendar.SATURDAY) && (day != Calendar.SUNDAY))
								workDays++;
							start.add(Calendar.DATE, 1);
						}
						temp += empChargeRate * workDays;
					} else if (empsow.getStartDate().after(empsow.getEndDate())) {
						temp += empChargeRate;
						addWarningMessage("End Date is Before Start Date");
					}
				}
				if (this.invoice.getStatementOfWork().getLocale() != this.invoice.getLocale()) {
					employeevalue += (temp * this.invoice.getStatementOfWork().getValueOfSowExhangeRate()) / this.invoice.getExhangeRate();
					sowValue += (temp * this.invoice.getStatementOfWork().getValueOfSowExhangeRate()) / this.invoice.getExhangeRate();
				} else {
					employeevalue += temp;
				}
			}
			this.invoice.setEmployeeValue(employeevalue);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepMilestoneSow() {
		try {
			if (invoice.getMilestoneStatementOfWork().getId() != null) {
				milestoneList = new ArrayList<MilestoneStatementOfWork>();
				milestoneList = milestonesowService.findSowMilestone(invoice.getStatementOfWork());
				double milestonevalue = 0.0;
				if (this.invoice.getStatementOfWork().getLocale() != this.invoice.getLocale())
					milestonevalue = (this.invoice.getMilestoneStatementOfWork().getMilestoneValue() * this.invoice.getStatementOfWork().getValueOfSowExhangeRate()) / this.invoice.getExhangeRate();
				else
					milestonevalue = this.invoice.getMilestoneStatementOfWork().getMilestoneValue();
				this.invoice.setMilestoneValue(milestonevalue);
			} else {
				addWarningMessage("Provide Milestone from Dropdown Menu");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepSOWValue() {
		if (this.invoice.getStatementOfWork().getLocale() != this.invoice.getLocale())
			sowValue += (this.invoice.getStatementOfWork().getValueSow() * this.invoice.getStatementOfWork().getValueOfSowExhangeRate()) / this.invoice.getExhangeRate();
		else
			sowValue += this.invoice.getStatementOfWork().getValueSow();
		this.invoice.setSowValue(sowValue);
	}

	public List<StatementOfWork> searchSOW(String description) {
		List<StatementOfWork> l = null;
		try {
			l = statementOfWorkService.findByNumerOrDescription(description);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public void insertInvoice() {
		try {
			invoiceService.create(invoice);
			addInfoMessage("Invoice Updated");
			prepNew();
			prepInvoiceList();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete Invoice from database
	 * 
	 * @author TechFinium
	 * @see Invoice
	 */
	public void invoiceDelete() {
		try {
			invoiceService.delete(invoice);
			prepNew();
			prepInvoiceList();
			super.addWarningMessage(super.getEntryLanguage("row.deleted "));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<MilestoneStatementOfWork> getMilestoneList() {
		return milestoneList;
	}

	public void setMilestoneList(List<MilestoneStatementOfWork> milestoneList) {
		this.milestoneList = milestoneList;
	}

	public List<EmployeeSOW> getEmployeeSowList() {
		return employeeSowList;
	}

	public void setEmployeeSowList(List<EmployeeSOW> employeeSowList) {
		this.employeeSowList = employeeSowList;
	}

	public List<Invoice> getInvoicefilteredList() {
		return invoicefilteredList;
	}

	public void setInvoicefilteredList(List<Invoice> invoicefilteredList) {
		this.invoicefilteredList = invoicefilteredList;
	}

	public LazyDataModel<Invoice> getInvoiceDataModel() {
		return invoiceDataModel;
	}

	public void setInvoiceDataModel(LazyDataModel<Invoice> invoiceDataModel) {
		this.invoiceDataModel = invoiceDataModel;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public Notes getNote() {
		return note;
	}

	public void setNote(Notes note) {
		this.note = note;
	}

	public double getSowValue() {
		return sowValue;
	}

	public void setSowValue(double sowValue) {
		this.sowValue = sowValue;
	}

	public StatementOfWorkService getStatementOfWorkService() {
		return statementOfWorkService;
	}

	public void setStatementOfWorkService(StatementOfWorkService statementOfWorkService) {
		this.statementOfWorkService = statementOfWorkService;
	}

	public UploadDocUI getUploadDocUI() {
		return uploadDocUI;
	}

	public void setUploadDocUI(UploadDocUI uploadDocUI) {
		this.uploadDocUI = uploadDocUI;
	}

	public List<InvoiceHist> getInvoiceHistList() {
		return invoiceHistList;
	}

	public void setInvoiceHistList(List<InvoiceHist> invoiceHistList) {
		this.invoiceHistList = invoiceHistList;
	}

	public double getSowRemainValue() {
		return sowRemainValue;
	}

	public void setSowRemainValue(double sowRemainValue) {
		this.sowRemainValue = sowRemainValue;
	}

	public List<Invoice> getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List<Invoice> invoiceList) {
		this.invoiceList = invoiceList;
	}

}
