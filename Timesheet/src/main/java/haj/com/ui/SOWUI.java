package haj.com.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.Doc;
import haj.com.entity.EmployeeSOW;
import haj.com.entity.MilestoneStatementOfWork;
import haj.com.entity.MilestoneStatementOfWorkHist;
import haj.com.entity.Notes;
import haj.com.entity.Projects;
import haj.com.entity.StatementOfWork;
import haj.com.entity.StatementOfWorkHist;
import haj.com.entity.Users;
import haj.com.entity.UsersCost;
import haj.com.entity.enums.SowStatusEnum;
import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.CurrencyService;
import haj.com.service.EmployeeSOWService;
import haj.com.service.MilestoneStatementOfWorkService;
import haj.com.service.NotesService;
import haj.com.service.ProjectsService;
import haj.com.service.StatementOfWorkService;
import haj.com.service.UsersCostService;
import haj.com.service.UsersService;

@ManagedBean(name = "sowiUI")
@ViewScoped
public class SOWUI extends AbstractUI {

	private static final long serialVersionUID = -8991602059834997673L;

	// Statement Of Work VARS
	// private List<StatementOfWork> sowList;
	private StatementOfWork sow;
	private LazyDataModel<StatementOfWork> dataModelSow;
	private List<StatementOfWorkHist> sowHistList;
	private List<StatementOfWork> sowFilterList;
	private List<StatementOfWork> sowList;
	private StatementOfWorkHist sowHistSelected;
	private StatementOfWorkService sowService = new StatementOfWorkService();

	// Employee and DualList VARS
	private List<Users> employeesSourceList;
	private List<Users> employeesSourceUserCostList;
	private List<Users> employeesTargetList;
	private UsersService employeeService = new UsersService();
	private DualListModel<Users> employeeDualList;

	// Projects var
	private List<Projects> projectsList;
	private ProjectsService projectsService = new ProjectsService();

	// UsersProjects Service

	// Notes and uploads
	@ManagedProperty(value = "#{uploadDocUI}")
	private UploadDocUI uploadDocUI;
	private Doc doc;
	private Notes note;
	private NotesService notesService = new NotesService();

	// MilestoneSOW VAR
	private MilestoneStatementOfWork milestonesow;
	private List<MilestoneStatementOfWork> milestonesowList;
	private MilestoneStatementOfWorkService milestonesowService = new MilestoneStatementOfWorkService();
	private List<MilestoneStatementOfWorkHist> milestonesowHistList;

	// EmployeeSOW VAR
	private EmployeeSOW employeesow;
	private List<EmployeeSOW> employeesowList;
	private EmployeeSOWService employeesowService = new EmployeeSOWService();

	private List<Users> employeeListNotSelected;
	private List<Users> employeeListSelected;
	private Users employeeTemp;

	private List<SowStatusEnum> SowStatList;

	private Boolean checkForSow = false;

	private UsersCost usercost;
	private List<UsersCost> usercostList;
	private UsersCostService usercostService = new UsersCostService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		projectsList = projectsService.allProjectsActive();
		SowStatList = new ArrayList<SowStatusEnum>();
		SowStatList.add(SowStatusEnum.Active);
		SowStatList.add(SowStatusEnum.Inactive);
		prepSOWNew();
		prepDoc();
		prepNote();
		prepDualListEmployeesNew();
	}

	public void prepMilestoneSOWNew() {
		milestonesow = new MilestoneStatementOfWork();
		milestonesowList = new ArrayList<MilestoneStatementOfWork>();
	}

	public void prepMilestoneSOWUpdate() {
		try {
			milestonesow = new MilestoneStatementOfWork();
			milestonesowList = milestonesowService.findSowMilestoneInvoiceNull(this.sow);
			if (milestonesowList.size() > 0)
				milestonesow = milestonesowList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void prepEmployeeSOWNew() {
		employeesow = new EmployeeSOW();
		employeesowList = new ArrayList<EmployeeSOW>();
	}

	public void prepEmployeeSOWUpdate() {
		try {
			employeesow = new EmployeeSOW();
			employeesowList = employeesowService.findBySOWandInvoiceNull(this.sow);
			if (employeesowList.size() > 0)
				employeesow = employeesowList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void prepDualListEmployeesNew() {
		employeesSourceList = new ArrayList<Users>();
		employeesTargetList = new ArrayList<Users>();
		employeesSourceUserCostList = new ArrayList<Users>();
		employeeDualList = new DualListModel<>(employeesSourceList, employeesTargetList);
	}

	public void prepDualListEmployeesUpdate() {
		try {

			if (this.sow.getProjects() != null) {
				employeesSourceList = employeeService.findByProject(this.sow.getProjects());
			} else {
				employeesSourceList = new ArrayList<Users>();
				addInfoMessage("No Employee Assigned To Project");
			}

			if (employeesowList.size() > 0) {
				for (EmployeeSOW empsow : employeesowList) {
					employeesTargetList.add(empsow.getEmployee());
				}
			} else {
				employeesTargetList = new ArrayList<Users>();
			}

			if (!employeesSourceList.isEmpty() && !employeesTargetList.isEmpty()) {
				for (Users empl : employeesSourceList) {
					if (!usercostService.findByUserB(empl))
						employeesSourceUserCostList.add(empl);
				}
				employeesSourceList.removeAll(employeesTargetList);
				if (!employeesSourceUserCostList.isEmpty())
					employeesSourceList.removeAll(employeesSourceUserCostList);
			}
			employeeDualList = new DualListModel<>(employeesSourceList, employeesTargetList);
			employeesSourceUserCostList = new ArrayList<Users>();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void prepSOWNew() {
		prepSowList();
		sow = new StatementOfWork();
		sow.setLocale(Locale.UK);
		onCountryChange();
		prepAuditHistSow();
		prepDualListEmployeesNew();
		employeeListNotSelected = new ArrayList<Users>();
		employeeListSelected = new ArrayList<Users>();
	}

	public void prepSOWUpdate() {
		prepEmployeeSOWUpdate();
		prepDualListEmployeesUpdate();
		prepAuditHistSow();
		prepEmployeeSOWUpdate();
		prepMilestoneSOWUpdate();
	}

	public void insertUpdateSOW() {
		try {
			if (this.sow.getId() == null) {
				this.sow.setUserSow(getSessionUI().getUser());
				sowService.create(this.sow);
				addInfoMessage("S.O.W Updated");
				if (!employeeDualList.getTarget().isEmpty()) {
					for (Users emptar : employeeDualList.getTarget()) {
						employeesow = new EmployeeSOW();
						employeesow.setEmployee(emptar);
						employeesow.setSow(this.sow);
						employeesow.setUpdatedBy(getSessionUI().getUser());
						employeesow.setStartDate(new java.util.Date());
						employeesow.setEndDate(new java.util.Date());
						employeesow.setChargeRate(0.0);
						Double empChargeRate = 0.0;
						usercost = new UsersCost();
						usercost = usercostService.findByOneUser(employeesow.getEmployee());
						empChargeRate = (usercost.getDayRate() * usercost.getExhangeRate()) / this.sow.getValueOfSowExhangeRate();
						employeesow.setPredictedChargeRate(empChargeRate);
						employeesowService.create(employeesow);
					}
				}
				if (this.sow.getSowTypeEnum() == SowTypeEnum.Fixed && this.sow.getPaymentMilestones() > 0) {
					for (Long i = 1l; i <= this.sow.getPaymentMilestones(); i++) {
						milestonesow = new MilestoneStatementOfWork();
						milestonesow.setSow(this.sow);
						milestonesow.setUpdatedBy(getSessionUI().getUser());
						milestonesow.setMilestoneNum((i));
						milestonesow.setMilestoneDate(new java.util.Date());
						milestonesow.setMilestoneValue(0.0);
						milestonesowService.create(milestonesow);
					}
				}
				prepSOWNew();
			} else if (this.sow.getId() != null) {
				if (!employeeListNotSelected.isEmpty()) {
					for (Users enl : employeeListNotSelected) {
						if (employeesowService.findByUsersAndSOWListB(enl.getUid(), this.sow.getId())) {
							employeesowService.delete(employeesowService.findByUsersAndSOWInvoiceNull(enl, this.sow));
						}
					}
				}
				for (Users emptar : employeeDualList.getTarget()) {
					if (!employeesowService.findByUsersAndSOWListB(emptar.getUid(), this.sow.getId())) {
						employeesow = new EmployeeSOW();
						employeesow.setEmployee(emptar);
						employeesow.setSow(this.sow);
						employeesow.setUpdatedBy(getSessionUI().getUser());
						employeesow.setStartDate(new java.util.Date());
						employeesow.setEndDate(new java.util.Date());
						employeesow.setChargeRate(0.0);
						Double empChargeRate = 0.0;
						usercost = new UsersCost();
						usercost = usercostService.findByOneUser(employeesow.getEmployee());
						empChargeRate = (usercost.getDayRate() * usercost.getExhangeRate()) / this.sow.getValueOfSowExhangeRate();
						employeesow.setPredictedChargeRate(empChargeRate);
						employeesowService.create(employeesow);
					}
				}
				if (this.sow.getSowTypeEnum() == SowTypeEnum.TimeMaterials) {
					if (milestonesowService.findSowMilestoneB(this.sow.getId())) {
						milestonesowList = milestonesowService.findSowMilestoneInvoiceNull(this.sow);
						for (MilestoneStatementOfWork msow : milestonesowList) {
							msow.setUpdatedBy(getSessionUI().getUser());
							milestonesowService.update(msow);
							milestonesowService.delete(msow);
						}
						milestonesowList = milestonesowService.findSowMilestoneInvoiceNull(this.sow);
						this.sow.setPaymentMilestones(0l);
					}
					checkForSow = true;
				}

				if (this.sow.getSowTypeEnum() == SowTypeEnum.Fixed && this.sow.getPaymentMilestones() > 0) {
					Long offset = (long) 0;
					if (milestonesowService.findSowMilestoneB(this.sow.getId())) {
						milestonesowList = milestonesowService.findSowMilestoneInvoiceNull(this.sow);
						if (milestonesowList.size() < this.sow.getPaymentMilestones()) {
							offset = Math.abs(this.sow.getPaymentMilestones() - milestonesowList.size());
						} else if (milestonesowList.size() > this.sow.getPaymentMilestones()) {
							for (MilestoneStatementOfWork msow : milestonesowList) {
								msow.setUpdatedBy(getSessionUI().getUser());
								milestonesowService.update(msow);
								milestonesowService.delete(msow);
							}
							milestonesowList = milestonesowService.findSowMilestoneInvoiceNull(this.sow);
							offset = this.sow.getPaymentMilestones();
						} else {
							offset = 0l;
							checkForSow = true;
						}
					}
					if (offset != 0 || offset < 0) {
						for (Long i = 1l; i <= offset; i++) {
							milestonesow = new MilestoneStatementOfWork();
							milestonesow.setSow(this.sow);
							milestonesow.setUpdatedBy(getSessionUI().getUser());
							if (milestonesowList.isEmpty())
								milestonesow.setMilestoneNum((i));
							else if (milestonesowList.size() < this.sow.getPaymentMilestones())
								milestonesow.setMilestoneNum((i + milestonesowList.size()));
							milestonesow.setMilestoneDate(new java.util.Date());
							milestonesow.setMilestoneValue(0.0);
							milestonesowService.create(milestonesow);
							checkForSow = true;
						}
					}
				}
				if (checkForSow) {
					this.sow.setUserSow(getSessionUI().getUser());
					sowService.create(this.sow);
					addInfoMessage("S.O.W Updated");
					prepSOWNew();
					prepDualListEmployeesNew();
				} else {
					addWarningMessage("S.O.W Could not be updated or created!!");
					addWarningMessage("Employee/s or Milestones Missing!");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void prepSowDataModel() {
		dataModelSow = new LazyDataModel<StatementOfWork>() {

			private static final long serialVersionUID = 1L;
			private List<StatementOfWork> retorno = new ArrayList<StatementOfWork>();

			@Override
			public List<StatementOfWork> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					sortOrder = SortOrder.DESCENDING;
					sortField = "createDate";
					retorno = sowService.allInStatementsOfWork(StatementOfWork.class, first, pageSize, sortField, sortOrder, filters);
					dataModelSow.setRowCount(sowService.count(StatementOfWork.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(StatementOfWork obj) {
				return obj.getId();
			}

			@Override
			public StatementOfWork getRowData(String rowKey) {
				for (StatementOfWork obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};
	}
	
	public void prepSowList(){
		try {
			sowList = new ArrayList<StatementOfWork>();
			sowList = sowService.allStatementOfWork();
		} catch (Exception e) {
			e.printStackTrace();
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void prepAuditHistSow() {
		try {
			sowHistSelected = new StatementOfWorkHist();
			if (this.sow.getId() != null)
				sowHistList = sowService.findByKeyHist(this.sow.getId());
			else
				sowHistList = new ArrayList<StatementOfWorkHist>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onCountryChange() {
		try {
			sow.setValueOfSowExhangeRate(CurrencyService.exhangeRateFromToBase(sow.getLocale()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepDoc() throws Exception {
		this.doc = new Doc();
		this.doc.setUser(getSessionUI().getUser());
		this.doc.setSow(sow);
	}

	public void safeFile(FileUploadEvent event) {
		try {
			uploadDocUI.setDocs(null);
			uploadDocUI.safeFile(event, this.doc, getSessionUI().getUser());
			runInit();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepNote() throws Exception {
		this.note = new Notes();
		this.note.setUsers(getSessionUI().getUser());
		this.note.setSow(sow);
	}

	public void notesInsert() {
		try {
			this.note.setSow(sow);
			notesService.create(this.note);
			prepNote();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			prepSOWNew();
			super.runClientSideExecute("PF('noteDialog').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Projects> searchProjects(String code) {
		List<Projects> l = null;
		try {
			l = projectsService.findLikeCodeName(code);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public void onProjectSelect(SelectEvent event) {
		if (event.getObject() instanceof Projects)
			try {
				if (this.sow.getProjects() != null) {
					if ((employeeService.findByProject(this.sow.getProjects()).size() != 0) || (employeeService.findByProject(this.sow.getProjects()).isEmpty())) {
						employeesSourceList = employeeService.findByProject(this.sow.getProjects());
						for (Users empl : employeesSourceList) {
							if (!usercostService.findByUserB(empl))
								employeesSourceUserCostList.add(empl);
						}
						if (!employeesSourceUserCostList.isEmpty())
							employeesSourceList.removeAll(employeesSourceUserCostList);
						if (!employeesTargetList.isEmpty()) {
							employeesSourceList.removeAll(employeesTargetList);
						}
					} else {
						employeesSourceList = new ArrayList<Users>();
						addInfoMessage(super.getEntryLanguage("no.user.project"));
					}
				}

				employeeDualList = new DualListModel<Users>(employeesSourceList, employeesTargetList);
				employeesSourceUserCostList = new ArrayList<Users>();
			} catch (Exception e) {
				addErrorMessage(e.getMessage(), e);
			}
	}

	public void onTransfer(TransferEvent event) {
		if (event.isAdd()) {
			for (Object item : event.getItems()) {
				employeeTemp = new Users();
				employeeTemp = (Users) item;
				employeeListSelected.add(employeeTemp);
				Iterator<Users> it = employeeListNotSelected.iterator();
				while (it.hasNext()) {
					if (employeeTemp.equals(it.next())) {
						it.remove();

					}
				}
			}
		}

		if (event.isRemove()) {
			for (Object item : event.getItems()) {
				employeeTemp = new Users();
				employeeTemp = (Users) item;
				employeeListNotSelected.add(employeeTemp);
				Iterator<Users> it = employeeListSelected.iterator();
				while (it.hasNext()) {
					if (employeeTemp.equals(it.next())) {
						it.remove();

					}
				}
			}
		}
	}

	public void saveMilestone() {
		try {
			if (this.milestonesow.getId() != 0) {
				milestonesowService.create(this.milestonesow);
				addInfoMessage("Milestone Updated Successfully!");
				prepMilestoneSOWUpdate();
			}
		} catch (Exception e) {
			addWarningMessage("Could not update Milestone");
		}
	}

	public void saveEmployeeSOW() {
		try {
			if (this.employeesow.getId() != 0) {
				employeesowService.create(this.employeesow);
				addInfoMessage("Employee Updated Successfully!");
				prepMilestoneSOWUpdate();
			}
		} catch (Exception e) {
			addWarningMessage("Could not update Employee");
		}
	}

	public boolean filterBySOWNum(Object value, Object filter, Locale locale) {
		try {
			String filterText = (filter == null) ? null : filter.toString().trim();
			if (filterText == null || filterText.equals("")) {
				return true;
			} else {
				sowFilterList = new ArrayList<StatementOfWork>();
				sowFilterList = sowService.findByNumerOrDescription(filterText);
			}
			if (value == null) {
				return false;
			}
			return sowFilterList.size() > 0;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

	/**
	 * Getter and Setters
	 * 
	 * @return
	 */

	public StatementOfWork getSow() {
		return sow;
	}

	public void setSow(StatementOfWork sow) {
		this.sow = sow;
	}

	public LazyDataModel<StatementOfWork> getDataModelSow() {
		return dataModelSow;
	}

	public void setDataModelSow(LazyDataModel<StatementOfWork> dataModelSow) {
		this.dataModelSow = dataModelSow;
	}

	public List<StatementOfWorkHist> getSowHistList() {
		return sowHistList;
	}

	public void setSowHistList(List<StatementOfWorkHist> sowHistList) {
		this.sowHistList = sowHistList;
	}

	public StatementOfWorkHist getSowHistSelected() {
		return sowHistSelected;
	}

	public void setSowHistSelected(StatementOfWorkHist sowHistSelected) {
		this.sowHistSelected = sowHistSelected;
	}

	public List<Users> getEmployeesSourceList() {
		return employeesSourceList;
	}

	public void setEmployeesSourceList(List<Users> employeesSourceList) {
		this.employeesSourceList = employeesSourceList;
	}

	public List<Users> getEmployeesTargetList() {
		return employeesTargetList;
	}

	public void setEmployeesTargetList(List<Users> employeesTargetList) {
		this.employeesTargetList = employeesTargetList;
	}

	public DualListModel<Users> getEmployeeDualList() {
		return employeeDualList;
	}

	public void setEmployeeDualList(DualListModel<Users> employeeDualList) {
		this.employeeDualList = employeeDualList;
	}

	public List<Projects> getProjectsList() {
		return projectsList;
	}

	public void setProjectsList(List<Projects> projectsList) {
		this.projectsList = projectsList;
	}

	public UploadDocUI getUploadDocUI() {
		return uploadDocUI;
	}

	public void setUploadDocUI(UploadDocUI uploadDocUI) {
		this.uploadDocUI = uploadDocUI;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public Notes getNote() {
		return note;
	}

	public void setNote(Notes note) {
		this.note = note;
	}

	public MilestoneStatementOfWork getMilestonesow() {
		return milestonesow;
	}

	public void setMilestonesow(MilestoneStatementOfWork milestonesow) {
		this.milestonesow = milestonesow;
	}

	public List<MilestoneStatementOfWork> getMilestonesowList() {
		return milestonesowList;
	}

	public void setMilestonesowList(List<MilestoneStatementOfWork> milestonesowList) {
		this.milestonesowList = milestonesowList;
	}

	public List<MilestoneStatementOfWorkHist> getMilestonesowHistList() {
		return milestonesowHistList;
	}

	public void setMilestonesowHistList(List<MilestoneStatementOfWorkHist> milestonesowHistList) {
		this.milestonesowHistList = milestonesowHistList;
	}

	public EmployeeSOW getEmployeesow() {
		return employeesow;
	}

	public void setEmployeesow(EmployeeSOW employeesow) {
		this.employeesow = employeesow;
	}

	public List<EmployeeSOW> getEmployeesowList() {
		return employeesowList;
	}

	public void setEmployeesowList(List<EmployeeSOW> employeesowList) {
		this.employeesowList = employeesowList;
	}

	public List<SowStatusEnum> getSowStatList() {
		return SowStatList;
	}

	public void setSowStatList(List<SowStatusEnum> sowStatList) {
		SowStatList = sowStatList;
	}

	public Boolean getCheckForSow() {
		return checkForSow;
	}

	public void setCheckForSow(Boolean checkForSow) {
		this.checkForSow = checkForSow;
	}

	public UsersCost getUsercost() {
		return usercost;
	}

	public void setUsercost(UsersCost usercost) {
		this.usercost = usercost;
	}

	public List<UsersCost> getUsercostList() {
		return usercostList;
	}

	public void setUsercostList(List<UsersCost> usercostList) {
		this.usercostList = usercostList;
	}

	public List<StatementOfWork> getSowFilterList() {
		return sowFilterList;
	}

	public void setSowFilterList(List<StatementOfWork> sowFilterList) {
		this.sowFilterList = sowFilterList;
	}

	public List<Users> getEmployeesSourceUserCostList() {
		return employeesSourceUserCostList;
	}

	public void setEmployeesSourceUserCostList(List<Users> employeesSourceUserCostList) {
		this.employeesSourceUserCostList = employeesSourceUserCostList;
	}

	public List<StatementOfWork> getSowList() {
		return sowList;
	}

	public void setSowList(List<StatementOfWork> sowList) {
		this.sowList = sowList;
	}
}
// super.getEntryLanguage("update.fail") new java.util.Date()
