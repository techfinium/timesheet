package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Users;
import haj.com.entity.UsersLevelEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.RegisterService;
import haj.com.service.UsersService;

@ManagedBean(name = "usersUI")
@ViewScoped
public class UsersUI extends AbstractUI {

	private UsersService service = new UsersService();
	private List<Users> usersList = null;
	private List<Users> usersfilteredList = null;
	private Users users = null;
	private UsersLevelEnum userLevel;
	private RegisterService registerService = new RegisterService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
	}

}
