package haj.com.ui;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import haj.com.bean.ReportBean;
import haj.com.constants.HAJConstants;
import haj.com.entity.Company;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Doc;
import haj.com.entity.HostingCompany;
import haj.com.entity.Timesheet;
import haj.com.entity.Users;
import haj.com.service.UsersService;

@ManagedBean(name = "sessionUI")
@SessionScoped
/**
 * This is the only object that should ever exist in session, anything required
 * on the session object should be defined as Serializable and access modifiers
 * to instance given here.
 * 
 *
 */
public class SessionUI implements Serializable {
	private static final long serialVersionUID = 1L;
	private Users user;
	private boolean admin;
	private boolean customerAdmin;
	private boolean member;
	private boolean director;
	private boolean showMenu = false;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm");
	private String lastLogin;
	private Boolean iAmAClient;
	private Company company;
	private CompanyUsers companyUsers;
	private boolean applyRoles;
	private List<CompanyUsers> companyUsersList;
	private List<Users> administrators;
	private String duid;
	private UsersService usersService = new UsersService();
	private Timesheet timesheet;
	private ReportBean reportBean;
	private Doc selDoc;
	private String filename;
	private HostingCompany hostingCompany;

	/**
	 * Should be executed before log off and after login, reset bean to default
	 * values.
	 */
	public void clear() {

	}

	/**
	 * This is vital for audit logging, any new items added to the SessionUI
	 * that may be required in the audit logging should be included into the map
	 * below.
	 * 
	 * @return
	 */
	public Map<String, Object> getMap() {
		Map<String, Object> map = new HashMap<String, Object>();

		// logged in user
		Long userId = null;
		if (user != null)
			userId = user.getUid();
		map.put("userId", userId);

		return map;
	}
	
	public void setHostingCompany(HostingCompany hostingCompany) {
		this.hostingCompany = hostingCompany;
	}


	@Override
	public String toString() {
		return getMap().toString();
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isShowMenu() {
		return showMenu;
	}

	public void setShowMenu(boolean showMenu) {
		this.showMenu = showMenu;
	}

	public String getLastLogin() {
		if (user != null && user.getLastLogin() != null) {
			lastLogin = sdf.format(user.getLastLogin());
		}
		return lastLogin;
	}

	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}

	public boolean isMember() {
		return member;
	}

	public void setMember(boolean member) {
		this.member = member;
	}

	public boolean isCustomerAdmin() {
		return customerAdmin;
	}

	public void setCustomerAdmin(boolean customerAdmin) {
		this.customerAdmin = customerAdmin;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
		doAdministratorCode();
	}

	private void doAdministratorCode() {
		try {
			this.administrators = usersService.getAllCompanyAdministrors(company);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public CompanyUsers getCompanyUsers() {
		return companyUsers;
	}

	public void setCompanyUsers(CompanyUsers companyUsers) {
		this.companyUsers = companyUsers;
	}

	public boolean isApplyRoles() {
		return applyRoles;
	}

	public void setApplyRoles(boolean applyRoles) {
		this.applyRoles = applyRoles;
	}

	public List<CompanyUsers> getCompanyUsersList() {
		return companyUsersList;
	}

	public void setCompanyUsersList(List<CompanyUsers> companyUsersList) {
		this.companyUsersList = companyUsersList;
	}

	public List<Users> getAdministrators() {
		return administrators;
	}

	public void setAdministrators(List<Users> administrators) {
		this.administrators = administrators;
	}

	public String getDuid() {
		return duid;
	}

	public void setDuid(String duid) {
		this.duid = duid;
	}

	public Boolean getiAmAClient() {
		return iAmAClient;
	}

	public void setiAmAClient(Boolean iAmAClient) {
		this.iAmAClient = iAmAClient;
	}

	public Timesheet getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(Timesheet timesheet) {
		this.timesheet = timesheet;
	}

	public ReportBean getReportBean() {
		return reportBean;
	}

	public void setReportBean(ReportBean reportBean) {
		this.reportBean = reportBean;
	}

	public boolean isDirector() {
		return director;
	}

	public void setDirector(boolean director) {
		this.director = director;
	}

	public String getFilename() {
		//#{sessionUI.reportBean.user.users.firstName}#{sessionUI.reportBean.user.users.lastName}_#{sessionUI.reportBean.fromDate}_to_#{sessionUI.reportBean.toDate}
		if (reportBean!=null && reportBean.getUser()!=null && reportBean.getUser().getUsers()!=null && reportBean.getFromDate() !=null && reportBean.getToDate()!=null) {
			filename = reportBean.getUser().getUsers().getFirstName().replaceAll(" ", "_");
			filename += reportBean.getUser().getUsers().getLastName().replaceAll(" ", "_");
			filename += "_from_"+HAJConstants.sdf.format(reportBean.getFromDate()) + "_to_"+HAJConstants.sdf.format(reportBean.getToDate());
		}
		else {
			filename = "file";
		}
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Doc getSelDoc() {
		return selDoc;
	}

	public void setSelDoc(Doc selDoc) {
		this.selDoc = selDoc;
	}

	public SimpleDateFormat getSdf() {
		return sdf;
	}

	public void setSdf(SimpleDateFormat sdf) {
		this.sdf = sdf;
	}

	public HostingCompany getHostingCompany() {
		return hostingCompany;
	}

}
