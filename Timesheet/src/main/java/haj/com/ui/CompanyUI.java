package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.Company;
import haj.com.framework.AbstractUI;
import haj.com.service.CompanyService;

@ManagedBean(name = "companyUI")
@ViewScoped
public class CompanyUI extends AbstractUI {

	private CompanyService service = new CompanyService();
	private List<Company> companyList = null;
	private List<Company> companyfilteredList = null;
	private Company company = null;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.service = new CompanyService();
		prepareNewCompany();
		companyInfo();
	}

	public void companyInfo() {
		try {
			companyList = service.allCompany();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void companyInsert() {
		try {
			company.setCreateDate(new Date());
			service.create(this.company);
			addInfoMessage("Insert successful");
			companyInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void companyDelete() {
		try {
			service.delete(this.company);
			companyInfo();
			super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void prepareNewCompany() {
		company = new Company();
	}

	public void companyUpdate() {
		try {
			service.update(this.company);
			addInfoMessage("Update successful");
			companyInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<SelectItem> getCompanyDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), "-------------Select-------------"));
		companyInfo();
		for (Company ug : companyList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<Company> getCompanyfilteredList() {
		return companyfilteredList;
	}

	public void setCompanyfilteredList(List<Company> companyfilteredList) {
		this.companyfilteredList = companyfilteredList;
	}

	public List<Company> complete(String desc) {
		List<Company> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
}
