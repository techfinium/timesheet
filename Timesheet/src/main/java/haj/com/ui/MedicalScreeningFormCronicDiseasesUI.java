package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;

import haj.com.entity.MedicalScreeningFormCronicDiseases;
import haj.com.entity.datamodel.MedicalScreeningFormCronicDiseasesDatamodel;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.MedicalScreeningFormCronicDiseasesService;

@ManagedBean(name = "medicalScreeningFormCronicDiseasesUI")
@ViewScoped
public class MedicalScreeningFormCronicDiseasesUI extends AbstractUI {

	private MedicalScreeningFormCronicDiseasesService service = new MedicalScreeningFormCronicDiseasesService();;
	private List<MedicalScreeningFormCronicDiseases> medicalScreeningFormCronicDiseasesList = null;
	private List<MedicalScreeningFormCronicDiseases> medicalScreeningFormCronicDiseasesfilteredList = null;
	private MedicalScreeningFormCronicDiseases medicalScreeningFormCronicDiseases;
	private LazyDataModel<MedicalScreeningFormCronicDiseases> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all MedicalScreeningFormCronicDiseases and prepare a for a create
	 * of a new MedicalScreeningFormCronicDiseases
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningFormCronicDiseases
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		medicalScreeningFormCronicDiseasesInfo();
	}

	/**
	 * Get all MedicalScreeningFormCronicDiseases for data table
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningFormCronicDiseases
	 */
	public void medicalScreeningFormCronicDiseasesInfo() {
		dataModel = new MedicalScreeningFormCronicDiseasesDatamodel();
	}

	/**
	 * Insert MedicalScreeningFormCronicDiseases into database
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningFormCronicDiseases
	 */
	public void medicalScreeningFormCronicDiseasesInsert() {
		try {
			service.create(this.medicalScreeningFormCronicDiseases);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			medicalScreeningFormCronicDiseasesInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update MedicalScreeningFormCronicDiseases in database
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningFormCronicDiseases
	 */
	public void medicalScreeningFormCronicDiseasesUpdate() {
		try {
			service.update(this.medicalScreeningFormCronicDiseases);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			medicalScreeningFormCronicDiseasesInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete MedicalScreeningFormCronicDiseases from database
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningFormCronicDiseases
	 */
	public void medicalScreeningFormCronicDiseasesDelete() {
		try {
			service.delete(this.medicalScreeningFormCronicDiseases);
			prepareNew();
			medicalScreeningFormCronicDiseasesInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of MedicalScreeningFormCronicDiseases
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningFormCronicDiseases
	 */
	public void prepareNew() {
		medicalScreeningFormCronicDiseases = new MedicalScreeningFormCronicDiseases();
	}

	/*
	 * public List<SelectItem> getMedicalScreeningFormCronicDiseasesDD() { List<SelectItem> l =new
	 * ArrayList<SelectItem>(); l.add(new
	 * SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
	 * medicalScreeningFormCronicDiseasesInfo(); for (MedicalScreeningFormCronicDiseases ug :
	 * medicalScreeningFormCronicDiseasesList) { // l.add(new SelectItem(ug.getUserGroupId(),
	 * ug.getUserGroupDesc())); } return l; }
	 */

	/**
	 * Complete.
	 *
	 * @param desc the desc
	 * @return the list
	 */
	public List<MedicalScreeningFormCronicDiseases> complete(String desc) {
		List<MedicalScreeningFormCronicDiseases> l = null;
		try {
			l = service.findByName(desc);
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}


	public List<MedicalScreeningFormCronicDiseases> getMedicalScreeningFormCronicDiseasesList() {
		return medicalScreeningFormCronicDiseasesList;
	}

	public void setMedicalScreeningFormCronicDiseasesList(List<MedicalScreeningFormCronicDiseases> medicalScreeningFormCronicDiseasesList) {
		this.medicalScreeningFormCronicDiseasesList = medicalScreeningFormCronicDiseasesList;
	}

	public MedicalScreeningFormCronicDiseases getMedicalScreeningFormCronicDiseases() {
		return medicalScreeningFormCronicDiseases;
	}

	public void setMedicalScreeningFormCronicDiseases(MedicalScreeningFormCronicDiseases medicalScreeningFormCronicDiseases) {
		this.medicalScreeningFormCronicDiseases = medicalScreeningFormCronicDiseases;
	}

	public List<MedicalScreeningFormCronicDiseases> getMedicalScreeningFormCronicDiseasesfilteredList() {
		return medicalScreeningFormCronicDiseasesfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @param medicalScreeningFormCronicDiseasesfilteredList the new
	 *                                         medicalScreeningFormCronicDiseasesfilteredList list
	 * @see MedicalScreeningFormCronicDiseases
	 */
	public void setMedicalScreeningFormCronicDiseasesfilteredList(List<MedicalScreeningFormCronicDiseases> medicalScreeningFormCronicDiseasesfilteredList) {
		this.medicalScreeningFormCronicDiseasesfilteredList = medicalScreeningFormCronicDiseasesfilteredList;
	}

	public LazyDataModel<MedicalScreeningFormCronicDiseases> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<MedicalScreeningFormCronicDiseases> dataModel) {
		this.dataModel = dataModel;
	}

}
