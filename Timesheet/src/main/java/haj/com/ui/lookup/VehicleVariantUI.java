package haj.com.ui.lookup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.lookup.VehicleVariant;
import haj.com.framework.AbstractUI;
import haj.com.service.lookup.VehicleVariantService;

@ManagedBean(name = "vehiclevariantUI")
@ViewScoped
public class VehicleVariantUI extends AbstractUI {

	private VehicleVariantService service = new VehicleVariantService();

	private List<VehicleVariant> vehiclevariantList = null;
	private List<VehicleVariant> vehiclevariantfilteredList = null;
	private VehicleVariant vehiclevariant = null;
	private LazyDataModel<VehicleVariant> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all VehicleVariant and prepare a for a create of a
	 * new VehicleVariant
	 * 
	 * @author TechFinium
	 * @see VehicleVariant
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		vehiclevariantInfo();
	}

	/**
	 * Get all VehicleVariant for data table
	 * 
	 * @author TechFinium
	 * @see VehicleVariant
	 */
	public void vehiclevariantInfo() {
		dataModel = new LazyDataModel<VehicleVariant>() {

			private static final long serialVersionUID = 1L;
			private List<VehicleVariant> retorno = new ArrayList<VehicleVariant>();
			private VehicleVariantService service;

			@Override
			public List<VehicleVariant> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allVehicleVariant(VehicleVariant.class, first, pageSize, sortField, sortOrder, filters);
					setRowCount(service.count(VehicleVariant.class, filters));
				} catch (Exception e) {
					e.printStackTrace();
				}
				return retorno;
			}

			@Override
			public Object getRowKey(VehicleVariant obj) {
				return obj.getId();
			}

			@Override
			public VehicleVariant getRowData(String rowKey) {
				for (VehicleVariant obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}
		};
	}

	/**
	 * Insert VehicleVariant into database
	 * 
	 * @author TechFinium
	 * @see VehicleVariant
	 */
	public void vehiclevariantInsert() {
		try {
			service.create(this.vehiclevariant);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			vehiclevariantInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update VehicleVariant in database
	 * 
	 * @author TechFinium
	 * @see VehicleVariant
	 */
	public void vehiclevariantUpdate() {
		try {
			service.update(this.vehiclevariant);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			vehiclevariantInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete VehicleVariant from database
	 * 
	 * @author TechFinium
	 * @see VehicleVariant
	 */
	public void vehiclevariantDelete() {
		try {
			service.delete(this.vehiclevariant);
			prepareNew();
			vehiclevariantInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of VehicleVariant
	 * 
	 * @author TechFinium
	 * @see VehicleVariant
	 */
	public void prepareNew() {
		vehiclevariant = new VehicleVariant();
	}

	/*
	 * public List<SelectItem> getVehicleVariantDD() { List<SelectItem> l =new
	 * ArrayList<SelectItem>(); l.add(new
	 * SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
	 * vehiclevariantInfo(); for (VehicleVariant ug : vehiclevariantList) { //
	 * l.add(new SelectItem(ug.getUserGroupId(), ug.getUserGroupDesc())); } return
	 * l; }
	 */

	/**
	 * Complete.
	 *
	 * @param desc the desc
	 * @return the list
	 */
	public List<VehicleVariant> complete(String desc) {
		List<VehicleVariant> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<String> completeVehicleMake(String desc) {
		List<String> l = null;
		try {
			l = service.findByMake(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}




	public List<VehicleVariant> getVehicleVariantList() {
		return vehiclevariantList;
	}

	public void setVehicleVariantList(List<VehicleVariant> vehiclevariantList) {
		this.vehiclevariantList = vehiclevariantList;
	}

	public VehicleVariant getVehiclevariant() {
		return vehiclevariant;
	}

	public void setVehiclevariant(VehicleVariant vehiclevariant) {
		this.vehiclevariant = vehiclevariant;
	}

	public List<VehicleVariant> getVehicleVariantfilteredList() {
		return vehiclevariantfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @param vehiclevariantfilteredList the new vehiclevariantfilteredList list
	 * @see VehicleVariant
	 */
	public void setVehicleVariantfilteredList(List<VehicleVariant> vehiclevariantfilteredList) {
		this.vehiclevariantfilteredList = vehiclevariantfilteredList;
	}

	public LazyDataModel<VehicleVariant> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<VehicleVariant> dataModel) {
		this.dataModel = dataModel;
	}

	public List<VehicleVariant> getVehiclevariantList() {
		return vehiclevariantList;
	}

	public void setVehiclevariantList(List<VehicleVariant> vehiclevariantList) {
		this.vehiclevariantList = vehiclevariantList;
	}

	public List<VehicleVariant> getVehiclevariantfilteredList() {
		return vehiclevariantfilteredList;
	}

	public void setVehiclevariantfilteredList(List<VehicleVariant> vehiclevariantfilteredList) {
		this.vehiclevariantfilteredList = vehiclevariantfilteredList;
	}

}
