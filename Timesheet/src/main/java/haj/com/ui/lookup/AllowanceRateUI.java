package haj.com.ui.lookup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.lookup.AllowanceRate;
import haj.com.framework.AbstractUI;
import haj.com.service.lookup.AllowanceRateService;

@ManagedBean(name = "allowancerateUI")
@ViewScoped
public class AllowanceRateUI extends AbstractUI {

	private AllowanceRateService service = new AllowanceRateService();
	private List<AllowanceRate> allowanceRateList = null;
	private List<AllowanceRate> allowanceRatefilteredList = null;
	private AllowanceRate allowancerate = null;
	private LazyDataModel<AllowanceRate> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all AllowanceRate and prepare a for a create of a
	 * new AllowanceRate
	 * 
	 * @author TechFinium
	 * @see AllowanceRate
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		//allowanceRateInfo();
		allowanceRateInfo2();
	}

	public void allowanceRateInfo2() {
		try {
			allowanceRateList = service.allAllowanceRate();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Get all AllowanceRate for data table
	 * 
	 * @author TechFinium
	 * @see AllowanceRate
	 */
	public void allowanceRateInfo() {
		dataModel = new LazyDataModel<AllowanceRate>() {

			private static final long serialVersionUID = 1L;
			private List<AllowanceRate> retorno = new ArrayList<AllowanceRate>();
		

			@Override
			public List<AllowanceRate> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allAllowanceRate(AllowanceRate.class, first, pageSize, sortField, sortOrder, filters);
					setRowCount(service.count(AllowanceRate.class, filters));
				} catch (Exception e) {
					e.printStackTrace();
				}
				return retorno;
			}

			@Override
			public Object getRowKey(AllowanceRate obj) {
				return obj.getId();
			}

			@Override
			public AllowanceRate getRowData(String rowKey) {
				for (AllowanceRate obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}
		};
	}

	/**
	 * Insert AllowanceRate into database
	 * 
	 * @author TechFinium
	 * @see AllowanceRate
	 */
	public void allowanceRateInsert() {
		try {
			service.create(this.allowancerate);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			allowanceRateInfo2();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update AllowanceRate in database
	 * 
	 * @author TechFinium
	 * @see AllowanceRate
	 */
	public void allowanceRateUpdate() {
		try {
			service.update(this.allowancerate);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			allowanceRateInfo2();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete AllowanceRate from database
	 * 
	 * @author TechFinium
	 * @see AllowanceRate
	 */
	public void allowanceRateDelete() {
		try {
			service.delete(this.allowancerate);
			prepareNew();
			allowanceRateInfo2();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	

	/**
	 * Create new instance of AllowanceRate
	 * 
	 * @author TechFinium
	 * @see AllowanceRate
	 */
	public void prepareNew() {
		allowancerate = new AllowanceRate();
	}

	public List<AllowanceRate> getAllowanceRateList() {
		return allowanceRateList;
	}

	public void setAllowanceRateList(List<AllowanceRate> allowanceRateList) {
		this.allowanceRateList = allowanceRateList;
	}

	public List<AllowanceRate> getAllowanceRatefilteredList() {
		return allowanceRatefilteredList;
	}

	public void setAllowanceRatefilteredList(List<AllowanceRate> allowanceRatefilteredList) {
		this.allowanceRatefilteredList = allowanceRatefilteredList;
	}

	public LazyDataModel<AllowanceRate> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<AllowanceRate> dataModel) {
		this.dataModel = dataModel;
	}

	public AllowanceRate getAllowancerate() {
		return allowancerate;
	}

	public void setAllowancerate(AllowanceRate allowancerate) {
		this.allowancerate = allowancerate;
	}






}
