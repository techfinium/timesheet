package haj.com.ui.lookup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.lookup.DailyRate;
import haj.com.framework.AbstractUI;
import haj.com.service.lookup.DailyRateService;

@ManagedBean(name = "dailyrateUI")
@ViewScoped
public class DailyRateUI extends AbstractUI {

	private DailyRateService service = new DailyRateService();

	private List<DailyRate> dailyRateList = null;
	private List<DailyRate> dailyRatefilteredList = null;
	private DailyRate dailyrate = null;
	private LazyDataModel<DailyRate> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all DailyRate and prepare a for a create of a
	 * new DailyRate
	 * 
	 * @author TechFinium
	 * @see DailyRate
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		//dailyRateInfo();
		dailyRateInfo2();
	}

	/**
	 * Get all DailyRate for data table
	 * 
	 * @author TechFinium
	 * @see DailyRate
	 */
	
	public void dailyRateInfo2() {
		try {
			dailyRateList = service.allDailyRate();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void dailyRateInfo() {
		dataModel = new LazyDataModel<DailyRate>() {

			private static final long serialVersionUID = 1L;
			private List<DailyRate> retorno = new ArrayList<DailyRate>();
		

			@Override
			public List<DailyRate> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allDailyRate(DailyRate.class, first, pageSize, sortField, sortOrder, filters);
					setRowCount(service.count(DailyRate.class, filters));
				} catch (Exception e) {
					e.printStackTrace();
				}
				return retorno;
			}

			@Override
			public Object getRowKey(DailyRate obj) {
				return obj.getId();
			}

			@Override
			public DailyRate getRowData(String rowKey) {
				for (DailyRate obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}
		};
	}

	/**
	 * Insert DailyRate into database
	 * 
	 * @author TechFinium
	 * @see DailyRate
	 */
	public void dailyRateInsert() {
		try {
			service.create(this.dailyrate);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			dailyRateInfo2();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update DailyRate in database
	 * 
	 * @author TechFinium
	 * @see DailyRate
	 */
	public void dailyRateUpdate() {
		try {
			service.update(this.dailyrate);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			dailyRateInfo2();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete DailyRate from database
	 * 
	 * @author TechFinium
	 * @see DailyRate
	 */
	public void dailyRateDelete() {
		try {
			service.delete(this.dailyrate);
			prepareNew();
			dailyRateInfo2();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of DailyRate
	 * 
	 * @author TechFinium
	 * @see DailyRate
	 */
	public void prepareNew() {
		dailyrate = new DailyRate();
	}

	public List<DailyRate> getDailyRateList() {
		return dailyRateList;
	}

	public void setDailyRateList(List<DailyRate> dailyRateList) {
		this.dailyRateList = dailyRateList;
	}

	public List<DailyRate> getDailyRatefilteredList() {
		return dailyRatefilteredList;
	}

	public void setDailyRatefilteredList(List<DailyRate> dailyRatefilteredList) {
		this.dailyRatefilteredList = dailyRatefilteredList;
	}

	public LazyDataModel<DailyRate> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<DailyRate> dataModel) {
		this.dataModel = dataModel;
	}

	public DailyRate getDailyrate() {
		return dailyrate;
	}

	public void setDailyrate(DailyRate dailyrate) {
		this.dailyrate = dailyrate;
	}






}
