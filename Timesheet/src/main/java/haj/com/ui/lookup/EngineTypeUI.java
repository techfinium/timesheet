package haj.com.ui.lookup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.lookup.EngineType;
import haj.com.framework.AbstractUI;
import haj.com.service.lookup.EngineTypeService;

@ManagedBean(name = "enginetypeUI")
@ViewScoped
public class EngineTypeUI extends AbstractUI {

	private EngineTypeService service = new EngineTypeService();

	private List<EngineType> engineTypeList = null;
	private List<EngineType> engineTypefilteredList = null;
	private EngineType enginetype = null;
	private LazyDataModel<EngineType> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all EngineType and prepare a for a create of a
	 * new EngineType
	 * 
	 * @author TechFinium
	 * @see EngineType
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		//engineTypeInfo();
		engineTypeInfo2();
	}

	/**
	 * Get all EngineType for data table
	 * 
	 * @author TechFinium
	 * @see EngineType
	 */
	
	public void engineTypeInfo2() {
		try {
			engineTypeList = service.allEngineType();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	
	public void engineTypeInfo() {
		dataModel = new LazyDataModel<EngineType>() {

			private static final long serialVersionUID = 1L;
			private List<EngineType> retorno = new ArrayList<EngineType>();
		

			@Override
			public List<EngineType> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allEngineType(EngineType.class, first, pageSize, sortField, sortOrder, filters);
					setRowCount(service.count(EngineType.class, filters));
				} catch (Exception e) {
					e.printStackTrace();
				}
				return retorno;
			}

			@Override
			public Object getRowKey(EngineType obj) {
				return obj.getId();
			}

			@Override
			public EngineType getRowData(String rowKey) {
				for (EngineType obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}
		};
	}

	/**
	 * Insert EngineType into database
	 * 
	 * @author TechFinium
	 * @see EngineType
	 */
	public void engineTypeInsert() {
		try {
			service.create(this.enginetype);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			engineTypeInfo2();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update EngineType in database
	 * 
	 * @author TechFinium
	 * @see EngineType
	 */
	public void engineTypeUpdate() {
		try {
			service.update(this.enginetype);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			engineTypeInfo2();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete EngineType from database
	 * 
	 * @author TechFinium
	 * @see EngineType
	 */
	public void engineTypeDelete() {
		try {
			service.delete(this.enginetype);
			prepareNew();
			engineTypeInfo2();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of EngineType
	 * 
	 * @author TechFinium
	 * @see EngineType
	 */
	public void prepareNew() {
		enginetype = new EngineType();
	}

	public List<EngineType> getEngineTypeList() {
		return engineTypeList;
	}

	public void setEngineTypeList(List<EngineType> engineTypeList) {
		this.engineTypeList = engineTypeList;
	}

	public List<EngineType> getEngineTypefilteredList() {
		return engineTypefilteredList;
	}

	public void setEngineTypefilteredList(List<EngineType> engineTypefilteredList) {
		this.engineTypefilteredList = engineTypefilteredList;
	}

	public EngineType getEnginetype() {
		return enginetype;
	}

	public void setEnginetype(EngineType enginetype) {
		this.enginetype = enginetype;
	}

	public LazyDataModel<EngineType> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<EngineType> dataModel) {
		this.dataModel = dataModel;
	}






}
