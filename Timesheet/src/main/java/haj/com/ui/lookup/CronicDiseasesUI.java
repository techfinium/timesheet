package  haj.com.ui.lookup;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

import org.primefaces.model.LazyDataModel;

import haj.com.entity.datamodel.CronicDiseasesDatamodel;
import haj.com.entity.lookup.CronicDiseases;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.lookup.CronicDiseasesService;

@ManagedBean(name = "cronicDiseasesUI")
@ViewScoped
public class CronicDiseasesUI extends AbstractUI {
	
	private CronicDiseasesService service =  new CronicDiseasesService();
	private List<CronicDiseases> cronicDiseasesList = null;
	private List<CronicDiseases> cronicDiseasesfilteredList = null;
	private CronicDiseases cronicDiseases = null;
	private LazyDataModel<CronicDiseases> dataModel;

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all CronicDiseases and prepare a for a create of a new CronicDiseases
 	 * @author TechFinium
 	 * @see    CronicDiseases
	 * @throws Exception the exception
 	 */
	private void runInit() throws Exception {
		prepareNew();
		cronicDiseasesInfo();
	}

	/**
	 * Get all CronicDiseases for data table
 	 * @author TechFinium
 	 * @see    CronicDiseases
 	 */
	public void cronicDiseasesInfo() {
			dataModel = new CronicDiseasesDatamodel();
	}

	/**
	 * Insert CronicDiseases into database
 	 * @author TechFinium
 	 * @see    CronicDiseases
 	 */
	public void cronicDiseasesInsert() {
		try {
				 service.create(this.cronicDiseases);
				 prepareNew();
				 addInfoMessage(super.getEntryLanguage("update.successful"));
				 cronicDiseasesInfo();
			 } catch (ValidationException e) {
				addErrorMessage(getEntryLanguage(e.getMessage()));
			} catch (Exception e) {
		 	   super.addCallBackParm("validationFailed", true);
				addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Update CronicDiseases in database
 	 * @author TechFinium
 	 * @see    CronicDiseases
 	 */
    public void cronicDiseasesUpdate() {
		try {
				 service.update(this.cronicDiseases);
				  addInfoMessage(super.getEntryLanguage("update.successful"));
				 cronicDiseasesInfo();
			 } catch (ValidationException e) {
				addErrorMessage(getEntryLanguage(e.getMessage()));
			} catch (Exception e) {
			   super.addCallBackParm("validationFailed", true);
				addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Delete CronicDiseases from database
 	 * @author TechFinium
 	 * @see    CronicDiseases
 	 */
	public void cronicDiseasesDelete() {
		try {
			 service.delete(this.cronicDiseases);
			  prepareNew();
			 cronicDiseasesInfo();
			 super.addWarningMessage(super.getEntryLanguage("row.deleted"));
			 } catch (ValidationException e) {
				addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of CronicDiseases
 	 * @author TechFinium
 	 * @see    CronicDiseases
 	 */
	public void prepareNew() {
		cronicDiseases = new CronicDiseases();
	}

/*
    public List<SelectItem> getCronicDiseasesDD() {
    	List<SelectItem> l =new ArrayList<SelectItem>();
    	l.add(new SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
    	cronicDiseasesInfo();
    	for (CronicDiseases ug : cronicDiseasesList) {
    		// l.add(new SelectItem(ug.getUserGroupId(), ug.getUserGroupDesc()));
		}
    	return l;
    }
  */

    /**
     * Complete.
     *
     * @param desc the desc
     * @return the list
     */
    public List<CronicDiseases> complete(String desc) {
		List<CronicDiseases> l = null;
		try {
			l = service.findByName(desc);
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		}
		catch (Exception e) {
		addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

    public List<CronicDiseases> getCronicDiseasesList() {
		return cronicDiseasesList;
	}
	public void setCronicDiseasesList(List<CronicDiseases> cronicDiseasesList) {
		this.cronicDiseasesList = cronicDiseasesList;
	}
	public CronicDiseases getCronicDiseases() {
		return cronicDiseases;
	}
	public void setCronicDiseases(CronicDiseases cronicDiseases) {
		this.cronicDiseases = cronicDiseases;
	}

    public List<CronicDiseases> getCronicDiseasesfilteredList() {
		return cronicDiseasesfilteredList;
	}

	/**
	 * Prepare auto complete data
 	 * @author TechFinium
 	 * @param cronicDiseasesfilteredList the new cronicDiseasesfilteredList list
 	 * @see    CronicDiseases
 	 */
	public void setCronicDiseasesfilteredList(List<CronicDiseases> cronicDiseasesfilteredList) {
		this.cronicDiseasesfilteredList = cronicDiseasesfilteredList;
	}


	public LazyDataModel<CronicDiseases> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<CronicDiseases> dataModel) {
		this.dataModel = dataModel;
	}

}
