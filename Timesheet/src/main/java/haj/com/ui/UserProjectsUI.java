package  haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.UserProjects;
import haj.com.framework.AbstractUI;
import haj.com.service.UserProjectsService;

@ManagedBean(name = "userprojectsUI")
@ViewScoped
public class UserProjectsUI extends AbstractUI {

	private UserProjectsService service = new UserProjectsService();
	private List<UserProjects> userprojectsList = null;
	private List<UserProjects> userprojectsfilteredList = null;
	private UserProjects userprojects = null;
	private LazyDataModel<UserProjects> dataModel; 

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all UserProjects and prepare a for a create of a new UserProjects
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 * @throws Exception
 	 */
	private void runInit() throws Exception {
		prepareNew();
		userprojectsInfo();
	}

	/**
	 * Get all UserProjects for data table
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 */
	public void userprojectsInfo() {
	 
			
			 dataModel = new LazyDataModel<UserProjects>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<UserProjects> retorno = new  ArrayList<UserProjects>();
			   
			   @Override 
			   public List<UserProjects> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					retorno = service.allUserProjects(UserProjects.class,first,pageSize,sortField,sortOrder,filters);
					dataModel.setRowCount(service.count(UserProjects.class,filters));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return retorno; 
			   }
			   
			    @Override
			    public Object getRowKey(UserProjects obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public UserProjects getRowData(String rowKey) {
			        for(UserProjects obj : retorno) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
			
	
	}

	/**
	 * Insert UserProjects into database
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 */
	public void userprojectsInsert() {
		try {
			 service.create(this.userprojects);
			 prepareNew();
			  addInfoMessage("Update successful");
			 userprojectsInfo();
		} catch (Exception e) {
		    super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Delete UserProjects from database
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 */
	public void userprojectsDelete() {
		try {
			 service.delete(this.userprojects);
			  prepareNew();
			 userprojectsInfo();
			 super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<UserProjects> getUserProjectsList() {
		return userprojectsList;
	}
	public void setUserProjectsList(List<UserProjects> userprojectsList) {
		this.userprojectsList = userprojectsList;
	}
	public UserProjects getUserprojects() {
		return userprojects;
	}
	public void setUserprojects(UserProjects userprojects) {
		this.userprojects = userprojects;
	}


	/**
	 * Create new instance of UserProjects 
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 */
	public void prepareNew() {
		userprojects = new UserProjects();
	}


	/**
	 * Update UserProjects in database
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 */
    public void userprojectsUpdate() {
		try {
			 service.update(this.userprojects);
			 addInfoMessage("Update successful");
			 userprojectsInfo();
		} catch (Exception e) {
		   super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Prepare select one menu data
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 */	
    public List<SelectItem> getUserProjectsDD() {
    	List<SelectItem> l =new ArrayList<SelectItem>();
    	l.add(new SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
    	userprojectsInfo();
    	for (UserProjects ug : userprojectsList) {
    		// l.add(new SelectItem(ug.getUserGroupId(), ug.getUserGroupDesc()));
		}
    	return l;
    }

    public List<UserProjects> getUserProjectsfilteredList() {
		return userprojectsfilteredList;
	}
	
	/**
	 * Prepare auto complete data
 	 * @author TechFinium 
 	 * @see    UserProjects
 	 */	
	public void setUserProjectsfilteredList(List<UserProjects> userprojectsfilteredList) {
		this.userprojectsfilteredList = userprojectsfilteredList;
	}

		public List<UserProjects> complete(String desc) {
		List<UserProjects> l = null;
		try {
			l = service.findByName(desc);
		}
		catch (Exception e) {
		addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
	
	
	public LazyDataModel<UserProjects> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<UserProjects> dataModel) {
		this.dataModel = dataModel;
	}
	
}
