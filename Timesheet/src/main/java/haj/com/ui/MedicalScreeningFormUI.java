package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;

import haj.com.entity.MedicalScreeningForm;
import haj.com.entity.MedicalScreeningFormCronicDiseases;
import haj.com.entity.datamodel.MedicalScreeningFormDatamodel;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.MedicalScreeningFormService;

@ManagedBean(name = "medicalScreeningFormUI")
@ViewScoped
public class MedicalScreeningFormUI extends AbstractUI {

	private MedicalScreeningFormService service = new MedicalScreeningFormService();
	private List<MedicalScreeningForm> medicalScreeningFormList = null;
	private List<MedicalScreeningForm> medicalScreeningFormfilteredList = null;
	private MedicalScreeningForm medicalScreeningForm;
	private MedicalScreeningFormCronicDiseases medicalScreeningFormCronicDiseases;
	private LazyDataModel<MedicalScreeningForm> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all MedicalScreeningForm and prepare a for a create
	 * of a new MedicalScreeningForm
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningForm
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		medicalScreeningFormInfo();
		if (dataModel == null) {
			prepareNew();
		} else {
			prepareLatestForm();
		}

	}

	/**
	 * Get all MedicalScreeningForm for data table
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningForm
	 */
	public void medicalScreeningFormInfo() {
		dataModel = new MedicalScreeningFormDatamodel();
	}

	/**
	 * Insert MedicalScreeningForm into database
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningForm
	 */
	public void medicalScreeningFormInsert() {
		try {
			service.createMedicalScreeningForm(this.medicalScreeningForm);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			medicalScreeningFormInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update MedicalScreeningForm in database
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningForm
	 */
	public void medicalScreeningFormUpdate() {
		try {
			service.update(this.medicalScreeningForm);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			medicalScreeningFormInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete MedicalScreeningForm from database
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningForm
	 */
	public void medicalScreeningFormDelete() {
		try {
			service.deleteMedicalSecreeningForm(this.medicalScreeningForm);
			prepareNew();
			medicalScreeningFormInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of MedicalScreeningForm
	 * 
	 * @author TechFinium
	 * @see MedicalScreeningForm
	 */
	public void prepareNew() {
		medicalScreeningForm = new MedicalScreeningForm();
		medicalScreeningForm.setUser(getSessionUI().getUser());
		medicalScreeningForm.getUser().setStillEmployed(true);
	}

	public void prepareLatestForm() throws Exception {
		medicalScreeningForm = service.findLatestFormByUser(getSessionUI().getUser());
		if (medicalScreeningForm == null) {
			prepareNew();
		} else {
			populateCronicDiseases();
			medicalScreeningForm.setId(null);
			medicalScreeningForm.setTemperature(null);
			medicalScreeningForm.setEmployeeSignature(false);
		}
	}

	public void populateCronicDiseases() throws Exception {
		service.populateCronicDiseases(this.medicalScreeningForm);
	}

	/**
	 * Complete.
	 *
	 * @param desc the desc
	 * @return the list
	 */
	public List<MedicalScreeningForm> complete(String desc) {
		List<MedicalScreeningForm> l = null;
		try {
			l = service.findByName(desc);
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<MedicalScreeningForm> getMedicalScreeningFormList() {
		return medicalScreeningFormList;
	}

	public void setMedicalScreeningFormList(List<MedicalScreeningForm> medicalScreeningFormList) {
		this.medicalScreeningFormList = medicalScreeningFormList;
	}

	public MedicalScreeningForm getMedicalScreeningForm() {
		return medicalScreeningForm;
	}

	public void setMedicalScreeningForm(MedicalScreeningForm medicalScreeningForm) {
		this.medicalScreeningForm = medicalScreeningForm;
	}

	public List<MedicalScreeningForm> getMedicalScreeningFormfilteredList() {
		return medicalScreeningFormfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @param medicalScreeningFormfilteredList the new
	 *                                         medicalScreeningFormfilteredList list
	 * @see MedicalScreeningForm
	 */
	public void setMedicalScreeningFormfilteredList(List<MedicalScreeningForm> medicalScreeningFormfilteredList) {
		this.medicalScreeningFormfilteredList = medicalScreeningFormfilteredList;
	}

	public LazyDataModel<MedicalScreeningForm> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<MedicalScreeningForm> dataModel) {
		this.dataModel = dataModel;
	}

	public MedicalScreeningFormCronicDiseases getMedicalScreeningFormCronicDiseases() {
		return medicalScreeningFormCronicDiseases;
	}

	public void setMedicalScreeningFormCronicDiseases(MedicalScreeningFormCronicDiseases medicalScreeningFormCronicDiseases) {
		this.medicalScreeningFormCronicDiseases = medicalScreeningFormCronicDiseases;
	}

}
