package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.Doc;
import haj.com.entity.EmployeeSOW;
import haj.com.entity.Invoice;
import haj.com.entity.InvoiceHist;
import haj.com.entity.MilestoneStatementOfWork;
import haj.com.entity.Notes;
import haj.com.entity.StatementOfWork;
import haj.com.entity.UsersCost;
import haj.com.entity.enums.SowTypeEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.CurrencyService;
import haj.com.service.EmployeeSOWService;
import haj.com.service.InvoiceService;
import haj.com.service.MilestoneStatementOfWorkService;
import haj.com.service.NotesService;
import haj.com.service.StatementOfWorkService;
import haj.com.service.UsersCostService;

@ManagedBean(name = "invoiceUI3")
@ViewScoped
public class InvoiceUI3 extends AbstractUI {

	private InvoiceService service = new InvoiceService();
	private List<Invoice> invoiceList = null;
	private List<Invoice> invoicefilteredList = null;
	private Invoice invoice = null;
	private LazyDataModel<Invoice> dataModel;

	private StatementOfWorkService statementOfWorkService = new StatementOfWorkService();

	private List<EmployeeSOW> employeesowList;
	private EmployeeSOW employeesow;
	private EmployeeSOW employeesowInsert;
	private EmployeeSOWService employeesowService = new EmployeeSOWService();

	private List<MilestoneStatementOfWork> milestonesowList;
	private MilestoneStatementOfWork milestonesow;
	private MilestoneStatementOfWork milestonesowInsert;
	private MilestoneStatementOfWorkService milestonesowService = new MilestoneStatementOfWorkService();

	private UsersCost usercost;
	private List<UsersCost> usercostList;
	private UsersCostService usercostService = new UsersCostService();

	private double predictedInvoiceValue;
	private double paidExchangeRate;
	private double invoicePredictValue;
	private double sowValue;
	private double paidPredictExchangeRate;
	private Double milestonevalue;
	private Double employeevalue;
	private List<InvoiceHist> invoiceHistList;

	@ManagedProperty(value = "#{uploadDocUI}")
	private UploadDocUI uploadDocUI;
	private Doc doc;
	private Notes note;
	private NotesService notesService = new NotesService();

	private Date empsowStartDate;
	private Date empsowEndDate;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all Invoice and prepare a for a create of a new
	 * Invoice
	 * 
	 * @author TechFinium
	 * @see Invoice
	 * @throws Exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		invoiceInfo();
		prepDoc();
	}

	public void prepDoc() throws Exception {
		this.doc = new Doc();
		this.doc.setUser(getSessionUI().getUser());
		this.doc.setInvoice(invoice);
	}

	public void prepNote() throws Exception {
		this.note = new Notes();
		this.note.setUsers(getSessionUI().getUser());
		this.note.setInvoice(invoice);
	}

	public void calcPaidExchange() {
		this.invoice.setPaidExhangeRate(this.invoice.getInvoiceValue());
	}

	public void prepEmpSow() {
		try {
			sowValue = sowValueExchangeChange();
			if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.TimeMaterials) {
				employeesowList = new ArrayList<EmployeeSOW>();
				if (this.invoice.getId() != null) {
					employeesowList = employeesowService.findBySOWInvoice(this.invoice.getStatementOfWork(), this.invoice);
				} else {
					employeesowList = employeesowService.findBySOWandInvoiceNull(this.invoice.getStatementOfWork());
				}
				if (employeesowList.isEmpty())
					addWarningMessage("No Employee Assigned to S.O.W!");
				else {
					employeeValueExchangeChange();
				}
			} else if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.Fixed) {
				milestonesowList = new ArrayList<MilestoneStatementOfWork>();
				milestonesowList = milestonesowService.findSowMilestoneInvoiceNull(this.invoice.getStatementOfWork());
				if (this.invoice.getId() != null) {
					milestonesow = new MilestoneStatementOfWork();
					milestonesow = milestonesowService.findSowInvoiceMilestone(this.invoice.getStatementOfWork(), this.invoice);
				} else {
					milestonesow = milestonesowList.get(0);
				}
			}

			prepAuditInvoice();
			addInfoMessage("Form Updated");
		} catch (Exception e) {
		}
	}

	public void checkMilestone() {
		try {
			StatementOfWork sow = new StatementOfWork();
			sow = this.invoice.getStatementOfWork();
			this.invoice = new Invoice();
			this.invoice.setStatementOfWork(sow);
			if (this.invoice.getExhangeRate() == null)
				this.invoice.setExhangeRate(1.0);
			addInfoMessage("New Invoice for Milestone on S.O.W - " + this.invoice.getStatementOfWork().getTitle());
			milestoneValueExchangeChange();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Get all Invoice for data table
	 * 
	 * @author TechFinium
	 * @see Invoice
	 */
	public void invoiceInfo() {

		dataModel = new LazyDataModel<Invoice>() {

			private static final long serialVersionUID = 1L;
			private List<Invoice> retorno = new ArrayList<Invoice>();

			@Override
			public List<Invoice> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allInvoice(Invoice.class, first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(service.count(Invoice.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Invoice obj) {
				return obj.getId();
			}

			@Override
			public Invoice getRowData(String rowKey) {
				for (Invoice obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

	}

	/**
	 * Insert Invoice into database
	 * 
	 * @author TechFinium
	 * @see Invoice
	 */
	public void invoiceInsert() {
		try {
			if (invoice.getPaid() && invoice.getPaidDate() == null)
				throw new Exception(super.getEntryLanguage("paidDate.required"));
			if (this.invoice.getId() != null && this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.Fixed) {
				MilestoneStatementOfWork msow = new MilestoneStatementOfWork();
				msow = milestonesow;
				milestonesow = milestonesowService.findSowInvoiceMilestone(this.invoice.getStatementOfWork(), this.invoice);
				milestonesow.setMilestoneDate(msow.getMilestoneDate());
				milestonesow.setMilestoneDescription(msow.getMilestoneDescription());
				milestonesow.setMilestoneValue((msow.getMilestoneValue() * this.invoice.getStatementOfWork().getValueOfSowExhangeRate()) / this.invoice.getExhangeRate());
				milestonesow.setUpdatedBy(msow.getUpdatedBy());
			}
			this.invoice.setSowTypeEnum(this.invoice.getStatementOfWork().getSowTypeEnum());
			service.create(this.invoice);
			insertMilestoneEmployeeSOW();
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			invoiceInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void insertMilestoneEmployeeSOW() {
		try {
			if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.TimeMaterials) {
				for (EmployeeSOW empsow : employeesowList) {
					if (empsow.getInvoice() == null) {
						employeesowInsert = new EmployeeSOW();
						employeesowInsert.setInvoice(this.invoice);
						employeesowInsert.setUpdatedBy(getSessionUI().getUser());
						employeesowInsert.setChargeRate(empsow.getChargeRate());
						employeesowInsert.setEmployee(empsow.getEmployee());
						employeesowInsert.setEndDate(empsow.getEndDate());
						employeesowInsert.setPredictedChargeRate(empsow.getPredictedChargeRate());
						employeesowInsert.setSow(empsow.getSow());
						employeesowInsert.setStartDate(empsow.getStartDate());
						employeesowService.create(employeesowInsert);
					} else {
						employeesowService.create(empsow);
					}
				}
			} else if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.Fixed) {
				if (milestonesow.getInvoice() == null) {
					milestonesowInsert = new MilestoneStatementOfWork();
					milestonesowInsert.setUpdatedBy(getSessionUI().getUser());
					milestonesowInsert.setInvoice(this.invoice);
					milestonesowInsert.setSow(this.invoice.getStatementOfWork());
					milestonesowInsert.setMilestoneDate(milestonesow.getMilestoneDate());
					milestonesowInsert.setMilestoneDescription(milestonesow.getMilestoneDescription());
					milestonesowInsert.setMilestoneValue(milestonesow.getMilestoneValue());
					milestonesowService.create(milestonesowInsert);
					milestonesow = new MilestoneStatementOfWork();
					milestonesowInsert = new MilestoneStatementOfWork();
				} else {
					milestonesowService.create(milestonesow);
				}
				addInfoMessage("Milestone Updated");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete Invoice from database
	 * 
	 * @author TechFinium
	 * @see Invoice
	 */
	public void invoiceDelete() {
		try {
			service.delete(this.invoice);
			prepareNew();
			invoiceInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted "));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of Invoice
	 * 
	 * @author TechFinium
	 * @see Invoice
	 */
	public void prepareNew() {
		invoice = new Invoice();
		invoice.setLocale(CurrencyService.baseLocale);
		invoice.setExhangeRate(1.0);
		invoice.setStatementOfWork(null);
		invoice.setPaid(Boolean.FALSE);
		invoice.setInvoiceDate(new java.util.Date());
		invoice.setCreatedBy(getSessionUI().getUser());
		invoiceHistList = new ArrayList<InvoiceHist>();
		predictedInvoiceValue = 0;
		paidExchangeRate = 0;
		invoicePredictValue = 0;
		paidPredictExchangeRate = 0;
		empsowStartDate = new java.util.Date();
		empsowEndDate = new java.util.Date();
		addInfoMessage("New Invoice Set");
	}

	/**
	 * Update Invoice in database
	 * 
	 * @author TechFinium
	 * @see Invoice
	 */
	public void invoiceUpdate() {
		try {
			service.update(this.invoice);
			addInfoMessage("Update successful");
			invoiceInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void prepAuditInvoice() {
		try {
			if (this.invoice.getId() != null)
				invoiceHistList = service.findByKeyHist(this.invoice.getId());
			else
				invoiceHistList = new ArrayList<InvoiceHist>();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @see Invoice
	 */
	public List<Invoice> complete(String desc) {
		List<Invoice> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<StatementOfWork> searchSOW(String description) {
		List<StatementOfWork> l = null;
		try {
			l = statementOfWorkService.findByNumerOrDescription(description);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public void onSOWSelect(SelectEvent event) {
		if (event.getObject() instanceof StatementOfWork)
			try {
				this.invoice.setLocale(this.invoice.getStatementOfWork().getLocale());
				this.invoice.setExhangeRate(CurrencyService.exhangeRateFromToBase(this.invoice.getLocale()));
				if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.TimeMaterials) {
					employeesowList = new ArrayList<EmployeeSOW>();
					if (this.invoice.getId() != null) {
						employeesowList.addAll(employeesowService.findBySOWInvoice(this.invoice.getStatementOfWork(), this.invoice));
					} else {
						employeesowList = employeesowService.findBySOWandInvoiceNull(this.invoice.getStatementOfWork());
					}
					if (employeesowList.isEmpty())
						addWarningMessage("No Employee Assigned to S.O.W!");
				} else

				if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.Fixed) {
					milestonesowList = new ArrayList<MilestoneStatementOfWork>();
					milestonesowList = milestonesowService.findSowMilestone(this.invoice.getStatementOfWork());
					milestonesow = new MilestoneStatementOfWork();
					addWarningMessage("No Milestones Assigned to S.O.W!");
				}
			} catch (Exception e) {
				addErrorMessage(e.getMessage(), e);
			}
	}

	public void onRowEdit(RowEditEvent event) {
		try {
			employeeValueExchangeChange();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Double sowValueExchangeChange() {
		
		if (this.invoice.getStatementOfWork().getValueOfSowExhangeRate().compareTo(this.invoice.getExhangeRate()) != 0)
			sowValue = (this.invoice.getStatementOfWork().getValueSow() * this.invoice.getStatementOfWork().getValueOfSowExhangeRate()) / this.invoice.getExhangeRate();
		else
			sowValue = this.invoice.getStatementOfWork().getValueSow();
		if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.Fixed)
			milestoneValueExchangeChange();
		if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.TimeMaterials)
			employeeValueExchangeChange();
		return sowValue;
	}

	public void calcInvoiceValueAfterEmployeeRowEdit() {
		try {
			Double temp = 0.0;
			Double empChargeRate = 0.0;
			for (EmployeeSOW empsow : employeesowList) {
				usercost = new UsersCost();
				usercost = usercostService.findByOneUser(empsow.getEmployee());
				empChargeRate = (usercost.getDayRate() * usercost.getExhangeRate()) / this.invoice.getExhangeRate();
				if ((this.invoice.getId() == null) && (empsow.getChargeRate() == null))
					empsow.setChargeRate(empChargeRate);
				empsow.setPredictedChargeRate(empChargeRate);
				empChargeRate = empsow.getChargeRate();
				if ((empsow.getStartDate() != null) && (empsow.getEndDate() != null)) {
					if (empsow.getStartDate().equals(empsow.getEndDate())) {
						temp += empChargeRate;
					} else if (empsow.getStartDate().before(empsow.getEndDate())) {
						long diff = empsow.getEndDate().getTime() - empsow.getStartDate().getTime();
						int diffDays = (int) (diff / (24 * 1000 * 60 * 60));
						temp += empChargeRate * ((diffDays / 7) * 5);
					} else if (empsow.getStartDate().after(empsow.getEndDate())) {
						temp += empChargeRate;
					}
				} else {
					temp += empChargeRate;
				}
			}
			sowValue = sowValueExchangeChange();
			predictedInvoiceValue = temp;
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void employeeValueExchangeChange() {
		Double temp = 0.0;
		Double empChargeRate = 0.0;
		employeevalue = 0.0;

		for (EmployeeSOW empsow : employeesowList) {
			empChargeRate = empsow.getChargeRate();

			if ((empsow.getStartDate() != null) && (empsow.getEndDate() != null)) {
				if (empsow.getStartDate().equals(empsow.getEndDate())) {
					temp += empChargeRate;
				} else if (empsow.getStartDate().before(empsow.getEndDate())) {
					long diff = empsow.getEndDate().getTime() - empsow.getStartDate().getTime();
					int diffDays = (int) (diff / (24 * 1000 * 60 * 60));
					temp += empChargeRate * ((diffDays / 7) * 5);
				} else if (empsow.getStartDate().after(empsow.getEndDate())) {
					temp += empChargeRate;
				}
			}

			if (this.invoice.getStatementOfWork().getValueOfSowExhangeRate().compareTo(this.invoice.getExhangeRate()) != 0)
				employeevalue += (temp * this.invoice.getStatementOfWork().getValueOfSowExhangeRate()) / this.invoice.getExhangeRate();
			else
				employeevalue += temp;
		}
		sowValue += employeevalue;
		this.invoice.setEmployeeValue(employeevalue);
		this.invoice.setSowValue(sowValue);
	}

	public void milestoneValueExchangeChange() {
		milestonevalue = 0.0;
		if (this.invoice.getStatementOfWork().getValueOfSowExhangeRate().compareTo(this.invoice.getExhangeRate()) != 0)
			milestonevalue = (this.milestonesow.getMilestoneValue() * this.invoice.getStatementOfWork().getValueOfSowExhangeRate()) / this.invoice.getExhangeRate();
		else
			milestonevalue = this.milestonesow.getMilestoneValue();
		sowValue += milestonevalue;
		this.invoice.setSowValue(sowValue);
	}

	public void safeFile(FileUploadEvent event) {
		try {
			uploadDocUI.setDocs(null);
			uploadDocUI.safeFile(event, this.doc, getSessionUI().getUser());
			runInit();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void onCountryChange() {
		try {
			invoice.setExhangeRate(CurrencyService.exhangeRateFromToBase(invoice.getLocale()));
			if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.TimeMaterials)
				employeeValueExchangeChange();
			if (this.invoice.getStatementOfWork().getSowTypeEnum() == SowTypeEnum.Fixed)
				sowValue = sowValueExchangeChange();

			addInfoMessage("Some Values Have Changed!!");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void notesInsert() {
		try {
			notesService.create(this.note);
			prepNote();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			invoiceInfo();
			super.runClientSideExecute("PF('noteDialog').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void milestoneToInvVal() {
		this.predictedInvoiceValue = this.milestonesow.getMilestoneValue();
	}

	public LazyDataModel<Invoice> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Invoice> dataModel) {
		this.dataModel = dataModel;
	}

	public UploadDocUI getUploadDocUI() {
		return uploadDocUI;
	}

	public void setUploadDocUI(UploadDocUI uploadDocUI) {
		this.uploadDocUI = uploadDocUI;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public Notes getNote() {
		return note;
	}

	public void setNote(Notes note) {
		this.note = note;
	}

	public double getPredictedInvoiceValue() {
		return predictedInvoiceValue;
	}

	public void setPredictedInvoiceValue(double invoiceValue) {
		this.predictedInvoiceValue = invoiceValue;
	}

	public double getPaidExchangeRate() {
		return paidExchangeRate;
	}

	public void setPaidExchangeRate(double paidExchangeRate) {
		this.paidExchangeRate = paidExchangeRate;
	}

	public List<InvoiceHist> getInvoiceHistList() {
		return invoiceHistList;
	}

	public void setInvoiceHistList(List<InvoiceHist> invoiceHistList) {
		this.invoiceHistList = invoiceHistList;
	}

	public List<EmployeeSOW> getEmployeesowList() {
		return employeesowList;
	}

	public void setEmployeesowList(List<EmployeeSOW> employeeesowList) {
		this.employeesowList = employeeesowList;
	}

	public List<Invoice> getInvoicefilteredList() {
		return invoicefilteredList;
	}

	public void setInvoicefilteredList(List<Invoice> invoicefilteredList) {
		this.invoicefilteredList = invoicefilteredList;
	}

	public EmployeeSOW getEmployeesow() {
		return employeesow;
	}

	public void setEmployeesow(EmployeeSOW employeesow) {
		this.employeesow = employeesow;
	}

	public List<MilestoneStatementOfWork> getMilestonesowList() {
		return milestonesowList;
	}

	public void setMilestonesowList(List<MilestoneStatementOfWork> milestonesowList) {
		this.milestonesowList = milestonesowList;
	}

	public MilestoneStatementOfWork getMilestonesow() {
		return milestonesow;
	}

	public void setMilestonesow(MilestoneStatementOfWork milestonesow) {
		this.milestonesow = milestonesow;
	}

	public UsersCost getUsercost() {
		return usercost;
	}

	public void setUsercost(UsersCost usercost) {
		this.usercost = usercost;
	}

	public List<UsersCost> getUsercostList() {
		return usercostList;
	}

	public void setUsercostList(List<UsersCost> usercostList) {
		this.usercostList = usercostList;
	}

	public double getInvoicePredictValue() {
		return invoicePredictValue;
	}

	public void setInvoicePredictValue(double invoicePredictValue) {
		this.invoicePredictValue = invoicePredictValue;
	}

	public double getPaidPredictExchangeRate() {
		return paidPredictExchangeRate;
	}

	public void setPaidPredictExchangeRate(double paidPredictExchangeRate) {
		this.paidPredictExchangeRate = paidPredictExchangeRate;
	}

	public Date getEmpsowStartDate() {
		return empsowStartDate;
	}

	public void setEmpsowStartDate(Date empsowStartDate) {
		this.empsowStartDate = empsowStartDate;
	}

	public Date getEmpsowEndDate() {
		return empsowEndDate;
	}

	public void setEmpsowEndDate(Date empsowEndDate) {
		this.empsowEndDate = empsowEndDate;
	}

	public double getSowValue() {
		return sowValue;
	}

	public void setSowValue(double sowValue) {
		this.sowValue = sowValue;
	}

	public MilestoneStatementOfWork getMilestonesowInsert() {
		return milestonesowInsert;
	}

	public void setMilestonesowInsert(MilestoneStatementOfWork milestonesowInsert) {
		this.milestonesowInsert = milestonesowInsert;
	}

	public EmployeeSOW getEmployeesowInsert() {
		return employeesowInsert;
	}

	public void setEmployeesowInsert(EmployeeSOW employeesowInsert) {
		this.employeesowInsert = employeesowInsert;
	}

	public Double getMilestonevalue() {
		return milestonevalue;
	}

	public void setMilestonevalue(Double milestonevalue) {
		this.milestonevalue = milestonevalue;
	}

	public Double getEmployeevalue() {
		return employeevalue;
	}

	public void setEmployeevalue(Double employeevalue) {
		this.employeevalue = employeevalue;
	}

	public List<Invoice> getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List<Invoice> invoiceList) {
		this.invoiceList = invoiceList;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

}
