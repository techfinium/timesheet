package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

import haj.com.entity.Users;
import haj.com.entity.Visitor;
import haj.com.entity.lookup.CronicDiseases;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.UsersService;
import haj.com.service.VisitorService;
import haj.com.service.lookup.CronicDiseasesService;

// TODO: Auto-generated Javadoc
/**
 * The Class SelectItemsUI.
 */
@ManagedBean(name = "selectItemsUI")
@SessionScoped
public class SelectItemsUI extends AbstractUI {

	private CronicDiseasesService cronicDiseasesService = new CronicDiseasesService();
	private UsersService usersService = new UsersService();
	private VisitorService visitorService = new VisitorService();

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Run init.
	 *
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {

	}

	/**
	 * Gets the select items occupation category.
	 *
	 * @return the select items occupation category
	 */

	/**
	 * Gets the select items bank.
	 *
	 * @return the select items bank
	 */

	public List<CronicDiseases> getCronicDiseases() {
		List<CronicDiseases> l = null;
		try {
			l = cronicDiseasesService.allCronicDiseases();
		} catch (

		ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
	
	public List<Users> getUsers() {
		List<Users> l = null;
		try {
			l = usersService.allUsers();
		} catch (

		ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
	
	public List<Visitor> getVisitor() {
		List<Visitor> l = null;
		try {
			l = visitorService.allVisitor();
		} catch (

		ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
}
