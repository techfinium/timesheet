package haj.com.ui;


import java.util.ArrayList;
import java.util.Currency;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import haj.com.bean.LocaleCurrency;
import haj.com.framework.AbstractUI;
import haj.com.service.CurrencyService;
import haj.com.utils.GenericUtility;

@ManagedBean(name="languageUI")
@SessionScoped
public class LanguageUI extends AbstractUI {
private static final long serialVersionUID = 1L;

	private String localeCode ;
	private Double amount = 30.99;
	private Double diplayAmount;

	private Locale locale;// = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	private Currency currency ;
	private List<LocaleCurrency> localeCurrency;
	private String flag;


    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		flag = "flag-icon-gb";
	//	locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		locale = FacesContext.getCurrentInstance().getApplication().getDefaultLocale();
		if (locale.getLanguage().equals(locale.ENGLISH.getLanguage())) {
			locale = Locale.UK;
		}

		localeCode = locale.getCountry();
		doCurrency();
		doLocaleCurrency();
	}

	private void doLocaleCurrency() {
		localeCurrency =  new ArrayList<LocaleCurrency>();
		 for (Map.Entry<String, Object> entry : countries.entrySet()) {
			 localeCurrency.add( new LocaleCurrency((Locale) entry.getValue()));
		 }
	}

	private void doCurrency() {
		try {
			currency = Currency.getInstance(locale);
		} catch (Exception e) {
		 try {
			 logger.fatal(e);
			 currency = Currency.getInstance(Locale.UK);
			 }
		 catch (Exception e2) {

			 logger.fatal(e2);
		 }


 		}

		this.diplayAmount = GenericUtility.roundToPrecision(CurrencyService.convertCurrency(amount, currency),2);

	}


	public Locale getLocale() {
	        return locale;
	 }

	private static Map<String,Object> countries;
	static{
		countries = new LinkedHashMap<String,Object>();
		countries.put(getEntryLanguage("select.language") ,Locale.UK);
		countries.put("UK",Locale.UK);
		countries.put("French", new Locale("fr","FR")); //label, value
		countries.put("India", new Locale("in","IN")); //label, value
		countries.put("Afrikaans", new Locale("af","ZA")); //label, value
		countries.put("Spanish", new Locale("es","ES")); //label, value
		countries.put("Italian", new Locale("it","IT")); //label, value
		countries.put("Germany", new Locale("de","DE")); //label, value
		countries.put("Belarusian", new Locale("be","BE")); //label, value
		countries.put("Finnish ", new Locale("fi","FI")); //label, value
		countries.put("Norwegian", new Locale("no","NO")); //label, value
		countries.put("Danish", new Locale("da","DK")); //label, value
		countries.put("Switzerland", new Locale("ch","CH")); //label, value
		countries.put("Portuguese", new Locale("pt","PT")); //label, value
		countries.put("Dutch", new Locale("nl","NL")); //label, value
		countries.put("Greek", new Locale("gr","GR")); //label, value
		countries.put("USA", Locale.US); //label, value



	}

	private static Map<Locale,String> flags;
	static{
		flags = new LinkedHashMap<Locale,String>();
		flags.put(Locale.UK, "flag-icon-gb");
		flags.put(new Locale("fr","FR"), "flag-icon-fr");
		flags.put(new Locale("in","IN"),"flag-icon-in" ); //label, value
		flags.put(new Locale("af","ZA"),"flag-icon-za"); //label, value
		flags.put(new Locale("es","ES"),"flag-icon-es"); //label, value
		flags.put( new Locale("it","IT"),"flag-icon-it"); //label, value
		flags.put( new Locale("de","DE"),"flag-icon-de"); //label, value
		flags.put( new Locale("be","BE"),"flag-icon-be"); //label, value
		flags.put( new Locale("fi","FI"),"flag-icon-fi"); //label, value
		flags.put(new Locale("no","NO"),"flag-icon-no"); //label, value
		flags.put( new Locale("da","DK"),"flag-icon-da"); //label, value
		flags.put( new Locale("ch","CH"),"flag-icon-ch"); //label, value
		flags.put( new Locale("pt","PT"),"flag-icon-pt"); //label, value
		flags.put( new Locale("nl","NL"),"flag-icon-nl"); //label, value
		flags.put( new Locale("gr","GR"),"flag-icon-gr"); //label, value
		flags.put( Locale.US,"flag-icon-us"); //label, value

		}

	public Map<String, Object> getCountriesInMap() {
		return countries;
	}


	public String getLocaleCode() {
		return localeCode;
	}


	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}


	public void countryLocaleCodeChanged(ValueChangeEvent e){

		String newLocaleValue = e.getNewValue().toString();

		//loop a map to compare the locale code
        for (Map.Entry<String, Object> entry : countries.entrySet()) {

        	if(entry.getValue().toString().equals(newLocaleValue)){

        		FacesContext.getCurrentInstance()
        			.getViewRoot().setLocale((Locale)entry.getValue());
        		this.locale =(Locale)entry.getValue();

        		if (flags.get(this.locale) == null) {
        			flag = "flag-icon-gb";
        		}
        		else {
        			this.flag = flags.get(this.locale);
        		}
         		doCurrency();

        	}
        }

	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Double getDiplayAmount() {
		return diplayAmount;
	}

	public void setDiplayAmount(Double diplayAmount) {
		this.diplayAmount = diplayAmount;
	}

	public List<LocaleCurrency> getLocaleCurrency() {
		return localeCurrency;
	}

	public void setLocaleCurrency(List<LocaleCurrency> localeCurrency) {
		this.localeCurrency = localeCurrency;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}





}
