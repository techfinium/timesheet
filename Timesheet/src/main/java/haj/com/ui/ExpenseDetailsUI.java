package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.ValidationException;

import org.primefaces.event.FileUploadEvent;

import haj.com.entity.Company;
import haj.com.entity.Doc;
import haj.com.entity.ExpenseForm;
import haj.com.entity.Projects;
import haj.com.entity.enums.ExpenseFormTypeEnum;
import haj.com.entity.lookup.DailyRate;
import haj.com.entity.lookup.EngineType;
import haj.com.entity.lookup.Title;
import haj.com.framework.AbstractUI;
import haj.com.service.DocService;
import haj.com.service.ExpenseFormService;
import haj.com.service.lookup.AllowanceRateService;
import haj.com.service.lookup.DailyRateService;
import haj.com.service.lookup.EngineTypeService;
import haj.com.service.lookup.TitleService;

@ManagedBean(name = "expensedetailsUI")
@ViewScoped
public class ExpenseDetailsUI extends AbstractUI {
	private Company company;
	private Projects projects;
	private EngineTypeService engineTypeService = new EngineTypeService();
	private TitleService titleService = new TitleService();
	private ExpenseFormService expenseFormService = new ExpenseFormService();
	private DailyRateService dailyRateService = new DailyRateService();
	private AllowanceRateService allowanceRateService = new AllowanceRateService();
	private DocService docService = new DocService();
	private List<EngineType> engineTypeList = new ArrayList<>();
	private List<Title> titleList = new ArrayList<>();
	private ExpenseForm expenseForm = new ExpenseForm();
	private List<ExpenseForm> expenseFormList = new ArrayList<>();
	private List<ExpenseForm> entertainmentFormList = new ArrayList<>();
	private List<ExpenseForm> miscellaneousFormList = new ArrayList<>();
	private List<ExpenseForm> travelExpenseList = new ArrayList<>();
	private List<DailyRate> dailyRateList = new ArrayList<>();
	private Integer activeIndex = 0;
	private ExpenseFormTypeEnum expenseFormType;
	private Doc doc;
	private DailyRate dailyRate;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		prepCompany();
		company = (Company) super.getParameter("company", true);
		engineTypeList = engineTypeService.allEngineType();
		titleList = titleService.allTitle();
		expenseFormType = null;
		dailyRateList = dailyRateService.allDailyRate();
		prepExpenseForm();

	}

	public void prepareNew() {
		expenseForm = new ExpenseForm();

	}

	public void expenseFormInsert() {
		try {
			if (expenseForm.getConsent() == null || !expenseForm.getConsent()) {
				addErrorMessage("Please Acccept the Consent");
			} else {
				expenseForm.setExpenseFormType(expenseFormType);
				expenseFormService.storeExpensform(this.expenseForm);
				addInfoMessage("Insert successful");
				expenseInfo();
				prepExpenseForm();
			}
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void calcPerDiem() {
		if (expenseForm.getDailyRate() != null && expenseForm.getDayNumber() != null) {
			expenseForm.setPerDiem(expenseForm.getDailyRate().getValue() * expenseForm.getDayNumber());
		}
	}

	public void expenseformDelete() {
		try {
			expenseFormService.delete(this.expenseForm);
			prepExpenseForm();
			expenseInfo();
			super.runClientSideUpdate("form");
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void changeIndex() {
		try {
			switch (expenseFormType) {
			case ExpenseForm:
				activeIndex = 0;
				break;
			case Entertainment:
				activeIndex = 1;
				break;
			case Miscellaneous:
				activeIndex = 2;
				break;
			case Travel:
				activeIndex = 3;
				break;
			default:
				break;
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void expenseInfo() {
		try {
			expenseFormList = expenseFormService.findByUserProjectType(expenseForm.getUser(), expenseForm.getProject(), ExpenseFormTypeEnum.ExpenseForm);
			entertainmentFormList = expenseFormService.findByUserProjectType(expenseForm.getUser(), expenseForm.getProject(), ExpenseFormTypeEnum.Entertainment);
			miscellaneousFormList = expenseFormService.findByUserProjectType(expenseForm.getUser(), expenseForm.getProject(), ExpenseFormTypeEnum.Miscellaneous);
			travelExpenseList = expenseFormService.findByUserProjectType(expenseForm.getUser(), expenseForm.getProject(), ExpenseFormTypeEnum.Travel);
			
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void hit() {
		try {
			System.out.println("HIT");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void hit(FileUploadEvent event) {
		try {
			System.out.println("HIT");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void safeFile(FileUploadEvent event) {
		try {
			if (this.expenseForm.getDocList() == null ) {
				this.expenseForm.setDocList(new ArrayList<>());
			}
			docService.save(doc, event.getFile().getContents(), event.getFile().getFileName(), getSessionUI().getUser());
			if (!this.expenseForm.getDocList().contains(doc)) {
				this.expenseForm.getDocList().add(doc);
			}
			addInfoMessage("Your file has been uploaded successfully!");
			super.runClientSideExecute("PF('dlgUpload').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void populateExpenseDocs() {
		try {
			expenseForm.setDocList(docService.searchByTargetKeyAndClass(expenseForm.getClass().getName(), expenseForm.getId()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepCompany() {
		company = new Company();
	}

	public void prepDoc() {
		doc = new Doc();
	}

	public void prepProjects() {
		projects = new Projects();
	}

	public void prepExpenseForm() {
		expenseForm = new ExpenseForm();
		expenseForm.setProject(company.getProjects());
		expenseForm.setUser(getSessionUI().getUser());
		expenseForm.setDocList(new ArrayList<>());
		try {
			expenseForm.setDailyRate(dailyRateService.findMostRecent());
			expenseForm.setAllowanceRate(allowanceRateService.findMostRecent());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void expenseDetailsUpdate() {
		try {
			expenseFormService.update(this.expenseForm);

		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Projects getProjects() {
		return projects;
	}

	public void setProjects(Projects projects) {
		this.projects = projects;
	}

	public List<EngineType> getEngineTypeList() {
		return engineTypeList;
	}

	public void setEngineTypeList(List<EngineType> engineTypeList) {
		this.engineTypeList = engineTypeList;
	}

	public ExpenseForm getExpenseForm() {
		return expenseForm;
	}

	public void setExpenseForm(ExpenseForm expenseForm) {
		this.expenseForm = expenseForm;
	}

	public List<ExpenseForm> getExpenseFormList() {
		return expenseFormList;
	}

	public void setExpenseFormList(List<ExpenseForm> expenseFormList) {
		this.expenseFormList = expenseFormList;
	}

	public Integer getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(Integer activeIndex) {
		this.activeIndex = activeIndex;
	}

	public ExpenseFormTypeEnum getExpenseFormType() {
		return expenseFormType;
	}

	public void setExpenseFormType(ExpenseFormTypeEnum expenseFormType) {
		this.expenseFormType = expenseFormType;
	}

	public ExpenseFormService getExpenseFormService() {
		return expenseFormService;
	}

	public void setExpenseFormService(ExpenseFormService expenseFormService) {
		this.expenseFormService = expenseFormService;
	}

	public List<ExpenseForm> getEntertainmentFormList() {
		return entertainmentFormList;
	}

	public void setEntertainmentFormList(List<ExpenseForm> entertainmentFormList) {
		this.entertainmentFormList = entertainmentFormList;
	}

	public List<ExpenseForm> getMiscellaneousFormList() {
		return miscellaneousFormList;
	}

	public void setMiscellaneousFormList(List<ExpenseForm> miscellaneousFormList) {
		this.miscellaneousFormList = miscellaneousFormList;
	}

	public List<ExpenseForm> getTravelExpenseList() {
		return travelExpenseList;
	}

	public void setTravelExpenseList(List<ExpenseForm> travelExpenseList) {
		this.travelExpenseList = travelExpenseList;
	}

	public List<Title> getTitleList() {
		return titleList;
	}

	public void setTitleList(List<Title> titleList) {
		this.titleList = titleList;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public List<DailyRate> getDailyRateList() {
		return dailyRateList;
	}

	public void setDailyRateList(List<DailyRate> dailyRateList) {
		this.dailyRateList = dailyRateList;
	}

	public DailyRate getDailyRate() {
		return dailyRate;
	}

	public void setDailyRate(DailyRate dailyRate) {
		this.dailyRate = dailyRate;
	}

}
