package  haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.BankingDetails;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.BankingDetailsService;

@javax.inject.Named("bankingdetailsUI")
@javax.faces.view.ViewScoped
public class BankingDetailsUI extends AbstractUI {

	private BankingDetailsService service = new BankingDetailsService();
	private List<BankingDetails> bankingdetailsList = null;
	private List<BankingDetails> bankingdetailsfilteredList = null;
	private BankingDetails bankingdetails = null;
	private LazyDataModel<BankingDetails> dataModel; 

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all BankingDetails and prepare a for a create of a new BankingDetails
 	 * @author TechFinium 
 	 * @see    BankingDetails
	 * @throws Exception the exception
 	 */
	private void runInit() throws Exception {
		prepareNew();
		bankingdetailsInfo();
	}

	/**
	 * Get all BankingDetails for data table
 	 * @author TechFinium 
 	 * @see    BankingDetails
 	 */
	public void bankingdetailsInfo() {
	 
			
			 dataModel = new LazyDataModel<BankingDetails>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<BankingDetails> retorno = new  ArrayList<BankingDetails>();
			   
			   @Override 
			   public List<BankingDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					retorno = service.allBankingDetails(BankingDetails.class,first,pageSize,sortField,sortOrder,filters);
					dataModel.setRowCount(service.count(BankingDetails.class,filters));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return retorno; 
			   }
			   
			    @Override
			    public Object getRowKey(BankingDetails obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public BankingDetails getRowData(String rowKey) {
			        for(BankingDetails obj : retorno) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
			
	
	}

	/**
	 * Insert BankingDetails into database
 	 * @author TechFinium 
 	 * @see    BankingDetails
 	 */
	public void bankingdetailsInsert() {
		try {
				 service.create(this.bankingdetails);
				 prepareNew();
				 addInfoMessage(super.getEntryLanguage("update.successful"));
				 bankingdetailsInfo();
			 } catch (ValidationException e) {
				addErrorMessage(getEntryLanguage(e.getMessage()));
			} catch (Exception e) {
		 	   super.addCallBackParm("validationFailed", true);
				addErrorMessage(e.getMessage(), e);
			}
	}
	
	/**
	 * Update BankingDetails in database
 	 * @author TechFinium 
 	 * @see    BankingDetails
 	 */
    public void bankingdetailsUpdate() {
		try {
				 service.update(this.bankingdetails);
				  addInfoMessage(super.getEntryLanguage("update.successful"));
				 bankingdetailsInfo();
			 } catch (ValidationException e) {
				addErrorMessage(getEntryLanguage(e.getMessage()));
			} catch (Exception e) {
			   super.addCallBackParm("validationFailed", true);
				addErrorMessage(e.getMessage(), e);
			}
	}

	/**
	 * Delete BankingDetails from database
 	 * @author TechFinium 
 	 * @see    BankingDetails
 	 */
	public void bankingdetailsDelete() {
		try {
			 service.delete(this.bankingdetails);
			  prepareNew();
			 bankingdetailsInfo();
			 super.addWarningMessage(super.getEntryLanguage("row.deleted"));
			 } catch (ValidationException e) {
				addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	/**
	 * Create new instance of BankingDetails 
 	 * @author TechFinium 
 	 * @see    BankingDetails
 	 */
	public void prepareNew() {
		bankingdetails = new BankingDetails();
	}

/*
    public List<SelectItem> getBankingDetailsDD() {
    	List<SelectItem> l =new ArrayList<SelectItem>();
    	l.add(new SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
    	bankingdetailsInfo();
    	for (BankingDetails ug : bankingdetailsList) {
    		// l.add(new SelectItem(ug.getUserGroupId(), ug.getUserGroupDesc()));
		}
    	return l;
    }
  */
  
    /**
     * Complete.
     *
     * @param desc the desc
     * @return the list
     */  
    public List<BankingDetails> complete(String desc) {
		List<BankingDetails> l = null;
		try {
			l = service.findByName(desc);
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		}
		catch (Exception e) {
		addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
    
    public List<BankingDetails> getBankingDetailsList() {
		return bankingdetailsList;
	}
	public void setBankingDetailsList(List<BankingDetails> bankingdetailsList) {
		this.bankingdetailsList = bankingdetailsList;
	}
	public BankingDetails getBankingdetails() {
		return bankingdetails;
	}
	public void setBankingdetails(BankingDetails bankingdetails) {
		this.bankingdetails = bankingdetails;
	}

    public List<BankingDetails> getBankingDetailsfilteredList() {
		return bankingdetailsfilteredList;
	}
	
	/**
	 * Prepare auto complete data
 	 * @author TechFinium 
 	 * @param bankingdetailsfilteredList the new bankingdetailsfilteredList list
 	 * @see    BankingDetails
 	 */	
	public void setBankingDetailsfilteredList(List<BankingDetails> bankingdetailsfilteredList) {
		this.bankingdetailsfilteredList = bankingdetailsfilteredList;
	}

	
	public LazyDataModel<BankingDetails> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<BankingDetails> dataModel) {
		this.dataModel = dataModel;
	}
	
}
