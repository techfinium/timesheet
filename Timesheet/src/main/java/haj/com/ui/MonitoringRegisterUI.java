package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;

import haj.com.entity.Address;
import haj.com.entity.MonitoringRegister;
import haj.com.entity.Visitor;
import haj.com.entity.datamodel.MonitoringRegisterDatamodel;
import haj.com.entity.enums.CovidPersonRegisterTypeEnum;
import haj.com.entity.enums.VisitorTypeEnum;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.MonitoringRegisterService;

@ManagedBean(name = "monitoringRegisterUI")
@ViewScoped
public class MonitoringRegisterUI extends AbstractUI {

	private MonitoringRegisterService service = new MonitoringRegisterService();;
	private List<MonitoringRegister> monitoringRegisterList = null;
	private List<MonitoringRegister> monitoringRegisterfilteredList = null;
	private MonitoringRegister monitoringRegister;
	private LazyDataModel<MonitoringRegister> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all MonitoringRegister and prepare a for a create of
	 * a new MonitoringRegister
	 * 
	 * @author TechFinium
	 * @see MonitoringRegister
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		monitoringRegisterInfo();

	}

	/**
	 * Get all MonitoringRegister for data table
	 * 
	 * @author TechFinium
	 * @see MonitoringRegister
	 */
	public void monitoringRegisterInfo() {
		dataModel = new MonitoringRegisterDatamodel();
	}

	public void populateUserData() throws Exception {
		service.populateUserData(this.monitoringRegister);
	}

	public void prepareVisitorData() throws Exception {
			populateVisitorData();
	}

	public void populateVisitorData() throws Exception {
		service.populateVisitorData(this.monitoringRegister);
	}

	public void populateUpdate() throws Exception {
		if (monitoringRegister.getPersonType().equals(CovidPersonRegisterTypeEnum.Employee)) {
			populateUserData();
		} else if (monitoringRegister.getPersonType().equals(CovidPersonRegisterTypeEnum.Visitor)) {
			monitoringRegister.setVisitorType(VisitorTypeEnum.Existing);
			populateVisitorData();
		}

	}
	
	public Boolean validation() {
		if (monitoringRegister.getEnterTime().after(monitoringRegister.getExitTime())) {
			addErrorMessage("Enter Time can not be after Exit Time");
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Insert MonitoringRegister into database
	 * 
	 * @author TechFinium
	 * @see MonitoringRegister
	 */
	public void monitoringRegisterInsert() {
		try {
			if (validation()) {
				service.createRegisterForm(this.monitoringRegister);
				prepareNew();
				addInfoMessage(super.getEntryLanguage("update.successful"));
				monitoringRegisterInfo();
			}
			
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update MonitoringRegister in database
	 * 
	 * @author TechFinium
	 * @see MonitoringRegister
	 */
	public void monitoringRegisterUpdate() {
		try {
			service.update(this.monitoringRegister);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			monitoringRegisterInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete MonitoringRegister from database
	 * 
	 * @author TechFinium
	 * @see MonitoringRegister
	 */
	public void monitoringRegisterDelete() {
		try {
			service.delete(this.monitoringRegister);
			prepareNew();
			monitoringRegisterInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of MonitoringRegister
	 * 
	 * @author TechFinium
	 * @see MonitoringRegister
	 */
	public void prepareNew() {
		monitoringRegister = new MonitoringRegister();
	}

	/*
	 * public List<SelectItem> getMonitoringRegisterDD() { List<SelectItem> l =new
	 * ArrayList<SelectItem>(); l.add(new
	 * SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
	 * monitoringRegisterInfo(); for (MonitoringRegister ug :
	 * monitoringRegisterList) { // l.add(new SelectItem(ug.getUserGroupId(),
	 * ug.getUserGroupDesc())); } return l; }
	 */

	/**
	 * Complete.
	 *
	 * @param desc the desc
	 * @return the list
	 */
	public List<MonitoringRegister> complete(String desc) {
		List<MonitoringRegister> l = null;
		try {
			l = service.findByName(desc);
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<MonitoringRegister> getMonitoringRegisterList() {
		return monitoringRegisterList;
	}

	public void setMonitoringRegisterList(List<MonitoringRegister> monitoringRegisterList) {
		this.monitoringRegisterList = monitoringRegisterList;
	}

	public MonitoringRegister getMonitoringRegister() {
		return monitoringRegister;
	}

	public void setMonitoringRegister(MonitoringRegister monitoringRegister) {
		this.monitoringRegister = monitoringRegister;
	}

	public List<MonitoringRegister> getMonitoringRegisterfilteredList() {
		return monitoringRegisterfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @param monitoringRegisterfilteredList the new monitoringRegisterfilteredList
	 *                                       list
	 * @see MonitoringRegister
	 */
	public void setMonitoringRegisterfilteredList(List<MonitoringRegister> monitoringRegisterfilteredList) {
		this.monitoringRegisterfilteredList = monitoringRegisterfilteredList;
	}

	public LazyDataModel<MonitoringRegister> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<MonitoringRegister> dataModel) {
		this.dataModel = dataModel;
	}

}
