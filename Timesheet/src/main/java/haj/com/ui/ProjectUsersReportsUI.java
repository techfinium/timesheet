package haj.com.ui;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.swing.ImageIcon;

import org.apache.commons.io.FileUtils;

import haj.com.bean.ProjectUsersReportBean;
import haj.com.bean.ReportBean;
import haj.com.constants.HAJConstants;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Projects;
import haj.com.framework.AbstractUI;
import haj.com.service.JasperService;
import haj.com.service.ReportService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "projectUsersReportsUI")
@ViewScoped
public class ProjectUsersReportsUI extends AbstractUI {

	private ReportService service = new ReportService();
	private Date fromDate;
	private Date toDate;
	private Projects project;
	private ReportBean reportBean;
	private List<ReportBean> reportBeanList;
	private List<ProjectUsersReportBean> projectUsersReportBeanList;
	private CompanyUsers companyUser;
	private Date todayDate = new Date();
	private boolean searched;
	private Double totalHours;
	private Long totalMins;
	private Integer totalHoursConverted;
	private JasperService jasperService = new JasperService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		toDate = GenericUtility.deductDaysFromDate(new Date(), 1);
		fromDate = GenericUtility.getFirstDayOfMonth(toDate);
		// toDate = todayDate;
		searched = false;
		totalHours = 0.0;
		totalMins = (long) 0;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Projects getProject() {
		return project;
	}

	public void setProject(Projects project) {
		this.project = project;
	}

	public void projectsFromToDateDownload() {
		jasperReportDownload(fromDate, toDate);
	}

	public void projectsFromToDateDownloadExcel() {
		jasperReportDownloadExcel(fromDate, toDate);
	}

	private void jasperReportDownload(Date fromDate, Date toDate) {

		Map<String, Object> params = new HashMap<String, Object>();
		String path = HAJConstants.APP_PATH;
		String sub_path = "/resources/hls/images/full-print.jpeg";
		byte[] buff;
		try {
			buff = FileUtils.readFileToByteArray(new File(path + sub_path));
			params.put("techLogo", (new ImageIcon(buff).getImage()));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		params.put("fromDateTime", fromDate);
		params.put("toDateTime", toDate);
		String fname = "project_hours.pdf";
		try {
			jasperService.createReportFromDBtoServletOutputStream("projectFromToDate.jasper", params, fname);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jasperReportDownloadExcel(Date fromDate, Date toDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		String path = HAJConstants.APP_PATH;
		String sub_path = "/resources/hls/images/full-print.jpeg";
		byte[] buff;
		try {
			buff = FileUtils.readFileToByteArray(new File(path + sub_path));
			params.put("techLogo", (new ImageIcon(buff).getImage()));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		params.put("fromDateTime", fromDate);
		params.put("toDateTime", toDate);

		try {
			String fname = "project_hours.xlsx";
			jasperService.exportToExcel("projectFromToDate.jasper", params, fname);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void projectsFromToDate() {
		try {
			totalHours = 0.0;
			totalMins = 0l;
			if (!getSessionUI().isDirector()) {

				reportBean = service.totalMinutesForProject(fromDate, toDate, getSessionUI().getCompany());
				reportBeanList = service.projectsFromToDateReport(fromDate, toDate, getSessionUI().getCompany());

				for (ReportBean reportBean : reportBeanList) {
					totalHours += reportBean.getHours();
					totalMins += reportBean.getLongOne();
				}
				
				if (totalMins == null) {
					totalMins = (long) 00;
				}
				
				if (totalMins >= 60) {
					int m = (int) (totalMins / 60);
					totalHours += m;
					totalMins -= m * 60;
				}

				totalHoursConverted = totalHours.intValue();

				if (reportBeanList.size() == 0) {
					addInfoMessage("No Entries Found For Selected Criteria");
				} else {
					// addInfoMessage("Report Generated");
				}
			} else {
				reportBean = service.totalMinutesForProjectDirector(fromDate, toDate);
				reportBeanList = service.projectsFromToDateDirector(fromDate, toDate);

				for (ReportBean reportBean : reportBeanList) {
					totalHours += reportBean.getHours();
					totalMins += reportBean.getLongOne();
				}
				
				if (totalMins == null) {
					totalMins = (long) 00;
				}
				
				if (totalMins >= 60) {
					int m = (int) (totalMins / 60);
					totalHours += m;
					totalMins -= m * 60;
				}

				totalHoursConverted = totalHours.intValue();

				if (reportBeanList.size() == 0) {
					addInfoMessage("No Entries Found For Selected Criteria");
				} else {
					// addInfoMessage("Report Generated");
				}
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void totalMinutesForProject() {
		try {
			totalHours = 0.0;
			totalMins = 0l;
			reportBean = service.totalMinutesForProject(project, fromDate, toDate, getSessionUI().getCompany());
			reportBeanList = service.projectDetailsReport(project, fromDate, toDate, getSessionUI().getCompany());
			for (ReportBean reportBean : reportBeanList) {
				totalHours += reportBean.getHours();
				totalMins += reportBean.getLongOne();
			}
			if (totalMins == null) {
				totalMins = (long) 00;
			}
			if (totalMins >= 60) {
				int m = (int) (totalMins / 60);
				totalHours += m;
				totalMins -= m * 60;
			}

			totalHoursConverted = totalHours.intValue();

			searched = true;
			if (reportBean != null) {
				setUpReportBean();
			}
			if (reportBeanList.size() == 0) {
				addInfoMessage("No Entries Found For Selected Criteria");
				searched = true;
			} else {
				// addInfoMessage("Report Generated");
			}

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void runProjectUsersReport(){
		try {
			totalHours = 0.0;
			totalMins = 0l;
			projectUsersReportBeanList = service.locateProjectsUsersReportByDateRange(fromDate, GenericUtility.getEndOfDay(toDate));
			if (projectUsersReportBeanList.size() == 0) {
				addWarningMessage("No Results Found");
			}else {
				for (ProjectUsersReportBean reportBean : projectUsersReportBeanList) {
					totalHours += reportBean.getTotalHours();
					totalMins += reportBean.getTotalMinutes().longValue();
				}
				if (totalMins == null) {
					totalMins = (long) 00;
				}
				if (totalMins >= 60) {
					int m = (int) (totalMins / 60);
					totalHours += m;
					totalMins -= m * 60;
				}
				totalHoursConverted = totalHours.intValue();
				searched = true;
				addInfoMessage("Report Generated");
			}
			
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void setUpReportBean() {
		reportBean.setFromDate(fromDate);
		reportBean.setToDate(toDate);
		getSessionUI().setReportBean(reportBean);
	}

	public void totalMinutesForUser() {
		try {
			totalHours = 0.0;
			totalMins = 0l;
			if (!getSessionUI().isDirector()) {
				reportBeanList = service.userDetailsMins(getSessionUI().getCompany(), companyUser, fromDate, toDate);
				for (ReportBean reportBean : reportBeanList) {
					totalHours += reportBean.getHours();
					totalMins += reportBean.getLongOne();
				}
				if (totalMins == null) {
					totalMins = (long) 00;
				}
				if (totalMins >= 60) {
					int m = (int) (totalMins / 60);
					totalHours += m;
					totalMins -= m * 60;
				}

				totalHoursConverted = totalHours.intValue();

				if (reportBeanList.size() == 0) {
					addInfoMessage("No Entries Found For Selected Criteria");
				} else {
					// addInfoMessage("Report Generated");
				}
			} else {
				reportBeanList = service.userDetailsMinsDirector(companyUser, fromDate, toDate);

				for (ReportBean reportBean : reportBeanList) {
					totalHours += reportBean.getHours();
					totalMins += reportBean.getLongOne();
				}
				if (totalMins == null) {
					totalMins = (long) 00;
				}
				if (totalMins >= 60) {
					int m = (int) (totalMins / 60);
					totalHours += m;
					totalMins -= m * 60;
				}

				totalHoursConverted = totalHours.intValue();

				if (reportBeanList.size() == 0) {
					addInfoMessage("No Entries Found For Selected Criteria");
				} else {
					// addInfoMessage("Report Generated");
				}
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public ReportBean getReportBean() {
		return reportBean;
	}

	public void setReportBean(ReportBean reportBean) {
		this.reportBean = reportBean;
	}

	public List<ReportBean> getReportBeanList() {
		return reportBeanList;
	}

	public void setReportBeanList(List<ReportBean> reportBeanList) {
		this.reportBeanList = reportBeanList;
	}

	public CompanyUsers getCompanyUser() {
		return companyUser;
	}

	public void setCompanyUser(CompanyUsers companyUser) {
		this.companyUser = companyUser;
	}

	public void gotoProjectsDetails() {
		try {
			setUpReportBean();
			String outcome = "/pages/reports/projectUsersWeek.jsf";
			super.redirect(outcome);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void gotoUserDetails() {
		try {
			// setUpReportBean();
			reportBean.setFromDate(fromDate);
			reportBean.setToDate(toDate);
			getSessionUI().setReportBean(reportBean);
			String outcome = "/pages/reports/userTimeSheetDetails.jsf";
			super.redirect(outcome);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void gotoUserByProject() {
		try {
			reportBean.setFromDate(fromDate);
			reportBean.setToDate(toDate);
			getSessionUI().setReportBean(reportBean);
			String outcome = "/pages/reports/projectUsers.jsf";
			super.redirect(outcome);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Date getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	public boolean isSearched() {
		return searched;
	}

	public void setSearched(boolean searched) {
		this.searched = searched;
	}

	public Double getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Double totalHours) {
		this.totalHours = totalHours;
	}

	public Long getTotalMins() {
		return totalMins;
	}

	public void setTotalMins(Long totalMins) {
		this.totalMins = totalMins;
	}

	public Integer getTotalHoursConverted() {
		return totalHoursConverted;
	}

	public void setTotalHoursConverted(Integer totalHoursConverted) {
		this.totalHoursConverted = totalHoursConverted;
	}

	public List<ProjectUsersReportBean> getProjectUsersReportBeanList() {
		return projectUsersReportBeanList;
	}

	public void setProjectUsersReportBeanList(List<ProjectUsersReportBean> projectUsersReportBeanList) {
		this.projectUsersReportBeanList = projectUsersReportBeanList;
	}

}
