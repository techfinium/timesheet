package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;

import haj.com.entity.ManagementStandard;
import haj.com.entity.datamodel.ManagementStandardDatamodel;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.ManagementStandardService;

@ManagedBean(name = "managementStandardUI")
@ViewScoped
public class ManagementStandardUI extends AbstractUI {

	private ManagementStandardService service = new ManagementStandardService();;
	private List<ManagementStandard> managementStandardList = null;
	private List<ManagementStandard> managementStandardfilteredList = null;
	private ManagementStandard managementStandard;
	private LazyDataModel<ManagementStandard> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all ManagementStandard and prepare a for a create
	 * of a new ManagementStandard
	 * 
	 * @author TechFinium
	 * @see ManagementStandard
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		managementStandardInfo();
	}

	/**
	 * Get all ManagementStandard for data table
	 * 
	 * @author TechFinium
	 * @see ManagementStandard
	 */
	public void managementStandardInfo() {
		dataModel = new ManagementStandardDatamodel();
	}

	/**
	 * Insert ManagementStandard into database
	 * 
	 * @author TechFinium
	 * @see ManagementStandard
	 */
	public void managementStandardInsert() {
		try {
			service.create(this.managementStandard);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			managementStandardInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Update ManagementStandard in database
	 * 
	 * @author TechFinium
	 * @see ManagementStandard
	 */
	public void managementStandardUpdate() {
		try {
			service.update(this.managementStandard);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			managementStandardInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete ManagementStandard from database
	 * 
	 * @author TechFinium
	 * @see ManagementStandard
	 */
	public void managementStandardDelete() {
		try {
			service.delete(this.managementStandard);
			prepareNew();
			managementStandardInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of ManagementStandard
	 * 
	 * @author TechFinium
	 * @see ManagementStandard
	 */
	public void prepareNew() {
		managementStandard = new ManagementStandard();
		managementStandard.setUser(getSessionUI().getUser());
	}
	

	

	/*
	 * public List<SelectItem> getManagementStandardDD() { List<SelectItem> l =new
	 * ArrayList<SelectItem>(); l.add(new
	 * SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
	 * managementStandardInfo(); for (ManagementStandard ug :
	 * managementStandardList) { // l.add(new SelectItem(ug.getUserGroupId(),
	 * ug.getUserGroupDesc())); } return l; }
	 */

	/**
	 * Complete.
	 *
	 * @param desc the desc
	 * @return the list
	 */
	public List<ManagementStandard> complete(String desc) {
		List<ManagementStandard> l = null;
		try {
			l = service.findByName(desc);
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}


	public List<ManagementStandard> getManagementStandardList() {
		return managementStandardList;
	}

	public void setManagementStandardList(List<ManagementStandard> managementStandardList) {
		this.managementStandardList = managementStandardList;
	}

	public ManagementStandard getManagementStandard() {
		return managementStandard;
	}

	public void setManagementStandard(ManagementStandard managementStandard) {
		this.managementStandard = managementStandard;
	}

	public List<ManagementStandard> getManagementStandardfilteredList() {
		return managementStandardfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @param managementStandardfilteredList the new
	 *                                         managementStandardfilteredList list
	 * @see ManagementStandard
	 */
	public void setManagementStandardfilteredList(List<ManagementStandard> managementStandardfilteredList) {
		this.managementStandardfilteredList = managementStandardfilteredList;
	}

	public LazyDataModel<ManagementStandard> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<ManagementStandard> dataModel) {
		this.dataModel = dataModel;
	}

}
