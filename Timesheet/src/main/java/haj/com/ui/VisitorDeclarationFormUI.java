package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;

import haj.com.entity.Address;
import haj.com.entity.Visitor;
import haj.com.entity.VisitorDeclarationForm;
import haj.com.entity.datamodel.VisitorDeclarationFormDatamodel;
import haj.com.entity.enums.VisitorTypeEnum;
import haj.com.exception.ValidationException;
import haj.com.framework.AbstractUI;
import haj.com.service.VisitorDeclarationFormService;

@ManagedBean(name = "visitorDeclarationFormUI")
@ViewScoped
public class VisitorDeclarationFormUI extends AbstractUI {

	private VisitorDeclarationFormService service = new VisitorDeclarationFormService();
	private List<VisitorDeclarationForm> visitorDeclarationFormList = null;
	private List<VisitorDeclarationForm> visitorDeclarationFormfilteredList = null;
	private VisitorDeclarationForm visitorDeclarationForm;
	private LazyDataModel<VisitorDeclarationForm> dataModel;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all VisitorDeclarationForm and prepare a for a
	 * create of a new VisitorDeclarationForm
	 * 
	 * @author TechFinium
	 * @see VisitorDeclarationForm
	 * @throws Exception the exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		visitorDeclarationFormInfo();
	}

	/**
	 * Get all VisitorDeclarationForm for data table
	 * 
	 * @author TechFinium
	 * @see VisitorDeclarationForm
	 */
	public void visitorDeclarationFormInfo() {
		dataModel = new VisitorDeclarationFormDatamodel();
	}
	
	public void populateVisitorAddresses() throws Exception {
		service.populateVisitorAddresses(this.visitorDeclarationForm);
	}
	

	/**
	 * Insert VisitorDeclarationForm into database
	 * 
	 * @author TechFinium
	 * @see VisitorDeclarationForm
	 */

	public void visitorDeclarationFormInsert() {
		try {
			service.createVisitorDeclarationForm(this.visitorDeclarationForm);
			prepareNew();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			visitorDeclarationFormInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}
	

	/**
	 * Update VisitorDeclarationForm in database
	 * 
	 * @author TechFinium
	 * @see VisitorDeclarationForm
	 */
	public void visitorDeclarationFormUpdate() {
		try {
			service.update(this.visitorDeclarationForm);
			addInfoMessage(super.getEntryLanguage("update.successful"));
			visitorDeclarationFormInfo();
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete VisitorDeclarationForm from database
	 * 
	 * @author TechFinium
	 * @see VisitorDeclarationForm
	 */
	public void visitorDeclarationFormDelete() {
		try {
			service.delete(this.visitorDeclarationForm);
			prepareNew();
			visitorDeclarationFormInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Create new instance of VisitorDeclarationForm
	 * 
	 * @author TechFinium
	 * @see VisitorDeclarationForm
	 */
	public void prepareNew() {
		visitorDeclarationForm = new VisitorDeclarationForm();
		
	}
	
	public void perpareVisitor() throws Exception {
			visitorDeclarationForm.setVisitor(new Visitor());
			visitorDeclarationForm.getVisitor().setPhysicalAddress(new Address());
			visitorDeclarationForm.getVisitor().setCompanyAddress(new Address());
	}
	

	/*
	 * public List<SelectItem> getVisitorDeclarationFormDD() { List<SelectItem> l
	 * =new ArrayList<SelectItem>(); l.add(new
	 * SelectItem(Long.valueOf(-1L),"-------------Select-------------"));
	 * visitorDeclarationFormInfo(); for (VisitorDeclarationForm ug :
	 * visitorDeclarationFormList) { // l.add(new SelectItem(ug.getUserGroupId(),
	 * ug.getUserGroupDesc())); } return l; }
	 */

	/**
	 * Complete.
	 *
	 * @param desc the desc
	 * @return the list
	 */
	public List<VisitorDeclarationForm> complete(String desc) {
		List<VisitorDeclarationForm> l = null;
		try {
			l = service.findByName(desc);
		} catch (ValidationException e) {
			addErrorMessage(getEntryLanguage(e.getMessage()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public List<VisitorDeclarationForm> getVisitorDeclarationFormList() {
		return visitorDeclarationFormList;
	}

	public void setVisitorDeclarationFormList(List<VisitorDeclarationForm> visitorDeclarationFormList) {
		this.visitorDeclarationFormList = visitorDeclarationFormList;
	}

	public VisitorDeclarationForm getVisitorDeclarationForm() {
		return visitorDeclarationForm;
	}

	public void setVisitorDeclarationForm(VisitorDeclarationForm visitorDeclarationForm) {
		this.visitorDeclarationForm = visitorDeclarationForm;
	}

	public List<VisitorDeclarationForm> getVisitorDeclarationFormfilteredList() {
		return visitorDeclarationFormfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @param visitorDeclarationFormfilteredList the new
	 *                                           visitorDeclarationFormfilteredList
	 *                                           list
	 * @see VisitorDeclarationForm
	 */
	public void setVisitorDeclarationFormfilteredList(List<VisitorDeclarationForm> visitorDeclarationFormfilteredList) {
		this.visitorDeclarationFormfilteredList = visitorDeclarationFormfilteredList;
	}

	public LazyDataModel<VisitorDeclarationForm> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<VisitorDeclarationForm> dataModel) {
		this.dataModel = dataModel;
	}

	public VisitorDeclarationFormService getService() {
		return service;
	}

	public void setService(VisitorDeclarationFormService service) {
		this.service = service;
	}
}
