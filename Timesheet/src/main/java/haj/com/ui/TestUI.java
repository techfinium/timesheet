package haj.com.ui;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.CompanyUsers;
import haj.com.entity.Timesheet;
import haj.com.entity.TimesheetDetails;
import haj.com.entity.Users;
import haj.com.framework.AbstractUI;
import haj.com.service.CompanyUsersService;
import haj.com.service.CurrencyService;
import haj.com.service.NotificationService;
import haj.com.service.TimesheetDetailsService;
import haj.com.service.TimesheetService;
import haj.com.service.UsersService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "testUI")
@ViewScoped
public class TestUI extends AbstractUI {

	private TimesheetService timesheetService = new TimesheetService();
	private NotificationService notificationService = new NotificationService();
	

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
	}

	public void resetDataBase() {
		try {
			addInfoMessage("Datase reset complete.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void runShedule() {
		try {
			timesheetService.genTimesheetForMonth();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testMail() {
		try {
			logger.info("START test()");
			//GenericUtility.sendMail("jonathan@hlspro.com", "Timesheet Subject", "Timesheet body", false, null);
			addInfoMessage(super.getEntryLanguage("mail"));
			logger.info("END test()");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void mail() {
		try {
			logger.info("START mail()");
			GenericUtility.sendMail("hendrik@hlspro.com", "Sample TechFINIUM LTD Q&A email template",
					"Hi<p>What do you guys think of this email template</p><br/>Thanks<br/>Hendrik ");
			logger.info("END mail()");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void testNotify(){
		try {
			notificationService.run();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void populateRates(){
		try {
			CurrencyService.populateRates();
			addInfoMessage("Populate Rates");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void populateUserLink(){
		try {
			CompanyUsersService companyUsersService = new CompanyUsersService();
			TimesheetDetailsService timesheetDetailsService = new TimesheetDetailsService();
			UsersService usersService = new UsersService();
			List<CompanyUsers> allcompanyUsers = companyUsersService.allCompanyUsers();
			for (CompanyUsers companyUsers : allcompanyUsers) {
				if (companyUsers.getUsers() != null) {
					Users mainUser = usersService.findByKey(companyUsers.getUsers().getUid());
					if (mainUser != null) {
						List<Timesheet> timesheets = timesheetService.findByCompanyUser(companyUsers);
						for (Timesheet timesheet : timesheets) {
							List<TimesheetDetails> timesheetList = timesheetDetailsService.findByTimesheet(timesheet);
							for (TimesheetDetails timesheetDetails : timesheetList) {
								for (TimesheetDetails mainEntry : timesheetDetails.getTimesheetDetailList()) {
									mainEntry.setUser(mainUser);
									timesheetDetailsService.update(mainEntry);
								}
							}
						}
					}
				}
			}
			addInfoMessage("Action Complete");
		} catch (Exception e) {
			logger.fatal(e);
		}
	}
	
	
	public void testDate(){
		try {
			TimesheetDetailsService service = new TimesheetDetailsService();
			TimesheetDetails ts = new TimesheetDetails();
			Timesheet timesheet = new Timesheet();
			timesheet.setId(167l);
			
			ts.setTimesheet(timesheet);
			
			Date today = new Date();
			Date fromDateTime = GenericUtility.getStartOfDay(today);
			Date toDateTime = GenericUtility.getEndOfDay(today);
			
			ts.setFromDateTime(fromDateTime);
			ts.setToDateTime(toDateTime);
			
			service.create(ts);
			System.out.println(ts.getId() + " " + ts.getFromDateTime() + " " + ts.getToDateTime());
			
			TimesheetDetails ts2 = service.findByKey(ts.getId());
			
			System.out.println(ts2.getId() + " " + ts2.getFromDateTime() + " " + ts2.getToDateTime());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	

}
