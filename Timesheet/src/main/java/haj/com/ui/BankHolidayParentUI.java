package haj.com.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.constants.HAJConstants;
import haj.com.entity.BankHolidayChild;
import haj.com.entity.BankHolidayParent;
import haj.com.framework.AbstractUI;
import haj.com.service.BankHolidayParentService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "bankholidayparentUI")
@ViewScoped
public class BankHolidayParentUI extends AbstractUI {

	private BankHolidayParentService service;
	private List<BankHolidayParent> bankholidayparentList = null;
	private List<BankHolidayParent> bankholidayparentfilteredList = null;
	private BankHolidayParent bankholidayparent = null;
	private BankHolidayChild bankHolidayChild;
	private Date maxDate;
	private SimpleDateFormat STF = new SimpleDateFormat("dd MM yyyy");

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		service = new BankHolidayParentService();
		prepareNewBankHolidayParent();
		bankholidayparentInfo();
		maxDate = GenericUtility.lastDateOfCurrentYear();
	}

	public void bankholidayparentInfo() {
		try {
			bankholidayparentList = service.allBankHolidayParent();
			for (BankHolidayParent bankHolidayParent : bankholidayparentList) {
				for (BankHolidayChild bankHolidayChild : bankHolidayParent.getBankHolidayChildrenList()) {
					/*System.out.println(bankHolidayChild.getRepeatDates());*/

				}
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void createChild() {
		try {
			service.create(bankHolidayChild);
			//System.out.println(bankHolidayChild.getBankHoliday());
			bankholidayparent = service.findByKey(bankholidayparent.getBankHolidayYear());
			bankholidayparentInfo();
			initBankHolidayChild();
		} catch (org.hibernate.exception.ConstraintViolationException e0)		 {
			addErrorMessage("You have already inserted a holiday for this date");
		}
		 catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void initBankHolidayChild() {
		try {
			bankHolidayChild = new BankHolidayChild();
			bankHolidayChild.setBankHolidayParent(bankholidayparent);
			bankHolidayChild.setBankHoliday(STF.parse("01 01 " + bankholidayparent.getBankHolidayYear()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void bankholidayparentInsert() {
		try {
			if (this.bankholidayparent.getBankHolidayYear().intValue()!=-1) {
			 service.createRepeat(this.bankholidayparent);
			 addInfoMessage("Insert successful");
			 bankholidayparentInfo();
			}
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void bankholidayparentDelete() {
		try {
			service.delete(this.bankholidayparent);
			bankholidayparentInfo();
			super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	
	public void bankholidaychildtDelete() {
		try {
			service.delete(this.bankHolidayChild);
			bankholidayparentInfo();
			bankholidayparent.setBankHolidayYear(null);
			super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<BankHolidayParent> getBankHolidayParentList() {
		return bankholidayparentList;
	}

	public void setBankHolidayParentList(List<BankHolidayParent> bankholidayparentList) {
		this.bankholidayparentList = bankholidayparentList;
	}

	public BankHolidayParent getBankholidayparent() {
		return bankholidayparent;
	}

	public void setBankholidayparent(BankHolidayParent bankholidayparent) {
		this.bankholidayparent = bankholidayparent;
	}

	public void prepareNewBankHolidayParent() {
		bankholidayparent = new BankHolidayParent();
	}

	public void bankholidayparentUpdate() {
		try {
			service.update(this.bankholidayparent);
			addInfoMessage("Update successful");
			bankholidayparentInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<SelectItem> getBankHolidayParentDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Integer.valueOf(-1), "-------------Select-------------"));
		// bankholidayparentInfo();
		int year = Integer.valueOf(HAJConstants.sdfYYYY.format(new Date()));
		for (int i = year; i < (year + 10); i++) {
			l.add(new SelectItem(i, "" + i));
		}

		return l;
	}

	public List<BankHolidayParent> getBankHolidayParentfilteredList() {
		return bankholidayparentfilteredList;
	}

	public void setBankHolidayParentfilteredList(List<BankHolidayParent> bankholidayparentfilteredList) {
		this.bankholidayparentfilteredList = bankholidayparentfilteredList;
	}

	public List<BankHolidayParent> complete(String desc) {
		List<BankHolidayParent> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public BankHolidayChild getBankHolidayChild() {
		return bankHolidayChild;
	}

	public void setBankHolidayChild(BankHolidayChild bankHolidayChild) {
		this.bankHolidayChild = bankHolidayChild;
	}

	public Date getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(Date maxDate) {
		this.maxDate = maxDate;
	}
}
