package  haj.com.ui;

import java.io.ByteArrayInputStream;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import haj.com.framework.AbstractUI;



@ManagedBean(name = "viewerUI")
@SessionScoped
public class ViewerUI extends AbstractUI {

	private StreamedContent content;

    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {

	}

	 public void onPrerender(ComponentSystemEvent event) {
		 try {
			 if (getSessionUI().getSelDoc()!=null) {
			  content = new DefaultStreamedContent(new ByteArrayInputStream(getSessionUI().getSelDoc().getFileContent()), "application/pdf"); 
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
	 }
	
	


		public StreamedContent getContent() {
			return content;
		}

		public void setContent(StreamedContent content) {
			this.content = content;
		}
	
}
