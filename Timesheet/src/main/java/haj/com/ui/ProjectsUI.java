package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.Company;
import haj.com.entity.Projects;
import haj.com.entity.UserProjects;
import haj.com.entity.Users;
import haj.com.framework.AbstractUI;
import haj.com.service.CompanyService;
import haj.com.service.ProjectsService;
import haj.com.service.UserProjectsService;
import haj.com.service.UsersService;

@ManagedBean(name = "projectsUI")
@ViewScoped
public class ProjectsUI extends AbstractUI {

	// entity layers
	private Projects projects = null;
	private Company selectedCompany;
	private UserProjects userProjects = null;
	private Users selectedUser = null;
	
	// service layers
	private ProjectsService service = new ProjectsService();
	private CompanyService companyService = new CompanyService();
	private UserProjectsService userProjectsService = new UserProjectsService();
	private UsersService usersService = new UsersService();
	
	// lists
	private List<Projects> companyProjectsList = new ArrayList<>();
	private List<Projects> companyProjectsFilteredList = null;
	private List<Projects> projectsList = null;
	private List<Projects> projectsfilteredList = null;
	private List<Company> companyList  = null;
	private List<Projects> projectsCodeList = null;
	private List<UserProjects> userProjectsList = null;
	private List<UserProjects> userProjectsFilteredList = null;
	private List<Projects> sessionUserProjects = new ArrayList<Projects>();
	private List<Projects> sessionUserFilteredProjects = new ArrayList<Projects>();
	
	// boolean
	private Boolean searchCompleted;
	private Boolean viewProjects;
	private boolean active;
	private boolean overhead;
	
	// string
	private String projectStatus;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	
	
	private void runInit() throws Exception {
		prepareNewProjects();
		projectsInfo();
		companyInfo();
		userProjects = new UserProjects();
		active = false;
		overhead = false;
		searchCompleted = false;
		viewProjects = false;
		projectStatus = "";
//		if (getSessionUI().isDirector()) {
//			sessionUserProjects = service.allProjects();
//		} else if (getSessionUI().isAdmin()) {
//			sessionUserProjects = service.allProjects();
//		} else {
//			sessionUserProjects = userProjectsService.findProjectsForUser(getSessionUI().getUser());
//		}
		
	}

	private void companyInfo() {
		try {
			companyList = companyService.allCompany();
			companyProjectsList = new ArrayList<>();
			companyProjectsFilteredList = new ArrayList<>();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void viewCompanyProjects(){
		try {
			companyProjectsList = service.findByCompanyReport(selectedCompany);
			searchCompleted = true;
			addInfoMessage(super.getEntryLanguage("search.completed"));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void projectsInfo() {
		try {
			if (super.getParameter("page", false) != null) {
				if (getSessionUI().isDirector()) {
					projectsList = service.allProjects();
				}else {
					projectsList= service.findByCompanyReport(getSessionUI().getCompany());
				}
			}else {
				if (!getSessionUI().isCustomerAdmin()) {
					projectsList = service.allProjectsActive();
				} else {
					projectsList = service.findByCompany(getSessionUI().getCompany());
				}
				if (getSessionUI().isAdmin()) {
					projectsList = service.allProjects();
				}
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void projectsInsert() {
		try {
			projectsCodeList = service.findByCodeNameCompany(this.projects.getCode(),
					this.projects.getCompany().getId());
			// projectsCodeList =
			// service.findByCodeName(this.projects.getCode());
			if (projectsCodeList.size() <= 0) {
				this.projects.setActive(active);
				this.projects.setOverhead(overhead);
				searchCompleted = false;
				runClientSideUpdate("resultsForm");
				service.create(this.projects);
				addInfoMessage(super.getEntryLanguage("insert.successful"));
				projectsInfo();
			} else {
				addWarningMessage(super.getEntryLanguage("insert.unique.code"));
			}

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void projectsDelete() {
		try {
			service.delete(this.projects);
			searchCompleted = false;
			runClientSideUpdate("resultsForm");
			projectsInfo();
			super.addWarningMessage(super.getEntryLanguage("row.deleted"));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Projects> getProjectsList() {
		return projectsList;
	}

	public void setProjectsList(List<Projects> projectsList) {
		this.projectsList = projectsList;
	}

	public Projects getProjects() {
		return projects;
	}

	public void setProjects(Projects projects) {
		this.projects = projects;
	}

	public void prepareNewProjects() {
		projects = new Projects();
		projects.setCompany(super.getSessionUI().getCompany());
	}

	public void projectsUpdate() {
		try {
			// projectsCodeList =
			// service.findByCodeNameCompany(this.projects.getCode(),
			// this.projects.getCompany().getId());
			// if (projectsCodeList.size() <= 0) {
			// }else {
			// addWarningMessage("Project Code Already Exists. Please Insert A
			// Unique Code.");
			// }
			if (active == false) {
				this.projects.setInactiveDate(new Date());
			}
			
			service.update(this.projects);
			searchCompleted = false;
			runClientSideUpdate("resultsForm");
			addInfoMessage(super.getEntryLanguage("update.successful"));
			projectsInfo();

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<SelectItem> getProjectsDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), (super.getEntryLanguage("select.drop.down"))));
		projectsInfo();
		for (Projects ug : projectsList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<Projects> getProjectsfilteredList() {
		return projectsfilteredList;
	}

	public void setProjectsfilteredList(List<Projects> projectsfilteredList) {
		this.projectsfilteredList = projectsfilteredList;
	}

	public List<Projects> complete(String desc) {
		List<Projects> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
	
	public void showProjectUsers(){
		try {
			viewProjects = true;
			userProjectsList = userProjectsService.findByProject(projects);
			if (projects.getActive()) {
				projectStatus = super.getEntryLanguage("active");
			}else {
				projectStatus = super.getEntryLanguage("inactive");
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 *  Prepares insert of new project user
	 */
	public void prepNewUserProject(){
		try {
			userProjects = new UserProjects();
			userProjects.setProject(projects);
			userProjects.setAccessToProject(projects.getActive());
			selectedUser = new Users();
			runClientSideExecute("PF('dlgAddUsers').show()");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		
	}
	
	public void prepUpdateProject(){
		if (viewProjects) {
			viewProjects = false;
			runClientSideUpdate("mainForm");
		}
	}
	
	/**
	 *  Creates new users project
	 */
	public void insertNewUserProject(){
		try {
			if (selectedUser == null && selectedUser.getUid() == null) {
				throw new Exception("Please select a valid user");
			}
			userProjects.setUser(selectedUser);
			userProjectsService.create(userProjects);
			addInfoMessage(super.getEntryLanguage("user.assigned.to.project.heading"));
			showProjectUsers();
			runClientSideExecute("PF('dlgAddUsers').hide()");
		} catch (Exception e) {
			logger.fatal(e);
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	public void updateUsersProject(){
		try {
			userProjectsService.create(userProjects);
			userProjects = new UserProjects();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			runClientSideExecute("PF('dlgUpdProjectAccess').hide()");
			showProjectUsers();
		} catch (Exception e) {
			logger.fatal(e);
			addErrorMessage(e.getMessage(), e);
		}
	}
	
	/**
	 * Finds all users (except directors and admins) to be assigned to company.
	 * @param name
	 * @return
	 */
	public List<Users> autoComplete(String name) {
		List<Users> allUsers = null;
		try {
			allUsers = usersService.findByNameLastnameUsersProject(name, projects);
		} catch (Exception e) {
			logger.fatal(e);
			addErrorMessage(e.getMessage(), e);
		}
		return allUsers;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isOverhead() {
		return overhead;
	}

	public void setOverhead(boolean overhead) {
		this.overhead = overhead;
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	public Company getSelectedCompany() {
		return selectedCompany;
	}

	public void setSelectedCompany(Company selectedCompany) {
		this.selectedCompany = selectedCompany;
	}

	public List<Projects> getCompanyProjectsList() {
		return companyProjectsList;
	}

	public void setCompanyProjectsList(List<Projects> companyProjectsList) {
		this.companyProjectsList = companyProjectsList;
	}

	public List<Projects> getCompanyProjectsFilteredList() {
		return companyProjectsFilteredList;
	}

	public void setCompanyProjectsFilteredList(List<Projects> companyProjectsFilteredList) {
		this.companyProjectsFilteredList = companyProjectsFilteredList;
	}

	public Boolean getSearchCompleted() {
		return searchCompleted;
	}

	public void setSearchCompleted(Boolean searchCompleted) {
		this.searchCompleted = searchCompleted;
	}

	public UserProjects getUserProjects() {
		return userProjects;
	}

	public void setUserProjects(UserProjects userProjects) {
		this.userProjects = userProjects;
	}

	public Boolean getViewProjects() {
		return viewProjects;
	}

	public void setViewProjects(Boolean viewProjects) {
		this.viewProjects = viewProjects;
	}

	public List<UserProjects> getUserProjectsFilteredList() {
		return userProjectsFilteredList;
	}

	public void setUserProjectsFilteredList(List<UserProjects> userProjectsFilteredList) {
		this.userProjectsFilteredList = userProjectsFilteredList;
	}

	public List<UserProjects> getUserProjectsList() {
		return userProjectsList;
	}

	public void setUserProjectsList(List<UserProjects> userProjectsList) {
		this.userProjectsList = userProjectsList;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public Users getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(Users selectedUser) {
		this.selectedUser = selectedUser;
	}



	public List<Projects> getSessionUserProjects() {
		return sessionUserProjects;
	}



	public void setSessionUserProjects(List<Projects> sessionUserProjects) {
		this.sessionUserProjects = sessionUserProjects;
	}



	public List<Projects> getSessionUserFilteredProjects() {
		return sessionUserFilteredProjects;
	}



	public void setSessionUserFilteredProjects(List<Projects> sessionUserFilteredProjects) {
		this.sessionUserFilteredProjects = sessionUserFilteredProjects;
	}

}
