package haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.Company;
import haj.com.entity.ExpenseForm;
import haj.com.entity.Projects;
import haj.com.framework.AbstractUI;
import haj.com.service.ExpenseFormService;

@ManagedBean(name = "expenseformUI")
@ViewScoped
public class ExpenseFormUI extends AbstractUI {
	private LazyDataModel<Company> companyDataModel;
	private List<Company> companyList = new ArrayList();
	private List<Projects> projectsList = new ArrayList();
	private ExpenseFormService expenseFormService = new ExpenseFormService();
	private Company company;
	private Projects projects;
	private ExpenseForm expenseForm = null;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		companies();
		prepCompany();

	}

	public void companies() {
		companyDataModel = new LazyDataModel<Company>() {
			private static final long serialVersionUID = 1L;
			private List<Company> retorno = new ArrayList<>();

			@Override
			public List<Company> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = expenseFormService.allcompanies(Company.class, first, pageSize, sortField, sortOrder, filters);
					companyDataModel.setRowCount(expenseFormService.countAllcompanies(Company.class));
				} catch (Exception e) {
					addErrorMessage(e.getMessage(), e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(Company obj) {
				return obj.getId();
			}

			@Override
			public Company getRowData(String rowKey) {
				for (Company obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}
		};
	}

	public void toggleCompany(Company company) {
		this.company = company;
		try {
			projectsList = expenseFormService.projectsByCompany(this.company);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		prepProjects();
	}

	public void setParameters() {
		company.setProjects(projects);
		super.setParameter("company", company, true);
		ajaxRedirect("/admin/expensedetails.jsf");
	}

	public void calcPerDiem() {
			if (expenseForm.getDailyRate() != null && expenseForm.getDayNumber() != null) {
				expenseForm.setPerDiem(expenseForm.getDailyRate().getValue() * expenseForm.getDayNumber());
			}
	}
		

	public void prepCompany() {
		company = new Company();
	}

	public void prepProjects() {
		projects = new Projects();
	}

	public LazyDataModel<Company> getCompanyDataModel() {
		return companyDataModel;
	}

	public void setCompanyDataModel(LazyDataModel<Company> companyDataModel) {
		this.companyDataModel = companyDataModel;
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	public List<Projects> getProjectsList() {
		return projectsList;
	}

	public void setProjectsList(List<Projects> projectsList) {
		this.projectsList = projectsList;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Projects getProjects() {
		return projects;
	}

	public void setProjects(Projects projects) {
		this.projects = projects;
	}

}
