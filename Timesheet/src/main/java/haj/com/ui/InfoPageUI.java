package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Projects;
import haj.com.entity.StatementOfWork;
import haj.com.entity.Tasks;
import haj.com.framework.AbstractUI;
import haj.com.service.ProjectsService;
import haj.com.service.StatementOfWorkService;
import haj.com.service.TasksService;
import haj.com.service.UserProjectsService;

@ManagedBean(name = "infoPageUI")
@ViewScoped
public class InfoPageUI extends AbstractUI {

	// Service Layers
	private ProjectsService projectsService = new ProjectsService();
	private UserProjectsService userProjectsService = new UserProjectsService();
	private TasksService tasksService = new TasksService();
	private StatementOfWorkService statementOfWorksService = new StatementOfWorkService();

	// Lists
	private List<Projects> sessionUserProjects = new ArrayList<Projects>();
	private List<Projects> sessionUserFilteredProjects;
	private List<StatementOfWork> statementOfWorksList = new ArrayList<StatementOfWork>();
	private List<StatementOfWork> statementOfWorksFilteredList;
	private List<Tasks> tasksList = new ArrayList<Tasks>();
	private List<Tasks> tasksfilteredList;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		populateProjectsList();
		populateSowList();
		populateTasksList();
	}

	private void populateProjectsList() {
		try {
			if (getSessionUI().isDirector() || getSessionUI().isAdmin()) {
				sessionUserProjects = projectsService.allProjects();
			} else {
				sessionUserProjects = userProjectsService.findProjectsForUser(getSessionUI().getUser());
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void populateSowList() {
		try {
			if (getSessionUI().isDirector() || getSessionUI().isAdmin()) {
				statementOfWorksList = statementOfWorksService.allStatementOfWork();
			} else {
				for (Projects project : sessionUserProjects) {
					statementOfWorksList.addAll(
							statementOfWorksService.findByProjectActive(project, getSessionUI().getUser(), false));
				}
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void populateTasksList() {
		try {
			if (getSessionUI().isDirector() || getSessionUI().isAdmin()) {
				tasksList = tasksService.allTasks();
			} else {
				tasksList = tasksService.allTasks();
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Projects> getSessionUserProjects() {
		return sessionUserProjects;
	}

	public void setSessionUserProjects(List<Projects> sessionUserProjects) {
		this.sessionUserProjects = sessionUserProjects;
	}

	public List<Projects> getSessionUserFilteredProjects() {
		return sessionUserFilteredProjects;
	}

	public void setSessionUserFilteredProjects(List<Projects> sessionUserFilteredProjects) {
		this.sessionUserFilteredProjects = sessionUserFilteredProjects;
	}

	public List<Tasks> getTasksList() {
		return tasksList;
	}

	public void setTasksList(List<Tasks> tasksList) {
		this.tasksList = tasksList;
	}

	public List<Tasks> getTasksfilteredList() {
		return tasksfilteredList;
	}

	public void setTasksfilteredList(List<Tasks> tasksfilteredList) {
		this.tasksfilteredList = tasksfilteredList;
	}

	public List<StatementOfWork> getStatementOfWorksList() {
		return statementOfWorksList;
	}

	public void setStatementOfWorksList(List<StatementOfWork> statementOfWorksList) {
		this.statementOfWorksList = statementOfWorksList;
	}

	public List<StatementOfWork> getStatementOfWorksFilteredList() {
		return statementOfWorksFilteredList;
	}

	public void setStatementOfWorksFilteredList(List<StatementOfWork> statementOfWorksFilteredList) {
		this.statementOfWorksFilteredList = statementOfWorksFilteredList;
	}

}
