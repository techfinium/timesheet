package haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.Address;
import haj.com.entity.Company;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Doc;
import haj.com.entity.Notes;
import haj.com.entity.Users;
import haj.com.entity.UsersCost;
import haj.com.entity.UsersCostHist;
import haj.com.entity.UsersLevelEnum;
import haj.com.entity.UsersStatusEnum;
import haj.com.entity.enums.EmployeeTypeEnum;
import haj.com.exception.NotifyUserException;
import haj.com.framework.AbstractUI;
import haj.com.service.AddressService;
import haj.com.service.CompanyUsersService;
import haj.com.service.CurrencyService;
import haj.com.service.LogonService;
import haj.com.service.NotesService;
import haj.com.service.RegisterService;
import haj.com.service.UsersCostService;
import haj.com.service.UsersService;

@ManagedBean(name = "companyAdminUsersUI")
@ViewScoped
public class CompanyAdminUsersUI extends AbstractUI {

	private UsersService service = new UsersService();
	private AddressService addressService = new AddressService();
	private CompanyUsersService companyUsersService = new CompanyUsersService();
	private List<CompanyUsers> users;
	private List<CompanyUsers> usersFiltered;
	private Users user;
	private Users userSelected;
	private RegisterService registerService = new RegisterService();
	private String password;
	private UsersLevelEnum level;
	private CompanyUsers companyUsers;
	private LazyDataModel<CompanyUsers> dataModelCompanyUser;
	private LogonService logonService = new LogonService();
	private Company company;
	private boolean showUsersCost;
	private UsersCost userscost = null;
	private UsersCostService usersCostService = new UsersCostService();
	private List<UsersCostHist> costHistory;
	private List<UsersStatusEnum> usersStatusList;

	// Ashton
	private Address homeAddress;
	private AddressService homeAddressService = new AddressService();
	private List<Users> userList;
	private List<Users> userFilteredList;
	private String niNumber;
	private EmployeeTypeEnum empType;

	private String street;
	private String area;
	private String state;
	private String country;
	private String postCode;

	// Notes and uploads
	@ManagedProperty(value = "#{uploadDocUI}")
	private UploadDocUI uploadDocUI;
	private Doc doc;
	private Notes note;
	private NotesService notesService = new NotesService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		if (getSessionUI().getCompanyUsers().getLevel() == UsersLevelEnum.Admin) {
			this.users = companyUsersService.allCompanyUsers();
			prepareNewUsers();
			prepDoc();
			prepNote();
			prepUserDataModel();
			prepUserStatus();
		}
		showUsersCost = false;
	}
	
	public void prepUserStatus(){
		try {
			usersStatusList = new ArrayList<UsersStatusEnum>();
			usersStatusList.add(UsersStatusEnum.Active);
			usersStatusList.add(UsersStatusEnum.InActive);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepUserDataModel() {
		dataModelCompanyUser = new LazyDataModel<CompanyUsers>() {

			private static final long serialVersionUID = 1L;
			private List<CompanyUsers> retorno = new ArrayList<CompanyUsers>();

			@Override
			public List<CompanyUsers> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = companyUsersService.allInCompanyUsers(CompanyUsers.class, first, pageSize, sortField, sortOrder, filters);
					dataModelCompanyUser.setRowCount(companyUsersService.count(CompanyUsers.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(CompanyUsers obj) {
				return obj.getId();
			}

			@Override
			public CompanyUsers getRowData(String rowKey) {
				for (CompanyUsers obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public void prepareNewUsers() {
		user = new Users();
		this.companyUsers = new CompanyUsers();
		this.companyUsers.setActive(Boolean.TRUE);
		this.companyUsers.setUsers(new Users());
		this.companyUsers.getUsers().setStatus(UsersStatusEnum.Active);
		this.homeAddress = new Address();
		this.homeAddress.setUser(user);
	}

	public List<CompanyUsers> getUsers() {
		return users;
	}

	public void setUsers(List<CompanyUsers> users) {
		this.users = users;
	}

	public List<CompanyUsers> getUsersFiltered() {
		return usersFiltered;
	}

	public void setUsersFiltered(List<CompanyUsers> usersFiltered) {
		this.usersFiltered = usersFiltered;
	}

	public void register() {
		try {
			if (company == null) {
				company = getSessionUI().getCompany();
			}
			this.user.setPassword(password);
			registerService.register(company, this.user, level, getSessionUI().getUser());
			AddressService addressService = new AddressService();
			addressService.createUpdate(homeAddress);
			runInit();
			addInfoMessage("User succesfully registered. The users will receice an email shortly with further login details.");
		} catch (NotifyUserException nu) {
			addInfoMessage(nu.getMessage());
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepUpdate() {
		try {
			List<Address> addressCheck = new ArrayList<>();
			empType = companyUsers.getUsers().getEmployeeType();
			addressCheck = addressService.byUserCheck(companyUsers.getUsers());
			if (addressCheck.size() == 0) {
				homeAddress = new Address();
				homeAddress.setUser(companyUsers.getUsers());
			} else {
				homeAddress = addressService.byUser(companyUsers.getUsers());
			}
			userSelected = new Users();
			userSelected = companyUsers.getUsers();
		} catch (Exception e) {
			logger.fatal(e);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UsersLevelEnum getLevel() {
		return level;
	}

	public void setLevel(UsersLevelEnum level) {
		this.level = level;
	}

	public List<SelectItem> getUserLevelDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (UsersLevelEnum selectItem : UsersLevelEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public void userUpdate() {
		try {
			if (companyUsers.getCompany() == null) {
				companyUsers.setCompany(getSessionUI().getCompany());
			}
			if (empType != null) {
				companyUsers.getUsers().setEmployeeType(empType);
			}
			service.update(userSelected);
			service.updateEndUser(this.companyUsers);
			
			addressService.createUpdate(homeAddress);
			this.users = companyUsersService.allCompanyUsers();
			addInfoMessage("User information updated.");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}

	}

	public CompanyUsers getCompanyUsers() {
		return companyUsers;
	}

	public void setCompanyUsers(CompanyUsers companyUsers) {
		this.companyUsers = companyUsers;
	}

	public void newPassword() {
		try {
			logonService.notifyUserNewPasswordEmail(this.companyUsers.getUsers().getEmail());
			addInfoMessage("A new password has been mailed to " + this.companyUsers.getUsers().getFirstName() + " " + this.companyUsers.getUsers().getLastName());
			super.runClientSideExecute("PF('confirmation').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void showCost(SelectEvent event) {
		try {
			showUsersCost = true;
			prepareNew();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public boolean isShowUsersCost() {
		return showUsersCost;
	}

	public void setShowUsersCost(boolean showUsersCost) {
		this.showUsersCost = showUsersCost;
	}

	public UsersCost getUserscost() {
		return userscost;
	}

	public void setUserscost(UsersCost userscost) {
		this.userscost = userscost;
	}

	/**
	 * Create new instance of UsersCost
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public void prepareNew() {
		try {
			this.companyUsers = companyUsersService.findByKey(this.companyUsers.getId());
			userscost = new UsersCost();
			userscost.setUsers(this.companyUsers.getUsers());
			userscost.setEffectiveDate(new java.util.Date());
			userscost.setLocale(Locale.UK);
			userscost.setExhangeRate(1.0);
			userscost.setUpdatedBy(getSessionUI().getUser());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Insert UsersCost into database
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public void userscostInsert() {
		try {
			usersCostService.create(this.userscost);
			prepareNew();
			addInfoMessage("Cost updated successfully");
			this.companyUsers = companyUsersService.findByKey(this.companyUsers.getId());
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void onCountryChange() {
		try {
			userscost.setExhangeRate(CurrencyService.exhangeRateFromToBase(userscost.getLocale()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void prepUpd() {
		try {
			userscost.setUpdatedBy(getSessionUI().getUser());
		} catch (Exception e) {
			logger.fatal(e);
		}
	}

	public void findHistory() {
		try {
			this.costHistory = usersCostService.findCostHistoryForUser(this.companyUsers.getUsers());
		} catch (Exception e) {
			logger.fatal(e);
		}
	}

	public List<UsersCostHist> getCostHistory() {
		return costHistory;
	}

	public void setCostHistory(List<UsersCostHist> costHistory) {
		this.costHistory = costHistory;
	}

	public Address getHomeAddress() {
		return homeAddress;
	}

	public void setHomeAddress(Address homeAddress) {
		this.homeAddress = homeAddress;
	}

	public AddressService getAddressService() {
		return addressService;
	}

	public void setAddressService(AddressService addressService) {
		this.addressService = addressService;
	}

	public List<Users> getUserList() {
		return userList;
	}

	public void setUserList(List<Users> userList) {
		this.userList = userList;
	}

	public List<Users> getUserFilteredList() {
		return userFilteredList;
	}

	public void setUserFilteredList(List<Users> userFilteredList) {
		this.userFilteredList = userFilteredList;
	}

	public String getNiNumber() {
		return niNumber;
	}

	public void setNiNumber(String niNumber) {
		this.niNumber = niNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public AddressService getHomeAddressService() {
		return homeAddressService;
	}

	public void setHomeAddressService(AddressService homeAddressService) {
		this.homeAddressService = homeAddressService;
	}

	public EmployeeTypeEnum getEmpType() {
		return empType;
	}

	public void setEmpType(EmployeeTypeEnum empType) {
		this.empType = empType;
	}

	public UploadDocUI getUploadDocUI() {
		return uploadDocUI;
	}

	public void setUploadDocUI(UploadDocUI uploadDocUI) {
		this.uploadDocUI = uploadDocUI;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public void prepDoc() throws Exception {
		this.doc = new Doc();
		this.doc.setUser(null);
	}

	public void safeFile(FileUploadEvent event) {
		try {
			this.doc.setUser(userSelected);
			uploadDocUI.setDocs(null);
			uploadDocUI.safeFile(event, this.doc, userSelected);
			runInit();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Notes getNote() {
		return note;
	}

	public void setNote(Notes note) {
		this.note = note;
	}

	public void prepNote() throws Exception {
		this.note = new Notes();
		this.note.setUsers(getSessionUI().getUser());
	}

	public void notesInsert() {
		try {
			this.note.setUsers(userSelected);
			notesService.create(this.note);
			prepNote();
			addInfoMessage(super.getEntryLanguage("update.successful"));
			prepareNewUsers();
			super.runClientSideExecute("PF('noteDialog').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Users getUserSelected() {
		return userSelected;
	}

	public void setUserSelected(Users userSelected) {
		this.userSelected = userSelected;
	}

	public LazyDataModel<CompanyUsers> getDataModelCompanyUser() {
		return dataModelCompanyUser;
	}

	public void setDataModelCompanyUser(LazyDataModel<CompanyUsers> dataModelCompanyUser) {
		this.dataModelCompanyUser = dataModelCompanyUser;
	}

	public List<UsersStatusEnum> getUsersStatusList() {
		return usersStatusList;
	}

	public void setUsersStatusList(List<UsersStatusEnum> usersStatusList) {
		this.usersStatusList = usersStatusList;
	}
}
