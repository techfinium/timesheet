package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.TimesheetDetails;
import haj.com.framework.AbstractUI;
import haj.com.service.TimesheetDetailsService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "timesheetdetailsUI")
@ViewScoped
public class TimesheetDetailsUI extends AbstractUI {

	private TimesheetDetailsService service = new TimesheetDetailsService();
	private List<TimesheetDetails> timesheetdetailsList = null;
	private List<TimesheetDetails> timesheetdetailsfilteredList = null;
	private TimesheetDetails timesheetdetails = null;
	private TimesheetDetails timesheetdetailsParent = null;
	private Double totalHours;
	private Date createDate = new Date();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.service = new TimesheetDetailsService();
		prepareNewTimesheetDetails();
		timesheetdetailsInfo();
	}

	public void timesheetdetailsInfo() {
		try {
			timesheetdetailsList = service.findByTimesheetBetweenDates(super.getSessionUI().getTimesheet());
			totalHours = 0.0;
			for (TimesheetDetails timesheetDetails : timesheetdetailsList) {
				if (timesheetDetails.getHours() != null) {
					totalHours = totalHours + timesheetDetails.getHours();
				} else {
					totalHours = totalHours + 0;
				}

			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public Double getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Double totalHours) {
		this.totalHours = totalHours;
	}

	public void timesheetdetailsInsert() {
		try {
			service.create(this.timesheetdetails);
			addInfoMessage("Insert successful");
			timesheetdetailsInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void timesheetdetailsDelete() {
		try {
			service.delete(this.timesheetdetails);
			timesheetdetailsInfo();
			super.addWarningMessage("Row deleted.");
			this.timesheetdetailsList = service.findByTimesheetBetweenDates(super.getSessionUI().getTimesheet());
			timesheetdetailsParent = service.findByKey(timesheetdetailsParent.getId());
			checkExpanded();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<TimesheetDetails> getTimesheetDetailsList() {
		return timesheetdetailsList;
	}

	public void setTimesheetDetailsList(List<TimesheetDetails> timesheetdetailsList) {
		this.timesheetdetailsList = timesheetdetailsList;
	}

	public TimesheetDetails getTimesheetdetails() {
		return timesheetdetails;
	}

	public void setTimesheetdetails(TimesheetDetails timesheetdetails) {
		this.timesheetdetails = timesheetdetails;
	}

	public void prepareNewTimesheetDetails() {
		timesheetdetails = new TimesheetDetails();
		timesheetdetails.setTimesheet(super.getSessionUI().getTimesheet());
		timesheetdetails.setFromDateTime(GenericUtility.getFirstDayOfMonth(new Date()));
		timesheetdetails.setToDateTime(GenericUtility.getLasttDayOfMonth(new Date()));
	}

	public void addNewDetail() {
		try {
			timesheetdetails = new TimesheetDetails();
			timesheetdetails.setTimesheetDetails(timesheetdetailsParent);
			timesheetdetails.setHours(0.0);
			timesheetdetails.setCreateDate(createDate);
			service.create(timesheetdetails);
			this.timesheetdetailsList = service.findByTimesheetBetweenDates(super.getSessionUI().getTimesheet());
			timesheetdetailsParent = service.findByKey(timesheetdetailsParent.getId());
			checkExpanded();
			checkCanedit();
			super.runClientSideUpdate("mainForm");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void canEdit() {
		timesheetdetails.setCanEdit(true);
		timesheetdetailsParent.setExpanded(true);
	}

	public void timesheetdetailsUpdate() {
		try {
			service.calcHours(timesheetdetailsParent);
			// Double newHours = timesheetdetailsParent.getHours() +
			// timesheetdetails.getHours();
			if (timesheetdetailsParent.getHours() > 24) {
				timesheetdetails.setHours(timesheetdetails.getHours() - (timesheetdetailsParent.getHours() - 24));
				addInfoMessage("Total hours per day cannot exceed 24");
			}
			service.update(this.timesheetdetails);
			addInfoMessage("Update successful");
			this.timesheetdetailsList = service.findByTimesheetBetweenDates(super.getSessionUI().getTimesheet());
			timesheetdetailsParent = service.findByKey(timesheetdetailsParent.getId());
			checkExpanded();
			checkCanedit();
			super.runClientSideUpdate("mainForm");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void closeCanEdit() {
		checkCanedit();
	}

	public void checkExpanded() {
		for (TimesheetDetails timesheetDetails : timesheetdetailsList) {
			if (timesheetDetails.getId().equals(timesheetdetailsParent.getId())) {
				timesheetDetails.setExpanded(true);
			}
		}
	}

	public void checkCanedit() {
		for (TimesheetDetails timesheetDetail : timesheetdetailsParent.getTimesheetDetailList()) {
			if (timesheetDetail.getId().equals(this.timesheetdetails.getId())) {
				timesheetDetail.setCanEdit(false);
			}
		}
	}

	public List<SelectItem> getTimesheetDetailsDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), "-------------Select-------------"));
		timesheetdetailsInfo();
		for (TimesheetDetails ug : timesheetdetailsList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<TimesheetDetails> getTimesheetDetailsfilteredList() {
		return timesheetdetailsfilteredList;
	}

	public void setTimesheetDetailsfilteredList(List<TimesheetDetails> timesheetdetailsfilteredList) {
		this.timesheetdetailsfilteredList = timesheetdetailsfilteredList;
	}

	public List<TimesheetDetails> complete(String desc) {
		List<TimesheetDetails> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public TimesheetDetails getTimesheetdetailsParent() {
		return timesheetdetailsParent;
	}

	public void setTimesheetdetailsParent(TimesheetDetails timesheetdetailsParent) {
		this.timesheetdetailsParent = timesheetdetailsParent;
	}
}
