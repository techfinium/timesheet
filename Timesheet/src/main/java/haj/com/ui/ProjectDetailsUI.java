package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.bean.ReportBean;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Timesheet;
import haj.com.framework.AbstractUI;
import haj.com.service.ReportService;

@ManagedBean(name = "projectDetailsUI")
@ViewScoped
public class ProjectDetailsUI extends AbstractUI {

	private ReportService service = new ReportService();
	private ReportBean reportBean;
	private List<ReportBean> reportBeanList;
	private CompanyUsers companyUser;
	private Timesheet timesheet;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		projectTaskInfo();
	}

	public void projectTaskInfo() {
		try {
			
			reportBeanList = service.projectTasks(getSessionUI().getReportBean().getProject(), getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate(),
					getSessionUI().getReportBean().getUser());
			
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public ReportBean getReportBean() {
		return reportBean;
	}

	public void setReportBean(ReportBean reportBean) {
		this.reportBean = reportBean;
	}

	public List<ReportBean> getReportBeanList() {
		return reportBeanList;
	}

	public void setReportBeanList(List<ReportBean> reportBeanList) {
		this.reportBeanList = reportBeanList;
	}

	public CompanyUsers getCompanyUser() {
		return companyUser;
	}

	public void setCompanyUser(CompanyUsers companyUser) {
		this.companyUser = companyUser;
	}

	public Timesheet getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(Timesheet timesheet) {
		this.timesheet = timesheet;
	}

}
