package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.ExpenseForm;
import haj.com.entity.Users;
import haj.com.framework.AbstractUI;
import haj.com.service.DocService;
import haj.com.service.ExpenseFormService;
import haj.com.service.UsersService;

@ManagedBean(name = "expenseOverviewUI")
@ViewScoped
public class ExpenseOverviewUI extends AbstractUI {
	private UsersService usersService = new UsersService();
	private ExpenseFormService expenseFormService = new ExpenseFormService();
	private List<Users> usersList = new ArrayList();
	private List<ExpenseForm> expenseFormList = new ArrayList();
	private Users user = new Users();
	private ExpenseForm expenseForm = new ExpenseForm();
	private DocService docService = new DocService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		usersList = usersService.allUsers();
	}

	public void generateExpenseForm() {
		try {
			expenseFormList = expenseFormService.findByUser(user);

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void populateExpenseDocs() {
		try {
			expenseForm.setDocList(docService.searchByTargetKeyAndClass(expenseForm.getClass().getName(), expenseForm.getId()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Users> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<Users> usersList) {
		this.usersList = usersList;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public List<ExpenseForm> getExpenseFormList() {
		return expenseFormList;
	}

	public void setExpenseFormList(List<ExpenseForm> expenseFormList) {
		this.expenseFormList = expenseFormList;
	}

	public ExpenseForm getExpenseForm() {
		return expenseForm;
	}

	public void setExpenseForm(ExpenseForm expenseForm) {
		this.expenseForm = expenseForm;
	}

}
