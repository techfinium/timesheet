package haj.com.ui;

import java.io.ByteArrayInputStream;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ComponentSystemEvent;

import org.omnifaces.util.Faces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import haj.com.entity.Doc;
import haj.com.entity.DocByte;
import haj.com.entity.DocumentTracker;
import haj.com.entity.Users;
import haj.com.entity.enums.DocumentTrackerEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.DocService;
import haj.com.service.DocumentTrackerService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "uploadDocUI")
@SessionScoped
public class UploadDocUI extends AbstractUI {

	private Doc doc;
	private List<Doc> docs;
	private List<Doc> docsfiltered;
	private DocService docService = new DocService();
	private List<DocumentTracker> documentTrackers;
	private DocumentTrackerService documentTrackerService = new DocumentTrackerService();
	private StreamedContent content;
	private List<Doc> docVersions;

	public void runInit() throws Exception {
		initDoc();
	}

	public List<Doc> getDocs() {
		return docs;
	}

	public void setDocs(List<Doc> docs) {
		this.docs = docs;
	}

	public List<Doc> getDocsfiltered() {
		return docsfiltered;
	}

	public void setDocsfiltered(List<Doc> docsfiltered) {
		this.docsfiltered = docsfiltered;
	}

	public void buildStreamContent() {
		getSessionUI().setSelDoc(doc);
		try {
			DocumentTrackerService.create(getSessionUI().getUser(), doc, DocumentTrackerEnum.Viewed);
		} catch (Exception e) {
			logger.fatal(e);
		}
	}

	public void download() {
		try {
			Users user = getSessionUI().getUser();
			DocumentTrackerService.create(user, doc, DocumentTrackerEnum.Downloaded);
			Faces.sendFile(doc.getFileContent(), GenericUtility.buidFileName(doc), true);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void onPrerender(ComponentSystemEvent event) {
		try {
			if (doc != null) {
				DocByte docByte = docService.findDocByteByKey(doc.getId());
				if (doc != null && docByte != null && docByte.getData() != null) {
					content = new DefaultStreamedContent(new ByteArrayInputStream(docByte.getData()), "application/pdf");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void safeFile(FileUploadEvent event, Doc doc) {
		try {

			docService.save(doc, event.getFile().getContents(), event.getFile().getFileName(), getSessionUI().getUser(), this.docs);
			addInfoMessage("Your file has been uploaded successfully!");
			// initDoc();
			findDoc(doc);

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void safeFile(FileUploadEvent event, Doc doc, Users user) {
		try {

			docService.save(doc, event.getFile().getContents(), event.getFile().getFileName(), user, this.docs);
			addInfoMessage("Your file has been uploaded successfully!");
			// initDoc();
			findDoc(doc);

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void safeFile(FileUploadEvent event) {
		try {

			docService.save(doc, event.getFile().getContents(), event.getFile().getFileName(), getSessionUI().getUser(), this.docs);
			addInfoMessage("Your file has been uploaded successfully!");
			// initDoc();
			findDoc(doc);

		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void findDoc(Doc doc) {
		try {
			this.docs = docService.search(doc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}

	}

	public void initDoc() {
		this.doc = new Doc();
		// this.doc.setHostingCompany(getSessionUI().getHostingCompany());
		this.doc.setUser(getSessionUI().getUser());

	}

	public void showHistory() {
		try {
			this.documentTrackers = documentTrackerService.byRoot(doc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<DocumentTracker> getDocumentTrackers() {
		return documentTrackers;
	}

	public void setDocumentTrackers(List<DocumentTracker> documentTrackers) {
		this.documentTrackers = documentTrackers;
	}

	public Doc getDoc() {
		return doc;
	}

	public void setDoc(Doc doc) {
		this.doc = doc;
	}

	public StreamedContent getContent() {
		return content;
	}

	public void setContent(StreamedContent content) {
		this.content = content;
	}

	public List<Doc> getDocVersions() {
		return docVersions;
	}

	public void setDocVersions(List<Doc> docVersions) {
		this.docVersions = docVersions;
	}
}
