package  haj.com.ui;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Users;
import haj.com.framework.AbstractUI;
import haj.com.service.AddressService;
import haj.com.service.LogonService;
import haj.com.service.UsersService;



@ManagedBean(name = "profileUI")
@ViewScoped
public class ProfileUI extends AbstractUI {
    
	private AddressService service = new AddressService();
	private UsersService usersService = new UsersService();
	private Users user = null;

	private String password,  newPassword;
	
	
    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception { 
		usersInfo();
	}

	public void usersInfo() {
		try {
			this.user = usersService.findByKeyl(getSessionUI().getUser().getUid());
	/*		List<Address> l = service.byUserId(this.user.getUid());
			for (Address a : l) {
				switch (a.getAddrType()) {
				case Residential:
				{
					this.resiAddr = a;
					break;
				}
				case Postal:
				{
					this.postalAddr= a;
					break;
				}				
				default:
					break;
				}
				
				if (resiAddr==null) {
					resiAddr = new Address();
					resiAddr.setAddrType(AddressTypeEnum.Residential);
				}
				else {
					
					model.addOverlay(new Marker(new LatLng(resiAddr.getLatitude(), resiAddr.getLongitude()), "M1"));
				}
				if (postalAddr==null) {
					postalAddr = new Address();
					postalAddr.setAddrType(AddressTypeEnum.Postal);
				}
				
			}
			*/
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
	



	


	

    
    public void usersUpdate() {
		try {
		//	if (this.users.getCompany().getCompanyId()==-1L) throw new Exception("Select a company");
			usersService.emailUser(this.user);
			 addInfoMessage("Update successful");
			 getSessionUI().setUser(this.user);
			 usersInfo();
		} catch (Exception e) {
		   super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);	
			}
	}
	
  
	public void usersChgOwnPwdUpdate() {
		try {
			 new LogonService(). changePassword(this.user.getEmail(), password,  newPassword);
			 super.addInfoMessage("Password changed");
			 log("User changed passwword",this.user.getEmail());
			 usersInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);	
			}
	}


	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
		
			
		
			
			
		}
