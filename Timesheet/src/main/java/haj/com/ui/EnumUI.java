package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.UsersLevelEnum;
import haj.com.entity.enums.CarTypeEnum;
import haj.com.entity.enums.CovidPersonRegisterTypeEnum;
import haj.com.entity.enums.EmployeeTypeEnum;
import haj.com.entity.enums.ExpenseFormTypeEnum;
import haj.com.entity.enums.HealthStatusEnum;
import haj.com.entity.enums.UserPermissionEnum;
import haj.com.entity.enums.VisitorTypeEnum;
import haj.com.entity.enums.YesNoEnum;
import haj.com.framework.AbstractUI;

@ManagedBean(name = "enumUI")
@ViewScoped
public class EnumUI extends AbstractUI {

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
	}

	public List<SelectItem> getUserLevelDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (UsersLevelEnum selectItem : UsersLevelEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	/**
	 * Only gets employee and manager levels from UsersLevelEnum
	 * 
	 * @return
	 */
	public List<SelectItem> getUserManEmpLevelDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (UsersLevelEnum selectItem : UsersLevelEnum.values()) {
			if (selectItem != selectItem.Admin && selectItem != selectItem.Director) {
				l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
			}

		}
		return l;
	}
	
	public List<SelectItem> getUserPermissionApprovalEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(UserPermissionEnum.View, super.getEntryLanguage((UserPermissionEnum.View.getRegistrationName()))));
		l.add(new SelectItem(UserPermissionEnum.Approve, super.getEntryLanguage(UserPermissionEnum.Approve.getRegistrationName())));
		l.add(new SelectItem(UserPermissionEnum.FinalApproval, super.getEntryLanguage((UserPermissionEnum.FinalApproval.getRegistrationName()))));
		return l;
	}
	
	public List<SelectItem> getUserPermissionEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (UserPermissionEnum val : UserPermissionEnum.values()) {
			l.add(new SelectItem(val, super.getEntryLanguage(val.getRegistrationName())));
		}
		return l;
	}


	public List<SelectItem> getUserTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (EmployeeTypeEnum selectItem : EmployeeTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getCarTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (CarTypeEnum selectItem : CarTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}

	public List<SelectItem> getExpenseFormTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ExpenseFormTypeEnum selectItem : ExpenseFormTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getYesNoEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (YesNoEnum selectItem :YesNoEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getHealthStatusDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (HealthStatusEnum selectItem :HealthStatusEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getCovidPersonRegisterTypeDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (CovidPersonRegisterTypeEnum selectItem :CovidPersonRegisterTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}
	
	public List<SelectItem> getVisitorTypeEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (VisitorTypeEnum selectItem :VisitorTypeEnum.values()) {
			l.add(new SelectItem(selectItem, selectItem.getFriendlyName()));
		}
		return l;
	}
	

}
