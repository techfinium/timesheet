package haj.com.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.Timesheet;
import haj.com.entity.UsersLevelEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.TimesheetService;
import haj.com.utils.GenericUtility;

@ManagedBean(name = "timesheetUI")
@ViewScoped
public class TimesheetUI extends AbstractUI {

	private TimesheetService service = new TimesheetService();
	private List<Timesheet> timesheetList = null;
	private List<Timesheet> timesheetfilteredList = null;
	private Timesheet timesheet = null;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.service = new TimesheetService();
		prepareNewTimesheet();
		timesheetInfo();
	}

	public void timesheetInfo() {
		try {
			if (getSessionUI().getCompanyUsers().getLevel() == UsersLevelEnum.Admin) {
				timesheetList = service.byCompany(getSessionUI().getCompany().getId());
				//timesheetList = service.allTimesheet();
			} else {
				timesheetList = service.findByCompanyUser(getSessionUI().getCompanyUsers());
			}
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void timesheetInsert() {
		try {
			service.create(this.timesheet);
			addInfoMessage("Insert successful");
			timesheetInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void timesheetDelete() {
		try {
			service.delete(this.timesheet);
			timesheetInfo();
			super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Timesheet> getTimesheetList() {
		return timesheetList;
	}

	public void setTimesheetList(List<Timesheet> timesheetList) {
		this.timesheetList = timesheetList;
	}

	public Timesheet getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(Timesheet timesheet) {
		this.timesheet = timesheet;
	}

	public void prepareNewTimesheet() {
		timesheet = new Timesheet();
		timesheet.setCompanyUsers(super.getSessionUI().getCompanyUsers());
		timesheet.setFromDate(GenericUtility.getFirstDayOfMonth(new Date()));
		timesheet.setToDate(GenericUtility.getLasttDayOfMonth(new Date()));
	}

	public void timesheetUpdate() {
		try {
			service.update(this.timesheet);
			addInfoMessage("Update successful");
			timesheetInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<SelectItem> getTimesheetDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), "-------------Select-------------"));
		timesheetInfo();
		for (Timesheet ug : timesheetList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<Timesheet> getTimesheetfilteredList() {
		return timesheetfilteredList;
	}

	public void setTimesheetfilteredList(List<Timesheet> timesheetfilteredList) {
		this.timesheetfilteredList = timesheetfilteredList;
	}

	public List<Timesheet> complete(String desc) {
		List<Timesheet> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}

	public void goToDetail() {
		try {
			super.getSessionUI().setTimesheet(this.timesheet);
			super.ajaxRedirect("/pages/timesheetdetails.jsf");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}
}
