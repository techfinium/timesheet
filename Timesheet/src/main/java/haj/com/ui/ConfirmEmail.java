package haj.com.ui;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.entity.Users;
import haj.com.framework.AbstractUI;
import haj.com.service.RegisterService;
import haj.com.service.UsersService;

@ManagedBean(name = "confirmEmailUI")
@ViewScoped
public class ConfirmEmail extends AbstractUI {

	private UsersService service = new UsersService();
	private RegisterService registerservice = new RegisterService();
	private String uuid;
	private boolean validLink;
	
    @PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		validLink=false;
		if (super.getParameter("uuid", false)!=null) {
			this.uuid = (String)super.getParameter("uuid", false);
			this.validLink = true;
			Users u = service.findUserByEmailGUID(this.uuid.trim());
			if (u==null) {
				this.validLink = false;
			}
			else {
				registerservice.confirmEmail(u);
			}
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public boolean isValidLink() {
		return validLink;
	}

	public void setValidLink(boolean validLink) {
		this.validLink = validLink;
	}
}
