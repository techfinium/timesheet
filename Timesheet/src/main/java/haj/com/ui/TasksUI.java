package haj.com.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import haj.com.entity.Tasks;
import haj.com.framework.AbstractUI;
import haj.com.service.TasksService;

@ManagedBean(name = "tasksUI")
@ViewScoped
public class TasksUI extends AbstractUI {

	private TasksService service = new TasksService();
	private List<Tasks> tasksList = null;
	private List<Tasks> tasksfilteredList = null;
	private Tasks tasks = null;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.service = new TasksService();
		prepareNewTasks();
		tasksInfo();
	}

	public void tasksInfo() {
		try {
			tasksList = service.allTasks();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void tasksInsert() {
		try {
			service.create(this.tasks);
			addInfoMessage("Insert successful");
			tasksInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void tasksDelete() {
		try {
			service.delete(this.tasks);
			tasksInfo();
			super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<Tasks> getTasksList() {
		return tasksList;
	}

	public void setTasksList(List<Tasks> tasksList) {
		this.tasksList = tasksList;
	}

	public Tasks getTasks() {
		return tasks;
	}

	public void setTasks(Tasks tasks) {
		this.tasks = tasks;
	}

	public void prepareNewTasks() {
		tasks = new Tasks();
	}

	public void tasksUpdate() {
		try {
			service.update(this.tasks);
			addInfoMessage("Update successful");
			tasksInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<SelectItem> getTasksDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), "-------------Select-------------"));
		tasksInfo();
		for (Tasks ug : tasksList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<Tasks> getTasksfilteredList() {
		return tasksfilteredList;
	}

	public void setTasksfilteredList(List<Tasks> tasksfilteredList) {
		this.tasksfilteredList = tasksfilteredList;
	}

	public List<Tasks> complete(String desc) {
		List<Tasks> l = null;
		try {
			l = service.findByName(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return l;
	}
}
