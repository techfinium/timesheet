package haj.com.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.bean.ReportBean;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Timesheet;
import haj.com.framework.AbstractUI;
import haj.com.service.ReportService;

@ManagedBean(name = "userDetailsUI")
@ViewScoped
public class userDetailsUI extends AbstractUI {

	private ReportService service = new ReportService();
	private ReportBean reportBean;
	private List<ReportBean> reportBeanList;
	private CompanyUsers companyUser;
	private Timesheet timesheet;
	private Double totalHours = 0.0;
	private Long totalMins;
	private Integer totalHoursConverted;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		userDetailInfo();
	}

	public void userDetailInfo() {
		try {
			if (getSessionUI().getReportBean().getProject() == null) {

				if (!getSessionUI().isDirector()) {
					reportBeanList = service.UserDetailsAll(getSessionUI().getCompany(),
							getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate(),
							getSessionUI().getReportBean().getUser());
					
					totalHours = 0.0;
					totalMins = 0l;
					
					for (ReportBean reportBean : reportBeanList) {
						totalHours += reportBean.getHours();
						totalMins += reportBean.getLongOne();
					}
					if (totalMins == null) {
						totalMins = (long) 00;
					}
					if (totalMins >= 60) {
						int m = (int) (totalMins / 60);
						totalHours += m;
						totalMins -= m * 60;
					}

					totalHoursConverted = totalHours.intValue();
				} else {
					reportBeanList = service.UserDetailsAllDirector(getSessionUI().getReportBean().getFromDate(),
							getSessionUI().getReportBean().getToDate(), getSessionUI().getReportBean().getUser());
					
					totalHours = 0.0;
					totalMins = 0l;
					
					for (ReportBean reportBean : reportBeanList) {
						totalHours += reportBean.getHours();
						totalMins += reportBean.getLongOne();
					}
					if (totalMins == null) {
						totalMins = (long) 00;
					}
					if (totalMins >= 60) {
						int m = (int) (totalMins / 60);
						totalHours += m;
						totalMins -= m * 60;
					}

					totalHoursConverted = totalHours.intValue();	
				}

			} else {
				if (!getSessionUI().isDirector()) {
					reportBeanList = service.UserDetails(getSessionUI().getCompany(),
							getSessionUI().getReportBean().getFromDate(), getSessionUI().getReportBean().getToDate(),
							getSessionUI().getReportBean().getUser(), getSessionUI().getReportBean().getWeekNum(),
							getSessionUI().getReportBean().getProject());
					
					totalHours = 0.0;
					totalMins = 0l;
					
					for (ReportBean reportBean : reportBeanList) {
						totalHours += reportBean.getHours();
						totalMins += reportBean.getLongOne();
					}
					if (totalMins == null) {
						totalMins = (long) 00;
					}
					if (totalMins >= 60) {
						int m = (int) (totalMins / 60);
						totalHours += m;
						totalMins -= m * 60;
					}

					totalHoursConverted = totalHours.intValue();
				} else {
					reportBeanList = service.UserDetailsDirector(getSessionUI().getReportBean().getFromDate(),
							getSessionUI().getReportBean().getToDate(), getSessionUI().getReportBean().getUser(),
							getSessionUI().getReportBean().getWeekNum(), getSessionUI().getReportBean().getProject());
					
					totalHours = 0.0;
					totalMins = 0l;
					
					for (ReportBean reportBean : reportBeanList) {
						totalHours += reportBean.getHours();
						totalMins += reportBean.getLongOne();
					}
					if (totalMins == null) {
						totalMins = (long) 00;
					}
					if (totalMins >= 60) {
						int m = (int) (totalMins / 60);
						totalHours += m;
						totalMins -= m * 60;
					}

					totalHoursConverted = totalHours.intValue();
				
				}
			}

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public ReportBean getReportBean() {
		return reportBean;
	}

	public void setReportBean(ReportBean reportBean) {
		this.reportBean = reportBean;
	}

	public List<ReportBean> getReportBeanList() {
		return reportBeanList;
	}

	public void setReportBeanList(List<ReportBean> reportBeanList) {
		this.reportBeanList = reportBeanList;
	}

	public CompanyUsers getCompanyUser() {
		return companyUser;
	}

	public void setCompanyUser(CompanyUsers companyUser) {
		this.companyUser = companyUser;
	}

	public Timesheet getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(Timesheet timesheet) {
		this.timesheet = timesheet;
	}

	public Double getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Double totalHours) {
		this.totalHours = totalHours;
	}

	public Long getTotalMins() {
		return totalMins;
	}

	public void setTotalMins(Long totalMins) {
		this.totalMins = totalMins;
	}

	public Integer getTotalHoursConverted() {
		return totalHoursConverted;
	}

	public void setTotalHoursConverted(Integer totalHoursConverted) {
		this.totalHoursConverted = totalHoursConverted;
	}

}
