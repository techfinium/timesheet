package haj.com.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import haj.com.entity.Users;
import haj.com.entity.UsersCost;
import haj.com.entity.UsersCostHist;
import haj.com.framework.AbstractUI;
import haj.com.service.CurrencyService;
import haj.com.service.UsersCostService;
import haj.com.service.UsersService;

@ManagedBean(name = "userscostUI")
@ViewScoped
public class UsersCostUI extends AbstractUI {

	private UsersCostService service = new UsersCostService();
	private List<UsersCost> userscostList = null;
	private List<UsersCost> userscostfilteredList = null;
	private UsersCost userscost = null;
	private LazyDataModel<UsersCost> dataModel;

	private double previousCostPerAnnum;
	private double dayRate;

	private List<Users> usersList;
	private UsersService usersService = new UsersService();

	private List<UsersCostHist> usersCostHistList;
	private UsersCostHist usersCostHist;

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Initialize method to get all UsersCost and prepare a for a create of a
	 * new UsersCost
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 * @throws Exception
	 */
	private void runInit() throws Exception {
		prepareNew();
		userscostInfo();
		prepAuditUserCost();
	}

	/**
	 * Get all UsersCost for data table
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public void userscostInfo() {

		dataModel = new LazyDataModel<UsersCost>() {

			private static final long serialVersionUID = 1L;
			private List<UsersCost> retorno = new ArrayList<UsersCost>();

			@Override
			public List<UsersCost> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

				try {
					retorno = service.allUsersCost(UsersCost.class, first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(service.count(UsersCost.class, filters));
				} catch (Exception e) {
					logger.fatal(e);
				}
				return retorno;
			}

			@Override
			public Object getRowKey(UsersCost obj) {
				return obj.getId();
			}

			@Override
			public UsersCost getRowData(String rowKey) {
				for (UsersCost obj : retorno) {
					if (obj.getId().equals(Long.valueOf(rowKey)))
						return obj;
				}
				return null;
			}

		};

	}

	public void prepAuditUserCost() {
		try {
			usersCostHist = new UsersCostHist();
			if (this.userscost.getId() != null) {
				usersCostHistList = service.findByKeyHist(this.userscost.getId());
				if (usersCostHistList.size() == 1)
				{
					usersCostHist = usersCostHistList.get(0);
 				} else if (usersCostHistList.size() > 1){
 					usersCostHist = usersCostHistList.get(1);
 				}
			} else {
				usersCostHistList = new ArrayList<UsersCostHist>();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Insert UsersCost into database
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public void userscostInsert() {
		try {
			this.userscost.setUpdatedBy(getSessionUI().getUser());
			service.create(this.userscost);
			prepareNew();
			addInfoMessage("Update successful");
			userscostInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Delete UsersCost from database
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public void userscostDelete() {
		try {
			service.delete(this.userscost);
			prepareNew();
			userscostInfo();
			super.addWarningMessage("Row deleted.");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public List<UsersCost> getUsersCostList() {
		return userscostList;
	}

	public void setUsersCostList(List<UsersCost> userscostList) {
		this.userscostList = userscostList;
	}

	public UsersCost getUserscost() {
		return userscost;
	}

	public void setUserscost(UsersCost userscost) {
		this.userscost = userscost;
	}

	/**
	 * Create new instance of UsersCost
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public void prepareNew() {
		userscost = new UsersCost();
		userscost.setLocale(Locale.ENGLISH);
		userscost.setExhangeRate(1.0);
		userscost.setCreateDate(new java.util.Date());
		prepAuditUserCost();
	}

	/**
	 * Update UsersCost in database
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public void userscostUpdate() {
		try {
			service.update(this.userscost);
			addInfoMessage("Update successful");
			userscostInfo();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	/**
	 * Prepare select one menu data
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public List<SelectItem> getUsersCostDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		l.add(new SelectItem(Long.valueOf(-1L), "-------------Select-------------"));
		userscostInfo();
		for (UsersCost ug : userscostList) {
			// l.add(new SelectItem(ug.getUserGroupId(),
			// ug.getUserGroupDesc()));
		}
		return l;
	}

	public List<UsersCost> getUsersCostfilteredList() {
		return userscostfilteredList;
	}

	/**
	 * Prepare auto complete data
	 * 
	 * @author TechFinium
	 * @see UsersCost
	 */
	public void setUsersCostfilteredList(List<UsersCost> userscostfilteredList) {
		this.userscostfilteredList = userscostfilteredList;
	}

	public List<Users> complete(String desc) {
		try {
			usersList = usersService.findByNameLastname(desc);
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
		return usersList;
	}

	public void onCountryChange() {
		try {
			userscost.setExhangeRate(CurrencyService.exhangeRateFromToBase(userscost.getLocale()));
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public LazyDataModel<UsersCost> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<UsersCost> dataModel) {
		this.dataModel = dataModel;
	}

	public List<UsersCost> getUserscostList() {
		return userscostList;
	}

	public void setUserscostList(List<UsersCost> userscostList) {
		this.userscostList = userscostList;
	}

	public double getPreviousCostPerAnnum() {
		return previousCostPerAnnum;
	}

	public void setPreviousCostPerAnnum(double previousCostPerAnnum) {
		this.previousCostPerAnnum = previousCostPerAnnum;
	}

	public double getDayRate() {
		return dayRate;
	}

	public void setDayRate(double dayRate) {
		this.dayRate = dayRate;
	}

	public List<Users> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<Users> usersList) {
		this.usersList = usersList;
	}

	public List<UsersCostHist> getUsersCostHistList() {
		return usersCostHistList;
	}

	public void setUsersCostHistList(List<UsersCostHist> usersCostHistList) {
		this.usersCostHistList = usersCostHistList;
	}

	public void setUsersCostHist(UsersCostHist usersCostHist) {
		this.usersCostHist = usersCostHist;
	}

	public UsersCostHist getUsersCostHist() {
		return usersCostHist;
	}

}
