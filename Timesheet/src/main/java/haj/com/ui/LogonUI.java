package haj.com.ui;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import haj.com.billing.BillingHelper;
import haj.com.entity.Address;
import haj.com.entity.AddressTypeEnum;
import haj.com.entity.CompanyUsers;
import haj.com.entity.Users;
import haj.com.entity.UsersLevelEnum;
import haj.com.framework.AbstractUI;
import haj.com.service.AccountsService;
import haj.com.service.CompanyUsersService;
import haj.com.service.HostingCompanyService;
import haj.com.service.LogonService;

@ManagedBean(name = "logonUI")
@ViewScoped
public class LogonUI extends AbstractUI {
	// "hs241267@gmail.com" "a@a.com"
	private String email, username;// ="jd01";//"hstrydom";//=
									// "hs241267@gmail.com";//"hendrik@hlspro.com";
	private String inputPassword, newpassword, phoneId, iosAndroid;
	private CompanyUsersService companyUsersService = new CompanyUsersService();
	private HostingCompanyService hostingCompanyService = new HostingCompanyService();
	private Address address;
	private AccountsService accountsService = new AccountsService();
	private CompanyUsers companyUsers;
	private LogonService logonService = new LogonService();

	@PostConstruct
	public void init() {
		try {
			runInit();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void runInit() throws Exception {
		this.address = new Address();
		this.address.setPrimaryAddr(Boolean.TRUE);
		this.address.setAddrType(AddressTypeEnum.Billing);

	}

	public void logon() {
		try {
			getSessionUI().setiAmAClient(Boolean.FALSE);
			getSessionUI().setDuid(null);
			// if (1==1) throw new Exception("TEST");
			Users user = logonService.logon(email, inputPassword);
			getSessionUI().setUser(user);
			super.setParameter("uobj", getSessionUI().getUser(), true);
			setCompany();
			determineHostingCompany();

			getSessionUI().setShowMenu(true);

			log();
			// if (getSessionUI().isAdmin()) {
			// createRoleAccess();
			// setAccessLevels();
			// super.ajaxRedirect("/pages/index.jsf");
			// }
			// else {
			if (getSessionUI().getUser().isChgPwdNow()) {
				super.runClientSideExecute("PF('dlgPwdChg').show()");
				super.runClientSideUpdate("usersPwdChgForm");
			}
			/*
			 * else if (getSessionUI().getUser().getAccounts() == null) {
			 * super.runClientSideExecute("PF('dlgBilling').show()");
			 * super.runClientSideUpdate("billingForm"); }
			 */
			else {
				if (getSessionUI().getUser().getAccounts() != null)
					BillingHelper.checkUnits(getSessionUI().getUser());
				showCompanies();
			}
			// }
		} catch (Exception e) {
			log("invalid logon for email", email);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void companyChange() {
		try {
			Users tu = getSessionUI().getUser();
			logoffGeneric();
			getSessionUI().setUser(tu);
			super.setParameter("uobj", tu, true);
			getSessionUI().setCompany(companyUsers.getCompany());
			getSessionUI().setCompanyUsers(companyUsers);
			getSessionUI().setCompanyUsersList(companyUsersService.byUser(getSessionUI().getUser()));
			getSessionUI().setShowMenu(true);
			createRoleAccess();
			setAccessLevels();
			if (getSessionUI().isAdmin() == true) {
				super.ajaxRedirect("/pages/dashboardAdmin.jsf");
			} else if (getSessionUI().isCustomerAdmin() == true) {
				super.ajaxRedirect("/pages/dashboardGeneral.jsf");
			} else if (getSessionUI().isDirector() == true) {
				super.ajaxRedirect("/pages/dashboardGeneral.jsf");
			} else {
				super.ajaxRedirect("/pages/dashboard.jsf");
			}
		} catch (Exception e) {
			log("invalid logon for email", email);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void logonViaDocLink() {
		try {
			getSessionUI().setUser(new LogonService().logonByUsername(username, inputPassword));
			super.setParameter("uobj", getSessionUI().getUser(), true);
			setCompany();
			getSessionUI().setShowMenu(true);
			log();
			getSessionUI().setCompanyUsersList(companyUsersService.byUser(getSessionUI().getUser()));
			// getSessionUI().setCompany(doc.getConfigDoc().getCompany());
			for (CompanyUsers cu : getSessionUI().getCompanyUsersList()) {
				getSessionUI().setCompany(cu.getCompany());
				getSessionUI().setCompanyUsers(cu);
				break;
			}
			createRoleAccess();
			setAccessLevels();
			if (getSessionUI().getUser().getAccounts() != null)
				BillingHelper.checkUnits(getSessionUI().getUser());
			super.ajaxRedirect("/pages/showdoc.jsf");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void completeLogon() throws Exception {
		if (getSessionUI().getCompanyUsers().getActive() != null && getSessionUI().getCompanyUsers().getActive() == false) {
			throw new Exception("You are not active");
		}
		getSessionUI().setCompany(getSessionUI().getCompanyUsers().getCompany());
		createRoleAccess();
		setAccessLevels();
		if (getSessionUI().isAdmin() == true) {
			super.ajaxRedirect("/pages/dashboardAdmin.jsf");
		} else if (getSessionUI().isCustomerAdmin() == true) {
			super.ajaxRedirect("/pages/dashboardGeneral.jsf");
		} else if (getSessionUI().isDirector() == true) {
			super.ajaxRedirect("/pages/dashboardGeneral.jsf");
		} else {
			super.ajaxRedirect("/pages/dashboard.jsf");
		}
	}

	private void showCompanies() throws Exception {
		if (getSessionUI().getCompanyUsersList() == null || getSessionUI().getCompanyUsersList().size() == 0) {
			throw new Exception("Please contact support..config error!");
		} else if (getSessionUI().getCompanyUsersList().size() == 1 || checkEmployee()) {
			completeLogon();
		} else {
			super.runClientSideExecute("PF('dlgCompanies').show()");
			super.runClientSideUpdate("companiesForm");
		}
	}

	private boolean checkEmployee() {
		for (CompanyUsers companyUsers : getSessionUI().getCompanyUsersList()) {
			if (companyUsers.getLevel() == UsersLevelEnum.Normal) {
				getSessionUI().setCompanyUsers(companyUsers);
				return true;
			}
		}
		return false;
	}

	public void updateBillingAddr() {
		try {
			accountsService.createDefaultAccount(getSessionUI().getUser(), address);
			showCompanies();
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void setCompany() throws Exception {
		getSessionUI().setCompanyUsersList(companyUsersService.byUser(getSessionUI().getUser()));
		if (getSessionUI().getCompanyUsersList().size() == 1) {
			for (CompanyUsers cu : getSessionUI().getCompanyUsersList()) {
				getSessionUI().setCompany(cu.getCompany());
				getSessionUI().setCompanyUsers(cu);
				break;
			}
		}

	}

	private void determineHostingCompany() {
		try {
			getSessionUI().setHostingCompany(hostingCompanyService.findByKey(1L));

		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void createRoleAccess() throws Exception {
		/*
		 * getSessionUI().setUsersRoles(userRoleService.createRoleAccess(
		 * getSessionUI().getCompanyUsers()));
		 * getSessionUI().setConfigDocRoles(configDocRolesService.createAccess(
		 * getSessionUI().getCompany())); if (getSessionUI().getUsersRoles()==null ||
		 * getSessionUI().getUsersRoles().size()==0 ||
		 * getSessionUI().getConfigDocRoles()==null) {
		 * getSessionUI().setApplyRoles(false); } else {
		 * getSessionUI().setApplyRoles(true); }
		 */
	}

	public void logonMobile() {
		try {
			getSessionUI().setUser(new LogonService().logon(email, inputPassword, phoneId, iosAndroid));
			super.setParameter("uobj", getSessionUI().getUser(), true);
			setAccessLevels();
			getSessionUI().setShowMenu(true);
			log();
			super.ajaxRedirect("/mobile/welcome.jsf");
		} catch (Exception e) {
			log("invalid logon for email", email);
			addErrorMessage(e.getMessage(), e);
		}
	}

	private void logoffCommon() throws Exception {
		logoffGeneric();
		super.removeParm("sessionUI", true);
		super.removeParm("uobj", true);
		log();
	}

	private void logoffGeneric() {
		getSessionUI().setUser(null);
		getSessionUI().setShowMenu(false);

		getSessionUI().setMember(false);
		getSessionUI().setAdmin(false);
		getSessionUI().setCustomerAdmin(false);

		getSessionUI().setLastLogin(null);
		getSessionUI().setUser(null);
		getSessionUI().setCompany(null);
		getSessionUI().setCompanyUsers(null);
		getSessionUI().setDuid(null);

		getSessionUI().setTimesheet(null);
		getSessionUI().setCompanyUsersList(null);
		getSessionUI().setAdministrators(null);
		getSessionUI().setApplyRoles(false);
	}

	public void logoff() {
		try {
			logoffCommon();
			super.redirect("/index.jsp");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void logoffMobile() {
		try {
			logoffCommon();
			super.ajaxRedirect("/mobile/logon.jsf");
		} catch (Exception e) {
			addErrorMessage(e.getMessage(), e);
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInputPassword() {
		return inputPassword;
	}

	public void setInputPassword(String inputPassword) {
		this.inputPassword = inputPassword;
	}

	public void idleListener() {
		getSessionUI().setUser(null);
		super.ajaxRedirect("/logon.jsf");

	}

	public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	/*
	 * public void changePwd() { try { Users u = new
	 * LogonService().changePassword(email, inputPassword, newpassword);
	 * getSessionUI().setUser(u); log("User changed passwword",email);
	 * super.ajaxRedirect("/pages/dashboard.jsf"); } catch (Exception e) { log(
	 * "invalid change pwd attempt for email",email);
	 * super.addCallBackParm("validationFailed", true);
	 * addErrorMessage(e.getMessage(),e); } }
	 */
	public Object getUobj() {
		return getParameter("uobj", true);
	}

	public Users getUser() {
		if (this.getSessionUI() != null)
			return getSessionUI().getUser();
		else
			return null;
	}

	private void setAccessLevels() {
		CompanyUsers u = getSessionUI().getCompanyUsers();
		if (u.getLevel() == UsersLevelEnum.Normal) {
			getSessionUI().setMember(true);
			getSessionUI().setAdmin(false);
		} else if (u.getLevel() == UsersLevelEnum.Manager) {
			getSessionUI().setMember(false);
			getSessionUI().setCustomerAdmin(true);
			getSessionUI().setAdmin(false);
		} else if (u.getLevel() == UsersLevelEnum.Admin) {
			getSessionUI().setMember(false);
			getSessionUI().setCustomerAdmin(false);
			getSessionUI().setAdmin(true);
		} else if (u.getLevel() == UsersLevelEnum.Director) {
			getSessionUI().setMember(false);
			getSessionUI().setCustomerAdmin(false);
			getSessionUI().setAdmin(false);
			getSessionUI().setDirector(true);
		}
	}

	public void mailUsername() {
		try {
			new LogonService().notifyUserAboutUsernames(email);
			addInfoMessage("Your username/s has been mailed to you.");
			super.runClientSideExecute("PF('dlgPwd').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void newPassword() {
		try {
			new LogonService().notifyUserNewPasswordEmail(email);
			addInfoMessage("A new password has been mailed to you.");
			super.runClientSideExecute("PF('dlgPwd').hide()");
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public void changePassword() {
		try {
			new LogonService().changePassword(getSessionUI().getUser(), inputPassword);
			addInfoMessage("A new password has been mailed to you.");
			showCompanies();
		} catch (Exception e) {
			super.addCallBackParm("validationFailed", true);
			addErrorMessage(e.getMessage(), e);
		}
	}

	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}

	public String getIosAndroid() {
		return iosAndroid;
	}

	public void setIosAndroid(String iosAndroid) {
		this.iosAndroid = iosAndroid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public CompanyUsers getCompanyUsers() {
		return companyUsers;
	}

	public void setCompanyUsers(CompanyUsers companyUsers) {
		this.companyUsers = companyUsers;
	}
}
