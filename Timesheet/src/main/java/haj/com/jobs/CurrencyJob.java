package haj.com.jobs;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import haj.com.service.CurrencyService;

public class CurrencyJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
				CurrencyService.populateRates();
	}

}
