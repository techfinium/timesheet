package haj.com.jobs;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.Serializable;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;

public class HAJScheduler implements Serializable {

	
	
	  public void run() throws Exception {
		  SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();

		  Scheduler sched = schedFact.getScheduler();

		  sched.start();

		  // define the job and tie it to our HelloJob class
		  JobDetail job = newJob(CurrencyJob.class)
		      .withIdentity("notificationJob", "group1")
		      .build();


		  Trigger trigger = newTrigger()
				    .withIdentity("notificationtrigger1", "group1")
				    .withSchedule(org.quartz.CronScheduleBuilder.dailyAtHourAndMinute(04, 00))
				    .forJob(job)
				    .build();
		  // Tell quartz to schedule the job using our trigger
		  sched.scheduleJob(job, trigger);
	  }
}
