/*
 *	Programmer: wesley
 *	Date: 20 Jul 2017
 *	Project: WesleyUtilities
 *	Package: com.wesley.utils.csv
 *	Using JRE: 1.8.0_73
*/
package haj.com.exception;

/**
 * The Class ValueRequiredException.
 */
public class ValidationException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	private Object[] params;

	/**
	 * Instantiates a new value required exception.
	 */
	public ValidationException() {
	}

	/**
	 * Instantiates a new value required exception.
	 *
	 * @param message
	 *            the message
	 */
	public ValidationException(String message) {
		super(message);
	}
	
	public ValidationException(String message, Object...params) {
		super(message);
		this.params = params;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

}
